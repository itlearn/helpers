---
title: Gitlab markdown
example: metadata
language: yaml
---

[[_TOC_]]

# GitLab Flavored Markdown

GitLab uses “GitLab Flavored Markdown” (GFM). It extends the [CommonMark specification](https://spec.commonmark.org/current/) (which is based on standard Markdown) in several ways to add additional useful functionality. It was inspired by [GitHub Flavored Markdown](https://docs.github.com/en/github/writing-on-github/basic-writing-and-formatting-syntax).



## Table of contents

You can add a table of contents to a Markdown file, wiki page, or issue/merge request description, by adding the tag `[[_TOC_]]` on its own line. It appears as an unordered list that links to the various headers. 

## Tasks list

- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3

## Blockquotes

> Blockquotes are very handy to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that is still quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.


### Multiline blockquote

GFM extends the standard Markdown by also supporting multi-line blockquotes fenced by `>>>`

>>>
If you paste a message from somewhere else

that spans multiple lines,

you can quote that without having to manually prepend `>` to every line!
>>>


## Code spans and blocks

You can highlight anything that should be viewed as code and not simple text.

Simple inline code is highlighted with single backticks ```:

Inline `code` has `back-ticks around` it.

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```