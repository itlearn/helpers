<!--
Table of Contents
=================

   * [Users management](#Users-management)
      * [UID](#UID)
      * [group](#group)
         * [group low level commands](#group-low-level-commands)
         * [groups system file](#groups-system-file)
         * [group password](#group-password)
      * [user](#user)
         * [Adding user in group or removing user from a group](#Adding-user-in-group-or-removing-user-from-a-group)
         * [default configuration](#default-configuration)
         * [users system file](#users-system-file)
         * [Low level commands](#Low-level-commands)
         * [no login account](#no-login-account)
         * [accout without password](#accout-without-password)
      * [User and group infos](#User-and-group-infos)
      * [Password creation](#Password-creation)
   * [Priviledged right or running a command with another user](#Priviledged-right-or-running-a-command-with-another-user)
      * [sudoers file](#sudoers-file)
      * [Environment and sudo](#Environment-and-sudo)
      * [sudo command](#sudo-command)
      * [sudo logs](#sudo-logs)
         * [Best practise](#Best-practise)
      * [Substitute User](#Substitute-User)
   * [Owners and Permissions](#Owners-and-permissions)
      * [File modes](#File-modes)
      * [special rights setuid setgid](#special-rights-setuid-setgid)
      * [chattr](#chattr)
      * [ACL](#ACL)
      * [Capabilities](#Capabilities)
   * [Authentication](#Authentication)
      * [PAM](#PAM)
-->
[[_TOC_]]

# Users management

## UID

https://linuxhandbook.com/uid-linux/

UID stands for user identifier. A UID is a number assigned to each Linux user. It is the user’s representation in the Linux kernel.

Do note that in most Linux distributions, UID 1-500 are usually reserved for system users. In Ubuntu and Fedora, UID for new users start from 1000.

In Linux, UID 0 and GID 0 is reserved for the root user.



## group
+ `addgroup gname` : add a group to the system
  + `--gid ID` : specify gid
  + `--system` : a system group will be added (=> gid < 1000)
+ `delgroup gname` - remove a group from the system
  + -`-only-if-empty` : the group won't be removed if it has any members left.
  <br> NB The primary group of an existing user cannot be removed

### group low level commands

+ `groupadd gname` - (low level) create a new group
  + `-p pwd` : encrypted password to specify password for group
  + `-r` : create system group (=> gid < 1000)
  + `-g id` : specify group gid


+ `groupdel gname` : remove a group (low level)
   <br>NB You may not remove the primary group of any existing user. You must remove the user before you remove the group.


+ `groupmod [options] gname` : modify a group definition on the system
  + `-g, --gid GID` :  group ID of the given GROUP will be changed to GID.
  + `-n, --new-name NEW_GROUP` : rename group
  + `-p, --password PASSWORD` : encrypted password

### groups system file

**groups** are stored in `/etc/group`

```
group_name:password:GID:user_list
vampadm:x:997:
```
### group password

**group password**
If a password is set the members can still use newgrp without a password, and non-members must supply the password.
If the group password is empty. Only group members will be allowed to use newgrp to join the named group.


 * `gpasswd group` : administer /etc/group and /etc/gshadow, create password for group
   + `-r, --remove-password` : Remove the password from the named group

**joining group**

 + `sg [group [-c ] command]` : execute command as different group ID
 + `newgrp [-] [group]` : log in to a new group,





```bash
set -v

# create new group with an existing id => fails
sudo groupadd vampadm -g 40

# create a new group choosing gid <= 999
sudo groupadd vampadm -r

grep vampadm /etc/group

# change gid
sudo groupmod -g 5000  vampadm
grep vampadm /etc/group

# assign password to group , or -p $(openssl passwd -1 test)
sudo groupmod -p $(mkpasswd test) vampadm
grep vampadm /etc/group

# sg vampadm -c id  # NB would ask passwd for group if not root
sudo sg vampadm -c id

sudo groupdel vampadm
grep vampadm /etc/group || echo "vampadm not a group"

set +v
```


    # create new group with an existing id => fails
    sudo groupadd vampadm -g 40
    groupadd : l'identifiant de groupe (GID) « 40 » existe déjà

    # create a new group choosing gid <= 999
    sudo groupadd vampadm -r

    grep vampadm /etc/group
    [01;31m[Kvampadm[m[K:x:998:

    # change gid
    sudo groupmod -g 5000  vampadm
    grep vampadm /etc/group
    [01;31m[Kvampadm[m[K:x:5000:

    # assign password to group , or -p $(openssl passwd -1 test)
    sudo groupmod -p $(mkpasswd test) vampadm
    grep vampadm /etc/group
    [01;31m[Kvampadm[m[K:x:5000:

    # sg vampadm -c id  # NB would ask passwd for group if not root
    sudo sg vampadm -c id
    uid=0(root) gid=5000(vampadm) groupes=5000(vampadm),0(root)

    sudo groupdel vampadm
    grep vampadm /etc/group || echo "vampadm not a group"
    vampadm not a group

    set +v


## user


+ `adduser`  : add a user to the system (by default creating a home dir), and prompt for user info
  + --uid ID : specify uid
  + --system : a system user will be added (=> uid < 1000) system users are placed in the nogroup group
  + --group : with --sytem create a group with the same name
  + --gid ID : put the user in that group
  + --ingroup group : put the user in that group
  + --home dir : use dir as user's home dir rather than the default
  + --no-create-home : Do not create the home directory, even if it doesn't exist.
  + --shell SHELL: Use SHELL as the user's login shell


+ `deluser` : remove a user from the system,  remove the user without removing the home directory
  + --remove-home : Remove the home directory of the user


+ `chsh` :  change login shell
   + -s, --shell SHELL : The name of the user's new login shell. Setting this field to blank causes the system to select the default login shell.

### Adding user in group or removing user from a group

NB group has to already exists

```
adduser username groupname
deluser username groupname
```

### default configuration

Edit `/etc/login.defs` and defines the following values to set password expiration or length defaults

    PASS_MAX_DAYS
    PASS_MIN_DAYS
    PASS_MIN_LEN
    PASS_WARN_AGE


### users system file

https://www.2daygeek.com/understanding-linux-etc-passwd-file-format/

**users** are stored in `/etc/passwd`

```
login_name:password:UID:GID:user_comment:home_dir:cmd_interpreter
```


### Low level commands

+ `useradd` : create a new user (by default without home dir, and create group using gid=uid)
  + -u, --uid UID : specify user uid
  + -U, --user-group : Create a group with the same name as the user,
  + -g, --gid GROUP : the group name or number of the user's initial login group. The group name must exist.
  + -m, --create-home : Create the user's home directory if it does not exist.
  + -M, --no-create-home  (by default)
  + -d, --home-dir HOME_DIR : specify home directory without create it if missing
  + -p, --password PASSWORD : The encrypted password, use `openssl passwd -1 TEXT` or `mkpasswd TEXT`
  + -r, --system : Create a system account
  + -s, --shell: Default shell for logon user e.g. `-s '/bin/bash'`, or use `/bin/false` if not for login
  + `-G,--groups GP1[,GP2,...[,GPn]]]` list of supplementary groups which the user is also a member of
  + -D : display default settings
  + -e : set expiration date : The date on which the user account will be disabled.
  + -f, --inactive n : number of days after a password expires until the account is permanently disabled


+ `userdel` : low level utility for removing users. delete a user account and related files
  + -f : forces the removal of the user account, even if the user is still logged in
  + -r :  the user's home directory will be removed


+ `usermod` : modify a user account
  + `-u, --uid UID` : The new numerical value of the user's ID.
  + `-g, --gid GROUP` : Change group
  + `-p, --password PASSWORD` : encrypted password to modify the password
  + `-G,--groups GRP1[,GRP2,...[,GRPN]]]` : list of additional groups
  + `-a, --append` :  Add the user to the supplementary group(s) to use with -G to not override existing groups
  + `-L, --lock` : Lock a user's password.
  + ` -U, --unlock` : unlock user's password

**adding user to group with low level command**

    usermod -Ga accounting [username] # the supplementary groups are added to any existing groups for the specific user.

remove user from group : https://linoxide.com/linux-how-to/remove-linux-user-from-group/

How To Properly Change Username In Linux : https://ostechnix.com/how-to-properly-change-username-in-linux/


**scripting samples**

```
# scritping

NS=user1
useradd -p $(mkpasswd "$NS") -d /home/$NS -m -g users -s /bin/bash "$NS"
```



### no login account

Create user with or without home (-M), with password locked and shell is `/bin/false` or `/usr/sbin/nologin`

```

useradd dwatch -r -M -s /usr/sbin/nologin
# pwd already locked by default
usermod dwatch -L

```

NB if shell is set to /usr/sbin/nologin, try to login with the account display message

```
$ sudo -u mail -i   # same effect with : sudo su - mail
This account is currently not available.

## do nothing if shell is /bin/false
$ sudo -u vboxadd -i


# cannot log in but can run command as nologin user
$ sudo -u vboxadd id
uid=999(vboxadd) gid=1(daemon) groupes=1(daemon)

```
### account without password

The --disabled-password option will not set a password, but login is still possible (for example with SSH  RSA keys)

```
sudo  adduser --no-create-home --disabled-login vault
#<=>
sudo useradd vault -s /bin/bash # by default password is locked and no home created

# lock password cf passwd command


```


```bash
set -v

useradd -D

# adduser vampu --uid 5555
# create user vampu uid 5555 gid 5555

# adduser vampu --uid 5555 --ingroup vamp

# NB by default adduser create system user with no group and no login shell (/bin/false)
# adduser vampsys --system --group --shell /bin/bash

# add system user specifying user id (<1000), by default create a group with the same name
sudo useradd vampsys -m -p $(openssl passwd -1 test) -u 789 -s '/bin/bash'

grep vamp /etc/{group,passwd} ; ls -d /home/v*

sudo su vampsys -c id
# display status on password
sudo passwd vampsys -S


# add system group
sudo groupadd vampadm -r
sudo groupadd vampmon

grep vamp /etc/{group,passwd}

# change group
sudo usermod vampsys -g vampadm
# Add a new group
sudo usermod vampsys -G vampmon

sudo su vampsys -c id
sudo su vampsys -c groups

grep vamp /etc/{group,passwd}

# del user account and remove home dir
sudo userdel vampsys -r
sudo groupdel vampsys
sudo groupdel vampmon
sudo groupdel vampadm

grep vamp /etc/{group,passwd}

set +v
```


    useradd -D
    GROUP=100
    HOME=/home
    INACTIVE=-1
    EXPIRE=
    SHELL=/bin/sh
    SKEL=/etc/skel
    CREATE_MAIL_SPOOL=no

    # adduser vampu --uid 5555
    # create user vampu uid 5555 gid 5555

    # adduser vampu --uid 5555 --ingroup vamp

    # NB by default adduser create system user with no group and no login shell (/bin/false)
    # adduser vampsys --system --group --shell /bin/bash

    # add system user specifying user id (<1000), by default create a group with the same name
    sudo useradd vampsys -m -p $(openssl passwd -1 test) -u 789 -s '/bin/bash'
    openssl passwd -1 test

    grep vamp /etc/{group,passwd} ; ls -d /home/v*
    [35m[K/etc/group[m[K[36m[K:[m[K[01;31m[Kvamp[m[Ksys:x:6792:
    [35m[K/etc/passwd[m[K[36m[K:[m[K[01;31m[Kvamp[m[Ksys:x:789:6792::/home/[01;31m[Kvamp[m[Ksys:/bin/bash
    [0m[01;34m/home/vampsys[0m

    sudo su vampsys -c id
    uid=789(vampsys) gid=6792(vampsys) groupes=6792(vampsys)
    # display status on password
    sudo passwd vampsys -S
    vampsys P 03/22/2018 0 99999 7 -1


    # add system group
    sudo groupadd vampadm -r
    sudo groupadd vampmon

    grep vamp /etc/{group,passwd}
    [35m[K/etc/group[m[K[36m[K:[m[K[01;31m[Kvamp[m[Ksys:x:6792:
    [35m[K/etc/group[m[K[36m[K:[m[K[01;31m[Kvamp[m[Kadm:x:998:
    [35m[K/etc/group[m[K[36m[K:[m[K[01;31m[Kvamp[m[Kmon:x:6793:
    [35m[K/etc/passwd[m[K[36m[K:[m[K[01;31m[Kvamp[m[Ksys:x:789:6792::/home/[01;31m[Kvamp[m[Ksys:/bin/bash

    # change group
    sudo usermod vampsys -g vampadm
    # Add a new group
    sudo usermod vampsys -G vampmon

    sudo su vampsys -c id
    uid=789(vampsys) gid=998(vampadm) groupes=998(vampadm),6793(vampmon)
    sudo su vampsys -c groups
    vampadm vampmon

    grep vamp /etc/{group,passwd}
    [35m[K/etc/group[m[K[36m[K:[m[K[01;31m[Kvamp[m[Ksys:x:6792:
    [35m[K/etc/group[m[K[36m[K:[m[K[01;31m[Kvamp[m[Kadm:x:998:
    [35m[K/etc/group[m[K[36m[K:[m[K[01;31m[Kvamp[m[Kmon:x:6793:[01;31m[Kvamp[m[Ksys
    [35m[K/etc/passwd[m[K[36m[K:[m[K[01;31m[Kvamp[m[Ksys:x:789:998::/home/[01;31m[Kvamp[m[Ksys:/bin/bash

    # del user account and remove home dir
    sudo userdel vampsys -r
    userdel : le groupe vampsys n'a pas été supprimé car ce n'est pas le groupe primaire de l'utilisateur vampsys
    userdel : l'emplacement de boîte aux lettres de vampsys (/var/mail/vampsys) n'a pas été trouvé
    sudo groupdel vampsys
    sudo groupdel vampmon
    sudo groupdel vampadm

    grep vamp /etc/{group,passwd}

    set +v


## User and group infos

+ `id [username]` :  print real and effective user and group IDs
  + -u :user
  + -g: group
  + -n: name for -u or -g option instead of id


+ `groups [username]` : user group file


+ `getent` : get entries from Service Switch libraries
  + group
  + hosts


+ `members groupname` :  outputs members of a group (=> apt install members)



```bash
set -v

id -a # <=> id

id -u -n  <=> id -un
id -u # -r

id -g -n
id -g # -r

groups

getent group | head

getent group adm

set +v
```


    id -a # <=> id
    uid=1000(cnuser) gid=1000(cnuser) groupes=1000(cnuser),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),113(lpadmin),128(sambashare)

    id -u -n
    cnuser
    id -u # -r
    1000

    id -g -n
    cnuser
    id -g # -r
    1000

    groups
    cnuser adm cdrom sudo dip plugdev lpadmin sambashare

    getent group | head
    root:x:0:
    daemon:x:1:
    bin:x:2:
    sys:x:3:
    adm:x:4:syslog,cnuser
    tty:x:5:
    disk:x:6:
    lp:x:7:
    mail:x:8:
    news:x:9:

    getent group adm
    adm:x:4:syslog,cnuser

    set +v


## user authentication log


Find If A User Is Using Password-based Or Key-based SSH Authentication
https://www.ostechnix.com/find-if-a-user-is-using-password-based-or-key-based-ssh-authentication/

```

# centos / redhat
grep 'password' /var/log/secure
# debian / ubuntu
grep 'password' /var/log/auth.log

```



## Password creation

6 Ways to generate a secure password on Debian
https://vitux.com/debian_secure_password/

**output encrypted password**

+ `mkpasswd pwd` : encrypts the given password with the crypt libc function using the given salt. (debian package whois / redhat package expect)
+ `openssl passwd pwd` : Generation of hashed passwords  (debian package openssl)
  + -crypt : standard Unix password algorithm (default)
  + -1 : MD5-based password algorithm

**modifying password**

+ `passwd [OPTION] [LOGIN]` : change user password for current user or LOGIN
  + -e, --expire : Immediately expire an account's password.
  + -d, --delete : Delete a user's password (make it empty).
  + -n, --mindays MIN_DAYS : Set the minimum number of days between password changes
  + -S, --status : Display account status information. -a : show status for all users
  + -l, --lock : Lock password
  + -u, --unlock :

```
# disabling root password
passwd root -d
passwd root -l

```

To lock a user with the passwd command, you can use the option -l or –lock in this manner:
```
passwd -l user_name
```

There is a major problem with locking users this way. Since it only works with the  `/etc/passwd file`, the locked user will still be able to log in via SSH keys (if login via SSH key is set). <br>To remediate this, you can change the shell of the user to `nologin` and this will not allow the user to login to a shell. Another method is to lock the user and provide an expired date in the past. What it does is that it disables the account on a past date and then locks it.
https://linuxhandbook.com/lock-unlock-user/



+ `chpasswd` :  update passwords in batch mode
   ```
   sudo bash -c 'echo udemo:toto | chpasswd'
   ```


+ `chage` : change user password expiry information
  + -E, --expiredate EXPIRE_DATE
  + -l, --list : Show account aging information.
  + -m, --mindays MIN_DAYS : Set the minimum number of days between password changes to MIN_DAYS.
  + -M, --maxdays MAX_DAYS :Set the maximum number of days during which a password is valid.

#### passwd system file

Understanding Linux /etc/shadow File Format
https://www.2daygeek.com/understanding-linux-etc-shadow-file-format/

The `/etc/shadow` file stores actual password in encrypted format and other passwords related information such as user name, last password change date, password expiration values.

The `/etc/login.defs` file provides default configuration information for user account password parameters. It defines, Password ageing related information such as password Min/Max days, password warning age.

The `/etc/shadow` file contain every user details as a single line with nine fields, each fields separated by colon :.

```
Username:EncryptedPassword:DateOfLastPasswordChange:MinimumPasswordAge:MaximumPasswordAge:PasswordWarningPeriod:PasswordInactivityPeriod:AccountExpirationDate:ReservedField
```

The password field comes with following three variants.
 * Usable Encrypted Password (Encrypted Password hash, which contains three parts like hash_algorithm, hash_salt, and hash_data). This allow us to login to the Linux system since it’s comes with proper password.
 * Locked Password: A password field which starts with a exclamation mark means that the password is locked.
 * (! and *) Represent Empty Password: This field may be empty, in which case no passwords are required to authenticate as the specified login name. These user will not be able to use a unix password to log in.





```bash
set -v

# generate encrypt password
mkpasswd test

openssl passwd -1 test

# change password
# sudo bash -c 'echo udemo:toto | chpasswd'
# sudo usermod udemo -p $(mkpasswd toto)

# login name / L locked, NP no pwd, P usable pwd, date of the last password change, minimum age, maximum age, warning period, inactivity period
sudo passwd -S -a | column -t | head

set +v

```


    mkpasswd test
    dyZ.XNOcP9ENk

    openssl passwd -1 test
    $1$mtuyvYv4$txFzcKJU0MuFXlUDEgFra1

    # sudo bash -c 'echo udemo:toto | chpasswd'

    # login name / L locked, NP no pwd, P usable pwd, date of the last password change, minimum age, maximum age, warning period, inactivity perio
    sudo passwd -S -a | column -t | head
    root               L   09/11/2017  0   99999  7   -1
    daemon             L   08/01/2017  0   99999  7   -1
    bin                L   08/01/2017  0   99999  7   -1
    sys                L   08/01/2017  0   99999  7   -1
    sync               L   08/01/2017  0   99999  7   -1
    games              L   08/01/2017  0   99999  7   -1
    man                L   08/01/2017  0   99999  7   -1
    lp                 L   08/01/2017  0   99999  7   -1
    mail               L   08/01/2017  0   99999  7   -1
    news               L   08/01/2017  0   99999  7   -1

    set +v


# Priviledged right or running a command with another user


Exploring the differences between sudo and su commands in Linux
https://www.redhat.com/sysadmin/difference-between-sudo-su

## Abstract

https://www.sudo.ws/

Sudo allows a system administrator to delegate authority by giving certain users the ability to run some commands as root or another user while providing an audit trail of the commands and their arguments.

Sudo in a Nutshell: https://www.sudo.ws/intro.html

Real sysadmins don't sudo<br>
https://www.redhat.com/sysadmin/sysadmins-dont-sudo

5 new sudo features you need to know in 2020 (central collection of session recordings, support for chroot within sudo, and a Python API)<br>
https://opensource.com/article/20/10/sudo-19

## sudoers file

10 Useful Sudoers Configurations for Setting ‘sudo’ in Linux (01/2017) <br>
https://www.tecmint.com/sudoers-configurations-for-setting-sudo-in-linux/

What you probably didn’t know about sudo<br>
https://opensource.com/article/19/10/know-about-sudo

Take Control of your Linux | sudoers file: How to with Examples<br>
https://www.garron.me/en/linux/visudo-command-sudoers-file-sudo-default-editor.html

Change Sudo Password Timeout In Linux (setting `timestamp_timeout`) :<br>
https://www.ostechnix.com/how-to-change-sudo-password-timeout-in-linux/<br>
https://linuxiac.com/make-sudo-remember-password-longer/

Generic Config » /etc » etc/sudoers<br>
https://fishilico.github.io/generic-config/etc/sudoers.raw.html

**Warning** use `visudo` to edit securely the file /etc/sudoers

**NB** This file is read from top to bottom, and the last setting wins.<br>
What this fact means for you is that you should start with generic settings and place exceptions at the end, otherwise exceptions are overridden by the generic settings.


To achieve the “least privilege” principle, individual commands should be authorized independently, per host, per user. This can become unwieldy at scale quickly with sudo.


The most basic syntax of a line in the sudoers file is this:
`USER PLACES=(AS_USER) [NOPASSWD:] COMMAND`
 * USER can be any existing user(s), user ID or User_Alias. It also can be a group, specifying the group by preceding it with the special character %. Groups also can be included in user aliases.
 * PLACES can be any combinations of hostname, domain_name, IP addresses or wild cards.
 * (AS_USER) can be any existing user(s), user ID or Runas_Alias.
 * COMMAND can be any existing command(s) or COMMAND_ALIASES.
 * [NOPASSWD:] is used to specify that the following commands can be run without being prompted for a password. Use it with caution and advisedly. Understand the risks and have compensating controls for those risks.


types of aliases available :
 * User_Alias
 * Cmnd_Alias
 * Host_Alias
 * Runas_Alias


`/etc/sudoers`


```
Defaults        env_reset
Defaults        mail_badpass
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
Defaults        env_keep += HOME

# Enable features for a certain group of users
# some funny messages for when someone mistypes a password, here only for wheel member
Defaults !insults
Defaults:%wheel insults

# enables session recording
# Defaults log_output

# force reenter pwd for each command, and ensure validity in the same tty
# Defaults:ALL    timestamp_timeout=0, tty_tickets

# sudo remembers your password for 15 minutes by default.
# Change Sudo Password Timeout In Linux
# Defaults timestamp_timeout=30

# User alias specification

User_Alias      BOSS = lol, piratebab
User_Alias     OPERATORS = joe, mike, jude

# Runas alias specification

Runas_Alias    OP = root, operator

# Cmnd alias specification

Cmnd_Alias      STOP = /sbin/shutdown, /sbin/reboot, /sbin/halt
Cmnd_Alias      APT = /usr/bin/dpkg, /usr/bin/apt-get, /usr/bin/aptitude
Cmnd_Alias	SHUTDOWN = /usr/sbin/shutdown

# Host alias specification
Host_Alias     OFNET = 10.1.2.0/255.255.255.0

# NB User privilege specification format:
# user host=(Runas_Spec) Comand  <=> who host=(accounts) commands
# NB user specified as : %gname designates group gname
# NB If no Runas_Spec is specified the command may be run as root and no group may be specified. ALL= <=> ALL=(root)

# User privilege specification
root    ALL=(ALL:ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL


# Tag_Spec :  A command may have zero or more tags associated with it
# NOPASSWD and PASSWD,  NOEXEC, EXEC

# by default if accounts is not specified root is used <=< ALL=(root)
delivery ALL= NOPASSWD:ALL

#The users in the OPERATORS group can run any command from any terminal.
OPERATORS ALL= ALL

# The user linus can run any command from any terminal as any user in the OP group (root or operator).
linus ALL=(OP) ALL



# See sudoers(5) for more information on "#include" directives:
#includedir /etc/sudoers.d


```

**Warning**
Wldcard in command line arguments should be used with care.
Wildcards are extremely dangerous. Don’t use them if you are not 100% sure that a malicious user is able to abuse it.

https://blog.compass-security.com/2012/10/dangerous-sudoers-entries-part-4-wildcards/

https://blog.compass-security.com/2012/10/dangerous-sudoer-entries-part-1-command-execution/

https://blog.compass-security.com/2012/10/dangerous-sudoers-entries-part-5-recapitulation/

When defining command if you leave command arguments off, it tells sudo that the user has blanket permissions to run this command with any arguments. For this reason, it is good practice to limit the command line arguments to only the functionality you wish to allow.


**Adding user to sudoers group**

```
# on debian/ubuntu add the user to the 'sudo' group
 adduser foo sudo
 usermod -aG sudo username

# on centos add the user to the 'wheel' group
 usermod -aG wheel username
```

## Environment and sudo

Any variables added to these locations will not be reflected when invoking them with a `sudo` command, as sudo has a default policy of resetting the Environment and setting a secure path (this behavior is defined in /etc/sudoers).

You can setup `sudo` not to reset certain environment variables by adding some explicit environment settings to keep in `/etc/sudoers`:<br>
`Defaults env_keep += "http_proxy SOMEOTHERVARIABLES ANOTHERVARIABLE ETC"`

## requiretty

Disable requiretty in /etc/sudoers<br>
https://linuxreference.wordpress.com/2010/11/22/disable-requiretty-in-etcsudoers/

## sudo logs


On redhat based linux systems like centos or fedora it is in:

    /var/log/secure

and for debian based systems like ubuntu it is in:

    /var/log/auth.log


How To Change Default Sudo Log File In Linux ( e.g. using `/var/log/sudo.log` instead )<br>
https://www.ostechnix.com/how-to-change-default-sudo-log-file-in-linux/

    Use another facility (Defaults syslog=local1) and configure rsyslog to log local1 in sudo.log

## sudo command


+ `sudo command` : execute a command as another user. sudo allows a permitted user to execute a command as the superuser or another user, as specified by the security policy (/etc/sudoers)
  +  `-E, --preserve-env` : Indicates to the security policy that the user wishes to preserve their existing environment variables (for instance preserve HTTP_PROXY variable for proxy settings).
  +  `-H` : set the HOME environment variable to the home directory specified by the target user
  + `-u user` : Run the command as a user other than the default target user (usually root)
  + `-g group, --group=group` : Run the command with the primary group set to group instead of the primary group.
  + `-i,  --login` : Run the shell specified by the target user's password database entry as a login shell (expect a home dir for the target user)
  + `-s` :  Run the shell specified by the SHELL environment variable
  +  `-l, --list` :  If no command is specified, list the allowed (and forbidden) commands for the invoking user.<br>
     (NB use `-U` user to  list the privileges for user).
  + `-v, --validate` : Update the user's cached credentials, authenticating the user if necessary.
  + `-k, --reset-timestamp` : invalidates the user's cached credentials


https://doc.ubuntu-fr.org/sudo-annexe#differences_entre_sudo_-i_et_sudo_-_et_pourquoi_il_est_preferable_de_ne_jamais_lancer_sudo_su




### Best practise

Use `-i` option to switch to root user

```
# log as root , NB -i => use  .bash_profile, .profile, .bashrc shell init
sudo -i

# log as specified user
sudo -u postgres -i
```

### Tips

```

# log as root using running shell specified by user env variable => use .bashrc
sudo -s

# tip : apply sudo to the last command
sudo !!

```


How To Find All Sudo Users In Linux (09/2021)<br>
https://ostechnix.com/find-sudo-users-linux-system/


### upgrade

How to update sudo version on Linux
https://www.xmodulo.com/update-sudo-version-linux.html

### sudo and redirection

NB any redirection is done before elevated commands are run

```
sudo date > /etc/shadow
bash: /etc/shadow: Permission denied


sudo bash -c 'date > /etc/shadow'
```

### Builtin command and sudo

sudo can run an external command but not a builtin directly.

To use a bash builtin commmand or redirect output with sudo it is possible to run an inline bash command (bash -c cmd).
Whenever code is put in a string the code should be single-quoted.

sudo /bin/bash -c 'echo "Acquire::http::proxy \"http://p-goodway:3128\";" > /etc/apt/apt.conf'





```bash
set -v

# create demo user
sudo useradd demo -m
sudo useradd demort -m

# validate if user is sudoer
sudo -u demo sudo -v

# adding demo as a sudoer
echo 'demo ALL=(ALL) NOPASSWD:ALL' | sudo tee -a /etc/sudoers.d/demo
echo 'demort ALL= NOPASSWD:ALL' | sudo tee -a /etc/sudoers.d/demort   # <=> ALL=(root)

# <=>
# sudo bash -c 'echo demo ALL=(ALL) NOPASSWD:ALL > /etc/sudoers.d/demo'

# run command as demo user, demo user is a sudoer
sudo -u demo sudo -l
sudo -u demo sudo -u demort whoami
sudo -u demo sudo whoami

sudo -u demort sudo -l
# can only sudo as root
# sudo -u demort sudo -u demo whoami
sudo -u demort sudo whoami

sudo userdel -r demo
sudo userdel -r demort
sudo rm /etc/sudoers.d/demo
sudo rm /etc/sudoers.d/demort

set +v
```


    # create demo user
    sudo useradd demo -m
    sudo useradd demort -m

    # validate if user is sudoer
    sudo -u demo sudo -v
    Désolé, l'utilisateur demo ne peut pas utiliser sudo sur workstation.

    # adding demo as a sudoer
    echo 'demo ALL=(ALL) NOPASSWD:ALL' | sudo tee -a /etc/sudoers.d/demo
    demo ALL=(ALL) NOPASSWD:ALL
    echo 'demort ALL= NOPASSWD:ALL' | sudo tee -a /etc/sudoers.d/demort   # <=> ALL=(root)
    demort ALL= NOPASSWD:ALL

    # <=>
    # sudo bash -c 'echo demo ALL=(ALL) NOPASSWD:ALL > /etc/sudoers.d/demo'

    # run command as demo user, demo user is a sudoer
    sudo -u demo sudo -l
    Entrées par défaut pour demo sur workstation :
        env_reset, mail_badpass,
        secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

    L'utilisateur demo peut utiliser les commandes suivantes sur workstation :
        (ALL) NOPASSWD: ALL
    sudo -u demo sudo -u demort whoami
    demort
    sudo -u demo sudo whoami
    root

    sudo -u demort sudo -l
    Entrées par défaut pour demort sur workstation :
        env_reset, mail_badpass,
        secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

    L'utilisateur demort peut utiliser les commandes suivantes sur workstation :
        (root) NOPASSWD: ALL
    # can only sudo as root
    # sudo -u demort sudo -u demo whoami
    sudo -u demort sudo whoami
    root

    sudo userdel -r demo
    userdel : l'emplacement de boîte aux lettres de demo (/var/mail/demo) n'a pas été trouvé
    sudo userdel -r demort
    userdel : l'emplacement de boîte aux lettres de demort (/var/mail/demort) n'a pas été trouvé
    sudo rm /etc/sudoers.d/demo
    sudo rm /etc/sudoers.d/demort

    set +v



```bash
set -v

sudo -l

sudo -l -U root

# run command as root
sudo bash -c 'echo $HOME'

# use root home
sudo -H bash -c 'echo $HOME'

# sample to use redirection to a file as root
# sudo bash -c 'echo 0 > /proc/sys/net/ipv6/conf/all/forwarding'


# For instance using pip installer script using user env
# sudo -H -E python /home/stack/devstack/files/get-pip.py

# upgrading pip install as root
# sudo -H pip install --upgrade pip
set +v
```


    sudo -l
    Entrées par défaut pour cnuser sur cuser-VirtualBox.adm.fr.clara.net :
        env_reset, mail_badpass,
        secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

    L'utilisateur cnuser peut utiliser les commandes suivantes sur
            cuser-VirtualBox.adm.fr.clara.net :
        (ALL : ALL) ALL
        (ALL) NOPASSWD: ALL

    sudo -l -U root
    Entrées par défaut pour root sur cuser-VirtualBox.adm.fr.clara.net :
        env_reset, mail_badpass,
        secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

    L'utilisateur root peut utiliser les commandes suivantes sur
            cuser-VirtualBox.adm.fr.clara.net :
        (ALL : ALL) ALL

    # run command as root
    sudo bash -c 'echo $HOME'
    /home/cnuser

    # use root home
    sudo -H bash -c 'echo $HOME'
    /root

    # sudo bash -c 'echo 0 > /proc/sys/net/ipv6/conf/all/forwarding'


    # For instance using pip installer script using user env
    # sudo -H -E python /home/stack/devstack/files/get-pip.py

    # upgrading pip install as root
    # sudo -H pip install --upgrade pip
    set +v


## Substitute User


+ `su [username]` : change user ID or become superuser (if no username is specified)
  + `-c, --command COMMAND` : Specify a command that will be invoked by the shell using its -c.
  + `-, -l` :  Provide an environment similar to what the user would expect had the user logged in directly (=> change directory to $HOME)
 +  `-p, --preserve-environment` :Preserve the current environment


```bash
# switch to root preserving the current environment (for instance continue to use the same type of shell)

sudo su -p

# <=>  -s option for sudo run the shell specified by the SHELL environment variable

sudo -s
```

```bash
# login as postgres user wihtout knowing postgres passwd

sudo -u postgres -i

sudo su - postgres

# with user password

su - stack

```



```bash
set -v

sudo useradd -u 6789 -p $(openssl passwd -1 test) -s /bin/bash -m udemo

sudo su udemo -c id
sudo su udemo -c 'echo $HOME $SHELL $PWD'

sudo -u udemo id
sudo -H -u udemo bash -c 'echo $HOME $SHELL $PWD'

sudo -u udemo -i bash -c 'echo $HOME $SHELL $PWD'

sudo userdel -r udemo

set +v
```


    sudo useradd -u 6789 -p $(openssl passwd -1 test) -s /bin/bash -m udemo
    openssl passwd -1 test

    sudo su udemo -c id
    uid=6789(udemo) gid=6789(udemo) groupes=6789(udemo)
    sudo su udemo -c 'echo $HOME $SHELL $PWD'
    /home/udemo /bin/bash /home/cnuser/git/samples/sys/linux

    sudo -u udemo id
    uid=6789(udemo) gid=6789(udemo) groupes=6789(udemo)
    sudo -H -u udemo bash -c 'echo $HOME $SHELL $PWD'
    /home/udemo /bin/bash /home/cnuser/git/samples/sys/linux

    sudo -u udemo -i bash -c 'echo $HOME $SHELL $PWD'
    /home/udemo /bin/bash /home/udemo

    sudo userdel -r udemo
    userdel : l'emplacement de boîte aux lettres de udemo (/var/mail/udemo) n'a pas été trouvé

    set +v


# Owners and permissions


+ `chown [OWNER][:[GROUP]] FILE` : change file owner and group
    + -R, --recursive : operate on files and directories recursively
    + -L  : traverse every symbolic link to a directory encountered


+ `chgrp GROUP FILE` : change group ownership
  + -R, --recursive : operate on files and directories recursively
  + -L  : traverse every symbolic link to a directory encountered


+ `umask [-p] [-S] [mode]` : Display or set file mode mask. Sets the user file-creation mask to MODE.  If MODE is omitted, prints the current value of the mask.
   + `-p` : if MODE is omitted, output in a form that may be reused as input
   + `-S` : makes the output symbolic; otherwise an octal number is output
NB by default files are created with the access mode 666 and directories with 777, umask mode apply to this default mode.


+ `ulimit` : get and set user limits
   + `-a` :	all current limits are reported


+ `chmod MODE FILE` : change file mode bits. If MODE begins with a digit, it is interpreted as an octal number; otherwise it is a symbolic mode string
   + -R, --recursive : change files and directories recursively


## File modes

Chmod Command in Linux (File Permissions)
https://linuxize.com/post/chmod-command-in-linux/

How to manage Linux permissions for users, groups, and others
https://sysadmin.prod.acquia-sites.com/sysadmin/manage-permissions

*MODE*
 + symbolic representation : `category of users (u/g/o & a for all) and permission (+/- r/w/x)`
 + absolute representation (octal numeric combinating) `4:r 2:w 1:x 1 byte for UserGroupOther`

```bash

# using numeric representation
chmod 600 ~/.ssh/id_rsa

# using symbolic representation
chmod a+x-w /usr/local/bin/git*

```

A directory is handled differently. Read access gives the right to consult the list of its entries (files and directories), write access allows creating or deleting files, and execute access allows crossing through it (especially to go there with the cd command). Being able to cross through a directory without being able to read it gives permission to access the entries therein that are known by name, but not to find them if you do not know their existence or their exact name.

The distinction between directories and files sometimes causes problems with recursive operations. That is why the “`X`” leter has been introduced in the symbolic representation of rights. It represents a right to execute which applies only to directories (and not to files lacking this right). Thus, `chmod -R a+X directory` will only add execute rights for all categories of users ( a ) for all of the sub-directories and files for which at least one category of user (even if their sole owner) already has execute rights.

The Linux permission mask is a mask that sets the permissions for newly created files.

NB  `chmod`  never  changes the permissions of symbolic links; the chmod system call cannot change their permissions.  This is not a problem since the permissions of symbolic links are never used.  However, for each symbolic link  listed on the command line, chmod changes the permissions of the pointed-to file. In contrast, chmod ignores symbolic links encountered during recursive directory traversals.

```

# recursive change of permission
find . -type f | xargs -I% chmod 644 '%' && find . -type d | xargs -I% chmod 755 '%'

```

## Operating on Symbolic Links

Symbolic links always have 777 permissions.

By default, when changing symlink’s permissions, chmod will change the permissions on the file the link is pointing to.



```bash
set -v
# sample add a new user

# display mask in octal
umask -p

# display file mode mask with symbolic mode string
umask -S

sudo useradd -u 6789 -p $(openssl passwd -1 test) udemo
grep udemo /etc/{passwd,group}
sudo -u udemo id

#sudo su udemo -c 'touch test'

touch test
sudo chown udemo: test

ls -l test

sudo chgrp root test

ls -l test

sudo chmod o+w test
ls -l test

sudo chmod 0644 test
ls -l test

sudo userdel udemo
sudo rm test

ulimit -a

set +v
```

    # sample add a new user

    # display mask in octal
    umask -p
    umask 0002

    # display file mode mask with symbolic mode string
    umask -S
    u=rwx,g=rwx,o=rx

    sudo useradd -N -u 6789 -g ssh -p $(openssl passwd -1 test) udemo
    openssl passwd -1 test
    grep udemo /etc/{passwd,group}
    [35m[K/etc/passwd[m[K[36m[K:[m[K[01;31m[Kudemo[m[K:x:6789:116::/home/[01;31m[Kudemo[m[K:
    sudo -u udemo id
    uid=6789(udemo) gid=116(ssh) groupes=116(ssh)

    #sudo su udemo -c 'touch test'

    touch test
    sudo chown udemo: test

    ls -l test
    -rw-rw-r-- 1 udemo ssh 0 févr.  3 20:30 test

    sudo chgrp root test

    ls -l test
    -rw-rw-r-- 1 udemo root 0 févr.  3 20:30 test

    sudo chmod o+w test
    ls -l test
    -rw-rw-rw- 1 udemo root 0 févr.  3 20:30 test

    sudo chmod 0644 test
    ls -l test
    -rw-r--r-- 1 udemo root 0 févr.  3 20:30 test

    sudo userdel udemo
    sudo rm test

    ulimit -a
    core file size          (blocks, -c) 0
    data seg size           (kbytes, -d) unlimited
    scheduling priority             (-e) 0
    file size               (blocks, -f) unlimited
    pending signals                 (-i) 7763
    max locked memory       (kbytes, -l) 64
    max memory size         (kbytes, -m) unlimited
    open files                      (-n) 1024
    pipe size            (512 bytes, -p) 8
    POSIX message queues     (bytes, -q) 819200
    real-time priority              (-r) 0
    stack size              (kbytes, -s) 8192
    cpu time               (seconds, -t) unlimited
    max user processes              (-u) 7763
    virtual memory          (kbytes, -v) unlimited
    file locks                      (-x) unlimited

    set +v


## special rights setuid setgid


Linux permissions: SUID, SGID, and sticky bit : https://www.redhat.com/sysadmin/suid-sgid-sticky-bit

To represent special rights, you can prefix a fourth digit to this number according to the same principle, where the setuid, setgid and sticky bits are `4`, `2` and `1`, respectively.

    chmod 4754 test #  will associate the setuid bit with the previously described rights.


In addition to read, write and execute bits for users, groups and others, there are special file permission bits that
can be set by using the `chmod` command. The bits that you need to use for creating collaborative directories are
the set group ID bit and the sticky bit. There are specific numeric values associated with these bits:
 * Set user ID bit  : 4
 * Set group ID bit : 2
 * Sticky bit       : 1

### setuid and setgid executables

You can use the set group ID bit for creating the group’s collaborative directories. The set UID (user ID) and set GID (group ID) bits are typically used on special executable files that allow commands to be run differently.


Two particular rights are relevant to executable files: ***setuid*** and ***setgid*** (symbolized with the leter “s”).

Note that we frequently speak of “bit”, since each of these boolean values can be represented by a 0 or a 1. These two rights allow any user to execute the program with the rights of the owner or the group, respectively.

This mechanism grants access to features requiring higher level permissions than those you would usually have.

**NB** setuid will not work with bash script (only binary executables) cf http://lea-linux.org/documentations/Dev-suid_scripts

Since a setuid root program is systematically run under the super-user identity, it is very important to ensure it is secure and reliable. Indeed, a user who would manage to subvert it to call a command of their choice could then impersonate the root user and have all rights on the system.

You can still set the suid for a file even if the file is not executable. As a consequence, the suid will be displayed with a capital S instead of a lowercase s.

The **setgid** bit also applies to directories. Any newly-created item in such directories is automatically assigned the owner group of the parent directory, instead of inheriting the creator's main group as usual. This setup avoids the user having to change its main group (with the newgrp command) when working in a file tree shared between several users of the same dedicated group.


The **"sticky"** bit (symbolized by the letter "t") is a permission that is only useful in directories. It is especially used for temporary directories where everybody has write access (such as /tmp/): it restricts deletion of files so that only their owner (or the owner of the parent directory) can do it. Lacking this, everyone could delete other users' files in /tmp/.




```bash
set -v

# showing sample of setuid
ls -l $(which passwd)

# using printid built using git/samples/programming/C/setuid
sudo chown root: printid
sudo ./printid

sudo chmod 6755 printid
ls -l printid
./printid

mkdir sample
sudo chown root:users sample
sudo chmod 2770 sample

sudo touch sample/file
echo -e '#!/bin/bash\n\nwhoami' | sudo tee sample/exe > /dev/null
ls -ld sample
sudo ls -l sample


sudo rm -rf sample

set +v
```


    # showing sample of setuid
    ls -l $(which passwd)
    -rwsr-xr-x 1 root root 59640 janv. 25  2018 [0m[37;41m/usr/bin/passwd[0m

    # using printid built using git/samples/programming/C/setuid
    sudo chown root: printid
    sudo ./printid
    Real UID	= 0
    Effective UID	= 0
    Real GID	= 0
    Effective GID	= 0

    sudo chmod 6755 printid
    ls -l printid
    -rwsr-sr-x 1 root root 8816 août   4  2018 [0m[37;41mprintid[0m
    ./printid
    Real UID	= 1000
    Effective UID	= 0
    Real GID	= 1000
    Effective GID	= 0

    mkdir sample
    sudo chown root:users sample
    sudo chmod 2770 sample

    sudo touch sample/file
    echo -e '#!/bin/bash\n\nwhoami' | sudo tee sample/exe > /dev/null
    ls -ld sample
    drwxrws--- 2 root users 4096 août  13 16:06 [0m[01;34msample[0m
    sudo ls -l sample
    total 4
    -rw-r--r-- 1 root users 20 août  13 16:06 exe
    -rw-r--r-- 1 root users  0 août  13 16:06 file


    sudo rm -rf sample

    set +v


## chattr

Chattr Command in Linux (File Attributes)<br>
https://linuxize.com/post/chattr-command-in-linux/

Prevent Files And Folders From Accidental Deletion Or Modification In Linux<br>
https://ostechnix.com/prevent-files-folders-accidental-deletion-modification-linux/


Chattr, short for Change Attribute, applies/removes certain attributes to a file or folder in your Linux system. So nobody can delete or modify the files and folders either accidentally or intentionally, even as root user.

Chattr can be used as additional tool to protect the important system files and data in your Linux system.

`chattr` - change file attributes on a Linux file system
    `chattr [ -RVf ] [ -v version ] [ -p project ] [ mode ] files...`

The format of a symbolic mode is `+-=[aAcCdDeijPsStTu]`

 * `+` : causes the selected attributes to be added to the existing attributes of the files.
 * `-` : causes the selected attributes to be removed.
 * `=` : causes the selected attributes to be the only attributes that the files have.
 * `a` : append only flag
 * `d` : no dump
 * `e` : extent format flag,
 * `i` : immutable flag
 * `j` : data journalling
 * `c` : compressed
 * `s` : secure deletion,
 * `u` : undeletable
 * `t` : no tail-merging,

`lsattr` - list file attributes on a Linux second extended file system
 + `-R` : Recursively list attributes of directories and their contents.
 + `-a` : List all files in directories, including files that start with `.`.
 + `-d` : List directories like other files, rather than listing their contents.
 + `-l` : Print the options using long names instead of single character abbreviations.



```bash
set -v

echo test > file.txt
lsattr file.txt
sudo chattr +i file.txt
sudo rm -f file.txt
sudo chattr -i file.txt
rm -f file.txt
ls file.txt

set +v
```


    echo test > file.txt
    lsattr file.txt
    --------------e----- file.txt
    sudo chattr +i file.txt
    sudo rm -f file.txt
    rm: impossible de supprimer 'file.txt': Opération non permise
    sudo chattr -i file.txt
    rm -f file.txt
    ls file.txt
    ls: impossible d'accéder à 'file.txt': Aucun fichier ou dossier de ce type

    set +v



```bash
echo ok
```

    ok


## ACL

Linux setfacl command<br>
https://www.computerhope.com/unix/usetfacl.htm

An introduction to Linux Access Control Lists (ACLs)<br>
https://www.redhat.com/sysadmin/linux-access-control-lists

Access control lists and external drives on Linux: What you need to know<br>
https://opensource.com/article/20/3/external-drives-linux

NB ACLs need to be enabled on a file system when that file system is mounted
Basic Linux file systems does not include ACL support by default. In order to add ACL support, we need to add the acl mount option when we mount it.



 + `getfacl` : get file access control lists. If a directory has a default ACL, getfacl also displays the default ACL.
   + -d, --default 	Display the default access control list.


 + `setfacl [{-m|-x} acl_spec] [{-M|-X} acl_file] file` : set file access control lists
   + -R, --recursive :	Apply operations to all files and directories recursively.
   + -d, --default : 	All operations apply to the Default ACL
   + -b, --remove-all : Remove all extended ACL entries
   + -x (--remove) and -X (--remove-file) : options remove ACL entries.
   + -m : modify the ACL of a file or directory

Setting up default ACLs on a directory enables your ACLs to be inherited.

ACL entries:
```
   [d[efault]:] [u[ser]:]uid [:perms]
          Permissions of a named user. Permissions of the file owner if uid is empty.

   [d[efault]:] g[roup]:gid [:perms]
          Permissions of a named group. Permissions of the owning group if gid is empty.

   [d[efault]:] m[ask][:] [:perms]
          Effective rights mask

   [d[efault]:] o[ther][:] [:perms]
          Permissions of others.
```

```
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
```

**NB** There is a close link between the group mask and the acl rights. Removing right for the group using setfacl can modify effective rights on acl rights given to users



```
ls -l
-rw-rw----+ 1 700 root 0 févr.  6 11:40 test

chomd u+x test # ok does not set acl mask

ls -l
-rwxrw----+ 1 700 root 0 févr.  6 11:40 test

chmod 700 test # set acl mask

ls -l
-rwx------+ 1 700 root 0 févr.  6 11:40 test

# reset mask => disable rights for all users acl
setfacl -m m:rw- test

ls -l
-rwx------+ 1 700 root 0 févr.  6 11:40 test

```




```bash
set -v
# sample add a new user


sudo useradd -N -u 6789 -g ssh -p $(openssl passwd -1 test) udemo


echo content > test.acl
sudo chmod 600 test.acl

getfacl test.acl
ls -l *.acl

sudo -u udemo cat test.acl

setfacl -m u:udemo:rw test.acl

ls -l *.acl  # nb rights appear on group and + is append at the end of right mask

sudo -u udemo cat test.acl

getfacl test.acl

rm -f test.acl
sudo userdel udemo


```

    # sample add a new user


    sudo useradd -N -u 6789 -g ssh -p $(openssl passwd -1 test) udemo
    openssl passwd -1 test
    useradd : l'utilisateur « udemo » existe déjà


    echo content > test.acl
    sudo chmod 600 test.acl

    getfacl test.acl
    # file: test.acl
    # owner: cnuser
    # group: cnuser
    user::rw-
    group::---
    other::---

    ls -l *.acl
    -rw------- 1 cnuser cnuser 8 mars  23 07:56 test.acl

    sudo -u udemo cat test.acl
    cat: test.acl: Permission non accordée

    setfacl -m u:udemo:rw test.acl

    ls -l *.acl  # nb rights appear on group and + is append at the end of right mask
    -rw-rw----+ 1 cnuser cnuser 8 mars  23 07:56 test.acl

    sudo -u udemo cat test.acl
    content

    getfacl test.acl
    # file: test.acl
    # owner: cnuser
    # group: cnuser
    user::rw-
    user:udemo:rw-
    group::---
    mask::rw-
    other::---


    rm -f test.acl
    sudo userdel udemo




## Capabilities

Linux capabilities provide a subset of the available root privileges to a process. This effectively breaks up root privileges into smaller and distinctive units. Each of these units can then be independently be granted to processes. This way the full set of privileges is reduced and decreasing the risks of exploitation.

Assigning the setuid bit to binaries is a common way to give programs root permissions. Linux capabilities is a great alternative to reduce the usage of setuid. Most of the binaries that have a setuid flag, can be changed to use capabilities instead.

cf `man 7 capabilities`

For  the  purpose  of  performing permission checks, traditional UNIX implementations distinguish two categories of processes: privileged processes (whose effective user ID is 0, referred to as superuser or root), and unprivileged processes (whose effective UID is nonzero).  Privileged processes bypass all kernel permission checks, while unprivileged processes are subject to full permission checking based on the process's credentials (usually: effective UID, effective GID, and supplementary group list).

Starting  with kernel 2.2, Linux divides the privileges traditionally associated with superuser into distinct units, known as capabilities, which can be independently enabled and disabled. Capabilities are a per-thread attribute.

### Thread capability sets

Each thread has three capability sets containing zero or more of the existing capabilities

* Permitted: This is a limiting superset for the effective capabilities that the thread may assume.  It is also a limiting superset for the capabilities that may be added to the inheritable set by a thread that does not have the CAP_SETPCAP capability in its effective set. If a thread drops a capability from its permitted set, it can never reacquire that capability.

* Inheritable: This is a set of capabilities preserved across an execve(2). Inheritable capabilities remain inheritable when executing any  program, and inheritable capabilities are added to the permitted set when executing a program that has the corresponding bits set in the file inheritable set.

* Effective: This is the set of capabilities used by the kernel to perform permission checks for the thread.

Capability bounding set

The capability bounding set is a security mechanism that can be used to limit the capabilities that can be gained during an execve.

The capability bounding set defines the upper level of available capabilities. During the time a process runs, no capabilities can be added to this list. Only the capabilities in the bounding set can be added to the inheritable set, which uses the capset() system call. If a capability is dropped from the boundary set, that process or its children can no longer have access to it.

### File capabilities

Since kernel 2.6.24, the kernel supports associating capability sets with an executable file using setcap. The file capability sets are stored in an extended attribute named security.capability. The file capability sets, in conjunction with the capability sets of the thread, determine the capabilities of a thread after an execve(2).

The three file capability sets are:

 * Permitted: These capabilities are automatically permitted to the thread, regardless of the thread's inheritable capabilities.
 * Inheritable: This set is ANDed with the thread's inheritable set to determine which inheritable capabilities are enabled in the permitted set of the thread after the execve.
 * Effective: This is not a set, but rather just a single bit. If this bit is set, then during an execve all of the new permitted capabilities for the thread are also raised in  the effective set. If this bit is not set, then after an execve, none of the new permitted capabilities is in the new effective set.


 + `capsh` : capability shell wrapper
 + `getcap filename` : examine file capabilities
 + `setcap  (capabilities|-|-r) filename` : set file capabilities
 + `getcaps <pid> [<pid> ...]` : displays the capabilities on the queried process(es).

 ```bash
 # set capabilites sample
 sudo setcap cap_ipc_lock=+pe /usr/bin/vault

 # wireshark
 sudo setcap cap_net_raw,cap_net_admin=eip /usr/bin/dumpcap

 ```

libcap-ng-utils :
 * pscap
 * filecap
 * netcap




```bash
set -v

# To see the highest capability number for your kernel,
cat /proc/sys/kernel/cap_last_cap

# To display the full list of available Linux capabilities for the active kernel
capsh --print

sudo capsh --print | grep Bounding | tr "," "\n"

set +v
```


    # To see the highest capability number for your kernel,
    cat /proc/sys/kernel/cap_last_cap
    37

    # To display the full list of available Linux capabilities for the active kernel
    capsh --print
    Current: =
    Bounding set =cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read
    Securebits: 00/0x0/1'b0
     secure-noroot: no (unlocked)
     secure-no-suid-fixup: no (unlocked)
     secure-keep-caps: no (unlocked)
    uid=1000(user)
    gid=1000(user)
    groups=4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),118(lpadmin),126(sambashare),999(docker),1000(user)


    sudo capsh --print | grep Bounding | tr "," "\n"
    Bounding set =cap_chown
    cap_dac_override
    cap_dac_read_search
    cap_fowner
    cap_fsetid
    cap_kill
    cap_setgid
    cap_setuid
    cap_setpcap
    cap_linux_immutable
    cap_net_bind_service
    cap_net_broadcast
    cap_net_admin
    cap_net_raw
    cap_ipc_lock
    cap_ipc_owner
    cap_sys_module
    cap_sys_rawio
    cap_sys_chroot
    cap_sys_ptrace
    cap_sys_pacct
    cap_sys_admin
    cap_sys_boot
    cap_sys_nice
    cap_sys_resource
    cap_sys_time
    cap_sys_tty_config
    cap_mknod
    cap_lease
    cap_audit_write
    cap_audit_control
    cap_setfcap
    cap_mac_override
    cap_mac_admin
    cap_syslog
    cap_wake_alarm
    cap_block_suspend
    cap_audit_read

    set +v



```bash
set -v

# show capabilities for the current process (current bash)
cat /proc/$$/status | grep Cap

capsh --decode=0000003fffffffff

getpcaps $$

set +v
```


    # show capabilities for the current process (current bash)
    cat /proc/$$/status | grep Cap
    [01;31m[KCap[m[KInh:	0000000000000000
    [01;31m[KCap[m[KPrm:	0000000000000000
    [01;31m[KCap[m[KEff:	0000000000000000
    [01;31m[KCap[m[KBnd:	0000003fffffffff
    [01;31m[KCap[m[KAmb:	0000000000000000

    capsh --decode=0000003fffffffff
    0x0000003fffffffff=cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read

    getpcaps $$
    Capabilities for `18671': =

    set +v



```bash
set -v

### Limiting the capabilities for processes

#  capsh command can run a particular process and restrict the set of available capabilities.
capsh --print -- -c "/bin/ping -c 1 localhost"

# Dropping capabilities with capsh
capsh --drop=cap_net_raw --print -- -c "/bin/ping -c 1 localhost"

set +v
```


    ### Limiting the capabilities for processes

    #  capsh command can run a particular process and restrict the set of available capabilities.
    capsh --print -- -c "/bin/ping -c 1 localhost"
    Current: =
    Bounding set =cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read
    Securebits: 00/0x0/1'b0
     secure-noroot: no (unlocked)
     secure-no-suid-fixup: no (unlocked)
     secure-keep-caps: no (unlocked)
    uid=1000(user)
    gid=1000(user)
    groups=4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),118(lpadmin),126(sambashare),999(docker),1000(user)
    PING localhost (127.0.0.1) 56(84) bytes of data.
    64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.041 ms

    --- localhost ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 0.041/0.041/0.041/0.000 ms

    # Dropping capabilities with capsh
    capsh --drop=cap_net_raw --print -- -c "/bin/ping -c 1 localhost"
    unable to raise CAP_SETPCAP for BSET changes: Operation not permitted

    set +v


# Authentication

## PAM

An introduction to Pluggable Authentication Modules (PAM) in Linux<br>
https://www.redhat.com/sysadmin/pluggable-authentication-modules-pam

Pluggable Authentication Modules for Linux
https://www.linuxjournal.com/article/2120

PAM&mdash;Securing Linux Boxes Everywhere
https://www.linuxjournal.com/magazine/pammdashsecuring-linux-boxes-everywhere

### Intro

If every program had to implement its own authentication logic, things would be a mess, because nobody could be certain that every application included the same tests, made the same checks and correctly implemented the same code. And, if extra controls were needed, everything would have to be reprogrammed!

With Linux servers, whenever a program needs some authentication task, it can call the PAM API, which will run all the required checks in its configuration file. If you modify such files on the fly, all PAM-aware programs instantly will apply the new rules.

PAM deals with four security areas, identified by specific keywords:
 * Account limitations: what is a valid user allowed to do, and when?
 * Auth (authentication) details: how is a valid user recognized?
 * Password-related functions, including password changing, for example.
 * Session management, including connection and logging.

Whenever an application makes a security request, PAM executes modules as specified in its configuration file to approve or reject said request.


### config

`/usr/lib64/security` : A collection of PAM libraries that perform various checks
`/etc/pam.d` : A collection of configuration files for applications that call libpam.

### modules

http://www.linux-pam.org/Linux-PAM-html/Linux-PAM_SAG.html

### pam_usb

pam_usb provides hardware authentication for Linux using ordinary USB Flash Drives.

 * https://github.com/mcdope/pam_usb
 * https://github.com/mcdope/pam_usb/wiki/Getting-Started




```bash

```
