
[[_TOC_]]


## Introduction

https://www.ssh.com/ssh/

Secure Shell (better known as SSH) is a cryptographic network protocol which allows users to securely perform a number of network services over an unsecured network.


## ssh server

*Installation*

```bash
aptitude install openssh-server
```

### Configuration


sshd_config - SSH Server Configuration : https://www.ssh.com/ssh/sshd_config/

Mozilla security guideline : https://infosec.mozilla.org/guidelines/openssh


OpenSSH SSH daemon configuration file

    /etc/ssh/sshd_config

For instance some defaults values on Debian
```
Protocol 2
PermitRootLogin without-password
PubkeyAuthentication yes
ChallengeResponseAuthentication no
PasswordAuthentication no  # NB to enforce security , default to yes
X11Forwarding yes
UsePAM yes  # PAM also provides session and account management, even if password based authentication is disabled
```

Some options:

 * `PermitRootLogin`
     * `without-password | prohibit-password` : bans all interactive authentication methods, allowing only public-key
     * `yes` : allow root user to login with a password (TO AVOID)

 * `Port 22` : Specifies the port number that sshd listens on.  The default is 22.
 * `PubkeyAuthentication` : Specifies whether public key authentication is allowed.  The default is yes.
 * `LoginGraceTime 120` : The server disconnects after this time if the user has not successfully logged in.
 * `HostbasedAuthentication no` : Specifies whether rhosts or /etc/hosts.equiv authentication together with successful public key client host authentication is allowed
 * `X11Forwarding yes` : Specifies whether X11 forwarding is permitted. by default `no`
 * `Protocol 2` : Verify that only protocol version 2 is allowed.
 * `AllowGroups ssh-users` : If specified, login is allowed only for users whose primary group or supplementary group list matches one of the patterns.
 * `StrictModes` : Specifies whether sshd should check file modes and ownership of the user's files and home directory before accepting login.
 * `Subsystem sftp /usr/lib/openssh/sftp-server` : enables SFTP file transfer subsystem
 * `UseDNS no` : Specifies whether sshd should look up the remote host name.


**TIP** Check the validity of the configuration file, output the effective configuration to stdout

    sshd -T -f /etc/ssh/sshd_config


**NB** ssh daemon configuration change will require a restart of service

    systemctl restart sshd



### ssh session and timeout

How to disable SSH timeout: https://www.simplified.guide/ssh/disable-timeout


How to Keep Your SSH Sessions Alive:<br>
https://sysadmincasts.com/episodes/39-cli-monday-how-to-keep-your-ssh-sessions-alive

 * ClientAliveInterval
 * ServerAliveInterval

client configuration `~/.ssh/config`

```
# keep ssh sessions fresh
Host *
  ServerAliveInterval 60
```

server configuation `/etc/ssh/sshd_config`

```
# keep ssh sessions fresh
ClientAliveInterval 60
```

---

5 Ways to Keep Remote SSH Sessions and Processes Running After Disconnection <br>
http://www.tecmint.com/keep-remote-ssh-sessions-running-after-disconnection/

### How to Harden SSH

Eight ways to protect SSH access on your system<br>
https://www.redhat.com/sysadmin/eight-ways-secure-ssh

Multiple Ways to Secure SSH Server<br>
https://linuxhint.com/secure_ssh_server/


Disabling Password-based Authentication => set in `sshd_config`

    PasswordAuthentication no


cf journal user / geek guide SSH: A MODERN LOCK FOR YOUR SERVER?
Tips for Hardening SSH
 * Change the Standard SSH Port
 * Make Users Knock for Access
 * Avoid Configuration Weaknesses
 * Prefer Keys over Passwords
 * Limit Password-Based Logins
 * Enable Access Rules
 * Use PAM (Pluggable Authentication Modules) for Checks
 * Block Brute-Force Attacks (DenyHosts, BlockHosts, Fail2Ban)


##### no root login with password

https://www.admin-linux.fr/nauthoriser-les-connexions-ssh-root-que-par-cle/

Modify `/etc/ssh/sshd_config` with:

    PermitRootLogin without-password

##### Disable root login

Disable SSH remote login for root. <br>This can be done by modifying the contents of a configuration file `/etc/ssh/sshd_config`.<br>

Look specifically for `PermitRootLogin` and set it to `no`.


##### Using certificates

How to Harden SSH with Identities and Certificates :
https://ef.gy/hardening-ssh

SSH certificates are the latest and greatest enhancement to the public and private key authentication SSH has to offer.

* Creating a Root Certificate

        ssh-keygen -b 4096 -t rsa -f example-com-ca -C "CA key for example.com"

* Signing Host Keys

        # sign a public key ssh_host_rsa_key.pub with the private key example-com-ca. NB -h Sign a host key.
        ssh-keygen -s example-com-ca -h -n host.example.com,host -V +52w -I host.example.com-key  /etc/ssh/ssh_host_rsa_key.pub

* Using Signed Host Keys

    * On the server, reconfigure OpenSSH daemon to use the shiny new certificate by editing /etc/ssh/sshd_config:

            HostCertificate /etc/ssh/ssh_host_rsa_key-cert.pub

    * On the client side, you need to add the signing public key to your user's ~/.ssh/known_hosts file.

            @cert-authority *.example.com contents-of-public-key-file

* Signing User Keys

        # sign a user public key, without -h option and  -n flag now specifies users and not host names
        ssh-keygen -s example-com-ca -n user -V +52w -I example.com-user ~/.ssh/id_rsa.pub

* Using Signed User Keys

    * On the server, copy the ca.pub into /etc/ssh and reconfigure OpenSSH daemon using:

            TrustedUserCAKeys /etc/ssh/example-com-ca.pub

    * On the client side, you can now log on using your signed key, without needing to update the authorized_keys file for each user you want to log on as.



Using SSH certificates: <br>
https://www.sweharris.org/post/2016-10-30-ssh-certs/

SSH Key Management & SSH Key managers : <br>
https://goteleport.com/blog/ssh-key-management/

##### ciphers

cf Strong Ciphers in SSH<br>
https://www.linuxjournal.com/content/cipher-security-how-harden-tls-and-ssh

Ciphers and algorithms choice<br>
https://infosec.mozilla.org/guidelines/openssh#ciphers-and-algorithms-choice

The various algorithms supported by a particular OpenSSH version can be listed with the following

```bash

ssh -Q cipher
ssh -Q cipher-auth
ssh -Q key

```


```bash
set -v
head /etc/ssh/sshd_config

# at openssh installation,  a pair of asymetric key is generated to identify the server (RSA and DSA)

ls -l /etc/ssh/ssh_host_{rsa,dsa}_key

ls -l /etc/ssh/ssh_host_{rsa,dsa}_key.pub

# Show fingerprint
ssh-keygen -l -f /etc/ssh/ssh_host_rsa_key.pub

ssh-keygen -l -f /etc/ssh/ssh_host_ecdsa_key.pub

set +v
```

    head /etc/ssh/sshd_config
    #	$OpenBSD: sshd_config,v 1.101 2017/03/14 07:19:07 djm Exp $

    # This is the sshd server system-wide configuration file.  See
    # sshd_config(5) for more information.

    # This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin

    # The strategy used for options in the default sshd_config shipped with
    # OpenSSH is to specify options with their default value where
    # possible, but leave them commented.  Uncommented options override the

    # at openssh installation,  a pair of asymetric key is generated to identify the server (RSA and DSA)

    ls -l /etc/ssh/ssh_host_{rsa,dsa}_key
    ls: impossible d'accéder à '/etc/ssh/ssh_host_dsa_key': Aucun fichier ou dossier de ce type
    -rw------- 1 root root 1675 août   2 21:55 /etc/ssh/ssh_host_rsa_key

    ls -l /etc/ssh/ssh_host_{rsa,dsa}_key.pub
    ls: impossible d'accéder à '/etc/ssh/ssh_host_dsa_key.pub': Aucun fichier ou dossier de ce type
    -rw-r--r-- 1 root root 417 août   2 21:55 /etc/ssh/ssh_host_rsa_key.pub

    # Show fingerprint
    ssh-keygen -l -f /etc/ssh/ssh_host_rsa_key.pub
    2048 SHA256:VPPvhMcwts+8qSefUK0fpDObTS0IQOyDPoIYbrMfjfA root@user-Lenovo-ideapad-330S-15IKB (RSA)

    ssh-keygen -l -f /etc/ssh/ssh_host_ecdsa_key.pub
    256 SHA256:VhDGxuU1FNdynS+WYazodcQ3H+z3FX54GdrdBbrdhHs root@user-Lenovo-ideapad-330S-15IKB (ECDSA)

    set +v


### Limit ssh connections

Limit The Number Of SSH Logins Per User/Group/System<br>
https://www.ostechnix.com/limit-the-number-of-ssh-logins-per-user-group-system/


```bash
# /etc/security/limits.conf

# limit the number of concurrent SSH sessions for a specific user
ostechnix	hard	maxlogins	1

# limit the number of concurrent SSH sessions for  a group of users
@mygroup	hard    maxlogins	1

#  limit SSH sessions of all users (global) in the system,
# *	hard    maxlogins	1

```

Allow Or Deny SSH Access To A Particular User Or Group In Linux<br>
https://www.ostechnix.com/allow-deny-ssh-access-particular-user-group-linux/


```bash
# /etc/ssh/sshd_config
AllowUsers sk ostechnix

AllowGroups root

```

Disable SSH Password Authentication For Specific User Or Group
https://www.ostechnix.com/disable-ssh-password-authentication-for-specific-user-or-group/
```
Match User senthil
  PasswordAuthentication no
Match Group mygroup
    PasswordAuthentication no  

```

**NB** Please note that Match is effective "until either another Match line or the end of the file." If a keyword appears in multiple Match blocks that are satisfied, only the first instance of the keyword is applied.

cf `man sshd_config | less +/^'Match  '`

### 2FA

linux pratique 120 p29 using mobile app freeotpplus

```
# debian
apt install libpam-oath

# edit /etc/pam.d/sshd for OTP
auth sufficient pam_oath.so userfile=/etc/users.oath window=10 digits=6

# create users.oath
sudo touch /etc/users.oath
sudo chmod go-rw /etc/users.oath

sudo apt install oathtool caca-utils grencode
git clone https://github.com/mcepl/gen-oath-safe.git

gen-oath-safe/gen-oath-safe arnaud totp
# flash the QR Code from mobile app freeotp+
# Add the 2 last lines in /etc/users.oath

```

How to Secure SSH with Google Authenticator’s Two-Factor Authentication
https://www.howtogeek.com/121650/how-to-secure-ssh-with-google-authenticators-two-factor-authentication/


### ssh logs

```
# on debian
tail -f /var/log/auth.log

# on redhat/centos
tail -f /var/log/secure

```


## ssh keys

SSH keys provide a more secure way of logging into a server with SSH than using a password alone.

Key types:

 * RSA keys are favored over ECDSA keys when backward compatibility ''is required''
 * ED25519 keys are favored over RSA keys when backward compatibility ''is not required''.

> Avoid DSA and ecdsa (ECDSA (or regular DSA for that matter) is just not a good algorithm, no matter what curve you use)

Upgrade Your SSH Key to Ed25519<br>
https://medium.com/risan/upgrade-your-ssh-key-to-ed25519-c6e8d60d3c54

### Set Up SSH Keys

Generating a key pair provides you with two long string of characters: a public and a private key.

`ssh-keygen` : authentication key generation, management and conversion
 + `-b bits` :  Specifies the number of bits in the key to create.  by default 2048 bits for RSA
 + `-t rsa  | ed25519`: Specifies the type of key to create.
 + `-N new_passphrase`
 + `-p` : Requests changing the passphrase of a private key file instead of creating a new private key.
 + `-C comment`
 + `-f output_keyfile` : Specifies the filename of the key file, by default ~/.ssh/id_rsa & ~/.ssh/id_rsa.pub
 + `-q` : silence ssh-keygen
 + `-E fingerprint_hash` : Specifies the hash algorithm used when displaying key fingerprints. “md5” “sha256”
 + `-l` : Show fingerprint of specified public key file (sha256 by default)
 + `-y` :  This option will read a private OpenSSH format file and print an OpenSSH public key to stdout.


       ssh-keygen -t rsa [-f key_filename] [-C comment] [-N ""]


**key format**

PEM format with an RSA key  (using -m pem) : -----BEGIN RSA PRIVATE KEY-----

OpenSSH format (by default) : -----BEGIN OPENSSH PRIVATE KEY-----

cf new openssh key format
https://flak.tedunangst.com/post/new-openssh-key-format-and-bcrypt-pbkdf

What is the difference between PEM format to DSA/RSA/ECC? (Might I confuse PEM with these)?<br>
https://security.stackexchange.com/questions/143114/what-is-the-difference-between-pem-format-to-dsa-rsa-ecc-might-i-confuse-pem-w

How to Create an SSH Key Passphrase in Linux<br>
https://www.linuxshelltips.com/create-ssh-key-passphrase/

    # Add a Passphrase for an Existing SSH Key
    ssh-keygen -p

 ### Secure SSH Keypairs

 * ED25519 https://ed25519.cr.yp.to/
 * RSA key of 3072 bits
 * ECDSA using the E 521 curve !!! It depends on how well your machine can generate a random number that will be used to create a signature !


`ssh-keygen for ED25519`
 * `-a` : It’s the numbers of KDF (Key Derivation Function) rounds.
 * `-o` : Save the private-key using the new OpenSSH format rather than the PEM format, NB by default for ed25519

```

     # 100 rounds , -o secure format
     ssh-keygen -a 100 -t ed25519

     # rsa
     ssh-keygen -o -a 100 -b 3072 -t rsa

     # ecdsa
     ssh-keygen -o -a 100 -b 521 -t ecdsa

```



```bash
set -v

rm -f rsa_*key* ed25519_key

# ed25519
ssh-keygen -a 100 -t ed25519 -f ed25519_key -N "" -C "test key ecdsa"

# 512 bytes
ssh-keygen -t rsa -b 4096 -f rsa_lkey -N "" -C "test key 512b"

# default 256 bytes
ssh-keygen -t rsa -f rsa_key -N "" -q -C "test key 256b"

ls -l rsa*

cat ed25519_key.pub

cat rsa_key.pub

cat rsa_lkey.pub

set +v
```


    rm -f rsa_*key* ed25519_key

    # ed25519
    ssh-keygen -a 100 -t ed25519 -f ed25519_key -N "" -C "test key ecdsa"
    Generating public/private ed25519 key pair.
    Your identification has been saved in ed25519_key
    Your public key has been saved in ed25519_key.pub
    The key fingerprint is:
    SHA256:WDkI8pU2wf4ljrPtiPsW4cj/LW6Eki/LL8rgI+78obI test key ecdsa
    The key's randomart image is:
    +--[ED25519 256]--+
    |  . ..oo         |
    |   o o=. .       |
    |    .o..+        |
    |      oo...      |
    |   . +.*So       |
    |    = * +        |
    |.  . + *         |
    |=+..+.=.+.       |
    |EB=o=O+=+..      |
    +----[SHA256]-----+

    # 512 bytes
    ssh-keygen -t rsa -b 4096 -f rsa_lkey -N "" -C "test key 512b"
    Generating public/private rsa key pair.
    Your identification has been saved in rsa_lkey
    Your public key has been saved in rsa_lkey.pub
    The key fingerprint is:
    SHA256:ofLEqU/tGDgqXvpHDjikLmLIAoGYsupnPO5GBZH9MWU test key 512b
    The key's randomart image is:
    +---[RSA 4096]----+
    |   .+   .E       |
    |   o . o.        |
    |o.  . . +        |
    |*.   o + .       |
    |+o. o = S        |
    |+o o B .         |
    |* +.B + .        |
    |B+oB * +         |
    |B=O+o o .        |
    +----[SHA256]-----+

    # default 256 bytes
    ssh-keygen -t rsa -f rsa_key -N "" -q -C "test key 256b"

    ls -l rsa*
    -rw------- 1 s0085440 s0085440 2602 mars  17 21:34 rsa_key
    -rw-r--r-- 1 s0085440 s0085440  567 mars  17 21:34 rsa_key.pub
    -rw------- 1 s0085440 s0085440 3381 mars  17 21:34 rsa_lkey
    -rw-r--r-- 1 s0085440 s0085440  739 mars  17 21:34 rsa_lkey.pub

    cat ed25519_key.pub
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG03pXh3c9YYIYV1Wkc5gGjOdpkgiT/QVmxHn0YhBOu4 test key ecdsa

    cat rsa_key.pub
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDQWxPlNuNpcQ4lELrEsT1Jqkb8sPsjCB4RPOYqKu8yYQHGZxl9jfdevFZk5DpJyxdUXeIkKXgRzLJD23cwvV785HuOTY+EngR5FW73tcxPv/Qsv7oL25du0PchnklKyGRDfcJyhk99tmDtbXOEKB7eGUSgOuv9F/ukpejr3gYGfiPOFny0heHHIPN5lfOeQ4i8Ls4HFN9DdyzpPMan2Q+Jnea3jOo7nJdNuBO/rXaJgGQHzIxKN0UH8qP340Ib5GNf+PPXycpxLaZDKHmguC3DKZYLHxImtfzJH8V2T9lN8HroifwDrbe1vk5CZuKE4YKu5Ejr1S3CeClSpE1yFifyQbEr6F3yQUV1HYQau76cq8lcbxXOcEnsiD/V5cJqHx5ka/vBkEhzO8dMdCocWxcBV+WmJSV4QVwy9mjUIwmzkiX2FouEZ6voem+4I5OKp2eEagKKqalCFFr/IZz3xMknGTIo5/qESkRRo4Zj81F4TBffrVt6wJIBSV5X6Nhjcfs= test key 256b

    cat rsa_lkey.pub
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCgQ7bQSiQoEF2rGBwY4Bg3mnV9zsJJv6XRkNTLpds/BVJQWaF+lEKVuskr8o6cpPizCOWz6mbX5AvXETcDDxJJtb08Covx2Anj26iQfOCMCYw/bv+aCJjzA09nPWkk2A3UhYxNFJSpUu+lgdwUPZZTJ/H9Vxn0YBlF5U9mtOtnZi/fAxvP2Ad9LZTl2dqgZ1/9v7ukLa9W101R32rCFzNLyOST+wHj64ow1iY1aY28BYEkdCEsaGs+FXqCemICoCVGz9vyK0NTtBaOPZGA1tBWU6k7I5Ey7vQdOixg6366tT2WX0fuhT9u1jmoYdwwX56Pk2Zy44TSXr3DGW5y7m1YDWEC2Fc9A/tf5Z8glj1z6DRdjzvcGxtILlrDfr/8gkdSa3yvY3ULqvcAzyMvAHW8Dsm3Bd0gAU92hSJyacBrfzPA7N7Xa18dKlSp/4GneKSs6aXRkyWLLI6nhjJM49uwGcGccjefcy+SxtifYBtbirkEJOX0HLRfMUSa92/lJ0VrIk/kjXEmeC3sGdQ2+k+amrmxxfKvEvW5EVcEHyX6cHNIwFh1phlOQTqvIJ77A01R5B3mtimG9vQfkoZU2v8ZmzrbV8WrGBUlsjqwUNirCxDQ++wToe2kS+mDO+MaTz+Qx+pm8yh+dQFmx4Fzl+W9RK5Lb+EcQW1mGRtonUEebw== test key 512b

    set +v


### ssh-keygen additional options to manage known_hosts entries


```

ssh-keygen -F myhost         # shows myhosts's line in the known_hosts file
ssh-keygen -l -F myhost      # additionally shows myhost's fingerprint
ssh-keygen -R myhost         # remove myhost's line from known_hosts


```

NB Depending on ssh client configuration (`HashKnownHosts yes`) the hostname stored in the `known_host` file can be hashes.


```bash
set  -v

# search bitbucket.org host in known_hosts
ssh-keygen -F bitbucket.org

# search bitbucket.org host in known_hosts and show fingerprint
ssh-keygen -l -F bitbucket.org

# first entry in known_hosts
sed -n 1p ~/.ssh/known_hosts

# compute hash of the first entry
ssh-keygen -l -f <(sed  -n 1p ~/.ssh/known_hosts | cut -d' ' -f2,3)

set +v
```


    # search bitbucket.org host in known_hosts
    ssh-keygen -F bitbucket.org
    # Host bitbucket.org found: line 1
    |1|w0LDHk+ATwnOslOeABb0vf/aOYM=|N/fV+cqlHqPSYSXTTj39B51WIIY= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==

    # search bitbucket.org host in known_hosts and show fingerprint
    ssh-keygen -l -F bitbucket.org
    # Host bitbucket.org found: line 1
    bitbucket.org RSA SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A

    # first entry in known_hosts
    sed -n 1p ~/.ssh/known_hosts
    |1|w0LDHk+ATwnOslOeABb0vf/aOYM=|N/fV+cqlHqPSYSXTTj39B51WIIY= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==

    # compute hash of the first entry
    ssh-keygen -l -f <(sed  -n 1p ~/.ssh/known_hosts | cut -d' ' -f2,3)
    sed  -n 1p ~/.ssh/known_hosts | cut -d' ' -f2,3
    2048 SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A no comment (RSA)

    set +v


### Retrieve the public key from a SSH private key


```bash
set -v

# How do I retrieve the public key from a SSH private key
ssh-keygen -y -f rsa_key

set +v
```


    # How do I retrieve the public key from a SSH private key
    ssh-keygen -y -f rsa_key
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5n/FOLIJG3gIPN7nvFEYT6g0wot00ACf0zH7CHYEVXuJuDih4YBixAYTrNWsTspwTcyA+IlQ2kBRn9s5ZeF8yVravM2Hy17yXhSTfY4Hob6mBMjzRovoVoWFveZJ3VWk1BkEXSkf66JgsMjHmQWB1+vcfXf6IvTeZghYKW8bDLteaTSwDkkzRlCpWgv2DtQgAwTlVPgNZ3lcIodNkScDWbSpp/ZTPa7XSD/phzbOFyzt1DqvkNqoDQzAuBm265QUu1vfjD3cNSfvbOpEDNSFDM6CmJz7k95iwxk0r1xvaatj7VfQ1vrEjcs+Ff53Xzr/5kYYf0vPaIa6CwxtbT7c1

    set +v


### Retrieve md5 fingerprint from public or private key


```bash
set -v

# How do I retrieve md5 fingerprint from public key

ssh-keygen -E md5 -lf  rsa_key.pub

# or private key
ssh-keygen -E md5 -lf  rsa_key

ssh-keygen -E md5 -lf  rsa_lkey.pub

set +v
```


    # How do I retrieve md5 fingerprint from public key

    ssh-keygen -E md5 -lf  rsa_key.pub
    2048 MD5:eb:29:9d:34:64:c0:ac:5a:f1:93:17:3c:21:f0:02:d2 test key 256b (RSA)

    # or private key
    ssh-keygen -E md5 -lf  rsa_key
    2048 MD5:eb:29:9d:34:64:c0:ac:5a:f1:93:17:3c:21:f0:02:d2 test key 256b (RSA)

    ssh-keygen -E md5 -lf  rsa_lkey.pub
    4096 MD5:89:40:b2:8c:b6:3c:00:65:a4:12:54:77:f2:21:08:71 test key 512b (RSA)

    set +v



```bash
set -v

# How do I retrieve fingerprint from public/private key

ssh-keygen -lf  rsa_lkey.pub

set +v
```


    # How do I retrieve fingerprint from public key

    ssh-keygen -lf  rsa_lkey.pub
    4096 SHA256:vBPddRpS0o8BqqY2Nn7zMFkB5Pe/8UfrrlcqKasY7e0 test key 512b (RSA)

    set +v



```bash
set -v

# -v show visual ASCCI random art image
ssh-keygen -v -lf rsa_lkey.pub

set +v
```


    ssh-keygen -v -lf rsa_lkey.pub
    4096 SHA256:GNFe3/1LNEwuVmHPWA1i/rgM5DkuIOcL26ZmiSLPqJg test key 512b (RSA)
    +---[RSA 4096]----+
    |      ..    o .++|
    |       .. .o ..*o|
    |      .. .....*.o|
    |       o.o ..=.=.|
    |    . + S = o + o|
    |     + . . + . ..|
    |   .... . . o . .|
    |++. ++.. .     . |
    |Eoooooo          |
    +----[SHA256]-----+

    set +v


### convert between OpenSSH and SSH2

https://burnz.wordpress.com/2007/12/14/ssh-convert-openssh-to-ssh2-and-vise-versa/


Convert OpenSSH key to SSH2 key

`-e`  : read a private or public OpenSSH key file and print to stdout the key in one of the formats specified by the -m option.  The default export format is “RFC4716” i.e. Secure Shell (SSH) Public Key File Format.

    ssh-keygen -e -f ~/.ssh/id_dsa.pub > ~/.ssh/id_dsa_ssh2.pub

Convert SSH2 key to OpenSSH key

`-i` : read an unencrypted private (or public) key file in the format specified by the -m option and print an
OpenSSH compatible private (or public) key to stdout.

    ssh-keygen -i -f ~/.ssh/id_dsa_1024_a.pub > ~/.ssh/id_dsa_1024_a_openssh.pub




```bash
set -v

# Convert openssh to ssh2 public key

ssh-keygen -e -f rsa_key.pub | tee rsa_key_ssh2.pub

# Convert ssh2 to openssh public key

ssh-keygen -i -f rsa_key_ssh2.pub

set +v
```


    # Convert openssh to ssh2 public key

    ssh-keygen -e -f rsa_key.pub | tee rsa_key_ssh2.pub
    ---- BEGIN SSH2 PUBLIC KEY ----
    Comment: "2048-bit RSA, converted by user@user-Lenovo-ideapad-330S-15I"
    AAAAB3NzaC1yc2EAAAADAQABAAABAQC5n/FOLIJG3gIPN7nvFEYT6g0wot00ACf0zH7CHY
    EVXuJuDih4YBixAYTrNWsTspwTcyA+IlQ2kBRn9s5ZeF8yVravM2Hy17yXhSTfY4Hob6mB
    MjzRovoVoWFveZJ3VWk1BkEXSkf66JgsMjHmQWB1+vcfXf6IvTeZghYKW8bDLteaTSwDkk
    zRlCpWgv2DtQgAwTlVPgNZ3lcIodNkScDWbSpp/ZTPa7XSD/phzbOFyzt1DqvkNqoDQzAu
    Bm265QUu1vfjD3cNSfvbOpEDNSFDM6CmJz7k95iwxk0r1xvaatj7VfQ1vrEjcs+Ff53Xzr
    /5kYYf0vPaIa6CwxtbT7c1
    ---- END SSH2 PUBLIC KEY ----

    # Convert ssh2 to openssh public key

    ssh-keygen -i -f rsa_key_ssh2.pub
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5n/FOLIJG3gIPN7nvFEYT6g0wot00ACf0zH7CHYEVXuJuDih4YBixAYTrNWsTspwTcyA+IlQ2kBRn9s5ZeF8yVravM2Hy17yXhSTfY4Hob6mBMjzRovoVoWFveZJ3VWk1BkEXSkf66JgsMjHmQWB1+vcfXf6IvTeZghYKW8bDLteaTSwDkkzRlCpWgv2DtQgAwTlVPgNZ3lcIodNkScDWbSpp/ZTPa7XSD/phzbOFyzt1DqvkNqoDQzAuBm265QUu1vfjD3cNSfvbOpEDNSFDM6CmJz7k95iwxk0r1xvaatj7VfQ1vrEjcs+Ff53Xzr/5kYYf0vPaIa6CwxtbT7c1

    set +v


### Converting public key to PKCS format


#### Generating PKCS#1

generate PEM DER ASN.1 PKCS#1 RSA Public key  : `-----BEGIN RSA PUBLIC KEY-----`

```bash
ssh-keygen -f rsa_key.pub -e -m pem > rsa_key.pub.pem  
```

#### Generating PKCS#8

Public key header : `-----BEGIN PUBLIC KEY-----`

```bash
ssh-keygen -f rsa_key.pub -e -m PKCS8 > rsa_key.pub.pem  
```

## ssh agent

SSH agents can be used to hold your private SSH keys in memory. The agent will then authenticate you to any hosts that trust your SSH key.

### Using an SSH Agent

Check if agent is started

    ps -p $SSH_AGENT_PID

First start your agent:

    eval $(ssh-agent)

NB when ssh-agent is ran, it outputs some variables definition (eval allows to retrieve those variables)
```
SSH_AUTH_SOCK=/tmp/ssh-kSWOhvU7CzZB/agent.7993; export SSH_AUTH_SOCK;
SSH_AGENT_PID=7994; export SSH_AGENT_PID;
echo Agent pid 7994;
```

NB SSH agents listen on a unix socket (SSH_AUTH_SOCK).

Then add your keys to it – you’ll need to enter your passphrase for any encrypted keys:

    ssh-add ~/dir/mykey

    ssh-add # add default private key ~/.ssh/id_rsa

List keys added in the ssh-agent

    ssh-add -L


`ssh-add` : adds private key identities to the authentication agent
 + -c : Indicates that added identities should be subject to confirmation before being used for authentication.
 + -D : Deletes all identities from the agent.
 + -d key.pub : delete specified key
 + -L : Lists public key parameters of all identities currently represented by the agent.
 + -l : Lists fingerprints of all identities currently represented by the agent.

terminate ssh-agent interactively

    eval $(ssh-agent -k)


### Agent for sudo

Using SSH agent for sudo authentication :<br> http://evans.io/legacy/posts/ssh-agent-for-sudo-authentication/    


### Agent forwarding

One way to avoid copying SSH private keys around is to use the ssh-agent program on your local machine, with agent forwarding. If you SSH from your laptop to host A, and you have agent forwarding enabled, then agent forwarding allows you to SSH from host A to host B using the private key that resides on your laptop.
ssh with the `-A` flag, enables agent forwarding

    ssh -A myuser@myappserver.example.com

To allow agent forwarding for all hosts

*~/.ssh/config* :
```
Host *
   ForwardAgent yes
   AddKeysToAgent yes

```

**Hardening**

Hardening the Agent forwarder<br>
https://infosec.mozilla.org/guidelines/openssh#hardening-the-agent-forwarder



## ssh client

### Installation

```bash
aptitude install openssh-client

```


### Copy the Public Key

You can copy the public key into the new machine's *`authorized_keys`* file with the `ssh-copy-id` command.   

`ssh-copy-id`:  use locally available keys to authorise logins on a remote machine

```
ssh-copy-id demo@198.51.100.0

# specifying publick key
ssh-copy-id -i pub.key user@remote

# alternative way
cat ~/.ssh/id_rsa.pub | ssh demo@198.51.100.0 "mkdir -p ~/.ssh && chmod 700 ~/.ssh && cat >>  ~/.ssh/authorized_keys"

```



```bash
set -v

NS=sshdemo
sudo useradd -p $(mkpasswd "$NS") -d /home/$NS -m -g users -s /bin/bash "$NS"

# disable passwd
sudo passwd -d sshdemo
sudo passwd -S sshdemo

sudo bash -c "mkdir /home/sshdemo/.ssh && cat rsa_key.pub > /home/sshdemo/.ssh/authorized_keys"

ssh sshdemo@localhost -i rsa_key pwd

# sudo cat /home/sshdemo/.ssh/authorized_keys

sudo userdel -r $NS

set +v
```


    NS=sshdemo
    sudo useradd -p $(mkpasswd "$NS") -d /home/$NS -m -g users -s /bin/bash "$NS"
    mkpasswd "$NS"

    # disable passwd
    sudo passwd -d sshdemo
    passwd : expiration du mot de passe modifiée.
    sudo passwd -S sshdemo
    sshdemo NP 03/03/2018 0 99999 7 -1

    sudo bash -c "mkdir /home/sshdemo/.ssh && cat rsa_key.pub > /home/sshdemo/.ssh/authorized_keys"

    ssh sshdemo@localhost -i rsa_key pwd
    /home/sshdemo

    # sudo cat /home/sshdemo/.ssh/authorized_keys

    sudo userdel -r $NS
    userdel : l'emplacement de boîte aux lettres de sshdemo (/var/mail/sshdemo) n'a pas été trouvé

    set +v


### ssh Forced command

#### Restricting SSH Commands

https://binblog.info/2008/10/20/openssh-going-flexible-with-forced-commands/

You might have some users (or scheduled automatisms) that you don’t want to be able to log on to that machine at all, but who should be permitted to execute only a given command. In order to achieve this, you can configure key-based authentication. Once this has been done, the key can be prefixed with a number of configuration options. Using one of these options, it is possible to enforce execution of a given command when this key is used for authentication.

In addition to enforcing a command, it is advisable to disable a number of advanced SSH features, such as TCP and X11 forwarding. Assignment of a pseudo terminal to the user’s SSH session may also be suppressed, by adding a number of additional configuration options next to the forced command:

```
command="/bin/ps -ef",no-port-forwarding,no-X11-forwarding,no-pty ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAp0KMipajKK468mfihpZHqmrMk8w+PmzTnJrZUFYZZNmLkRk+icn+m71DdEHmza2cSf9WdiK7TGibGjZTE/Ez0IEhYRj5RM3dKkfYqitKTKlxVhXNda7az6VqAJ/jtaBXAMTjHeD82xlFoghLZOMkScTdWmu47FyVkv/IM1GjgX/I8s4307ds1M+sICyDUmgxUQyNF3UnAduPn1m8ux3V8/xAqPF+bRuFlB0fbiAEsSu4+AkvfX7ggriBONBR6eFexOvRTBWtriHsCybvd6tOpJHN8JYZLxCRYHOGX+sY+YGE4iIePKVf2H54kS5UlpC/fnWgaHbmu/XsGYjYrAFnVw== Test key

```

This is called an SSH forced command. With a forced command in place, the SSH server ignores the command requested by the user. The SSH server the command that was requested, stores it in `$SSH_ORIGINAL_COMMAND` and thus makes it available within the environment of the forced command.

```
command="/bin/echo Command was: $SSH_ORIGINAL_COMMAND" ssh-dss AAAAB3NzaC1kcMAAAEBANpgvvTslst2m0ZJA0ayhh1Mqa3aWwU3kfv0m9+myFZ9veFsxM7IVxIjWfAlQh3jp lY+Q78fMzCTiG+ZrGZYn8adZ

```

#### Restricting ip source

```
from="10.0.0.?,*.example.com",no-X11-forwarding ssh-rsa AB3Nz...EN8w== xavki@monhost
```

### ssh connection

https://www.ssh.com/ssh/command


`ssh  [user@]hostname [command]` : OpenSSH SSH client (remote login program)
 + `-F configfile` : Specifies an alternative per-user configuration file (default `~/.ssh/config`)
 + `-A` : Enables forwarding of the authentication agent connection.
 + `-a` : Disables forwarding of the authentication agent connection.
 + `-i identity_file` : file from which the identity (private key) for public key authentication
 + `-p port` : Port to connect to on the remote host
 + `-X` : Enables X11 forwarding.
 + `-x` : Disables X11 forwarding.
 + `-t` : allocate a pseudo tty (default for an interactive ssh connexion, e.g. running remote sudo requiring password prompt)
 + `-T` : no pseudo tty (default when running a remote command)
 + `-l login_name` : Specifies the user to log in as on the remote machine.
 + `-o option=value` : Can be used to give options in the format used in the configuration file. cf available option with `man ssh_config`
    + `IdentitiesOnly` defaults to no, but when set to yes, tells SSH to use only the identity specified on the command line or in the configuration file. The client will not try other identities, even if offered by ssh-agent or a PK11 provider.


```bash
ssh user@hostname
# <=>
ssh -l user hostname
```

Tip Force use of password

    ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no example.com


#### ssh connection using ipv6 link local

https://www.itdojo.com/ssh-using-link-local-ipv6-addresses/

```bash
ssh colin@fe80::216:eaff:fe5f:4cad%eth1

```

#### Testing connection

```
# -T : no pseudo tty
ssh -T git@github.com
```



```bash
ssh -T git@github.com
```

    Hi monteilhet! You've successfully authenticated, but GitHub does not provide shell access.




NB When a pseudo tty is required use `-t` option

    ssh -t user@sample sudo ls /etc/sudoers.d # user is not sudoer without password

    # display pseudo terminal allocated
    ssh -t user@sample 'echo $SSH_TTY'

#### Active ssh connections

How to Find Active SSH Connections on Linux<br>
https://www.howtoforge.com/how-to-find-active-ssh-connections-on-linux/

### Running ssh command

https://www.2daygeek.com/execute-run-linux-commands-remote-system-over-ssh/

```
ssh [User_Name]@[Rremote_Host_Name or IP] [Command or Script]

# simple command
ssh user1@server1 uptime

# running a local script remotely
ssh $HOST < script.sh

# using heredoc
ssh -T user@host.com << EOF
echo "The current local working directory is: $PWD"
echo "The current remote working directory is: \$PWD"
EOF

# using heredoc and defining variable locally and used remotely
ssh user@host.com A=1 B=2 bash <<'EOF'
echo $A
echo $B
echo $PWD
EOF


# using heredoc and defining args locally and used remotely
ssh user@host.com bash -s -- home <<'EOF'
echo $1
echo $PWD
EOF


ssh myserver ps > /tmp/ps.out # run command remotly and redirect on local system
ssh myserver ps \> /tmp/ps.out  # run command and redirect remotly


# using root and redirection remotely
PTFM=bas-dssp
NAME=integration-dssp
ssh $PTFM <<IN "sudo bash -c 'cat >> /etc/hosts'"
10.0.0.10    kast.$NAME.thalesgroup.com vault.kast.$NAME.thalesgroup.com
IN

```

A few ways to execute commands remotely using SSH:<br>
https://zaiste.net/posts/few-ways-to-execute-commands-remotely-ssh/


+ `-n` : Redirects stdin from /dev/null (actually, prevents reading from stdin). This must be used when ssh is run in the background.

```bash

update_dns() {
local server=${1}
local host=${2}
local ip=${3}
log update dns $server for $ip $h
set +e
ssh -n ${SSHOPT} ansible@${server} "sudo sed -i '/$ip/d'  /etc/hosts"
set -e
ssh -n ${SSHOPT} ansible@${server} "sudo sed -i '/10.10.0.254/a$ip   $h.internal $h'  /etc/hosts"
}

while read ip h ; do
    update_dns ${SERVICES_SERVER} $h $ip
done < $hosts_file


```




### Client Configuration File

#### system configuration

   man ssh_config

     ssh obtains configuration data from the following sources in the following order:

      1.   command-line options
      2.   user's configuration file (~/.ssh/config)
      3.   system-wide configuration file (/etc/ssh/ssh_config)


NB global client configuration file : `/etc/ssh/ssh_config`

```
Host *
#   ForwardAgent no
#   IdentityFile ~/.ssh/id_rsa
#   IdentityFile ~/.ssh/id_dsa
#   StrictHostKeyChecking ask
#   CheckHostIP yes
#   Port 22
#   Protocol 2
 HashKnownHosts yes


```
Useful option :
 * `HashKnownHosts yes` : hashes  the hostname stored in the known_host file.
 * `CheckHostIP` : If set to yes (the default), ssh will additionally check the host IP address in the known_hosts file (to detect if a host key changed due to DNS spoofing)

How to disable SSH timeout on the client side (https://www.simplified.guide/ssh/disable-timeout), use option

    # system config in /etc/ssh/ssh_config or at user level in ~/.ssh/config
    ServerAliveInterval 30



#### User-specific SSH configuration file

cf Configuration Files @ https://web.archive.org/web/20181105163235/http://pentestmonkey.net/cheat-sheet/ssh-cheat-sheet


SSH Config File<br>
https://www.ssh.com/ssh/config/


**tips**

```bash
# bypass ~/.ssh/config configuration
ssh -F /dev/null vagrant@remote
```


It is also possible to configure options for the SSH client in ~/.ssh/config.

```
Host <template>
   <option 1>
   <option 2>
```

> **NB** <br> Note that the configuration file should have a line like `Host *` followed by one or more parameter-value pairs. <br> `Host *` means that it will match any host. Essentially, the parameters following `Host *` are the general defaults. <br>Because the ***first matched value*** for each SSH parameter is used, you want to add the host-specific or subnet-specific parameters to the beginning of the file.

Matching between file and cli options

| config                           | ssh option                           |
|:---------------------------------|:-------------------------------------|
|   `HostName <ip or domain_name>` |                                      |
|   `User <name>`                  | `-l <user>`                          |
|   `IdentityFile <file>`          | `-i <file>`                          |  
|   `IdentitiesOnly yes`           | `-o IdentitiesOnly=yes  `            |
|   `Port <port>`                  | `-p <port> or -P <port> using scp`   |
|   `ForwardX11 yes`               | `-X`                                 |
|   `ProxyCommand <cmd>`           | `-o ProxyCommand <cmd>`              |
|   `Compression yes`              |  `-C`                                |
|   `StrictHostKeyChecking no`     | `-o StrictHostKeyChecking=no`        |
|   `UserKnownHostsFile=/dev/null` | `-o UserKnownHostsFile=/dev/null`    |
|   `CheckHostIP no`               | `-o CheckHostIP=no`                  |
| `LocalForward <port local> <hôte distant>:<port distant>`| `-L <port local>:<hôte distant<\:<port distant>` |
| `RemoteForward <port distant> <hôte local>:<port local>` | `-R <port distant>:<hôte local>:<port local>`    |
| `SendEnv VER`                    | `-o SendEnv=VER`                     |




```
Host recette-*.th2.prod qt-*.th2.prod pp-*.th2.prod
 User monitor
 IdentityFile ~/.ssh/id_rsa.monitor

# generic settings
Host monclient-*
 IdentityFile ~/.ssh/id_rsa_clients

# more specific settings
Host monclient-firewall
 HostName 12.34.56.78
 User guest
 Port 2222

Host monclient-mail monclient-intranet
 User root

Host monclient-mail
 ProxyCommand ssh monclient-firewall nc 192.168.0.11 22

Host 10.0.0.1
 Port 2222
 User ptm
 ForwardX11 yes

```

### Disabling ssh host key checking

 * https://www.symantec.com/connect/articles/ssh-host-key-protection
 * https://www.shellhacks.com/disable-ssh-host-key-checking/
 * https://medium.com/opsops/comprehensive-dont-bother-me-for-ssh-87d416ec3395 (ansible config)
 * How to Automatically Accept SSH Key Fingerprint? (using ssh-keyscan)


Each time the SSH client connects with a server, it will store a related signature (a key) of the server. This information is stored in a file names named known_hosts. The known_hosts file itself is available in the .ssh subdirectory of the related user (on the client). In the case the signature of the server changes, SSH will protect the user by notifying about this chance.

`~/.ssh/known_hosts` file contains a convenient list of all servers to which you connect.

To reduce the risk of storing a clear picture of the network, the solution introduced was hashing the hostname. To enable this functionality, the `HashKnownHosts` option can be set to yes.

When you login to a remote host for the first time, the remote host's host key is most likely unknown to the SSH client. The default behavior is to ask the user to confirm the fingerprint of the host key.
```
$ ssh peter@192.168.0.100
The authenticity of host '192.168.0.100 (192.168.0.100)' can't be established.
RSA key fingerprint is 3f:1b:f4:bd:c5:aa:c1:1f:bf:4e:2e:cf:53:fa:d8:59.
Are you sure you want to continue connecting (yes/no)?
```
If your answer is yes, the SSH client continues login, and stores the host key locally in the file `~/.ssh/known_hosts`. You only need to validate the host key the first time around: in subsequent logins, you will not be prompted to confirm it again.


How to get rid of : REMOTE HOST IDENTIFICATION HAS CHANGED Warning,<br>
If a host is reinstalled and has a different key in ‘known_hosts

*NB* For instance with ansible use, host_key_checking = False  

To disable (or control disabling) authentification warning, add the following lines to the beginning ssh config :

    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no user@hostname

    alias ssh0='ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=ERROR'


In ssh config file

```
Host 192.168.0.*
   StrictHostKeyChecking no
   UserKnownHostsFile /dev/null
   LogLevel QUIET
```

NB if not using quiet log level, in this case at each connection print a Warning as knownhost does not exist : Warning: Permanently added '10.1.1.100' (ECDSA) to the list of known hosts.


#### Clean known-hosts when remote host identification has changed


For virtual machine destroyed and recreated fingerprint might change and the following message will be printed:

<pre>
The fingerprint for the ECDSA key sent by the remote host is
SHA256:HZxebz5j0Hc5F5bghlUv/nwy0sLBouK7O4QgwKRb3M4.
Please contact your system administrator.
Add correct host key in /home/user/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /home/user/.ssh/known_hosts:15
  remove with:
  ssh-keygen -f "/home/user/.ssh/known_hosts" -R "[localhost]:2222"
ECDSA host key for [localhost]:2222 has changed and you have requested strict checking.
Host key verification failed.
</pre>

Apply suggested command :

```bash
ssh-keygen -f "/home/user/.ssh/known_hosts" -R "[localhost]:2222"
```

or you case can also use to remove the 15th line

```bash
sed -i 15d ~/.ssh/known_hosts
```

Linux sysadmin basics: Troubleshooting known_hosts failures
https://www.redhat.com/sysadmin/linux-knownhosts-failures

### Connexion behind a proxy


Requirement :  `apt-get install connect-proxy`

```
Host bitbucket.org
 User bba
 IdentityFile ~/.ssh/id_rsa
 ProxyCommand connect -H proxy-http:8080 %h %p
```

### ssh jump host

Back to Basics : Le Bastion SSH
https://blog.octo.com/le-bastion-ssh/

Protect your infrastructure with SSH Jump Hosts
https://etherarp.net/ssh-jump-hosts/index.html

####  Using ProxyJump

SSH to remote hosts though a proxy or bastion with ProxyJump
https://www.redhat.com/sysadmin/ssh-proxy-bastion-proxyjump


The ProxyJump, or the -J flag, was introduced in ssh version 7.3.
`ssh -J <bastion1>,<bastion2> <remote>`

```bash
ssh -J 172.17.0.2 devops@172.17.0.3
```

or in ssh/config

```
Host srv1
  Hostname 172.17.0.3
  User devops
  ProxyJump bastion

```

#### Using netcat

Historically, we are using `nc` on the jump host, to forward the connection to the target host.

```
# gateway behind proxy
Host gtw
 Hostname 10.188.178.178
 User adm
 IdentityFile ~/.ssh/id_rsa
 ProxyCommand connect -S proxy:1080 %h %p

Host lab
  Hostname 192.168.50.100
  User gos
  # IdentityFile ~/.ssh/id_rsa  (not needed if use the same key)
  ProxyCommand ssh gtw "nc %h 22"
```

#### Using ssh -W option (netcat mode)

NB ssh jump host without nc (netcat), using `ssh gtw -W %h:22`
https://chrigl.de/posts/2014/03/03/ssh-jump-host-without-nc-netcat.html


`-W host:port` :  Requests that standard input and output on the client be forwarded to host on port over the secure channel.  Implies -N, -T, ExitOnForwardFailure and ClearAllForwardings and works with Protocol version 2 only.


```
Host bastion
  Hostname 80.80.10.01
  User int
  IdentityFile ~/.ssh/id_rsa

Host server-10.10.*.*
  User sv
  ProxyCommand ssh -A int@bastion -W $(echo %h|cut -d- -f2):%p
```

Using command line

```bash

ssh -o UserKnownHostsFile=/dev/null \
    -o StrictHostKeyChecking=no \
    -o ProxyCommand="ssh -o UserKnownHostsFile=/dev/null \
                         -o StrictHostKeyChecking=no \
                         -i ${SSH_KEY_PATH} \
                         -W %h:%p ${DEF_USER}@${SSH_PROXY}" \
    -i ${SSH_KEY_PATH}${USER}@${SSHIP}

```




### Port Forwarding

SSH tunnel : https://www.ssh.com/ssh/tunneling/

SSH as Virtual Private Network
https://system.cs.kuleuven.be/cs/system/security/ssh/tunneling/vpn.shtml

How to Set up SSH Tunneling (Port Forwarding)
https://linuxize.com/post/how-to-setup-ssh-tunneling/

SSH tunneling or SSH port forwarding is a method of creating an encrypted SSH connection between a client and a server machine through which services ports can be relayed.


here are three types of SSH port forwarding:

 * Local Port Forwarding. - Forwards a connection from the client host to the SSH server host and then to the destination host port.
 * Remote Port Forwarding. - Forwards a port from the server host to the client host and then to the destination host port.
 * Dynamic Port Forwarding. - Creates SOCKS proxy server which allows communication across a range of ports

ssh client options useful for Port Forwarding
 * `-N` : Do not execute a remote command.  This is useful for just forwarding ports.
 * `-f` : Requests ssh to go to background just before command execution.

#### Local Forwarding

cf Local Forwarding @ http://pentestmonkey.net/cheat-sheet/ssh-cheat-sheet


Make services on the remote network accessible to your host via a local listener.

The service running on the remote host on TCP port 1521 is accessible by connecting to 10521 on the SSH client system

    ssh -L 127.0.0.1:10521:127.0.0.1:1521 user@10.0.0.1

or in ssh config
```
Host test
 Hostname 10.0.0.1
 LocalForward 127.0.0.1:10521 127.0.0.1:1521
```

NB can use any address of 127.0.0.0/8 subnet as local address, implicitely 127.0.0.1


    ssh -L 127.0.0.2:8080:localhost:80 root@gateway

```
LocalForward 127.0.0.2:8080 localhost:80
# http://127.0.0.2:8080 => will use port 80 on localhost
```

Using 0.0.0.0, hosts on the same network as the SSH client can also connect to the remote service

    ssh -L 0.0.0.0:10521:127.0.0.1:1521 10.0.0.1


In this example, 10.0.0.99 is a host that’s accessible from the SSH server.  We can access the service it’s running on TCP port 1521 by connecting to 10521 on the SSH client.

    ssh -L 127.0.0.1:10521:10.0.0.99:1521 10.0.0.1

Or for instance access service on port 80 on intranet connecting through firewall host, using local port 8080
    ssh -L 8080:intranet:80 root@firewall  #  http://localhost:8080


#### Remote Forwarding

Make services on your local system / local network accessible to the remote host via a remote listener.  This sounds like an odd thing to want to do, but perhaps you want to expose a services that lets you download your tools.

The SSH server will be able to access TCP port 80 on the SSH client by connecting to 127.0.0.1:8000 on the SSH server.

    ssh -R 127.0.0.1:8000:127.0.0.1:80 10.0.0.1

#### Dynamic Port Forwarding

Proxy socks https://linuxize.com/post/how-to-setup-ssh-socks-tunnel-for-private-browsing/
<br>How to set up SSH dynamic port forwarding on Linux https://www.redhat.com/sysadmin/ssh-dynamic-port-forwarding

Dynamic port forwarding allows you to create a socket on the local (ssh client) machine which acts as a SOCKS proxy server. When a client connects to this port the connection is forwarded to the remote (ssh server) machine, which is then forwarded to a dynamic port on the destination machine.

This way, all the applications using the SOCKS proxy will connect to the SSH server and the server will forward all the traffic to its actual destination.

In Linux, macOS and other Unix systems to create a dynamic port forwarding (SOCKS)

```bash
ssh -D [LOCAL_IP:]LOCAL_PORT [USER@]SSH_SERVER
```

Create a SOCKS proxy on a Linux server with SSH to bypass content filters
https://ma.ttias.be/socks-proxy-linux-ssh-bypass-content-filters/

```bash
ssh -D 1337 -q -C -N -f user@ma.ttias.be

```

### X11 Forwarding


If your SSH client is also an X-Server then you can launch X-clients (e.g. Firefox) inside your SSH session and display them on your X-Server.

    SSH -X 10.0.0.1

~/.ssh/config:

```
ForwardX11 yes   
```

## ssh bastion

An SSH bastion host is a regular Linux host, accessible from the Internet. What makes it a bastion is the fact that it’s the only server which accepts SSH connections from the outside. If a user wants to access another machine, they need to connect to the bastion first, and then make another SSH connection from the bastion to the final destination. Sometimes this process is called “jumping” and SSH bastions are also called “jump hosts”.

Another definition:
A so-called bastion is a machine used as a single entry point by operational teams (such as sysadmins, developers, devops, database admins, etc.) to securely connect to other machines of an infrastructure, usually using ssh.
The bastion provides mechanisms for authentication, authorization, traceability and auditability for the whole infrastructure.


Setting Up an SSH Bastion Host : https://goteleport.com/blog/ssh-bastion-host/

Back to Basics : Le Bastion SSH  https://blog.octo.com/le-bastion-ssh/


***tips***

Create account without bash, end user can use ssh bounce to access hosts through the bastion

```
scaf:x:1010:1010::/home/scaf:/bin/false
```



```bash
# testing x-display
sudo apt-get install x11-apps
xeyes &
xclock &

```

## Encryption with rsa key-pairs

Using openssl


 * https://bjornjohansen.no/encrypt-file-using-ssh-key
 * http://krisjordan.com/essays/encrypting-with-rsa-key-pairs



## Tips

xavki
https://gitlab.com/xavki/presentation-ssh-fr/-/blob/master/11-astuces-fichiers/slides.md


```
# rate testing

yes | pv | ssh 172.17.0.2 "cat > /dev/null"

```

```
# diff between remote and local files

ssh 172.17.0.2 cat /tmp/xavki | diff /tmp/xavki/test -

```

How to Fix “SSH Too Many Authentication Failures” Error
https://www.tecmint.com/fix-ssh-too-many-authentication-failures-error/

To fix this error, you need to add the IdentitiesOnly with a value of yes, which instructs ssh to only use the authentication identity files specified on the command line or the configured in the ssh_config file(s), even if ssh-agent offers additional identities.

    ssh -o IdentitiesOnly=yes vps2


# Tools

4 Ways to Transfer Files and Directories on Linux
https://devconnected.com/4-ways-to-transfer-files-and-directories-on-linux/

## scp


scp copies files between hosts on a network.  It uses ssh for data transfer, and uses the same authentication and provides the same security as ssh.

`scp` — secure copy (remote file copy program
 + -P port : Specifies the port to connect to on the remote host.
 + -r : Recursively copy entire directories.
 + -C : Enable compression,
 + -v : Verbose mode.
 + -p : Preserves modification times, access times,


    scp [[user@]host1:]file1 ... [[user@]host2:]file

To upload files:

    scp /home/stacy/images/image*.jpg stacy@myhost.com:/home/stacy/archive

To download files:

    scp stacy@myhost.com:/home/stacy/archive/image*.jpg /home/stacy/downloads

To transfer files between to remote hosts

    scp someuser@alpha.com:/somedir/somefile.txt someuser@beta.com:/anotherdir


## sshfs

It is possible to use sshfs to mount a remote file system locally.

=> install sshfs package

`sshfs` -  filesystem client based on ssh

    sshfs [user@]host:[dir] mountpoint [options]

For instance

    sshfs 4556@sshfs.zaclys.com:/zclef /media/zclef

To unmount remote filesystem

    umount /media/zclef


Permanent configuration in /etc/fstab

    sshfs#id@gist:/home/user / fuse port=22,user,noauto,noatime 0 0

Sharing Data with SSHFS : http://www.admin-magazine.com/HPC/Articles/Sharing-Data-with-SSHFS
cf config for perf optim

SSHFS for Shared Storage : https://www.admin-magazine.com/HPC/Articles/SSHFS-for-Shared-Storage

## sftp


`sftp` — secure file transfer program

sftp is an interactive file transfer program, similar to ftp, which performs all operations over an encrypted ssh(1) transport. It may also use many features of ssh, such as public key authentication and compression.  sftp connects and logs into the specified host, then enters an interactive command mode.


    sftp -P 2222 sftp_user@89.185.10.1

How To Setup Chrooted SFTP In Linux
https://www.ostechnix.com/setup-chrooted-sftp-linux/


## sshpass

SSH password automation in Linux with sshpass
https://www.redhat.com/sysadmin/ssh-automation-sshpass

`sshpass` noninteractive ssh password provider
 + `-p`password : The password is given on the command line.
 + `-f`filename : The password is the first line of the file filename.
 + `-e` : The password is taken from the environment variable "SSHPASS".


sshpass is a utility designed for running ssh using the mode referred to as "keyboard-interactive" password authentication, but in non-interactive mode.


    sshpass -f <(echo $pass) ssh -4 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \
    -o GSSAPIAuthentication=no ${USER}@${SSHIP}

## cluster ssh

https://www.putorius.net/cluster-ssh.html

cssh - Cluster administration tool

```bash
apt-get install clusterssh
```

The hosts to run on are defined in `/etc/clusters` or `~/.clusterssh/clusters` like this:

```
clusters = cluster1 cluster2 all

cluster1 = test1 test2
cluster2 = test3 test4
all = test1 test2 test3 test4
```

    cssh -l <username> <clustername>


```bash
cssh root@serveur1 root@serveur2
```




## kanif

http://taktuk.gforge.inria.fr/kanif/

kanif is a tool for cluster management and administration

kanif provides three simple commands for clusters administration and management:

    kash: runs the same command on multiple nodes
    kaput: broadcasts the copy of files or directories to several nodes
    kaget: gathers several remote files or directories


    kash -n localhost,supernode uptime


## pdsh

Parallel command execution with pdsh
https://www.rittmanmead.com/blog/2014/12/linux-cluster-sysadmin-parallel-command-execution-with-pdsh/

    pdsh -w root@rnmcluster02-node0[1-4] date

pdsh Parallel Shell
http://www.admin-magazine.com/HPC/Articles/pdsh-Parallel-Shell

## rsync

 + http://www.informatix.fr/tutoriels/php/rsync-comment-synchroniser-des-fichiers-a-travers-une-connexion-ssh-164
 + https://www.tecmint.com/rsync-local-remote-file-synchronization-commands/
 + https://linuxconfig.org/examples-on-how-to-use-rsync-for-local-and-remote-data-backups-and-synchonization
 + https://www.cloudbooklet.com/how-to-transfer-files-with-rsync-over-ssh-google-cloud/
 * https://www.redhat.com/sysadmin/5-rsync-tips

`rsync` -- a fast, versatile, remote (and local) file-copying tool
  + `-a, --archive` : archive mode (<=> -rlptgo ) allows copying files recursively and it also preserves symbolic links, file permissions, user & group ownerships and timestamps
  + `-v, --verbose` : increase verbosity
  + `-h` : human-readable, output numbers in a human-readable format
  + `-r, --recursive` : recurse into directories
  + `-l, --links` : copy symlinks as symlinks,  When symlinks are encountered, recreate the symlink on the destination.
  + `-p, --perms` : preserve permissions
  + `-x, --one-file-system` : don't cross filesystem boundaries (=>ignore mount points)
  + `-t, --times` : preserve modification times
  + `-g, --group` : preserve group
  + `-o, --owner` : preserve owner (super-user only)
  + `-z, --compress` : compress file data during the transfer
  + `-n, --dry-run` : makes rsync perform a trial run that doesn’t make any changes
  + `-u, --update` : skip files that are newer on the receiver
  + `-e` : specify the remote shell to use
  + `--exclude=PATTERN` : exclude files matching PATTERN
  + `--exclude-from=FILE` : read exclude patterns from FILE
  + `--max-size=SIZE` : tells rsync to avoid transferring any file that is larger than the specified SIZE
  + `--bwlimit=RATE` : allows you to specify the maximum transfer rate for the data sent over the socket
  + `--delete` : delete extraneous files from dest dirs
  + `--delete-after` : receiver deletes after transfer, not during
  + `--inplace` : update destination files in-place
  + `--partial` : keep partially transferred files
  + `--append` : append data onto shorter files
  + `--progress` : show progress during transfer
  + `--stats` : print a verbose set of statistics on  the file transfer,
  + `--rsync-path=PROGRAM` : specify the rsync to run on remote machine

    rsync [options] source-folder copy-folder


***Use cases***:

 * Copy/Sync Files and Directory Locally

 * Copy/Sync Files and Directory to or From a Server (NB over ssh)

        # local to remote
        rsync -avz rpmpkgs/ root@192.168.0.101:/home/
        # remote to local
        rsync -avzh root@192.168.0.100:/home/tarunika/rpmpkgs /tmp/myrpms


***aliases***


```bash
alias "resync='rsync -avz --stats'"
alias "fresync='rsync -n -avz --delete --stats'"
```

**NB** by default rsync remote copy use *ssh* when specifying a target as `user@host:/path/to/files`, but it can be useful ot use `-e ssh` option to specify a specific port or private key to use, e.g.  `-e 'ssh -p 2222'` or `-e 'ssh -i /path/to/private_key'`  <br> cf https://serverfault.com/questions/378939/do-you-need-e-ssh-for-rsync

**NB** Run rsync with root permission on remote machine
```bash
# allow to use priviledged command on remote
rsync --rsync-path="sudo rsync" /tmp/binaries username@remote:/opt

# => edit /etc/sudoers with username ALL= NOPASSWD:/usr/bin/rsync

```


Copying large files with Rsync, and some misconceptions<br>
https://fedoramagazine.org/copying-large-files-with-rsync-and-some-misconceptions/

```
# Problem 1: Thin provisioning =>  -S or –sparse and it tells rsync to handle sparse files efficiently
rsync -avS vmdk_file syncuser@host1:/destination

# Problem 2: Updating files
# the default behaviour of rsync is to create a new copy of the file in the destination and to move it into the right place when the transfer is completed.
# To change this default behaviour of rsync, use these options to send only the deltas:
rsync -av --partial --inplace --append --progress vmdk_file syncuser@host1:/destination
```

SCP user’s migration guide to rsync<br>
https://fedoramagazine.org/scp-users-migration-guide-to-rsync/

#####


```bash
set -v

mkdir /tmp/D

# Copy/Sync a Directory on Local Computer

# Copy/Sync a File on a Local Computer
rsync -vh /etc/passwd /tmp/D
ls -l /tmp/D

# copy directory and content
rsync -avzh /usr/share/doc/rsync /tmp/D
ls -R /tmp/D


# Copy/Sync Files and Directory to or From a Server
# by default remote rsync use (ssh)
readlink -f $(which rsh)  # check rsh symlink


rm -rf /tmp/D

set +v
```


    mkdir /tmp/D

    # Copy/Sync a Directory on Local Computer

    # Copy/Sync a File on a Local Computer
    rsync -vh /etc/passwd /tmp/D
    passwd

    sent 2.41K bytes  received 35 bytes  4.89K bytes/sec
    total size is 2.33K  speedup is 0.95
    ls -l /tmp/D
    total 4
    -rw-r--r-- 1 user user 2327 juil. 19 08:33 passwd

    # copy directory and content
    rsync -avzh /usr/share/doc/rsync /tmp/D
    sending incremental file list
    rsync/
    rsync/README.Debian.gz
    rsync/README.gz
    rsync/TODO.gz
    rsync/changelog.Debian.gz
    rsync/copyright
    rsync/tech_report.tex.gz
    rsync/examples/
    rsync/examples/logrotate.conf.rsync
    rsync/examples/rsyncd.conf
    rsync/scripts/
    rsync/scripts/atomic-rsync.gz
    rsync/scripts/cull_options.gz
    rsync/scripts/cvs2includes.gz
    rsync/scripts/file-attr-restore.gz
    rsync/scripts/files-to-excludes.gz
    rsync/scripts/git-set-file-times.gz
    rsync/scripts/logfilter.gz
    rsync/scripts/lsh.gz
    rsync/scripts/mnt-excl.gz
    rsync/scripts/munge-symlinks.gz
    rsync/scripts/rrsync.gz
    rsync/scripts/rsyncstats.gz

    sent 34.40K bytes  received 416 bytes  69.63K bytes/sec
    total size is 33.93K  speedup is 0.97
    ls -R /tmp/D
    /tmp/D:
    passwd  [0m[01;34mrsync[0m

    /tmp/D/rsync:
    [01;31mchangelog.Debian.gz[0m  [01;34mexamples[0m          [01;31mREADME.gz[0m  [01;31mtech_report.tex.gz[0m
    copyright            [01;31mREADME.Debian.gz[0m  [01;34mscripts[0m    [01;31mTODO.gz[0m

    /tmp/D/rsync/examples:
    logrotate.conf.rsync  rsyncd.conf

    /tmp/D/rsync/scripts:
    [01;31matomic-rsync.gz[0m  [01;31mfile-attr-restore.gz[0m   [01;31mlogfilter.gz[0m  [01;31mmunge-symlinks.gz[0m
    [01;31mcull_options.gz[0m  [01;31mfiles-to-excludes.gz[0m   [01;31mlsh.gz[0m        [01;31mrrsync.gz[0m
    [01;31mcvs2includes.gz[0m  [01;31mgit-set-file-times.gz[0m  [01;31mmnt-excl.gz[0m   [01;31mrsyncstats.gz[0m


    # Copy/Sync Files and Directory to or From a Server
    # by default remote rsync use
    readlink -f $(which rsh)
    /usr/bin/ssh


    rm -rf /tmp/D

    set +v
