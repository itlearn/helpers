
<!--
Table of Contents
=================
  
  * [Install from sources](#Install-from-sources)
  * [Package manager](#Package-manager)
  * [Debian/Ubuntu](#Debian/Ubuntu)
    * [Repositories](#Repositories)
    * [GPG Trusted keys](#GPG-Trusted-keys)
    * [Mirroring](#Mirroring)
    * [APT sources](#APT-repository-sources)
    * [Cache Repository](#Cache-repository)
    * [List installed packages](#List-installed-packages)
    * [Details on installed package](#Details-on-installed-package)
    * [Manage system packages](#Manage-system-packages)
    * [Advanced](#Advanced)
  * [Redhat/Centos](#Redhat/Centos)
    * [Repositories](#RPM-Repositories)
    * [Mirroring](#RPM-Mirroring)
    * [RPM sources](#RPM-sources)
      * [dnf](#dnf)
    * [Cache Repository](#RPM-Cache-repository)
    * [List installed packages](#RPM-List-installed-packages)
    * [Details on installed package](#RPM-Details-on-installed-packages)
    * [Manage system packages](#RPM-Manage-system-packages)
    * [Advanced](#RPM-Advanced)
-->

[[_TOC_]]

# Install from sources


Building Programs from Source on any Linux Distribution in a simple way<br>
https://trendoceans.com/building-programs-from-source-on-any-linux-distribution-in-a-simple-way/

Compiling Linux Software from Source Code<br>
https://www.control-escape.com/linux/lx-swinstall-tar.html

Basics Of Compiling Software From Source Code In Linux<br>
http://www.linuxandubuntu.com/home/basics-of-compiling-software-from-source-code-in-linux

----

# Package manager

5 reasons to use Linux package managers https://opensource.com/article/21/2/linux-package-management

Package managers track all components of the software you install, making updates, reinstalls, and troubleshooting much easier.

A package manager is a tool that facilitates the management of software packages on your system by letting you download and install them, update them, uninstall them, and more. There are many different packaging systems in the Linux world, which are often associated with particular distributions.

 * RPM for Red Hat-based distributions
 * DEB for Debian-like distributions

For systems using RPM, yum is by far the most popular package manager.

As for APT, the apt-get tool comes with most distributions.

Although their syntax differs slightly, both programs basically have the same features—given a package name, they will download software online and install it automatically.


How To List Installed Packages In Linux (Arch linux, alpine, debian, rhel):<br>
https://ostechnix.com/how-to-list-installed-packages-in-linux/

How To List Installed Packages Sorted By Installation Date In Linux<br>
https://ostechnix.com/list-installed-packages-sorted-installation-date-linux/

How to Check Available Package Updates in Linux (yum, dnf, apt, zypper etc...)<br>
https://www.2daygeek.com/check-available-package-updates-in-linux/

Remove Packages Installed On Certain Date/Time In Linux (09/2021)<br>
https://ostechnix.com/remove-packages-installed-on-certain-date-time-in-linux/


## What is a software repository?

The default method of installing applications on Linux is from a **distribution software repository**. You get an application from a software repository through a package manager, which enables your Linux system to record and track every component of what you've installed.

# Debian/Ubuntu

## Repositories

How to add/remove PPA repositories in Debian : https://vitux.com/how-to-add-ppa-repositories-in-debian/

PPA : Personal Package Archives are repositories provided by Launchpad.net : `ppa:<user>/<ppa-name`

Personal Package Archives (PPA) are repositories that are used to install or upgrade packages that are missing in the Ubuntu official repositories. PPAs are usually hosted on the launchpad.

`add-apt-repository [OPTIONS] REPOSITORY` : Adds a repository into the `/etc/apt/sources.list` or `/etc/apt/sources.list.d` or removes an existing one. REPOSITORY can be either
 * a line that can be added directly to sources.list
 * in the form `ppa:<user>/<ppa-name>` for adding Personal Package Archives, or
 * a distribution component  to enable.

In the first form, REPOSITORY will just be appended to `/etc/apt/sources.list`.

In the second form, `ppa:<user>/<ppa-name>` will be expanded to the full deb line of the PPA and added into a new file in the `/etc/apt/sources.list.d/` directory. The GPG public key of the newly added PPA will also be downloaded and added to apt's keyring.

In the third form, the given distribution component will be enabled for all sources.

 + `-r, --remove` : Remove the specified repository

```bash

# ppa samples
sudo add-apt-repository ppa:alexlarsson/flatpak
sudo apt update
sudo apt install flatpak

sudo add-apt-repository ppa:nitrokey-team/ppal
# create /etc/apt/sources.list.d/nitrokey-team-ubuntu-ppa-xenial.list
# add key /etc/apt/trusted.gpg.d/nitrokey-team_ubuntu_ppa.gpg
sudo apt update
sudo apt install nitrokey-app

# repository sample
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"


```

How to Remove PPAs on Ubuntu-Based Distributions
https://www.maketecheasier.com/remove-ppas-ubuntu-based-distributions/

```bash
    sudo apt-get autoremove --purge PACKAGENAME
    sudo add-apt-repository --remove ppa:someppa/ppa
    sudo apt-get autoclean


# or use ppa-purge tool
```

What is PPA Purge? How to Use it in Ubuntu and other Debian-based Distributions?
https://itsfoss.com/ppa-purge/


## GPG Trusted keys

How to List and Remove a GPG Key in Ubuntu<br>
https://www.2daygeek.com/how-to-list-and-remove-repository-gpg-key-in-ubuntu/

4 Easy Ways to Remove/Delete a PPA on Ubuntu
https://www.2daygeek.com/how-to-remove-delete-ppas-on-ubuntu-linux-mint/


### apt-key

The trusted keys are managed with the `apt-key` command found in the apt package. This program maintains a keyring of GnuPG public keys, which are used to verify signatures in the Release.gpg files available on the mirrors. It can be used to add new keys manually (when non-official mirrors are needed). Generally however, only the official Debian keys are needed. These keys are automatically kept up-to-date by the debian-archive-keyring package (which puts the corresponding keyrings in `/etc/apt/trusted.gpg.d`).

`apt-key` - APT key management utility for the APT Package Manager on Debian and Ubuntu.

`apt-key` is used to manage the list of keys used by apt to authenticate packages. Packages which have been authenticated using these keys will be considered trusted.

COMMANDS:
 * `add filename` : Add a new key to the list of trusted keys. <br>The key is read from the filename given with the parameter filename or if the filename is `-` from standard input.
 * `del keyid` : Remove a key from the list of trusted keys.
 * `export keyid` : Out:put the key keyid to standard output.
 * `exportall` : Output all trusted keys to standard output.
 * `list, finger` : List trusted keys with fingerprints.
 * `adv` : Pass advanced options to gpg. With `adv --recv-key` you can e.g. download key from keyservers directly into the trusted set of keys.

`/etc/apt/trusted.gpg` : Keyring of local trusted keys, new keys will be added here.<br>Configuration Item:           `Dir::Etc::Trusted`.

`/etc/apt/trusted.gpg.d/` : File fragments for the trusted keys, additional keyrings can be stored here (by other           packages or the administrator).<br>Configuration Item `Dir::Etc::TrustedParts`.

**Adding trusted keys**
When a third-party package source is added to the `sources.list` file, APT needs to be told to trust the corresponding GPG authentication key (otherwise it will keep complaining that it can't ensure the authenticity of the packages coming from that repository). The first step is of course to get the public key. More often than not, the key will be provided as a small text file, which we will call `key.asc` in the following examples.
To add the key to the trusted keyring, the administrator can run `apt-key add < key.asc`


```bash
# List trusted keys with fingerprints
apt-key list
apt-key fingerprint

# add GPG public key
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

wget https://www.dotdeb.org/dotdeb.gpg
sudo apt-key add dotdeb.gpg

# vlc
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886

apt-key fingerprint 0EBFCD88

```

### Verifying gpg

How to verify the integrity of a Linux distribution iso image
https://linuxconfig.org/how-to-verify-the-integrity-of-a-linux-distribution-iso-image

### tips

How to transfer apt keyring to another Debian based distribution
https://trendoceans.com/how-to-transfer-apt-keyring-to-another-debian-based-distribution/


## Mirroring

Debian worldwide mirror sites : https://www.debian.org/mirror/list

### build a local mirror

debmirror : Debian partial mirror script, with ftp, http or rsync and package pool support
  * -p, --progress : Displays progress bars as files are downloaded.
  * -v, --verbose : Displays progress between file downloads.
  * -h, --host=remotehost : Specify the remote host to mirror from.
  * -r, --root=directory : Specifies the directory on the remote host e.g. debian having dists subdir
  * --method=method : Specify the method to download files. Currently, supported methods are ftp, http, https, and rsync.
  * -d, --dist=foo[,bar,..] :  Specify the distribution
  * -s, --section=foo[,bar,..] : Specify the section to mirror.
  * -a, --arch=foo[,bar,..] :  Specify the architectures to mirror
  * --source : Include source in the mirror (default).
  * --nosource : Do not include source.
  * --i18n :  Additionally download 'Translation-<lang>.*' files
  * --check-gpg, --no-check-gpg : Controls whether gpg signatures from the Release.gpg file should be checked.
  * --getcontents : Additionally download Contents.<arch>.gz files used by apt-file
  * --nocleanup : Do not clean up the local mirror.

Create a local mirror using deb-mirror

```bash
# create mirror user
adduser --system --home /var/mirror --gid www-data --no-create-home mirror
mkdir /var/mirror
chown mirror:www-data /var/mirror
chmod u+rwx,g+rxs-w,o-rwx /var/mirror
su - mirror
mkdir /var/mirror/debian

# import gpg keys to check received packages (optional)
gpg --keyring /usr/share/keyrings/debian-role-keys.gpg --export | gpg --import
gpg --keyring /usr/share/keyrings/debian-archive-keyring.gpg --export | gpg --import
gpg --no-default-keyring -a --keyring /usr/share/keyrings/debian-archive-keyring.gpg --export A70DAF536070D3A1 | gpg --no-default-keyring --keyring ~/.gnupg/trustedkeys.gpg --import -

debmirror -v --arch=i386,amd64 --dist=sarge,etch,lenny,sid,stable,testing,unstable --source --method=rsync --host=mirror.ens-lyon.fr --root=:debian --section=main,contrib,non-free,main/debian-installer --getcontents --cleanup /mnt/debian/
```

```bash
# debmirror script for debian stretch

# Arch=         -a,--arch
arch=amd64
# Section=      -s,--section
section=main,contrib,non-free,main/debian-installer
# dist=      -d,--dist
dist="stretch,stretch-updates,stretch-backports"
# Server=       -h,--host
server=ftp.fr.debian.org
# Dir=          -r,--root
inPath=/debian
# Proto=        --method=
# Protocol to use for transfer (http, ftp, hftp, rsync)
proto=rsync
# Outpath=
# Directory to store the mirror in
outPath=/var/mirror/debian

debmirror       -a $arch             \
                --no-source          \
                --i18n               \
                -s $section          \
                -h $server           \
                -d $dist          \
                -r $inPath           \
                --progress           \
                --method=$proto      \
                --no-check-gpg       \
                --ignore-release-gpg \
                $outPath



```

directories structure of repository :
```
http://ftp.fr.debian.org/debian/dists/
	stretch-backports/
	stretch-updates/
	stretch/
      InRelease
      Release
      Release.gpg
      contrib/
      main/
        Contents-amd64.gz
        binary-amd64/
            Packages.gz
            Release
      non-free/

```

### local package repo

**Easy way to create a Debian package and local package repository** <br>
https://linuxconfig.org/easy-way-to-create-a-debian-package-and-local-package-repository

### Debian

https://packages.debian.org/stable/

list of packages version available for the stable release of debian
 * https://packages.debian.org/stable/allpackages
 * https://packages.debian.org/<release>/allpackages

search package
https://www.debian.org/distrib/packages

### Ubuntu


Use search box to search for a specific package and retrieve package in a specific release
 * http://packages.ubuntu.com

packages for a given relase
 * by section for xenial  : https://packages.ubuntu.com/xenial/
 * all packages for bionic : https://packages.ubuntu.com/bionic/allpackages

list of all packages version available for given release of ubuntu as text format
 * https://packages.ubuntu.com/<release>/allpackages?format=txt.gz
 * https://packages.ubuntu.com/xenial/allpackages?format=txt.gz


## APT repository sources


APT Advanced Package Tool

APT needs to be given a “list of package sources”: the file `/etc/apt/sources.list` will list the different repositories (or “sources”) that publish Debian packages.

https://wiki.debian.org/DebianRepository/Format

Each active line of the `/etc/apt/sources.list` file contains the description of a source, made of 3 parts separated by spaces.

```
 deb [ option1=value1 option2=value2 ] uri suite [component1] [component2] [...]
 deb-src [ option1=value1 option2=value2 ] uri suite [component1] [component2] [...]

```

 1. the source type:
    +  `deb` for binary packages,
    +  `deb-src` for source packages.<br>
    OPTIONALLY specify available architecture e.g.:  [arch=amd64]
 2. the base URL of the source
 3. the last field depends on the structure of the repository<br>
    For instance structured like a Debian mirror with multiple distributions each having multiple components<br>
    distributions: `stable`, `testing`, `unstable` (or by name `trusty`, `xenial`, `sid`)<br>
    sections: `main`, `contrib`, `non-free`

If many package sources are referenced, it can be useful to split them in multiple files. <br>Each part is then stored in `/etc/apt/sources.list.d/filename.list`


/etc/apt/sources.list (debian version)

```ini
# Security updates
deb http://security.debian.org/ jessie/updates main contrib non-free
deb-src http://security.debian.org/ jessie/updates main contrib non-free

## Debian mirror
# Base repository
deb http://ftp.debian.org/debian jessie main contrib non-free
deb-src http://ftp.debian.org/debian jessie main contrib non-free

# Stable updates
deb http://ftp.debian.org/debian jessie-updates main contrib non-free
deb-src http://ftp.debian.org/debian jessie-updates main contrib non-free

# Stable backports
deb http://ftp.debian.org/debian jessie-backports main contrib non-free
deb-src http://ftp.debian.org/debian jessie-backports main contrib non-free
```

NB To download packages from a repository apt would download a InRelease or Release file from the `$ARCHIVE_ROOT/dists/$DISTRIBUTION` directory.

```bash

# install package from update
aptitude update && aptitude -t wheezy-updates install clamav

# install package from backports
apt-get -t wheezy-backports install samba

```

### mutli-architectures

```bash
apt-config dump | grep Architecture  # show APT::Architecture settings
dpkg --printarchitecture #  display the current machine architecture,
dpkg --print-foreign-architectures # will show any architecture for which multiarch support is enabled

```

On Ubuntu amd64 installations, i386 is enabled as an additional architecture by default. apt & co. expect repositories to provide all configured architectures.
If multiarch-support is installed, using option `arch=...`  can be used to limit the architectures


arch=arch1,arch2,...  can be used to specify for which architectures information should be downloaded. If this option is not set all architectures defined by the APT::Architectures option will be downloaded.

```bash
deb [arch=amd64] http://mirror.adm.fr.clara.net/ubuntu/ xenial main restricted
```


### apt pinning


cf 6.2.5. Managing Package Priorities
https://debian-handbook.info/browse/en-US/stable/sect.apt-get.html#sect.apt.priorities

https://wiki.debian-fr.xyz/L%27etiquetage_de_paquets_via_le_fichier_/etc/apt/preferences

The APT pinning feature allows administrators to force APT to choose particular versions of packages which may be available in different versions from different repositories.

`/etc/apt/preferences` : The 'preferences' file is where the actual pinning takes place


For instance to force `atom` package version, define a preference file /etc/apt/preferences.d/atom with
```ini
Package: atom
Pin: version 1.55.0
Pin-Priority: 1001
```

```bash

# With no arguments it will print out the priorities of each source.
$ apt-cache policy

# prints out detailed information about the priority selection of the named package.
$ apt-cache policy paquet

## show policy for the specified packet
$ apt-cache policy mono-complete

```

### repositories info


How To Find The List Of Installed Repositories From Commandline In Linux<br>
https://ostechnix.com/find-list-installed-repositories-commandline-linux/

```
# print out the priorities of each source => list all sources
sudo apt-cache policy

# find the installed repositories in your DEB based system.
sudo grep -Erh ^deb /etc/apt/sources.list*

```


## Cache repository

The `apt-cache` command line tool is used for searching apt software package cache. The apt-cache command can display much of the information stored in APT's internal database. This information is a sort of cache since it is gathered from the different sources listed in the sources.list file.

APT stores a copy of the reference Packages files located on Debian mirrors (in `/var/lib/apt/lists/`) and searches are done within those local files. Similarly, `/var/cache/apt/archives/` contains a cache of already downloaded packages to avoid downloading them again if you need to reinstall them after a removal.

### Update cache

```bash
# update cache
apt-get update

aptitude update

apt update

```

### Packages in cache


```bash

## list all the available packages
apt-cache pkgnames

## list all the available packages beginning by token
apt-cache pkgnames gperf

## find package containing token (NB token can be a regexp)
apt-cache search lxml

## find package containing token only in the package name
apt-cache search lxml --names-only
apt-cache search --names-only '^gcc$'


## madison sub command attempts to mimic the output format of the Debian archive management tool
apt-cache madison vim
       vim | 2:8.2.0716-3ubuntu2 | http://fr.archive.ubuntu.com/ubuntu groovy/main amd64 Packages

## show package detail
apt-cache show python-lxml

# show package policy (apt pining preferences)
# With no arguments it will print out the priorities of each source.
apt-cache policy

# Otherwise it prints out detailed information about the priority selection of the named package.
apt-cache policy nano

## show package dependences
apt-cache depends xen-tools

## show package reverse dependences
apt-cache rdepends build-essential

## displays information about package (available versions and reverse dependencies)
apt-cache showpkg python-lxml

```

*using apt or aptitude*

```bash
aptitude search python-wxgtk

# search - search in package descriptions (relies on apt-cache), output is sorted and includes package version
apt search python-wxgtk

```

### Cleaning cache

APTs cached files are located in:

    /var/cache/apt/archives/

    sudo du -sh /var/cache/apt/archive

To remove downloaded packages from the cache directory, do the following.

```bash

apt-get clean
aptitude clean

```

Use autoclean to remove only the packages from cache which can no longer be downloaded.

```bash

apt-get autoclean
aptitude autoclean
```

### Versions of packages

`apt-show-versions` - Lists available package versions with distribution
 * `-p package, --package=package` : Print available and installed versions for specified package.
 * `-r, --regex` : interpret package from option -p as a regex for installed versions.
 * `-R, --regex-all` : like --regex, but also show matching packages which are not installed
 * `-a, --allversions` : Print all available versions of the selected packages
 * `-u, --upgradeable` : Print only upgradeable packages

```bash
# require installation
apt-get install apt-show-versions
```

```bash

apt-show-versions -a -p netcat

apt-show-versions -a -R "netcat"

# show upgradable package
apt-show-versions -u

apt-show-versions -r "python*" | head

```

or using
```bash

apt list netcat -a

```


`rmadison` -- Remotely query the Debian archive database about packages
(from devscripts package)

```
rdmadison haproxy
```




### Files in packages

`apt-file` : APT package searching utility - apt-file is a command line tool for searching files in packages for the APT package management system
  +  `-a, --architecture architecture` : useful if you search a package for a different architecture from the               one installed on your system.
  +  `-s, --sources-list sources.list` : Sets the sources.list file to a different value from its default /etc/apt/sources.list.


https://www.it-connect.fr/apt-file-pour-explorer-les-packages-debian/

```bash

## update cache for apt-file
apt-file update

## show package files list
apt-file show xen-tools

##  can specify an alternate source list using -s or --sources-list
apt-file show apache2 --sources-list /opt/sources.list.test

## find the package that provides the specified file
apt-file search /usr/bin/htpasswd

## can specify architecture using -a
apt-file search /usr/bin/owncloud -a amd64

```



```bash
set -v

sudo  apt-file search /usr/bin/htpasswd
set +v
```


    apt-file search /usr/bin/htpasswd
    apache2-dbg: /usr/lib/debug/usr/bin/htpasswd
    apache2-utils: /usr/bin/htpasswd
    nanoweb: /usr/bin/htpasswd.nanoweb
    set +v


## List installed packages

### Using dpkg

`dpkg` package manager for Debian

```
## list system packages
dpkg-query -l
dpkg -l | less

## list python packages
dpkg -l 'python*'

## list installed packages
dpkg -l | grep '^.i'

## list python packages
dpkg --get-selections | grep python
```



```bash
set -v
## list system packages

LC_ALL=C dpkg -l | sed -n 1,2p

LC_ALL=C dpkg -l | sed -n '1,4!p' | head

## list installed packages
LC_ALL=C dpkg -l | grep '^.i' | head

## list python packages
## ii Desired=Install Status=Install
LC_ALL=C dpkg -l 'python*' | grep '^.i' | head

## list python packages
dpkg --get-selections | grep '^python' | head

set +v
```

    ## list system packages

    LC_ALL=C dpkg -l | sed -n 1,2p
    Desired=Unknown/Install/Remove/Purge/Hold
    | Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend

    LC_ALL=C dpkg -l | sed -n '1,4!p' | head
    +++-===========================================================-============================================-============-================================================================================
    ii  accountsservice                                             0.6.40-2ubuntu11.3                           amd64        query and manipulate user account information
    ii  acl                                                         2.2.52-3                                     amd64        Access control list utilities
    ii  acpi-support                                                0.142                                        amd64        scripts for handling many ACPI events
    ii  acpid                                                       1:2.0.26-1ubuntu2                            amd64        Advanced Configuration and Power Interface event daemon
    ii  adduser                                                     3.113+nmu3ubuntu4                            all          add and remove users and groups
    ii  adwaita-icon-theme                                          3.18.0-2ubuntu3.1                            all          default icon theme of GNOME (small subset)
    ii  aglfn                                                       1.7-3                                        all          Adobe Glyph List For New Fonts
    ii  alsa-base                                                   1.0.25+dfsg-0ubuntu5                         all          ALSA driver configuration files
    ii  alsa-utils                                                  1.1.0-0ubuntu5                               amd64        Utilities for configuring and using ALSA
    sed: impossible d'écrire 155 items à stdout: Relais brisé (pipe)
    dpkg-query: error: error writing to '<standard output>': Broken pipe

    ## list installed packages
    LC_ALL=C dpkg -l | grep '^.i' | head
    ii  accountsservice                                             0.6.40-2ubuntu11.3                           amd64        query and manipulate user account information
    ii  acl                                                         2.2.52-3                                     amd64        Access control list utilities
    ii  acpi-support                                                0.142                                        amd64        scripts for handling many ACPI events
    ii  acpid                                                       1:2.0.26-1ubuntu2                            amd64        Advanced Configuration and Power Interface event daemon
    ii  adduser                                                     3.113+nmu3ubuntu4                            all          add and remove users and groups
    ii  adwaita-icon-theme                                          3.18.0-2ubuntu3.1                            all          default icon theme of GNOME (small subset)
    ii  aglfn                                                       1.7-3                                        all          Adobe Glyph List For New Fonts
    ii  alsa-base                                                   1.0.25+dfsg-0ubuntu5                         all          ALSA driver configuration files
    ii  alsa-utils                                                  1.1.0-0ubuntu5                               amd64        Utilities for configuring and using ALSA
    ii  anacron                                                     2.3-23                                       amd64        cron-like program that doesn't go by time
    grep: erreur en écriture: Relais brisé (pipe)
    dpkg-query: error: error writing to '<standard output>': Broken pipe

    ## list python packages
    ## ii Desired=Install Status=Install
    LC_ALL=C dpkg -l 'python*' | grep '^.i' | head
    ii  python                        2.7.11-1                     amd64        interactive high-level object-oriented language (default version)
    ii  python-apt                    1.1.0~beta1build1            amd64        Python interface to libapt-pkg
    ii  python-apt-common             1.1.0~beta1build1            all          Python interface to libapt-pkg (locales)
    ii  python-cairo                  1.8.8-2                      amd64        Python bindings for the Cairo vector graphics library
    ii  python-dbus                   1.2.0-3                      amd64        simple interprocess messaging system (Python interface)
    ii  python-defusedxml             0.4.1-2ubuntu0.16.04.1       all          XML bomb protection for Python stdlib modules (for Python 2)
    ii  python-dev                    2.7.11-1                     amd64        header files and a static library for Python (default)
    ii  python-gconf                  2.28.1+dfsg-1.1              amd64        Python bindings for the GConf configuration database system
    ii  python-gi                     3.20.0-0ubuntu1              amd64        Python 2.x bindings for gobject-introspection libraries
    ii  python-gi-cairo               3.20.0-0ubuntu1              amd64        Python Cairo bindings for the GObject library
    grep: erreur en écriture: Relais brisé (pipe)

    ## list python packages
    dpkg --get-selections | grep '^python' | head
    python						install
    python-apt					install
    python-apt-common				install
    python-cairo					install
    python-dbus					install
    python-defusedxml				install
    python-dev					install
    python-gconf					install
    python-gi					install
    python-gi-cairo					install

    set +v



### Using apt

`apt list` : list subcommand list packages based on package names
   + `--installed    | -i `
   + `--upgradable   | -u`
   + `--all-versions | -a`


```bash
set -v

## list all installed packages with apt
apt --installed list | head

## list packages upgradable
apt --upgradable list  | head

set +v
```


    ## list all installed packages with apt
    apt --installed list | head

    WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

    En train de lister…
    accountsservice/bionic,now 0.6.45-1ubuntu1 amd64  [installé]
    acl/bionic,now 2.2.52-3build1 amd64  [installé]
    acpi-support/bionic,now 0.142 amd64  [installé]
    acpid/bionic,now 1:2.0.28-1ubuntu1 amd64  [installé]
    adduser/bionic,bionic,now 3.116ubuntu1 all  [installé]
    adwaita-icon-theme/bionic,bionic,now 3.28.0-1ubuntu1 all  [installé]
    alsa-base/bionic,bionic,now 1.0.25+dfsg-0ubuntu5 all  [installé]
    alsa-utils/bionic,now 1.1.3-1ubuntu1 amd64  [installé]
    amd64-microcode/bionic-updates,now 3.20180524.1~ubuntu0.18.04.2 amd64  [installé, automatique]

    ## list packages upgradable
    apt --upgradable list  | head

    WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

    En train de lister…
    apport/bionic-updates,bionic-updates 2.20.9-0ubuntu7.6 all [pouvant être mis à jour depuis : 2.20.9-0ubuntu7.5]
    apport-gtk/bionic-updates,bionic-updates 2.20.9-0ubuntu7.6 all [pouvant être mis à jour depuis : 2.20.9-0ubuntu7.5]
    apt/bionic-updates 1.6.10 amd64 [pouvant être mis à jour depuis : 1.6.6ubuntu0.1]
    apt-transport-https/bionic-updates,bionic-updates 1.6.10 all [pouvant être mis à jour depuis : 1.6.6ubuntu0.1]
    apt-utils/bionic-updates 1.6.10 amd64 [pouvant être mis à jour depuis : 1.6.6ubuntu0.1]
    base-files/bionic-updates 10.1ubuntu2.4 amd64 [pouvant être mis à jour depuis : 10.1ubuntu2.3]
    bind9-host/bionic-updates,bionic-security 1:9.11.3+dfsg-1ubuntu1.7 amd64 [pouvant être mis à jour depuis : 1:9.11.3+dfsg-1ubuntu1.5]
    binutils/bionic-updates 2.30-21ubuntu1~18.04.1 amd64 [pouvant être mis à jour depuis : 2.30-21ubuntu1~18.04]
    binutils-common/bionic-updates 2.30-21ubuntu1~18.04.1 amd64 [pouvant être mis à jour depuis : 2.30-21ubuntu1~18.04]

    set +v




## Details on installed package

### Using aptitude

```bash

aptitude versions python-wxgtk

## package information
aptitude show python-wxgtk2.8

```

### Using apt


```bash

# show - show package details (relies on apt-cache)
# NB show APT-Sources
apt show python-wxgtk2.8

# search in changelog
apt changelog sudo | grep CVE-2021-3156

```

### Using apt-get

```bash
# changelog tries to download the changelog of a package and displays it
apt-get changelog vsftpd

# check is a diagnostic tool; it updates the package cache and checks for broken dependencies.
apt-get check pkgname

```

### Using dpkg

```bash
## show package metadata of installed packages
dpkg --status xen-tools
dpkg -s xen-tools

##  package status
dpkg -s xen-tools | grep Status

## display list of files installed by package
dpkg --listfiles xen-tools
dpkg -L xen-tools

## find the installed package that provides the specified file
dpkg -S /etc/nanorc
locate /etc/nanorc

```


## Manage system packages

### Using aptitude

```
# forbid update for the specified package
aptitude forbid-version nom_du_paquet=X.Y

# aptitude safe-upgrade upgrades currently installed packages
# and can install new packages to resolve new dependencies, but never removes packages.
aptitude safe-upgrade

# full-upgrade: To do complete upgrade of all packages,
# and also to install packages which safe-upgrade cannot do
aptitude full-upgrade

aptitude update && aptitude upgrade

# upgrade only specified package
aptitude update && aptitude upgrade iceweasel

# install/remove package
sudo aptitude install <pkg_name>

sudo aptitude remove <pkg_name>

sudo aptitude purge <pkg_name>

suod aptitude reinstall <pkg_name>

```


### Using apt

apt Command in Linux
https://linuxize.com/post/how-to-use-apt-command/

What’s the difference between apt and apt-get command? https://www.2daygeek.com/comparison-between-apt-vs-apt-get-difference/


`apt` provides a high-level commandline interface for the package management system (**ubuntu only**)<br>
The  apt command provides a convenient subset of the functionality of various other apt- commands (e.g., apt-get, apt-cache), with colorized display and progress bars.

`apt` is designed for interactive use.

`apt` combines the function of `apt-get`, `apt-cache` and `dpkg -l`

`apt` basic commands

* `list` - list packages based on package names
* `search` - search in package descriptions (relies on apt-cache)
* `show` - show package details (relies on apt-cache)
* `update` - update list of available packages (relies on apt-get)
* `install` - install packages (relies on apt-get)
* `remove` - remove packages (relies on apt-get)
* `upgrade` - upgrade the system by installing/upgrading packages (relies on apt-get)
* `full-upgrade` - upgrade will remove currently installed packages if this is needed to upgrade the system as a whole
* `edit-sources` - edit /etc/apt/sources.list

```bash

# update list of available packages, if there are upgradable packages suggest to use apt --upgradable list
apt update

# install specific distribution package
apt install package/distribution

# resintall a package
apt --reinstall install postfix

# To upgrade a single package, pass the package name:
apt upgrade package_name


```


### Using apt-get

The `apt-get` utility is a powerful and free package management command line program, that is used to work with APT (Advanced Packaging Tool) library to perform installation of new software packages, removing existing software packages, upgrading of existing software packages and even used to upgrading the entire operating system.

#### Upgrade

The `upgrade` command is used to upgrade all the currently installed software packages on the system. Under any circumstances currently installed packages are not removed or packages which are not already installed neither retrieved and installed to satisfy upgrade dependencies.

```bash

# upgrade all the currently installed software packages on the system
# NB upgrades currently installed packages, but never installs or removes packages.
apt-get update && apt-get upgrade

# upgrade the specified package
apt-get install --only-upgrade <packagename>
apt-get --only-upgrade install sudo

# dist-upgrade in addition to performing the function of upgrade, also intelligently handles changing dependencies with new versions of packages => software packages will be added or removed to fulfill dependencies
apt-get dist-upgrade

```

#### Install

The ‘install‘ sub command is tracked by one or more packages wish for installation or upgrading.

```bash
# install multiple packages
apt-get install -y dkms build-essential module-assistant

apt-get --reinstall install package

# install Packages without Upgrading: --no-upgrade prevent already installed packages from upgrading
apt-get install packageName --no-upgrade


#
apt-get install --no-install-recommends packageName

# -f, --fix-broken : Fix; attempt to correct a system with broken dependencies in place.
apt-get install -f

```

Use Apt-Get to Install Specific Version of Package
https://sourcedigit.com/23234-apt-get-install-specific-version-of-package-ubuntu-apt-get-list/

#### Remove

```bash

# simulate package removing
apt-get remove –dry-run vsftpd

# Remove Packages Without Configuration
apt-get remove vsftpd

# To remove software packages including their configuration files, use the ‘purge‘ sub command.
apt-get purge vsftpd
# <=>
apt-get remove --purge vsftpd


# In order to remove dangling dependencies (i.e dependencies not used anymore),
# you have to use the autoremove option.
# sudo apt-get autoremove <package>

sudo apt-get autoremove
```

Using deborphan
```
sudo apt install deborphan

# orphaned package finder
deborphan

sudo apt purge $(deborphan)
```


### Using dpkg

dpkg is a low level package manager for Debian. It is a tool for installing, removing, and querying individual packages
dpkg is the base command for handling Debian packages on the system. If you have .deb packages, it is dpkg that allows installation or analysis of their contents.

```bash

# Install a package : -i | --install package.deb
dpkg -i /path/to/vim_7.3.429-2ubuntu2_amd64.deb


# List contents of debian package : -c|--contents package.deb
dpkg --contents /path/to/redis_2.8.3-1_amd64.deb

# display header package : -I|--info package.deb
dpkg -I|--info package.deb

# Remove a package : -r | --remove packagename
dpkg -r package

# Purge package : To remove a package using dpkg along with its corresponding configuration files,
dpkg -P package # <=> dpkg --purge package

# find all packages to purge and purge them
dpkg -l | grep '^rc' | awk '{print $2}' | xargs dpkg -P



```

### Packet reconfiguration

 `dpkg-reconfigure` - reconfigure an already installed package

```

dpkg-reconfigure tzdata


```

## Advanced


### Download package without installing

```bash

# Download a Package Without Installing it. NB download the given binary package into the current directory
apt-get download nethogs

apt download nethogs

aptitude download nethogs

```

#### Download Recursive Dependencies Of A Package

Download Recursive Dependencies Of A Package In Ubuntu https://www.ostechnix.com/download-recursive-dependencies-of-a-package-in-ubuntu/

```bash
# For vim
apt-cache depends --recurse --no-recommends --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances vim | grep "^\w" | sort -u


apt-get download $(apt-cache depends --recurse --no-recommends --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances vim | grep "^\w" | sort -u)

```

### Create dummy package

Using equivs package
https://eric.lubow.org/2010/creating-dummy-packages-on-debian/

Essentially it allows you to create dummy debs to provide packages that you installed from source (circumvent dependency checking)

 * equivs-control - create a configuration file for equivs-build
 * equivs-build - make a Debian package to register local software


### Misc

Recreate Debian Binary Packages That Are Already Installed On A System<br>
https://www.ostechnix.com/recreate-debian-binary-packages-that-are-already-installed-on-a-system/


How to remove broken packages in Ubuntu
https://net2.com/how-to-remove-broken-packages-in-ubuntu/


-----------

# Redhat/Centos

## RPM-Repositories

A software repository (“repo” in short) is a central file storage location to keep and maintain software packages, from which users can retrieve packages and install on their computers.


### Setup a new repo

```bash

# Manually create file name.repo in /etc/yum.repos.d/

cat <<IN > /etc/yum.repos.d/mysql57-community.repo
[mysql57-community]
name=MySQL 5.7 Community Server
baseurl=http://repo.mysql.com/yum/mysql-5.7-community/el/7/$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql
IN

# Alternative method to setup a new repository
# setup repo config by installing an .rpm with the repository information.
rpm -vhi http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm

# Or download repo config file
cd /etc/yum.repos.d/
wget http://repos.fedorapeople.org/repos/mmcgrath/nagios/fedora-nagios.repo

```

**How to add a Yum repository**<br>
https://www.redhat.com/sysadmin/add-yum-repository

### yum/dnf config-manager

`yum config-manager` (provided by `yum-utils` of `dnf-utils` package)


 * Manage main and repository DNF configuration options
 * Toggle which repositories are enabled or disabled,
 * Add new repositories.


 * **Adding new repository** : `yum-config-manager --add-repo repository_url` (where repository_url is a link to the .repo file)

```bash

yum-config-manager --add-repo=https://copr.fedorainfracloud.org/coprs/carlwgeorge/ripgrep/repo/epel-7/carlwgeorge-ripgrep-epel-7.repo
yum install ripgrep

sudo yum config-manager --add-repo ftp://la-firme.fr/mirrors/centos-docker/download.docker.com/linux/centos/docker-ce.repo

```

 * **Disable and enable repositories** using yum-config-manager as follows:

```bash
$ sudo yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
$ sudo yum-config-manager --disable remi-php54
$ sudo yum-config-manager --enable remi-php72
```

 * Display complete information for a repository

```bash
$ sudo yum-config-manager --dump mysql57-community
```


### Extra repositories

Available Repositories for CentOS<br>
https://wiki.centos.org/AdditionalResources/Repositories

*NB extras is installed but disabled by default in centos 5/6*

```
# activate extras repo
yum config-manager --enable extras
```

**Top 5 Yum Repositories for CentOS/RHEL**<br>
RPMFusin / EPEL / REMI / ELRepo / Webtatic <br>
https://tecadmin.net/top-5-yum-repositories-for-centos-rhel-systems/

*PowerTools*
PowerTools - Available only for CentOS8, the PowerTools repository provides most of the developer tools. Disabled by default.

How to enable PowerTools on CentOS 8
https://www.xmodulo.com/how-to-enable-powertools-centos.html

#### EPEL

How to Install the EPEL Repository on RHEL 8 and CentOS 8
https://www.2daygeek.com/install-enable-epel-repository-on-rhel-centos-oracle-linux/

Install EPEL repository on CentOS/RHEL 6, 7 & 8
https://thelinuxgurus.com/install-epel-repository-on-centos-rhel-6-7-8/

How to Install and Enable EPEL Repository on CentOS 8/7/6
https://www.tecmint.com/install-epel-repository-on-centos/

EPEL (Extra Packages for Enterprise Linux) is open source and free community based repository project from Fedora team which provides 100% high quality add-on software packages for Linux distribution including RHEL (Red Hat Enterprise Linux), CentOS, and Scientific Linux.

**setup epel repo**

```bash
wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -ivh epel-release-latest-7.noarch.rpm
# <=>
yum install http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-1.noarch.rpm
            https://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/e/

# <=>
yum install epel-release
```

=> create `/etc/yum.repos.d/epel.repo`:
```
[epel]
name=Extra Packages for Enterprise Linux 7 - $basearch
#baseurl=http://download.fedoraproject.org/pub/epel/7/$basearch
metalink=https://mirrors.fedoraproject.org/metalink?repo=epel-7&arch=$basearch
failovermethod=priority
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7

```

### GPG Trusted keys

```bash
# Import an RPM GPG key
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-8

wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | rpm --import -

# List all Imported RPM GPG keys in Linux
rpm -qa gpg-pubkey*

```

#### Verifying gpg

RPM and GPG: How to verify Linux packages before installing them<br>
https://www.redhat.com/sysadmin/rpm-gpg-verify-packages

## RPM-Mirroring

List of CentOS Mirrors : https://www.centos.org/download/mirrors/

Official mirror : http://mirror.centos.org/

### Build a local repo (mirror)

How to Setup Local HTTP Yum Repository on CentOS 7<br>
https://www.tecmint.com/setup-local-http-yum-repository-on-centos-7/

> NB using `createrepo` & yum-utils package (`reposync`)

Create a Centos Repository Mirror (05/2019) <br>
https://www.unixfu.ch/create-a-centos-repository-mirror/

Use the DNF local plugin to speed up your home lab<br>
https://fedoramagazine.org/use-the-dnf-local-plugin-to-speed-up-your-home-lab/


`reposync` : Synchronize yum repositories to a local directory

#### local mirror using rsync

```bash
# alternative use rsync
mirror_addr=rsync://ftp.pasteur.fr/mirrors/CentOS/7/
dest=/var/mirrors/centos/7

exclusions="--exclude local* "

rsync --archive       \
      --verbose       \
      --update        \
      --hard-links    \
      --delete        \
      --delete-after  \
      --delay-updates \
      --timeout=600   \
      $exclusions     \
      $mirror_addr $dest

```

directories structure of repository :
```
ftp.pasteur.fr/mirrors/CentOS/7/
  centosplus
  extras
  isos
  os
    x86_64
      repodata/
        Packages
        repomd.xml
  updates

http://ftp.pasteur.fr/mirrors/CentOS/7/os/x86_64/repodata/

```

#### Mirroring k8s


```bash

### retrieve packages.cloud.google.com/yum/repos

mirror_addr=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
dest=/media/user/mirrors/centos-kubernetes
#exclusions="--exclude-directories="

wget --directory-prefix $dest --page-requisites --no-parent --mirror $mirror_addr $exclusions

```

```bash

### to retrieve rpms packages.cloud.google.com/yum/pool

docker run -it -v ~+/repos:/mnt centos /bin/bash

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
        https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# yum install -y kubelet kubeadm kubectl

yum install yum-utils createrepo

mkdir -p /mnt/yum_repo/repos/
reposync -r kubernetes -p /mnt/yum_repo/repos/ --allow-path-traversal

```

### Local custom repo


How to create a custom rpm repository on Linux<br>
https://linuxconfig.org/how-to-create-a-custom-rpm-repository-on-linux

### Local fake repo


 * disable existing repos

```
for f in $(ls /etc/yum.repos.d/*.repo)
do
	sed -i -e "s/enabled=1/enabled=0/g" $f
done
```

 * create dummy repo

`/etc/yum.repos.d/dummyrepo` :
```
[dummy]
name=dummy
baseurl=file:///tmp/dummyrepo
gpgcheck=0
enabled=1
```

```
sudo -i
mkdir /tmp/dummyrepo;
createrepo /tmp/dummyrepo
dnf upgrade -y
```

## RPM sources

### Setup repositories source

`/etc/yum.repos.d/*.repo`

`/etc/yum.repos.d/epel.repo`

```bash
[epel]
name=Extra Packages for Enterprise Linux 7 - $basearch
baseurl=ftp://la-firme.fr/mirrors/epel/pub/7/$basearch
failovermethod=priority
enabled=1
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
```

Using offline repos: `/etc/yum.repos.d/offline.repo`
```
[BaseOS]
name=CentOS-8.2 - Base
baseurl=http://10.1.1.3/centos/BaseOS/os
gpgcheck=0

[extras]
name=CentOS-8.2 - Extras
baseurl=http://10.1.1.3/centos/extras/os
gpgcheck=0

[AppStream]
name=CentOS-8.2 - AppStream
baseurl=http://10.1.1.3/centos/AppStream/os
gpgcheck=0

```

### Repositories info


```bash

# list all repositories configured - Display all the configured software repositories
yum repolist all

# Display information about enabled yum repositories
yum repoinfo epel

```

### Configuration


How To Speed Up DNF Package Manager In Fedora, RHEL, CentOS, AlmaLinux, Rocky Linux<br>
https://ostechnix.com/how-to-speed-up-dnf-package-manager-in-fedora/

## RPM-Cache repository

### Update cache

```bash
# Download yum repository data to cache
yum makecache
```

### Clean cache


```bash
# Clear out cached package data

# Delete packages saved in cache
yum clean packages

# Clean out all packages and meta data from cache
yum clean all

```

### Search package

```bash
## Search package names and descriptions for a term
# find package containing token
yum search samba
```

#### repoquery

```bash
# Use repoquery from yum-utils package to find repository which provides specified package
repoquery -i rsyslog

# Listing files in a package  (not already installed)
repoquery -l netstat

```

### dnf

CentOS 8 Package Management with DNF on the Command Line<br>
https://www.howtoforge.com/centos-8-package-management-with-dnf-on-the-command-line/


DNF stands for Dandified YUM is a software package manager for RPM-based Linux distributions. It is used to install, update and remove packages in the Fedora/RHEL/CentOS operating system. It is the default package manager of Fedora 22, CentOS8 and RHEL8

```bash
dnf --version
```

```bash
# <=> yum-config-manager
dnf config-manager --set-enabled extras
```


DNF for APT users (11/2020) <br>
https://www.redhat.com/sysadmin/dnf-apt-users

Linux package managers: dnf vs apt (07/2021)<br>
https://opensource.com/article/21/7/dnf-vs-apt

Linux package management with dnf (06/2021) <br>
https://opensource.com/article/21/6/dnf-linux

How to install only security and bugfixes updates with DNF (08/2021)<br>
https://fedoramagazine.org/how-to-install-only-security-and-bugfixes-updates-with-dnf/

```bash
dnf check-update --security
dnf check-update --bugfix
dnf updateinfo list --security

# install only security updates:
dnf update --security

dnf update --security --bugfix

# install specific update
dnf update --advisory=FEDORA-2021-74ebf2f06f
```


## RPM List installed packages


### Using yum/dnf

```bash
# List all installed packages
yum list installed

# Show All Versions Of Package
yum list <package_name> --showduplicates

# Find out if a package is installed
yum list installed openssh

# List all available packages
yum list available

# List installed and available packages
yum list *ncurses*

# List installed and available packages
yum list all

# List installed and available kernel packages
yum list kernel

# Work with packages in a particular repository, e.g. list packages from epel repo
yum repo-pkgs epel list

```

### Using rpm


```bash
# check with rpm the software currently installed in the system.
rpm -qa

# Check Recently Installed RPM Packages on Linux
rpm -qa --last

# List versions of all matching packages:
rpm -qa 'mariadb*'

# Show version of installed python package:
rpm -q python

```


## RPM Details on installed packages

### Using yum/dnf

```bash
# Display information about a package
yum info vsftpd

# Display dependencies for a package
yum deplist nfs-utils

# Find packages that provide the queried file
yum provides /usr/bin/htpasswd
# <=>
yum whatprovides '*bin/dig'
```

How To Find Dependencies For A Particular Package In CentOS (12/2016)<br>
https://ostechnix.com/how-to-find-dependencies-for-a-particular-package-in-centos/

```
# sudo dnf install yum-utils
# repoquery --requires --resolve <package>

```

### Using rpm

```bash

# display list of files installed by package
rpm -ql python

# Pass -i to get more information about the queried package:
rpm -qi java-11-openjdk-devel

# List Dependency Packages From an RPM File
rpm -qR nano-2.0.9-7.el6.x86_64

# Identify owner of a file and show version of the package:
rpm -qf /etc/postfix/main.cf

#  find the configuration files of any package
rpm -qc httpd

# Find Manual Page of Any Package with RPM
rpm -qd httpd

#  use rpm to see their changelogs
rpm -q --changelog fpaste

# Check if a Given Vulnerability is Fixed in a Specific Package in Linux
rpm -q --changelog python-2.6.6 | grep -i "CVE-2019-9636"


```


How RPM packages are made: the spec file<br>
https://fedoramagazine.org/how-rpm-packages-are-made-the-spec-file/

spec file sample : https://src.fedoraproject.org/rpms/fpaste/blob/master/f/fpaste.spec

## RPM Manage system packages


Linux package management with YUM and RPM
https://www.redhat.com/sysadmin/how-manage-packages

### Using yum/dnf

YUM (Yellowdog Updater Modified) is a widely used package management tool for RPM (RedHat Package Manager) based Linux systems, which makes sofware installation easy on Red Hat/CentOS Linux. Yum is an automatic updater and package installer/uninstaller for rpm systems. It automatically resolve dependencies while installing the package.

Three Ways to Exclude Specific Packages from Yum Update : <br>
https://www.2daygeek.com/redhat-centos-yum-update-exclude-specific-packages/

4 Ways to Disable/Lock Certain Package Updates Using Yum Command <br>
https://www.tecmint.com/yum-lock-disable-blacklist-certain-package-update-version/


```bash

### Update

# Query repositories for available package updates
yum check-update

# Update all packages with available updates
yum update

# Update all packages excluding multiple packages
yum update --exclude=kernel* --exclude=php*

# upgrage the system updating the installed package (prompt for changes if not using -y)
yum -y update

# Update the httpd package (if available)
yum update httpd

# Apply security-related package updates
yum update --security

# Update packages taking obsoletes into account
yum upgrade

### Install/Remove

# install pacakge
yum install pkg_name

# Install Specific Version Of Package
yum install <package_name>-<version_info>

# Install a package from a local file, http, or ftp
yum localinstall abc-1-1.i686.rpm

# Reinstall the current version of a package
yum reinstall util-linux

# Install/Delete all packages from epel repo
yum repo-pkgs epel install/remove

# Remove the vsftpd package and dependencies
yum remove vsftpd

# gives you an overview of what happened in past transactions
yum history

```

### Using rpm

https://www.howtoforge.com/centos-redhat-rpm-command/

rpm stands for "Red Hat Package Manager" and it is a powerful package management tool for Red Hat system.
The application rpm was developed by Red Hat as a software package manager. It allows the user to install new packages very easily, and it keeps a database with all the software installed in the system. The name RPM refers to .rpm file format that containing compiled software’s and necessary libraries for the package.


```

# install rpm file package
# -h show us the progress of the installation
# -v to show as much information as possible
# --nodeps option to not handle dependencies on upgrage or remove
rpm -ivh package.rpm

# To upgrade an RPM package
rpm -Uvh package.rpm

# To downgrade a package using rpm package
rpm -Uvh --oldpackage [downloaded_lower_version_package]


# remove package
rpm -e gedit

# dry run using --test
sudo rpm -Uvh --test package.rpm

```

#### Details on rpm packages

```
# Get Information About an RPM Package Before Installing it
rpm -qp nano-2.0.9-7.el6.x86_64.rpm

# Detailed Information About an RPM Package
rpm -qip nano-2.0.9-7.el6.x86_64.rpm

# List Files From an RPM File
rpm -qlp nano-2.0.9-7.el6.x86_64.rpm

# List Dependency Packages From an RPM File
rpm -qRp nano-2.0.9-7.el6.x86_64.rpm

# Show scriptlets from an RPM file
rpm -qp --scripts some.rpm


```

### Download package


`yumdownloader` : Download a package from a repo to current directory

```
# download RPM packages
yumdownloader nano

```

`yum install --downloadonly vsftpd` :  Download to /var/cache/yum/arch/prod/repo/packages/, but don’t install


#### Download package and its dependencies

How To Download A RPM Package With All Dependencies :<br>
https://www.ostechnix.com/download-rpm-package-dependencies-centos/

Downloading RPM Packages with dependencies :<br>
https://www.thegeekdiary.com/downloading-rpm-packages-with-dependencies-yumdownloader-vs-yum-downloadonly-vs-repoquery/

How To Download A RPM Package With All Dependencies In CentOS, Fedora, RHEL, AlmaLinux, Rocky Linux<br>
https://ostechnix.com/download-rpm-package-dependencies-centos/


*Using yum-downloadonly plugin*

```bash
# setup epel repository
curl -O http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -ivh epel-release-latest-7.noarch.rpm
yum repolist

mkdir nginx ; cd nginx
sudo yum install --downloadonly --downloaddir=. nginx
tar zcvf nginx-1.16.1-1.el7.x86_64.tar.gz *.rpm

```

*Using yumdownloader*
```bash
# Using yumdownloader (NB --resolve on yumdownloader fails)
repoquery -R --resolve --recursive firefox | xargs -r yumdownloader
```

`repotrack` : Download a package and all its dependencies

## RPM Advanced

How to create an RPM package from a tarball<br>
https://www.xmodulo.com/create-rpm-package-tarball.html


```bash

```
