[[_TOC_]]

# Vagrant

## Introduction

Vagrant acts as a type of wrapper around virtualization software, which greatly speeds up many of the tasks associated with setting up, tearing down, and sharing virtual machines.

Vagrant is open-source software for creating and configuring virtual development environments.

Vagrant main use cases

 * Create and configure lightweight, reproducible, and portable development environments.
 * Playing around with new OS
 * Testing Package updates or OS upgrade
 * Service Deployment
 * Configuration management tweaks
 * Reproduce ticket related issues, performance problems, or test client server interactions.

Vagrant uses the terms *`machine`* to refer to a virtual machine and *`box`* to refer to a virtual machine image.



```bash
vagrant --version
```

    Vagrant 2.2.14



```bash
vagrant
```

    [0mUsage: vagrant [options] <command> [<args>]

        -h, --help                       Print this help.

    Common commands:
         autocomplete    manages autocomplete installation on host
         box             manages boxes: installation, removal, etc.
         cloud           manages everything related to Vagrant Cloud
         destroy         stops and deletes all traces of the vagrant machine
         global-status   outputs status Vagrant environments for this user
         halt            stops the vagrant machine
         help            shows the help for a subcommand
         hostmanager     plugin: vagrant-hostmanager: manages the /etc/hosts file within a multi-machine environment
         init            initializes a new Vagrant environment by creating a Vagrantfile
         login
         package         packages a running vagrant environment into a box
         plugin          manages plugins: install, uninstall, update, etc.
         port            displays information about guest port mappings
         powershell      connects to machine via powershell remoting
         provision       provisions the vagrant machine
         push            deploys code in this environment to a configured destination
         rdp             connects to machine via RDP
         reload          restarts vagrant machine, loads new Vagrantfile configuration
         resume          resume a suspended vagrant machine
         sandbox
         snapshot        manages snapshots: saving, restoring, etc.
         ssh             connects to machine via SSH
         ssh-config      outputs OpenSSH valid configuration to connect to the machine
         status          outputs status of the vagrant machine
         suspend         suspends the machine
         up              starts and provisions the vagrant environment
         upload          upload to machine via communicator
         validate        validates the Vagrantfile
         vbguest         plugin: vagrant-vbguest: install VirtualBox Guest Additions to the machine
         version         prints current and latest Vagrant version
         winrm           executes commands on a machine via WinRM
         winrm-config    outputs WinRM configuration to connect to the machine

    For help on any individual command run `vagrant COMMAND -h`

    Additional subcommands are available, but are either more advanced
    or not commonly used. To see all subcommands, run the command
    `vagrant list-commands`.
            --[no-]color                 Enable or disable color output
            --machine-readable           Enable machine readable output
        -v, --version                    Display Vagrant version
            --debug                      Enable debug output
            --timestamp                  Enable timestamps on log output
            --debug-timestamp            Enable debug output with timestamps
            --no-tty                     Enable non-interactive output
    [0m




### Vagrant environment configuration

Environmental Variables
https://www.vagrantup.com/docs/other/environmental-variables.html

**`VAGRANT_LOG`**

VAGRANT_LOG specifies the verbosity of log messages from Vagrant. By default, Vagrant does not actively show any log messages.


To enable all Vagrant logs set environment variable VAGRANT_LOG to the desire log level (for instance `VAGRANT_LOG=debug`).<br>

*To change log level* (debug, info, warn, error) :

```bash
$ VAGRANT_LOG=info vagrant up
```


**`VAGRANT_HOME`**

`VAGRANT_HOME` can be set to change the directory where Vagrant stores global state. By default, this is set to ~/.vagrant.d. The Vagrant home directory is where things such as boxes are stored, so it can actually become quite large on disk.

=> use VAGRANT_HOME or create a mount point `~/.vagrant.d` to use an alternative location (e.g. external disk)

```bash
# alt solution
cd ~/.vagrant.d ; rmdir boxes

# create symbolic link to use vm storage => vm is mounted in /home/user/vm and vagrant_boxes dir is created
ln -sf /home/user/vm/vagrant_boxes boxes
```

**`VAGRANT_BOX_UPDATE_CHECK_DISABLE`**

By default, Vagrant will query the metadata API server to see if a newer box version is available for download. This optional can be disabled on a per-Vagrantfile basis with `config.vm.box_check_update`, but it can also be disabled globally setting VAGRANT_BOX_UPDATE_CHECK_DISABLE to any non-empty value.

**`VAGRANT_VAGRANTFILE`**

This specifies the filename of the Vagrantfile that Vagrant searches for. By default, this is `"Vagrantfile"`. Note that this is not a file path, but just a filename

```bash
VAGRANT_VAGRANTFILE=multi_yaml vagrant status
```

**`VAGRANT_DEFAULT_PROVIDER`**

This configures the default provider Vagrant will use. By setting this, you will force Vagrant to use this provider for any new Vagrant environments. Existing Vagrant environments will continue to use the provider they came up with.


## Vagrant boxes

https://www.vagrantup.com/docs/boxes

Vagrant boxes are just ‘Templates’

For instance a VirtualBox vagrant *box* is an archive of the resulting files of exporting a VirtualBox virtual machine (ovf format + vmdk disk). It has to includes additional files :
 * `metadata.json` : to set the provider name (e.g. virtual box `{"provider":"virtualbox"}` )
 * `Vagrantfile` : to set the  MAC address of the NAT network device  (08002798B676)

Boxes contain a base operating systems already setup

A VM used to create a box must have some installed software :
 * a `vagrant` main user that should have password-less sudo privileges
 * *SSH server* with key-based authentication setup. (for the vagrant user)
 * *VirtualBox Guest Additions* (for shared folders, port forwarding) (optional)


### Box commands



```bash
vagrant box
```

    [0mUsage: vagrant box <subcommand> [<args>]

    Available subcommands:
         add
         list
         outdated
         prune
         remove
         repackage
         update

    For help on any individual subcommand run `vagrant box <subcommand> -h`
            --[no-]color                 Enable or disable color output
            --machine-readable           Enable machine readable output
        -v, --version                    Display Vagrant version
            --debug                      Enable debug output
            --timestamp                  Enable timestamps on log output
            --debug-timestamp            Enable debug output with timestamps
            --no-tty                     Enable non-interactive output
    [0m


```bash
# to list available installed boxes
vagrant box list

# to add a new box
vagrant box add boxname url
```

NB Adding vagrant box : 2 cases
 * using url referencing metadata => `vagrant box add name url`
 * using local box package => `vagrant box add name file.box`

How To Delete Outdated Vagrant Boxes In Linux<br>
https://ostechnix.com/how-to-delete-outdated-vagrant-boxes-in-linux/


```
# check for updates vagrant box update
vagrant box outdated
```

###  Boxes public hosting

Vagrant boxes created by the community are made available at the following url:

https://app.vagrantup.com/boxes/search?u



### Box packaging

Create package.box (NB creates automatically metadata.json & Vagrantfile inside box archive)

```bash

vagrant package --base vbox_name [--output my_package.box]

```

## Vagrant configuration


Vagrant `init` allows you to initialize a new Vagrant environment. This will place a `Vagrantfile` in your current directory. A Vagrant environment can be a single Vagrant virtual machine, or a collection of virtual machines.

### Vagrantfile

https://www.vagrantup.com/docs/vagrantfile

Vagrant is configured on a per project basis. Each of these projects has its own `Vagranfile`

The `Vagrantfile` is a text file (using ruby language) in which vagrant reads that sets up our environment. There is a description of what OS, how much RAM, and what software to be installed etc.

You can version control this file so your development team can stay in sync on changes.

>  **Lookup path** : When you run any vagrant command, Vagrant climbs your directory tree starting first in the current directory you are in to find a `Vagrantfile` setting file.



> Instead of building a virtual machine from scratch, Vagrant uses a base image called **box** to quickly clone a virtual machine.

`vagrant init` initializes Vagrant with a Vagrantfile and ./.vagrant directory, using no specified base image. Before you can do vagrant up, you'll need to specify a base image (box) in the Vagrantfile.

**Create a `Vagrantfile` template in current directory**

```bash
# create a Vagrantfile template
$ vagrant init
```

**Create a `Vagrantfile` minimal template in current directory**


```bash
# using --minimal to create a minimal vagrant template
$ vagrant init --minimal
```


`./Vagrantfile` :

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "base"
end

```


To use a specific box, modify `Vagrantfile` to specify name of the added box (e.g. `config.vm.box = "precise32"`) or at initialization specify the box to use



```bash
#  vagrant init [box-name] [box-url]
$  vagrant init generic/ubuntu1804
```


### vagrant commands


 * `vagrant init [box-name] [box-url]` : creating an initial Vagrantfile if one doesn't already exist.

 * `vagrant up [--provider providername]` : creates and configures guest machines according to your Vagrantfile. NB Can specify `providername` if a box is available for multiple providers ( virtualbox, vmware etc...)

 * `vagrant status` : display the state of the machines Vagrant is managing.

 * `vagrant suspend` : save the current running state of the machine and stop it

 * `vagrant resume` : resumes a Vagrant managed machine that was previously suspended,

 * `vagrant halt [--force]` : gracefully shut down the guest operating system and power down the guest machine

 * `vagrant reload` : is equivalent to halt then up

 * `vagrant destroy [-f]` : remove all traces of the guest machine from your system. (-f : force destroying without asking confirmation)

 * `vagrant provision [--provision-with x,y,z]` : Runs any configured provisioners (or specified x,y,z) against the running Vagrant managed machine.

 * `vagrant ssh-config` : display ssh config


boot up the virtual machine (Vagrant runs the virtual machine without a UI)

```bash
$ vagrant up
```

### vagrant snaphost commands

see https://www.vagrantup.com/docs/cli/snapshot

Manage snapshots with the guest machine. Snapshots record a point-in-time state of a guest machine.

>  If you are using push and pop, avoid using save and restore which are unsafe to mix.


 * `vagrant snapshot push` : takes a snapshot and pushes it onto the snapshot stack.
 * `vagrant snapshot pop` : restore the pushed state
     * `--no-delete` - Prevents deletion of the snapshot after restoring
 * `vagrant snapshot save [vm-name] NAME` : saves a new named snapshot
 * `vagrant snapshot restore [vm-name] NAME` : restores the named snapshot.
 * `vagrant snapshot list` : list all the snapshots taken.
 * `vagrant snapshot delete [vm-name] NAME` :  delete the named snapshot.



### Vagrantfile settings

https://www.vagrantup.com/docs/vagrantfile/machine_settings.html

#### ruby syntax

Use :

 * Local variables with lowercase ( `something` )
 * Instance variables prefixed with @ ( `@something` )
 * Constants with upercase or camelcase ( `Something` / `SOMETHING` )
 * Global variables prefixed with $ ( `$something` )

Variables and comments in Vagrantfile<br>
https://0x63.me/variables-and-comments-in-vagrantfile/

#### using environment variable

```ruby

# define variables using environment varialbes
username = ENV['OS_USERNAME'] || "defuser"
num_compute_nodes = (ENV['DEVSTACK_NUM_COMPUTE_NODES'] || 1).to_i

if ENV["TGT"] == "xenial"
 boxname="ubuntu/xenial64"
 script="xenial-docker-provision.sh"
 vm_name="oln_tp_xenial"
else
 boxname="ubuntu/trusty64"
 script="trusty-docker-provision.sh"
 vm_name="oln_tp_trusty"
end

if ENV['OS_GUESTNAME']
  $vbname_suffix = "_#{ENV['OS_GUESTNAME']}"
else
  $vbname_suffix = "_default"
end


```

#### using yaml config

```ruby

###################
# using yaml config
require 'yaml'
if File.file?('config.yaml')
  conf = YAML.load_file('config.yaml')
else
  abort "configuration file 'config.yaml' does not exist."
end

# use yaml/env value or default value
pwd = conf['pwd'] || "secret"
```

#### box settings


 * `config.vm.box` *(string)* - This configures what box the machine will be brought up against.

 * `config.vm.box_check_update` *(boolean)* - If true, Vagrant will check for updates to the configured box on every vagrant up. If false => disable automatic box update checking. If you disable this, then boxes will only be checked for updates when the user runs `vagrant box outdated`.

 * `config.vm.box_url` *(string, array of strings)* - The URL that the configured box can be found at.

```ruby
config.vm.box_url = "ftp://lab.fr/mirrors/vagrantbox/rhel7.box"
config.vm.box = "generic/rhel7"
config.vm.box_check_update = false # To disable check for updates
```

#### hostname

 * `config.vm.hostname` (string) - The hostname the machine should have. Defaults to nil. If nil, Vagrant will not manage the hostname. If set to a string, the hostname will be set on boot. If set, Vagrant will update /etc/hosts on the guest with the configured hostname.


> By default, this will modify /etc/hosts, adding the hostname on a loopback interface (`127.0.X.1 `) that is not in use.


#### multi-machine

https://www.vagrantup.com/docs/multi-machine

Vagrant is able to define and control multiple guest machines per Vagrantfile.

> **NB** specify a **primary** machine (`primary: true`) that will be the default machine used when a specific machine in a multi-machine environment is not specified

> By default in a multi-machine environment, vagrant up will start all of the defined machines. The **autostart** (`autostart: false`) setting allows you to tell Vagrant to not start specific machines.


```ruby
config.vm.define :m1 , primary: true do |m1|
  m1.vm.box = "apache"
end

config.vm.define :m2, autostart: false do |m2|
  m1.vm.box = "mysql"
end

```


#### ssh settings

https://www.vagrantup.com/docs/vagrantfile/ssh_settings


 * `config.ssh.insert_key` *(boolean /def=true)* : automatically insert a keypair to use for SSH, replacing the default Vagrant's insecure key
 * `config.ssh.private_key_path`  *(string, array of strings)*  :  path to the private key to use (if using a specific SSH public key into the guest machine).

 * `config.ssh.username` *(string)* : This sets the username that Vagrant will SSH as by default. Providers are free to override this if they detect a more appropriate user. By default this is "vagrant", since that is what most public boxes are made as.

```ruby
# to continue using insecure key
config.ssh.insert_key = false
```

Setup a new ssh key pair
https://www.devopsroles.com/vagrant-ssh-key-pair/


##### ssh agent

 * `config.ssh.forward_agent` *(boolean)* - If true, agent forwarding over SSH connections is enabled. Defaults to false.

```ruby

  # any SSH connections made will enable agent forwarding.
  config.ssh.forward_agent = true

```

##### Providing external ssh credentials

```ruby

# Copy id_rsa on each VM
config.vm.provision "file", source: "files/id_rsa", destination: "/home/vagrant/.ssh/id_rsa"
public_key = File.read("files/id_rsa.pub")
# Authorize public ssh key on each VM
config.vm.provision "shell", inline: <<-SCRIPT
  chmod 600 /home/vagrant/.ssh/id_rsa
  chmod 700 /home/vagrant/.ssh/
  echo '#{public_key}' >> /home/vagrant/.ssh/authorized_keys
  chmod -R 600 /home/vagrant/.ssh/authorized_keys
  SCRIPT

```
Inject SSH pub key to Vagrant image<br>
https://robert-reiz.com/2016/11/06/inject-ssh-pub-key-to-vagrant-image/

### Shared folder

https://www.vagrantup.com/docs/synced-folders

Synced folders enable Vagrant to sync a folder on the host machine to the guest machine.

 * `config.vm.synced_folder` - Configures synced folders on the machine, so that folders on your host machine can be synced to and from the guest machine. Additional options:
   * `create` *(boolean)* - If true, the host path will be created if it does not exist. Defaults to false.
   * `disabled` *(boolean)* - If true, this synced folder will be disabled and will not be setup.
   * `type` *(string)* - The type of synced folder.

```ruby

# available options : create, disabled, owner, group
config.vm.synced_folder "src/", "/srv/website", owner: "root", group: "root"

config.vm.synced_folder $HOST_GIT_DIR, "/home/vagrant/git", mount_options: ["dmode=775","fmode=664"]

config.vm.synced_folder $HOST_GIT_DIR, "/home/vagrant/git", mount_options: ["umask=0022","uid=1000","gid=1000"]

```


By default, Vagrant will share your project directory (the directory with the Vagrantfile) to `/vagrant`.

```ruby
# to disable vagrant shared folder
config.vm.synced_folder ".", "/vagrant", disabled: true

# Force vagrant shared folder in case box use synced_folder ".", "/vagrant", type: "rsync"
config.vm.synced_folder ".", "/vagrant", type: "virtualbox"

```

#### virtualbox provider specific options

```ruby

# to disable symbolic link creation inside box in the vagrant shared folders
config.vm.synced_folder ".", "/vagrant", SharedFoldersEnableSymlinksCreate: false

# automount: If true, the --automount flag will be used when using the VirtualBox tools
# to share the folder with the guest vm
config.vm.synced_folder "src/", "/srv/website", automount: true

```

#### tips

```bash
# mount manually shared folder, .e.g vagrant
mount -t vboxsf -o uid=$(id -u vagrant),gid=$(id -g vagrant) var_www /var/www

```



### Networking

https://www.vagrantup.com/docs/networking

Networking : Vagrant provides high-level networking options for port forwarding, network connectivity, and network creation. These networking options represent an abstraction that enables cross-provider portability. That is, the same Vagrantfile used to provision a VirtualBox VM could be used to provision a VMware machine.

Vagrant assumes there is an available NAT device on eth0 (or the first network adapter). This ensures that Vagrant always has a way of communicating with the guest machine.

> NB With VirtualBox, Vagrant requires the first network device attached to the virtual machine to be a NAT device. The NAT device is used for port forwarding, which is how Vagrant gets SSH access to the virtual machine.


For virtualbox provider:
 * VM is setup with a first network interface using NAT with host machine on a private network 10.0.2.0
 * VM ip is `10.0.2.15`, host ip is `10.0.2.2`

>  cf https://www.virtualbox.org/manual/ch06.html#network_nat
The virtual machine receives its network address and configuration on the private network from a DHCP server integrated into Oracle VM VirtualBox
As more than one card of a virtual machine can be set up to use NAT, the first card is connected to the private network 10.0.2.0
<br> NB the NAT inferface is given `10.0.2.15` ip address,  NAT subnet is `10.0.2.0/24` with host device `10.0.2.2` and dns `10.0.2.3`



By default, Vagrant forward 22 guest port on 2222, allowing to connect using ssh (`vagrant ssh` <=> `ssh vagrant@localhost -p 2222`)

 * `config.vm.usable_port_range` *(range)* - A range of ports Vagrant can use for handling port collisions and such. Defaults to 2200..2250.



####  Network configuration

All networks are configured within your Vagrantfile using the `config.vm.network`

> A public or private network with an assigned IP may be flagged for hostname. In this case, the hostname will be added to the flagged network (`hostname: true`).


#### Port forwarding

https://www.vagrantup.com/docs/networking/forwarded_ports.html

 * *Forwarded Ports* : Vagrant will set up a port on the host to forward to a port on the guest
     * `auto_correct` (boolean) - If true, the host port will be changed automatically in case it collides with a port already in use. By default, this is false.

     * `guest` (int) - The port on the guest that you want to be exposed on the host. This can be any port.

     * `guest_ip` (string) - The guest IP to bind the forwarded port to. If this is not set, the port will go to every IP interface. By default, this is empty.

     * `host` (int) - The port on the host that you want to use to access the port on the guest. This must be greater than port 1024 unless Vagrant is running as root (which is not recommended).

     * `host_ip` (string) - The IP on the host you want to bind the forwarded port to. If not specified, it will be bound to every IP. By default, this is empty.

     * `protocol` (string) - Either "udp" or "tcp". This specifies the protocol that will be allowed through the forwarded port. By default this is "tcp".

     * `id` (string) - Name of the rule (can be visible in VirtualBox). By default this is "protocol""guest" (example : "tcp123").

```ruby
config.vm.network "forwarded_port", guest: 80, host: 8080, auto_correct: true

config.vm.network "forwarded_port", guest: 8080, host: 8080, host_ip: "127.0.0.2"

# overriding ssh default forwarded port
config.vm.network :forwarded_port, guest: 22, host: 3200, id: 'ssh'
```

> to update after forwarded port change, use vagrant reload --provision

Using `vagrant port` command to list forwarded port

```bash
$ vagrant port default
The forwarded ports for the machine are listed below. Please note that
these values may differ from values configured in the Vagrantfile if the
provider supports automatic port collision detection and resolution.

    80 (guest) => 1234 (host)
    22 (guest) => 2202 (host)


```


#### private network

https://www.vagrantup.com/docs/networking/private_network

Vagrant private networks allow you to access your guest machine by some address that is not publicly accessible from the global internet.
Vagrant creates a network that is private to your host and the guest machines on that host.

> For virtualbox by default, private networks are **host-only** networks

```ruby
  # using a static ip
  config.vm.network :private_network, ip: "192.168.50.4" [, netmask: "255.255.255.0"]

  config.vm.network: private_network, type: "dhcp" [, name: vboxnet1]

```

> NB for dhcp, can specify `name` attribute to designate host-only network name to use

> NB to list available vboxnet defined in virtualbox : `VBoxManage list dhcpservers`


If you want to manually configure the network interface yourself, you can disable Vagrant's auto-configure feature by specifying `auto_config: false`


Additional options:
 * `auto_config` *(boolean)*
 * `mac` *(string)* : specify mac address for static ip
 * `netmask` *(string* : specify different netmask (by default 255.255.255.0)
 * `type` *(string)* : by default "static", can set "dhcp"
 * `name` *(string)* : specify host network name to use

####  public or bridged network

https://www.vagrantup.com/docs/networking/public_network

Bridged networking bridges the virtual machine onto a device on your physical machine, making the virtual machine look like another separate physical machine on the network.

```ruby
$itf_bridge = %x(ip route | awk '/^default/ {print $5}').chomp

config.vm.network "public_network", bridge:$itf_bridge
```

 * `use_dhcp_assigned_default_route: true` : Using the DHCP Assigned Default Route

manual configuration: use `auto_config: false` and shell provisionner to configure network interface

#### virtualbox provider

https://www.vagrantup.com/docs/providers/virtualbox/networking

 * `nic_type: "virtio"` : specify a specific NIC type for the created network interface

> can also use `vb.customize ["modifyvm", :id, "--nictype1", "virtio"]` in provider config

#### promiscuous mode

> Use virtualbox vboxmanage customize parameter
>
> --nicpromisc<n> with n for the nic number (1: first nic used for NAT)


```bash
		v.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"] # use in promisc mode
```


### Provider Virtualbox


https://www.vagrantup.com/docs/providers/virtualbox/configuration

3 vagrant settings you should check out to optimize your vm <br>
http://station.clancats.com/3-vagrant-settings-you-should-check-out-to-optimize-your-vm/

```ruby

  # RAM and CPU config
  config.vm.provider "virtualbox" do |vb|   # <=> config.vm.provider :virtualbox do |vb|
    # Set Virtual Machine Name
    vb.name = "#{$prefix}-node"

    # By default, VirtualBox machines are started in headless mode,
    vb.gui = true

    # for desktop vm setup 2 screens
    vb.customize ["modifyvm", :id, "--monitorcount", "2"]

    # Customize the amount of memory on the VM:
    vb.memory = "2048"
    # Customize the amount of cpu on the VM:
    vb.cpus = 2

    # Checking for Guest Additions, By default Vagrant will check for the VirtualBox Guest Additions
    # when starting a machine, and will output a warning if the guest additions are missing or out-of-date.
    vb.check_guest_additions = false

    # Linked clones are based on a master VM, for vagrant >= 1.8
    vb.linked_clone = true

    # to use a specific NIC type by default for guests, different from Virtualbox default
    vb.default_nic_type = "virtio"

    ### VBoxManage Customizations
    # change NIC type
    vb.customize ["modifyvm", :id, "--nictype1", "virtio"]

    v.customize ["modifyvm", :id, "--nictype2", "virtio"]
    v.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"]


    # Put the VM in the VirtualBox group 'Test'
    vb.customize ['modifyvm', :id, '--groups', '/Test']
    # Disable audio
    vb.customize ['modifyvm', :id, '--audio', 'none']
    # Enable ioapic
    vb.customize ["modifyvm", :id, "--ioapic", "on"]
    # Sync time with local host
    vb.customize [ "guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 1000 ]

    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]

  end
```

### Provider qemu/kvm


Vagrant Libvirt : This is a Vagrant plugin that adds a Libvirt provider to Vagrant, allowing Vagrant to control and provision machines via Libvirt toolkit.

> On Linux, your distribution may already have libvirt installed,

#### install vagrant libvirt plugin

```
apt install ruby-libvirt libvirt-dev
vagrant plugin install vagrant-libvirt

```

#### vagrant boxes


```bash
vagrant box add fedora/32-cloud-base --provider=libvirt
vagrant box add debian/bullseye64 --provider=libvirt

```

> vagrant-mutate plugin convert box

https://github.com/sciurus/vagrant-mutate
Vagrant-mutate is a vagrant plugin to convert vagrant boxes to work with different providers.

#### Using libvirt Vagranfile

Vagrant Libvirt Provider<br>
https://github.com/vagrant-libvirt/vagrant-libvirt


To explicitly tells Vagrant to use libvirt KVM to run the virtual machine use:
 * `--provider=libvirt` option with `vagrant up`
 * set environment variable  `VAGRANT_DEFAULT_PROVIDER`
    ```bash
    export VAGRANT_DEFAULT_PROVIDER=libvirt

    ```
 * In Vagrantfile use : `ENV['VAGRANT_DEFAULT_PROVIDER'] = 'libvirt'`


Vagrantfile sample:


```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_DEFAULT_PROVIDER'] = 'libvirt'

Vagrant.configure("2") do |config|
  config.vm.box = "debian/bullseye64"
  config.vm.synced_folder ".", "/vagrant", type: "rsync"
  config.ssh.insert_key = false
  config.vm.hostname = "test.kvm.local"

  config.vm.network :private_network, :ip => "10.20.30.41"
  config.vm.provider "libvirt" do |libvirt|
    libvirt.cpus = 1
    libvirt.memory = 1024
    libvirt.title = "test libvirt"
    libvirt.default_prefix = "demo_"
  end
end

```

> **NB** by default libvirt skipped the forwarding of the ssh-port because you can access the machine directly using management interface.

#### multi-machine configuration

Some providers parallelize deployment of multiple machines, and it appears that the default behavior is to parallelize if possible. This is the case for libirt provider

Passing `--no-parallel` to `vagrant up` enforces that they are run sequentially in multi-machine Vagrantfiles.

> use `--no-parallel` to deploy sequentially multi-machine configuration

#### Synced folder

 * vagrant-libvirt supports rsync with unidirectional synced folders.
 * vagrant-libvirt supports Virtio-fs with bidirectional synced folders. For virtiofs shares, a mount: false option allows to define synced folders without mounting them at boot.*


### Provider vmware

official VMware Fusion and VMware Workstation  provider for Vagrant<br>
https://www.vagrantup.com/docs/providers/vmware

How to setup the Vagrant VMware provider<br><
https://michaelnieto.com/vmware-vagrant-provider/

How to Use Vagrant with VMware Workstation Pro 16 on Ubuntu 20.04 LTS<br>
https://linuxhint.com/vagrant-vmware-workstation-pro-16/

> Require VMware Workstation Pro (=> vmrun utility)

Prerequisites:
 * Install VMware Workstation Pro

 * install Vagrant VMware Utility Installation https://www.vagrantup.com/vmware/downloads

```bash
   $ sudo apt install ./vagrant-vmware-utility_1.0.20_x86_64.deb`
```

 * Install the Vagrant VMware Plugin


```bash
   $ vagrant plugin install vagrant-vmware-desktop
```

vmware configuration<br>
https://www.vagrantup.com/docs/providers/vmware/configuration



## Provisioning

https://www.vagrantup.com/docs/provisioning


https://www.vagrantup.com/docs/provisioning/basic_usage.html (Run once or always)


Vagrant has built-in support for automated provisioning (provisioner : shell, puppet, chef, salt etc...).

You can specify as many different provisioners as you’d like, or use multiple of the same provision, which is sometimes useful for many shell scripts.

Vagrant will provision the guest machine using each provisioner in the order they’re specified.

If you define provisioners at multiple "scope" levels (such as globally in the configuration block, then in a multi-machine definition, then maybe in a provider-specific override), then the outer scopes will always run before any inner scopes.


Provisioners are run in three cases: the initial `vagrant up`, `vagrant provision`, and `vagrant reload --provision`.


```bash
# bring up your environment and explicitly not run provisioners by specifying --no-provision
vagrant up --no-provision

# flag must be present to force provisioning on subsequent start
vagrant up --provision

# restart the virtual machine and force provisioning
vagrant reload --provision

# vagrant provision is used on a running environment.
vagrant provision

```

Optionally, you can configure provisioners to run on every up or reload. They will only be not run if the `--no-provision` flag is explicitly specified. To do this set the run option to "always" (e.g. `run: always`)

The `--provision-with` flag can be used if you only want to run a specific provisioner if you have multiple provisioners specified. The arguments to `--provision-with` can be the provisioner type (such as "shell") or the provisioner name (such as "bootstrap" from above).

> Provisioners can also be named, allowing to select specific provisionner to use: `--provition-with name`

### File provisionner

https://www.vagrantup.com/docs/provisioning/file.html

The Vagrant file provisioner allows you to upload a file or directory from the host machine to the guest machine.

**NB** cannot specify destination rights or owner

```ruby
config.vm.provision "file", source: "files/sshd_config", destination: "/tmp/sshd_config"
```

### Shell provisionner


https://www.vagrantup.com/docs/provisioning/shell

> **NB** shell provisionner is run as root by default
>  * `privileged` (boolean) - Specifies whether to execute the shell script as a privileged user or not (sudo). By default this is "true".
> can use su $user -c cmd


```bash

# run shell provisionner only
vagrant provision --provision-with shell


# run a specific provisionner
vagrant provision --provision-with backup_net
```

#### define inline shell script with variables

TIPS How To Use Heredoc in Ruby
https://www.rubyguides.com/2018/11/ruby-heredoc/

```ruby

# define variable with inline shell scripts
set_route = <<-SCRIPT
ip route del default
ip route add 10.0.0.0/24 via 10.0.2.2 dev enp0s3
ip route add default via 192.168.53.1 dev enp0s8
SCRIPT

# define /etc/hosts
set_hosts = <<-SCRIPT
<<IN cat >> /etc/hosts
192.168.53.60 oc-controller
192.168.53.100 services
192.168.53.61 oc-master
192.168.53.62 oc-node-1
192.168.53.63 oc-node-2
# 10.0.0.4      quay.io
IN
SCRIPT

fix_docker = <<-SCRIPT
rm -f /etc/docker/daemon.json
SCRIPT

# NB using quoted begin delimiter disable interpolation
msg2 = <<-'MSG'
    echo "With a
     little help
     of my friend" #{$hostname}
    MSG

```

#### inline scripts

```ruby

   # using inline scripts defined by internal variables
   config.vm.provision "shell", inline: set_route, run: "always"
   config.vm.provision "shell", inline: set_hosts

   config.vm.provision "shell", inline: <<-SHELL
      su vagrant -c 'mkdir -p ~/git'
      su vagrant -c 'cd ~/git && git clone http://go.lab.fr/EDGE/openshift-ansible.git'
   SHELL

  # block-based syntax, allow with more than a couple options can greatly improve readability.
  config.vm.provision "shell" do |s|
    s.inline = "echo $2 $1 >> /etc/hosts"
    s.args   = [$hostname, $ip] # use an array or a string to describe args
  end

  config.vm.provision "shell" do |s|
    s.inline = "hostnamectl set-hostname $1"
    s.args   = [hostname] # use an array or a string to describe args
  end

  config.vm.provision "shell", inline: 'echo "inline setup $*"', args: ['one',2]


```

#### external scripts

```ruby
# External Script : shell script to upload and execute
config.vm.provision "backup_net", type: "shell",  path: "script.sh", privileged: false

config.vm.provision "shell", path: script, args: [true]

```

### ansible provisionner

Ansible and Vagrant<br>
https://www.vagrantup.com/docs/provisioning/ansible_intro

Shared Ansible Options<br>
https://www.vagrantup.com/docs/provisioning/ansible_common

2 available provisioners :
 * [ansible](https://www.vagrantup.com/docs/provisioning/ansible) :  where Ansible is executed on the Vagrant host
 * [ansible_local](https://www.vagrantup.com/docs/provisioning/ansible_local) : where Ansible is executed on the Vagrant guest

#### ansible with Auto-Generated Inventory

```ruby
config.vm.provision "ansible" do |ansible|
 ansible.playbook = "playbook.yml"
end

```

When Vagrant runs, it generates an Ansible inventory file named `.vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory`


 * `host_vars` option can be used to set variables for individual hosts in the generated inventory file
 * `groups` option can be used to pass a hash of group names and group members to be included in the generated inventory file.
 * `config_file` : path to an Ansible Configuration file (ansible.cfg) , by default use `ansible.cfg` found in the same dir as the used Vagrantfile.

> NB For multi machine configuration, Ansible aliases ( e.g.  vagrant1 , vagrant2 , vagrant3 ) defined in inventory match the names assigned to the machines in the Vagrantfile.

Vagrant is running ansible-playbook once for each virtual machine, and it uses the `--limit` flag so that the provisioner runs against only a single virtual machine at a time.

Sample of auto-generated inventory with additional groups and hosts vars

```ruby
    node.vm.provision "ansible" do |ansible|
      ansible.playbook = "ansible/playbook.yml"
      ansible.config_file = "ansible/ansible.cfg"
      ansible.limit = "all"  # run the provisioning on the last VM, apply playbook to all inventory hosts
      ansible.host_vars = {
        master0: { offline: true , version: 1.2 }
      }
      ansible.groups = {
        worker: [ "app0" ],
        "worker:vars": { zoom: "on" }
      }

```

#### ansible with Static Inventory

With the `inventory_path` option, you can reference a specific inventory resource. Vagrant will then use this inventory information instead of generating it.

```ruby
  node.vm.provision "ansible" do |ansible|  # using a specific inventory
    ansible.playbook = "ansible/playbook.yml"
    ansible.inventory_path = "ansible/hosts"
  end
```


### triggers

Vagrant is capable of executing machine triggers before or after Vagrant commands.

https://www.vagrantup.com/docs/triggers
https://www.vagrantup.com/docs/triggers/usage

> Triggers can be defined at global scope (for all guest machines) or also  within the scope of guests in a Vagrantfile.

```ruby
# multiple commands for this trigger
config.trigger.before [:up, :destroy, :halt, :package] do |trigger|
...
end

```

The trigger config block takes two different operations that determine when a trigger should fire:

  * `before`
  * `after`

Trigger action command (symbol or array) : is the first argument that comes after either `before` or `after` operation. valid action commands for triggers :
 * destroy, halt, provision, reload, resume, suspend, up


Trigger options:
 * info (string) - A message that will be printed at the beginning of a trigger.
 * name (string) - The name of the trigger. If set, the name will be displayed when firing the trigger.
 * run (hash) - A collection of settings to run a inline or remote script on the host.
 * run_remote (hash) - A collection of settings to run a inline or remote script with on the guest.




## Vagrant plugins


### Available Vagrant Plugins

https://github.com/hashicorp/vagrant/wiki/Available-Vagrant-Plugins


 * *vagrant-proxyconf* : Configures the VM to use proxies -- http://tmatilai.github.io/vagrant-proxyconf/
 * *vagrant-hostmanager* : manages the hosts file on guest machines (and optionally the host) -- https://github.com/smdahlen/vagrant-hostmanager
 * *Sahara* : snapshot support -- https://github.com/jedi4ever/sahara
 * *vagrant-vbguest* : automatically update VirtualBox guest additions if necessary -- https://github.com/dotless-de/vagrant-vbguest
 * *vagrant-disksize* : Resize disk for VirtualBox machines -- https://github.com/sprotheroe/vagrant-disksize
 * *vagrant-vbinfo* : Vagrant plugin for outputting detailed VirtualBox information (Mac and Linux only) -- https://github.com/miroswan/vbinfo
 * *vagrant-persistent-storage* : creates a persistent storage and attaches it to guest machine -- https://github.com/kusnier/vagrant-persistent-storage
 * *vagrant-sshfs : adds synced folder support for mounting folders from the Vagrant host into the Vagrant guest via SSHFS. -- https://github.com/dustymabe/vagrant-sshfs



### vagrant plugin command

```bash

# list vagrant installed plugin
vagrant plugin list
# install plugin
vagrant plugin install vagrant-hostmanager

```



```bash
vagrant plugin help
```

    [0mUsage: vagrant plugin <command> [<args>]

    Available subcommands:
         expunge
         install
         license
         list
         repair
         uninstall
         update

    For help on any individual command run `vagrant plugin COMMAND -h`
            --[no-]color                 Enable or disable color output
            --machine-readable           Enable machine readable output
        -v, --version                    Display Vagrant version
            --debug                      Enable debug output
            --timestamp                  Enable timestamps on log output
            --debug-timestamp            Enable debug output with timestamps
            --no-tty                     Enable non-interactive output
    [0m


```ruby
# Vagrantfile

# test if plugin is installed set config
  if Vagrant.has_plugin?("vagrant-vbguest")
    config.vbguest.auto_update = false # use cli command to update vbox guest additions
  end

# check if plugin is installed otherwise abort execution
unless Vagrant.has_plugin?("vagrant-disksize")
  abort 'vagrant-disksize is not installed! Run vagrant plugin install vagrant-disksize and relauch vagrant'
end


```

#### vbguest


Using vbguest CLI

`vagrant vbguest [vm-name] [--do start|rebuild|install] [--status] [-f|--force] [-b|--auto-reboot] [-R|--no-remote] [--iso VBoxGuestAdditions.iso] [--no-cleanup]`

 * `--do COMMAND`         Manually `start`, `rebuild` or `install` GuestAdditions.
 * `--status`             Print current GuestAdditions status and exit.
 * `-f, --force`          Whether to force the installation. (Implied by --do start|rebuild|install)
 * `-b, --auto-reboot`    Allow rebooting the VM after installation. (when GuestAdditions won't start)
 * `-R, --no-remote`      Do not attempt do download the iso file from a webserver
 * `--iso file_or_uri`    Full path or URI to the VBoxGuestAdditions.iso
 * `--no-cleanup`         Do not run cleanup tasks after installation. (for debugging)

```bash
# display virtualbox guest additions status in VM
vagrant vbguest --status

# manually run vbguest additions installation or upgrade
vagrant vbguest --do install

```

```yaml

if Vagrant.has_plugin?("vagrant-vbguest")
    config.vbguest.auto_update = false # => use cli command to update vbox guest additions
end
```




#### sahara

```bash
# use vagrant sandbox https://github.com/jedi4ever/sahara

# enter sandbox mode : create snapshot sahara-sandbox
vagrant sandbox on

# apply the change : recreate snapshot sahara-sandbox with the current state
vagrant sandbox commit

# rollback to the previous commit : restore snapshot sahara-sandbox
vagrant sandbox rollback

# exit sanbox mode : remove snapshot sahara-sandbox
vagrant sandbox exit


```

## Tips

### run command in vagrant box from host

```bash
vagrant ssh -- cat /etc/hosts

# -c execute a ssh command directly
vagrant ssh -c "hostname -I | cut -d' ' -f2" 2>/dev/null
```

### ssh connexion without using vagrant ssh

```bash
User=vagrant
Port=2200
IdentityFile=~/.vagrant.d/insecure_private_key
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=ERROR $User@localhost -p $Port -i $IdentityFile

```
> Use `-o IdentitiesOnly=yes`  to Fix “SSH Too Many Authentication Failures”

### shell command in ruby


```ruby
# Run shell command & get output in ruby
# Built-in syntax, %x( cmd )
$itf_bridge = %x(ip route | awk '/^default/ {print $5}').chomp
$default_route = %x(ip route | awk '/^default/ {print $3}').chomp


# transform string in array of string
config.hostmanager.aliases = %w(example-box.localdomain example-box-alias)  # array of string

```


### Testing

```bash

# test if vbox guest additions are installed
vagrant ssh -- lsmod | grep vboxguest
vagrant ssh -- lsmod | grep vboxsf

vagrant ssh -- systemctl status vboxadd-service

ls /lib/modules/$(uname -r)/misc  # vboxguest.ko  vboxsf.ko  vboxvideo.ko

# test if vagrant is mount
vagrant ssh -- mount | grep -c '/vagrant '
vagrant ssh -- df -h | grep '/vagrant'

```

###  Shell Scripting


```bash

# retrieve vagrant ssh options as shell variables
# HostName, User, Port, UserKnownHostsFile, StrictHostKeyChecking, PasswordAuthentication,
# IdentityFile, IdentitiesOnly, LogLevel

ssh_params=$(vagrant ssh-config)
while read name value ; do
echo name is $name
echo value is $value
eval $name=$value
done < <(echo "$ssh_params")

```


### disable nat dns

Use case : use a private network and associated dns

=> disable nat interface dns retrieved by dhcp

add shell provisioning




### temporary ssh port forwarding


```bash

# The service running on the vagrant guest on TCP port 8000 is accessible by connecting to 3000 on the host system
vagrant ssh -- -L 3000:localhost:8080
```

### Vagrant debugging


To enable all Vagrant logs set environment variable VAGRANT_LOG to the desire log level (for instance `VAGRANT_LOG=debug`).<br>
*To change log level* (debug, info, warn, error) :

```bash
$ VAGRANT_LOG=info vagrant up
```

### Test sub-command in Vagrantfile

```ruby
 if ARGV[0] == "up" or ARGV[0] == "provision"
   puts "shell provisioning #{HOST_IP}"
 end

```

Define sub-command to display debug info

```ruby
# define special command : vagrant debug to display variables value
if ARGV[0] == "info"
  debug  # call debug function to display config informations
  abort ""
end
```


```bash

```
