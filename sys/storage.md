[[_TOC_]]

# Introduction


## Storage concepts


Storage Concepts And Technologies Explained In Detail<br>
https://www.ostechnix.com/storage-concepts-and-technologies-explained-in-detail/


### storage solution

 * **DAS** : Direct Attachement Storage, collection of disks, which are directly attached to Host


 * **NAS** : Network Attached Storage is basically a low-level network storage device, ideal for serving media files across a network as a repository for office files and other projects. (Files server using `nfs`,`smb/cifs`)


 * **SAN** : Storage Area Network most commonly deployed SAN is based on iSCSI, which is SCSI over TCP/IP

### storage type


 * Block Storage: Data is stored in “logical blocks” these blocks are smallest units of storage with addresses attached to them in any storage subsystem. Disk level read/write operations can be used for block storage and block storage access.


 * File Storage: Any data file is nothing but collection of “block of blocks” of data. Any file typically will contain two parts:
    * *Meta data* of a file which stores the directory structure and information about the file.
    * *File content* which contains the actual file content part of the data. File storage leads to File Systems, which will have directories, files, regular files and etc file related meta data inside them. These File Systems are logically arranged for ease of access and data operation.


### storage interface

 * ATA (Advanced Technology Attachment) standard interface or IDE or PATA (Parallel ATA)
 * SATA : Serial ATA
 * SCSI (Small Computer System Interface) NB pronounced as “scuzzy”
 * SAS : Serial Attached SCSI
 * iscsi
 * fiber-channel
 *  NVMe (NVM express) open logical device interface specification for accessing non-volatile storage media attached via a PCI Express (PCIe) bus (or M.2 slot). <br>Using this bus allows the SSD to communicate directly with the CPU instead of going through the motherboard as SATA previously required, increasing read/write speeds and decreasing program load times.


What’s The Difference Between SATA And SAS Hard Drives? :<br> https://www.pickaweb.co.uk/kb/difference-between-sata-sas-hard-drives/

### storage hardware

 * HDD: Hard-Disk Drive (traditional ‘spinning platter hard drives’)


 * SSD : Solid-State Drive (NAND-based)
    NB SSD initially were using SATA or SAS buses


 * NVMe drive : Non-Volatile Memory (NVM), including solid-state drives (SSDs), PCI Express (PCIe) add-in cards, M.2 card
   * M.2 : newest form factor of SSD that slots in a M.2 slot connected via NVMe to the CPU.


 * NVMeoF Non-Volatile Memory Express over Fabric : NVMe drives across an NVMeoF network.
   By design, NVMe drives are intended to provide local access to the machines they are plugged in to; however, the NVMe over Fabric specification seeks to address this very limitation by enabling remote network access to that same device.

How To Find If The Disk Is SSD Or HDD In Linux<br>
https://www.ostechnix.com/how-to-find-if-the-disk-is-ssd-or-hdd-in-linux/


M.2 vs. PCIe (NVMe) vs. SATA SSDs: What’s the Difference?<br>
https://www.performance-computer.com/learn/ssds-compared/

NVMe vs SSD: Speed, Storage & Mistakes to Avoid<br>
https://www.promax.com/blog/nvme-vs-ssd-speed-storage-mistakes-to-avoid


NVMe vs M.2: What's the difference?<br>
https://www.redhat.com/sysadmin/nvme-vs-m2



drive form factor :
 * 3.5"
 * 2.5"
 * M.2




#### iScsi

LUN : Logical Unit Number
https://www.techopedia.com/definition/321/logical-unit-number-lun

How to Setup iSCSI Storage Server on Ubuntu 18.04 LTS
https://www.howtoforge.com/how-to-setup-iscsi-storage-server-on-ubuntu-1804/

How to Setup an iSCSI Storage Server on Ubuntu 20.04 LTS
https://www.howtoforge.com/tutorial/how-to-setup-iscsi-storage-server-on-ubuntu-2004-lts/

How to Find WWN, WWNN and WWPN Number of HBA Card in Linux
https://www.2daygeek.com/how-to-find-wwn-wwnn-wwpn-number-of-hba-card-in-linux/

How to scan\detect new LUN’s & SCSI disks in Linux?
https://www.2daygeek.com/scan-detect-luns-scsi-disks-on-redhat-centos-oracle-linux/

## storage performance

Storage performance: IOPS, latency and throughput<br>
https://www.opvizor.com/storage-performance-iops-latency-and-throughput

Understanding IOPS, Latency and Storage Performance<br>
https://louwrentius.com/understanding-iops-latency-and-storage-performance.html

What is Latency? And How is it Different from IOPS?<br>
https://storageswiss.com/2013/12/10/what-is-latency-and-how-is-it-different-from-iops/

When bandwidth and storage size matters: Bits vs. bytes<br>
https://www.redhat.com/sysadmin/bits-vs-bytes


**Throughput** measures how many units of information a system can process in a period of time. It can refer to the number of I/O operations per second, but is typically measured in bytes per second. IO rate a certain disk can deliver. This number is usually expressed in Megabytes / Second (MB/s)

**I/O latency** is defined simply as the time that it takes to complete a single I/O operation. latency is measured in milliseconds (ms) and should be as low as possible. latency is a measure of the length of time it takes for a single I/O request to be completed from the application's point of view.

**IOPS** (input/output operations per second) is the standard unit of measurement for the maximum number of reads and writes to non-contiguous storage locations. IOPS is pronounced EYE-OPS. Also the amount of read or write operations that could be done in one seconds time.

> Average IO size x IOPS = Throughput in MB/s


How to stress test your Linux system (stress --io)<br>
https://www.networkworld.com/article/3563334/how-to-stress-test-your-linux-system.html


# tools

## disk tools

### smartmontools

Monitor and Analyze Hard Drive Health with Smartctl in Linux<br>
https://www.linuxtechi.com/smartctl-monitoring-analysis-tool-hard-drive/

How to Check SSD/HDD health in Linux (07/2021)<br>
https://www.howtoforge.com/how-to-check-ssd-hdd-health-in-linux/


`smartctl` - Control and Monitor Utility for SMART Disks

smartctl controls the Self-Monitoring, Analysis and Reporting Technology (SMART) system built into most ATA/SATA and SCSI/SAS hard drives and solid-state drives.

```bash
$ sudo apt-get install smartmontools
$ sudo smartctl -i /dev/xxx
```


### hdparm

How To Find Hard Disk Data Transfer Speed In Linux (06/2021)  using hdparm
https://ostechnix.com/how-to-find-hard-disk-data-transfer-speed-in-linux/

`hdparm` - get/set SATA/IDE device parameters

hdparm provides a command line interface to various kernel interfaces supported by the Linux SATA/PATA/SAS "libata" subsystem and the older IDE driver subsystem.
 + `-I` : Request identification info directly from the drive
 + `-i` : Display the identification info which the kernel drivers (IDE, libata) have stored  from  boot/configuration  time.
 + `-t` : Perform timings of device reads for benchmark and comparison  purposes.

```bash
# information on disk
sudo hdparm -i /dev/sda

# detailed information on disk
sudo hdparm -I /dev/sda

###buffered and cached disk read speed
# Buffered disk read test for /dev/sda ##
sudo hdparm -t /dev/sda1
# Cache read benchmark for /dev/sda ###
sudo hdparm -T /dev/sda1

# Cache read benchmark for /dev/sda ###
sudo hdparm -tT /dev/sda1

```


## dd

A `dd` command is a command-line utility that is main work is used to convert and copy, and It stands for “Data Duplicator”.


Using dd as a Swiss Army knife<br>
https://www.linuxprogrammingblog.com/dd-as-a-swiss-army-knife

How dd command works in Linux with examples (05/2020)<br>
https://linuxconfig.org/how-dd-command-works-in-linux-with-examples

La commande dd
https://wiki.debian-fr.xyz/La_commande_dd

`dd` - (data duplicator) convert and copy a file<br>
`dd [OPREAND] : dd if=path/to/input_file of=/path/to/output_file bs=block_size count=number_of_blocks`
  + `bs=BYTES` : (block size) read and write up to BYTES bytes at a time (default: 512) unit: K,M,G
  + `count=N` : copy only N input blocks
  + `if=FILE` : read from FILE instead of stdin
  + `of=FILE` : write to FILE instead of stdout
  + `status=progress` : display progress bar
  + `seek=N` : skip N obs-sized blocks at start of output
  + `skip=N` : skip N ibs-sized blocks at start of input
  + `iflag|oflag=flags`
      * `direct` (use direct I/O for data)  oflag=direct and/or iflag=direct to completely bypass OS buffering during operation
      * `dsync` (use synchronized I/O for data).  causes flushing data to the medium
        NB This option get rid of caching and gives you good and accurate results
      * `sync` (dsync likewise, but also for metadata)
  + `conv` : options of dd is used to apply data conversions
    * `fdatasync`: physically write output file data before finishing or tells dd to require a complete “sync” once, right before it exits. This option is equivalent to oflag=dsync.
    * `fsync`: fdatasync likewise, but also write metadata

### dd use cases

```bash
# compact disk size
dd if=/dev/zero of=/EMPTY bs=1M ; rm /EMPTY

# create a plain empty disk image file
dd if=/dev/zero of=/home/disk1.img bs=512K count=20000

# create a disk image file (dynamic allocation)
dd if=/dev/zero of=/home/disk2.img bs=512K count=0 seek=20000

# create bootable usb using iso file
dd if=image.iso of=/dev/sdb bs=4M status=progress oflag=sync

# backup sd card to image file
dd bs=4M if=/dev/sdX of=from-sd-card.img count=xxx

# Compressing the data read by dd
sudo dd if=/dev/sda bs=1M | gzip -c -9 > sda.dd.gz

# Restoring
gzip -cd sda.dd.gz | dd of=/dev/sdXn

# show MBR
sudo dd if=/dev/sdb bs=512 count=1 |hexdump -C

# backup  hidden data between the MBR and the first partition on the disk
sudo dd if=/dev/sda of=hidden-data-after-mbr count=2047 skip=1

# erase all partition on media ( erase the first 512*4096=2MB  )
sudo dd if=/dev/zero of=/dev/sdb bs=512 count=4096

```

## sync


Hard Disk Cache Settings

`sync` - Synchronize cached writes to persistent storage


## benchmarking disk performance

How to benchmark Disk performance on Linux (06/2020) hdparm/dd<br>
https://linuxconfig.org/how-to-benchmark-disk-performance-on-linux

How to measure disk performance with fio and IOPing (01/2017)<br>
https://www.unixmen.com/how-to-measure-disk-performance-with-fio-and-ioping/

Linux File System Read Write Performance Test (using dd, hdparm,  iozone, bonnie+) (11/2012)<br>
https://www.slashroot.in/linux-file-system-read-write-performance-test

SSHFS – Installation and Performance (iozone)<br>
http://www.admin-magazine.com/HPC/Articles/Sharing-Data-with-SSHFS


> dd is the not the recommended tool for benchmarking I/O performance.
  * it is a single-threaded, sequential-write test.
  * it writes a small amount of data
  * it executes for just a few seconds


How to stress test your Linux system (stress --io)<br>
https://www.networkworld.com/article/3563334/how-to-stress-test-your-linux-system.html


### using dd

Linux I/O Performance Tests using dd (02/2016)<br>
https://www.thomas-krenn.com/en/wiki/Linux_I/O_Performance_Tests_using_dd

Test read/write speed of usb and ssd drives with dd command on Linux (01/2019)<br>
https://www.binarytides.com/linux-test-drive-speed/

Linux and Unix Test Disk I/O Performance With dd Command (08/2020)<br>
https://www.cyberciti.biz/faq/howto-linux-unix-test-disk-performance-with-dd-command/

Check storage performance with dd (05/2019)<br>
https://fedoramagazine.org/check-storage-performance-with-dd/

Modern operating systems do not normally write files immediately to RAID systems or hard disks. Temporary memory that is not currently in use will be used to cache writes and reads

So that I/O performance measurements will not be affected by these caches (temporary memory), the oflag parameter can be used.
 * direct (use direct I/O for data) NB  direct I/O for data which will eliminate most if not all of the caching the operating system does
 * dsync (use synchronized I/O for data)
 * sync (likewise, but also for metadata)

> When using if=/dev/zero and bs=1G, Linux will need 1GB of free space in RAM

```bash
# Throughput With Cache : One gigabyte was written for the test,
dd if=/dev/zero of=/root/testfile bs=1G count=1 oflag=direct
# Throughput Without Cache => retry this with the cache deactivated : hdparm -W0 /dev/sda

# 512 bytes were written one thousand times,
dd if=/dev/zero of=/root/testfile bs=512 count=1000 oflag=direct

#-------
## Use the dd command to measure server throughput (write speed)
dd if=/dev/zero of=/tmp/test1.img bs=1G count=1 oflag=dsync # oflag=dsync <=> conf=fdatasync

## Use the dd command to measure server latency
dd if=/dev/zero of=/tmp/test2.img bs=512 count=1000 oflag=dsync

## amount of data size thats larger than the RAM.
dd if=/dev/zero of=speedtest bs=64k count=3200 conv=fdatasync


# flush cache
echo 3 | sudo tee /proc/sys/vm/drop_caches
# Use dd command on Linux to test read speed
time dd if=/path/to/bigfile of=/dev/null bs=8k

# to test real disk I/O and not memory add sync option as follows
time sh -c “dd if=/dev/zero of=/tmp/testfile bs=100k count=1k && sync”

##-------
## # Basic write/read speed test on usb drive
# => disable i/o cache with mount option
# Write speed
dd if=/dev/zero of=./largefile bs=1M count=1024
# Read speed
sudo sh -c "sync && echo 3 > /proc/sys/vm/drop_caches"
dd if=./largefile of=/dev/null bs=4k

## or use conv=fdatasync option that causes flushing data to the medium


```

### using iozone

How To Measure Linux Filesystem I/O Performance With iozone<br>
https://www.cyberciti.biz/tips/linux-filesystem-benchmarking-with-iozone.html

https://www.iozone.org/

`Iozone` - Filesystem Benchmark
 The benchmark generates and measures a variety of  file  operations.
 The benchmark tests file I/O performance for the following operations :
  Read, write, re-read, re-write, read backwards, read strided, fread, fwrite,  random  read/write, pread/pwrite variants
 + `-a` : Used  to  select  full  automatic  mode.  Produces output that covers all tested file operations for record sizes of 4k to 16M for file sizes of 64k to 512M
 + `-R` : Generate Excel report.
 + `-b filename` : Used to specify a filename that will be used for output of an Excel compatible file that contains the results.

 + `-i #` : Used  to  specify which tests to run. (0=write/rewrite, 1=read/re-read, 2=random-read/write, 3=Read-backwards, 4=Re-write-record, 5=stride-read, 6=fwrite/re-fwrite, 7=fread/Re-fread, 8=mixed workload,9=pwrite/Re-pwrite, 10=pread/Re-pread, 11=pwritev/Re-pwritev, 12=preadv/Re-preadv).
 + `-r #` : Used to specify the record size, in Kbytes, to test. One may also specify -r #k (size in Kbytes)  or -r #m (size in Mbytes) or -r #g (size in Gbytes).
 + `-s #` : Used to specify the size, in Kbytes, of the file to test.  One  may  also  specify  -s #k  (size  in Kbytes) or -s #m (size in Mbytes) or -s #g (size in Gbytes).
 + `-l #` : Set the lower limit on number of processes to run.
 + `-u #` : Set the upper limit on number of processes to run.

> All the output are in KB/Sec

```bash

### From Sharing-Data-with-SSHFS

# sequential write test using IOzone.
iozone -i 0 -r 64k -s 16G -w -f iozone.tmp > iozone_16G_w.out
#  sequential read tests
iozone -i 1 -r 64k -s 16G -f iozone.tmp > iozone_16G_r.out
# random IOPS testing
iozone -i 2 -r 4k -s 8G -w -O -f iozone.tmp > iozone_8G_random_iops.out
```

### using bonnie

Benchmark disk IO with DD and Bonnie++<br>
https://www.jamescoyle.net/how-to/599-benchmark-disk-io-with-dd-and-bonnie


Bonnie++ is a small utility with the purpose of benchmarking file system IO performance.

-d – is used to specify the file system directory to use to benchmark.
-u – is used to run a a particular user. This is best used if you run the program as root. This is the UID or the name.
-g – is used to run as a particular group. This is the GID or the name.
-r – is used to specify the amount of RAM in MB the system has installed. This is total RAM, and not free RAM. Use free -m to find out how much RAM is on your system.
-b – removes write buffering and performs a sync at the end of each bonnie++ operation.
-s – specifies the dataset size to use for the IO test in MB.

```bash
sudo apt install bonnie++

bonnie++ -d /tmp -r 2048 -u james

```

### using IOping

Measure disk latency with IOPing

`ioping` - simple disk I/O latency monitoring tool
 + `-c count` : Stop after count requests.
 + `-i interval` : Set time between requests to interval (1s).
 + `-p period` : Print raw statistics for every period requests (see format below).


```bash
# shows the latency measures of the disk.
ioping -c 100 .

99 requests completed in 22.6 ms, 396 KiB read, 4.39 k iops, 17.1 MiB/s
generated 100 requests in 1.65 min, 400 KiB, 1 iops, 4.04 KiB/s
min/avg/max/mdev = 65.8 us / 227.8 us / 413.6 us / 59.2 us

# min minimal request time (nanoseconds)
# avg average request time (nanoseconds)
# max maximum request time (nanoseconds)
# mdev request time standard deviation (nanoseconds)

ioping -p 100 -c 200 -i 0 .
```

### using fio

Testing IOPS with fio

```bash
# RW Performance
fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=random_read_write.fio --bs=4k --iodepth=64 --size=4G --readwrite=randrw --rwmixread=75

# Random read performance
 fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=random_read.fio --bs=4k --iodepth=64 --size=4G --readwrite=randread

# Random write performance
fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=random_write.fio --bs=4k --iodepth=64 --size=4G --readwrite=randwrite

```


### Disk I/O monitoring


How to Monitor Disk I/O performance in Linux (iotop,iostat) (01/2021)<br>
https://www.2daygeek.com/check-monitor-disk-io-linux/

6 best tools to monitor disk IO performance in Linux (03/2012) iotop,iostat,vmstat,atop,dstat,ioping<br>
https://www.2daygeek.com/linux-disk-io-performance-monitoring/

#### iotop

iotop is similar to top command utility (simple top-like I/O monitor), for displaying real-time disk activity.

iotop watches I/O usage information from the Linux kernel and displays a table of current I/O usage through processes or threads on the system.

It displays the I/O bandwidth read and written from each process/thread. It also displays the percentage of time the thread/process spent while swapping or waiting on I/O.

To check which processes are actually utilizing the disk IO, run the iotop command with -o or –only option to visualize it.

 * `IO`: It shows Input/Output utilization of each process, which includes disk and swap.
 * `SWAPIN`: It shows only the swap usage of each process.

```bash
iotop -o

```


#### iostat

`iostat` is used to report Central Processing Unit (CPU) statistics and input/output statistics for devices and partitions.

The iostat command is used to monitor system input/output device loading by observing the time the devices are active in relation to their average transfer rates.

The iostat command generates two types of reports,

 * CPU Utilization report
 * Device Utilization report.

install from `sysstat` system package


##### /proc/diskstats

How to Read Linux’s /Proc/Diskstats Easily
https://www.xaprb.com/blog/2010/05/14/how-to-read-linuxs-procdiskstats-easily/


# disk management

Adding a new storage medium =>
 * install the new hard drive
 * partition the new disk
 * create the file systems on the new disk
 * mount the file system


## block devices

https://en.wikipedia.org/wiki/Device_file

The Linux system identifies hardware devices as special files, called device files.
Applications often access devices by way of special files created within `/dev/`. These are special files that represent disk drives (for instance, `/dev/hda` and `/dev/sdc`), partitions (`/dev/hda1` or `/dev/sdc3`).

`udev` (userspace /dev) is a device manager for the Linux kernel, it manages device nodes in the /dev directory by adding, symlinking and renaming them.

Each hardware peripheral device is represented under Unix with a special file, usually stored in the file tree under `/dev/` (DEVices).

Two types of special files exist according to the nature of the device: “character mode” and “block mode” files, each mode allowing for only a limited number of operations. While character mode limits interaction with read/write operations, block mode also allows seeking within the available data.

Block files are for devices that can handle data in large blocks at a time, such as disk drives.

Naming convetions:

 * `/dev/hd` : (“classic”) IDE driver (previously used for ATA hard disk drive, `hda`: the master device on the first ATA channel
 * `/dev/sd` :  mass-storage SCSI driver, `sda`: first registered device
 * `/dev/sg` : generic SCSI layer
 * `/dev/sr` : ROM” driver (data-oriented optical disc drives; scd is just a secondary alias)
 * `/dev/nvme` : storage driver for NVMe, nvme0n1  first registered device's first namespace (block device)
 * `/dev/mmcblk` : storage driver for MMC Media (SD Cards, eMMC chips), mmcblk0: first registered device
 * `/dev/loop` : loopback devices
 * `/dev/dm` : device mapper used for LVM or dm-crypt (maps physical block devices onto higher-level virtual block devices)



### block devices listed in sysfs


The sysfs filesystem is a pseudo-filesystem which provides an interface to kernel data structures. The files under sysfs provide information about devices, kernel modules, filesystems, and other kernel components.

The sysfs filesystem is commonly mounted at `/sys`.

 * `/sys/block` :  This subdirectory contains one symbolic link for each block device that has been discovered on the system.  The symbolic links point to corresponding directories under /sys/devices.


```bash
ls /sys/block/
```


```bash
set -v

ls /sys/block

set +v
```


    ls /sys/block
    [0m[01;36mdm-0[0m  [01;36mloop0[0m   [01;36mloop11[0m  [01;36mloop14[0m  [01;36mloop17[0m  [01;36mloop4[0m  [01;36mloop7[0m  [01;36mnvme0n1[0m
    [01;36mdm-1[0m  [01;36mloop1[0m   [01;36mloop12[0m  [01;36mloop15[0m  [01;36mloop2[0m   [01;36mloop5[0m  [01;36mloop8[0m
    [01;36mdm-2[0m  [01;36mloop10[0m  [01;36mloop13[0m  [01;36mloop16[0m  [01;36mloop3[0m   [01;36mloop6[0m  [01;36mloop9[0m

    set +v


### list devices using lsblk

`lsblk` - list block devices

`lsblk`  prints all block devices (except RAM disks) in a tree-like format by default.<br>
When used with the `-f` option, it prints file system type on partitions as well (NAME MAJ:MIN RM SIZE RO TYPE MOUNTPOINT)
 * `-f, --fs` : Output info about filesystems. LABEL UUID FSTYPE (<=> -o NAME,FSTYPE,LABEL,UUID,MOUNTPOINT)
 * `-l, --list` : Produce output in the form of a list.
 * `-a, --all` : Also list empty devices (.e.g /dev/loopx not associated).
 * `-o, --output list` : Specify which output columns to print.
NAME KNAME MAJ:MIN FSTYPE MOUNTPOINT LABEL UUID PARTTYPE PARTLABEL PARTUUID PARTFLAGS RA RO RM HOTPLUG MODEL SERIAL SIZE STATE OWNER GROUP MODE ALIGNMENT MIN-IO OPT-IO PHY-SEC LOG-SEC ROTA SCHED RQ-SIZE TYPE DISC-ALN DISC-GRAN DISC-MAX DISC-ZERO WSAME WWN RAND PKNAME HCTL TRAN SUBSYSTEMS  REV VENDOR ZONED


```bash
lsblk -f

# specify output columns
lsblk -lf -o +MAJ:MIN,RM,SIZE,RO,TYPE

# show specific device
lsblk /dev/sda
```

### loopback devices

How to use loop devices<br>
https://sleeplessbeastie.eu/2017/07/03/how-to-use-loop-devices/

A loopback device is a pseudo-device that makes a file accessible as a block device.

`losetup` - set up and control loop devices
losetup is used to associate loop devices (/dev/loop) with regular files or block devices, to detach loop devices, and to query the status of a loop device.
 + `-f, --find [file]` : Find the first unused loop device. If a file argument is present, connect the found loop device to the specified file.
 + `-d` loopdev` : Detach a loop device
 + `-D` : Detach all associated loop devices:
 + `-l loopdev` : Show loop device


```bash
# display first loop device unused (/dev/loop<x>)
losetup -f
# extract id
ID=$(losetup -f | sed 's#/dev/loop##')

# create file for loop device
dd if=/dev/zero of=/data/disks/disk0 bs=1M count=100

# create loop file interface with the first loop device unused
losetup -f /data/disks/disk0
# <=> losetup /dev/loop${ID} /data/disks/disk0

# then /dev/loop${ID} can be used as a block device e.g. pvcreate /dev/loop${ID}

# display loop device info
losetup -l /dev/loop${ID}

# detach loop device
losetup -d /dev/loop${ID}


```

### nvme management

Data in a Flash, Part I: the Evolution of Disk Storage and an Introduction to NVMe<br>
https://www.linuxjournal.com/content/data-flash-part-i-evolution-disk-storage-and-introduction-nvme

Data in a Flash, Part II: Using NVMe Drives and Creating an NVMe over Fabrics Network <br>
https://www.linuxjournal.com/content/data-flash-part-ii-using-nvme-drives-and-creating-nvme-over-fabrics-network

```bash
# list nvme devices
ls /sys/block/|grep nvme

# nvme kernel module
lsmod|grep nvme

# using nvmecli (install system package sudo apt-get install nvme-cli)
# https://github.com/linux-nvme/nvme-cli
sudo nvme list
# list           List all NVMe devices and namespaces on machine
# list-subsys    List nvme subsystems
# id-ctrl        Send NVMe Identify Controller
# id-ns          Send NVMe Identify Namespace, display structure
# list-ns        Send NVMe Identify List, display structure

```

see also:  Accessing the Drive across a Network (using Soft RDMA over Converged Ethernet (RoCE) network on top of traditional TCP/IP)

NVMe over Fabrics Using TCP

linux kernel 5.0  introduced native TCP support for NVMe targets<br>
https://www.linuxjournal.com/content/data-flash-part-iii-nvme-over-fabrics-using-tcp


## RAID management

RAID means Redundant Array of Independent Disks. The goal of this system is to prevent data loss and ensure availability in case of hard disk failure. The general principle is quite simple: data are stored on several physical disks instead of only one, with a configurable level of redundancy. Depending on this amount of redundancy, and even in the event of an unexpected disk failure, data can be losslessly reconstructed from the remaining disks.


 * Data striping is the technique of segmenting logically sequential data, such as a single file, so that segments can be assigned to multiple physical devices.
 * A Parity bit is a bit that is added to ensure that the number of bits with the value one in a set of bits is even or odd. Parity bits are used as the simplest form of error detection code.
 * Disk mirroring is the replication of logical disk volumes onto separate physical hard disks in real time to ensure continuous availability.


RAID Levels:

 * RAID0 : stripe
 * RAID1 : mirror
 * RAID5 : Drives with parity
 * RAID6 : Drives with double parity
 * RAID01 : RAID 0+1 (stripe + mirror)
 * RAID10 : RAID 1+0 (mirror + stripe)
 * RAID50 : RAID 5+0 (parity + stripe)
 * RAID60 : RAID 6+0 (double parity + stripe)



RAID : tirer le meilleur de ses disques durs (01/2014)<br>
https://buzut.net/tirez-le-meilleur-de-vos-disques-durs-avec-raid/

Installer Debian 7 avec un RAID 1 logiciel (04/2014)<br>
https://www.it-connect.fr/installer-debian-7-avec-un-raid-1-logiciel/

Linux Software Raid 1 Setup (04/2021)<br>
https://linuxconfig.org/linux-software-raid-1-setup


`mdadm` - manage MD devices aka Linux Software RAID

RAID  devices  are  virtual devices created from two or more real block devices. This allows multiple devices (typically disk drives or partitions thereof) to be combined into a single device to hold (for example) a single  filesystem. Some RAID levels include redundancy and so can survive some degree of device failure.



```bash
mdadm [mode] <raiddevice> [options] <component-devices>

```


##  Partition table

MBR vs GPT - Things to Know When Partitioning<br>
https://linoxide.com/storage/mbr-vs-gpt-things-know-when-partitioning/

Modes de partitionnement MBR et GPT (MBR and GPT structure details)<br>
https://papy-tux.legtux.org/doc1064/index.php

What’s the Difference Between GPT and MBR When Partitioning a Drive?<br>
http://www.howtogeek.com/193669/whats-the-difference-between-gpt-and-mbr-when-partitioning-a-drive/

partition table type :
 * MSDOS
 * GPT
 * others bsd, sun etc..

**MBR** (Master Boot Record) and **GPT** (GUID Partition Table) are two different ways of storing the partitioning information on a drive. This information includes where partitions start and begin, so your operating system knows which sectors belong to each partition and which partition is bootable.

### MBR


*MBR* was first introduced with IBM PC DOS 2.0 in 1983. It’s called Master Boot Record because the MBR is a special boot sector located at the beginning of a drive. This sector contains a boot loader for the installed operating system and information about the drive’s logical partitions.

 * MBR only works with disks up to 2 TB in size.
 * MBR also only supports up to four primary partitions

The MBR architecture has its particularity because normally it supports only 4 primary partitions. I
 * The first three partitions should be primary partition,
 * The last partition should be an extended partition which can be subdivided into smaller partitions called logical partitions.

To operate on MBR disks, you need to use the fdisk, cfdisk, or parted command

### GPT

*GPT* stands for GUID Partition Table. It’s a new standard part of Intel’s EFI specification that’s gradually replacing MBR. It’s associated with UEFI. It’s called GUID Partition Table because every partition on your drive has a “globally unique identifier,” or GUID—a random string so long that every GPT partition on earth likely has its own unique identifier.

GUID Partition Table (GPT) includes  support for larger disks (MBR tops out at 2TiB), partition names, CRC values to help spot corrupt partition tables.

To operate on GPT disks, you need to use the gdisk or parted

NB Windows can only boot from GPT on UEFI-based computers running 64-bit versions of Windows 10, 8, 7, Vista, and corresponding server versions.


You’ll probably want to use GPT when setting up a drive. It’s a more modern, robust standard that all computers are moving toward. If you need compatibility with old systems — for example, the ability to boot Windows off a drive on a computer with a traditional BIOS — you’ll have to stick with MBR for now.


### Show partition table


`blkid` - locate/print block device attributes<br>
blkid device : display device info including UUID



```bash
set -v

# using blkid on device show partition table type
sudo blkid /dev/nvme0n1

set +v
```


    # using blkid on device show partition table type
    sudo blkid /dev/nvme0n1
    /dev/nvme0n1: PTUUID="cf54d91e-571d-4d11-9fdc-f892a33acf3b" PTTYPE="gpt"

    set +v


`wipefs` - wipe a signature from a device

 When  used  without  any options, wipefs lists all visible filesystems and the offsets of their basic signatures.

 * -n, --no-act : Causes everything to be done except for the write() call.


```
#  Prints information about sda and all partitions on sda.    
sudo wipefs -n /dev/sdb
```


**for a gpt partition table only**

`gdisk` - Interactive GUID partition table (GPT) manipulator

 `gdisk [ -l ] device`
 * -l : List the partition table for the specified device and then exits.

```bash
sudo gdisk -l /dev/sda
```


```bash
set -v
sudo gdisk -l /dev/nvme0n1
set +v
```

    sudo gdisk -l /dev/nvme0n1
    GPT fdisk (gdisk) version 1.0.4

    Partition table scan:
      MBR: protective
      BSD: not present
      APM: not present
      GPT: present

    Found valid GPT with protective MBR; using GPT.
    Disk /dev/nvme0n1: 1000215216 sectors, 476.9 GiB
    Model: KXG60ZNV512G TOSHIBA                    
    Sector size (logical/physical): 512/512 bytes
    Disk identifier (GUID): CF54D91E-571D-4D11-9FDC-F892A33ACF3B
    Partition table holds up to 128 entries
    Main partition table begins at sector 2 and ends at sector 33
    First usable sector is 34, last usable sector is 1000215182
    Partitions will be aligned on 2048-sector boundaries
    Total free space is 12909 sectors (6.3 MiB)

    Number  Start (sector)    End (sector)  Size       Code  Name
       1            2048          534527   260.0 MiB   EF00  EFI system partition
       2          534528          567295   16.0 MiB    0C01  Microsoft reserved ...
       3          567296       117600255   55.8 GiB    0700  Basic data partition
       4       998240256      1000204287   959.0 MiB   2700  Basic data partition
       5       117600256       119697407   1024.0 MiB  8300  boot
       6       119697408       998240255   418.9 GiB   8300  sys
    set +v


### Erase partition table

```
 sudo wipefs -a /dev/sdb
```

## Partitions management

### List partitions

List Harddisk Partitions on Linux<br>
https://vitux.com/linux_partition_table/


```bash
# show disk partitions
cat /proc/partitions
```


`disktype` — disk format detector
disktype detect the content format of a disk or disk image. It knows about common file systems, partition tables, disk images, archive formats and boot codes.

```bash
sudo disktype /dev/sda

# analyse iso or img file
sudo disktype rancheros.iso
sudo disktype rpi.img
```



#### using lsblk
`lsblk` - list block devices


```bash
# show block devices and partitions
lsblk

# only partition names
lsblk -o NAME

```

#### using parted

```bash
# list partitions for all devices
sudo parted -l

# Display the partition table of the specified device
sudo parted /dev/sdx print

# Display the partition table of the specified device with specified unit
sudo parted /dev/sdb unit Gb print

# Display the partition table of the specified device and free space
sudo parted /dev/sdb print free

```

#### using fdisk

```bash

# list system partitions
sudo fdisk -l

# list partitions for specified disk
sudo fdisk -l /dev/sda
```



### Manage partitions


How To Create Disk Partitions on Linux (fdisk, parted, gparted)<br>
https://devconnected.com/how-to-create-disk-partitions-on-linux/

How to Partition and Format Drives on Linux<br>
https://pimylifeup.com/partition-and-format-drives-on-linux/

#### parted

How to manage disk partitions using parted command<br>
https://www.2daygeek.com/how-to-manage-disk-partitions-using-parted-command/

Parted Commands to Manage Disk Partition<br>
https://linoxide.com/linux-command/parted-commands-manage-disk-partition/

Creating and managing partitions in Linux with parted (04/2020)<br>
https://www.redhat.com/sysadmin/partitions-parted

`GNU Parted` - a partition manipulation program
`parted [options] [device [command [options...]...]]`
 * `-l, --list` : lists partition layout on all block devices
 * `-s, --script` :  never prompts for user intervention
 * `-a alignment-type, --align alignment-type` : with alignment-type = none, cylinder, minimal, optimal

`command [options]:`
 * `print` :  Display the partition table.
 * `mklabel label-type`
     Create a new disklabel (partition table) of label-type.  label-type should  be  one
     of "aix", "amiga", "bsd", "dvh", "gpt", "loop", "mac", "msdos", "pc98", or "sun".
 * `mkpart part-type [fs-type] start end`
     Make  a  part-type  partition  for  filesystem fs-type (if specified), beginning at
     start and ending at end (by default in megabytes).  part-type should be one of  "primary", "logical", or "extended".<br>
     FS-TYPE : zfs, btrfs, nilfs2, ext4, ext3, ext2, fat32, fat16, hfsx, hfs+, hfs, jfs, swsusp, linux-swap(v1), linux-swap(v0), ntfs, reiserfs, freebsd-ufs, hp-ufs, sun-ufs, xfs, apfs2, apfs1, asfs, amufs5, amufs4, amufs3, amufs2, amufs1, amufs0, amufs, affs7, affs6, affs5, affs4, affs3, affs2, affs1, affs0, linux-swap, linux-swap(new), linux-swap(old)
 * `resizepart NUMBER END`  : resize partition NUMBER
 * `rm partition` : Delete partition.
 * `set number flag on|off` : change flag on partition number


**creating partition samples**

```bash
# sample creating partition
sudo apt-get install -y parted
# create gpt table partition
sudo parted /dev/sdb mklabel gpt --script
# create a first primary partition
sudo parted --align optimal /dev/sdb mkpart primary 0% 100% --script
# set flag lvm on first partition
sudo parted /dev/sdb set 1 lvm on --script

```

```bash
# create dos partition and first partition beginning at byte 52
parted /dev/sdb mklabel msdos mkpart primary fat32 52 100%
```


**resize partition**

```bash
# to increase partition size with remaining free space
parted /dev/sda unit B print free
parted /dev/sda resizepart 2 <freesize-end>

```

**Using multiple commands**

```bash
sudo parted -a optimal --script /dev/sdb mklabel msdos mkpart primary fat32 0% 80% mkpart primary 80% 100%
```

#### fdisk

Fdisk usage on Linux<br>
https://net2.com/fdisk-usage-on-linux/

`fdisk` - manipulate disk partition table
fdisk is a dialog-driven program for creation and manipulation of partition tables.
 + -l, --list : List  the  partition tables for the specified devices and then exit. If no devices are given, those mentioned in /proc/partitions (if that file exists) are used.

**Creating partition**
```bash
sudo fdisk /dev/sdb
> n : create new partition
> p : primary | e : extended
> <n> : partition number
> w : write partition
```

Linux: Script to Partition Disk<br>
https://stackpointer.io/unix/linux-script-to-partition-disk/632/

**scripting**
```bash
# g: create gpt table, n: create new partition, 1: partition number, ' ': first sector, ' ': last sector, w: write partition change
fdisk /dev/sdb << FDISK_CMDS
g
n
1


w
FDISK_CMDS
pvcreate /dev/sdb1
vgcreate local_storage /dev/sdb1
lvcreate -n minio -L 50G local_storage
S

```

### LVM

LVM : Logical Volume Manager

Introduction to Logical Volume Manager (04/2020)<br>
https://www.redhat.com/sysadmin/introduction-logical-volume-manager

Logical Volume Manager (LVM) versus standard partitioning in Linux (12/2020)<br>
https://www.redhat.com/sysadmin/lvm-vs-partitioning

How to Manage and Use LVM (Logical Volume Management) in Ubuntu (07/2017) <br>
https://www.howtogeek.com/howto/40702/how-to-manage-and-use-lvm-logical-volume-management-in-ubuntu/

Utilisation LVM (Logical Volume Manager) sous Linux (12/2012)<br>
https://www.vincentliefooghe.net/content/utilisation-lvm-logical-volume-manager-sous-linux

![LVM cheatsheet](https://www.howtogeek.com/wp-content/uploads/2011/01/lvm-cheatsheet.png)


LVM is a abstraction layer between your operating system and physical hard drives.

LVM allows you to create “virtual” partitions that span over several disks. The benefits are twofold: the size of the partitions are no longer limited by individual disks but by their cumulative volume, and you can resize existing partitions at any time, possibly after adding an additional disk when needed.

LVM uses a particular terminology: a virtual partition is a “logical volume” (LV), which is part of a “volume group” (VG), or an association of several “physical volumes” (PV). Each of these terms in fact corresponds to a “real” partition (or a software RAID device).

Withs LVMs, the concept is that physical disk partitions (PV) are added to pools of space, known as volume groups (VG).  Logical volumes (LV) are then assigned space from volume groups as needed.

With LVM, you can easily shrink the file system to reclaim the disk space (=> requires to unmount the logical volume). With LVM, you will be able to add more space to a logical volume from the volume group while is still in use.

 * PV : Physical Volume
 * VG : Volume Group
 * LV : Logical Volume

#### LVM prerequisites

```bash

# check if lvm is available (debian/ubuntu)

dpkg -l "lvm2"

# installation
sudo apt-get install lvm2

```


#### Physical Volume

The PV (Physical Volume) is the entity closest to the hardware: it can be partitions on a disk, or a full disk, or even any other block device (including, for instance, a RAID array).

```bash
# PV : The physical volume commands are for adding or removing hard drives in volume groups.

# pvs - Display information about physical volumes
sudo pvs  # <=> sudo pvdisplay -C

# -v : verbose mode showing additionals column

# pvdisplay - Display various attributes of physical volume(s)
sudo pvdisplay

# pvscan - pvscan - List all physical volumes
sudo pvscan

# create the physical volume on the created partition or on whole disk
sudo pvcreate /dev/sda<x>

# resize physical volume (if partition has been resized)
sudo pvresize /dev/sda2

# Remove physical volume
sudo pvremove /dev/sda<x>

```

#### Volume Group

A number of PVs can be clustered in a VG (Volume Group), which can be compared to disks both virtual and extensible. VGs are abstract, and don't appear in a device file in the /dev hierarchy, so there is no risk of using them directly.

Use the `vgcreate` command to create a new Volume Group. The VG must have at least one member.

    vgcreate name-of-new-VG PV-members

```bash

# VG : Volume group commands are for changing what abstracted set of physical partitions are presented to your operating in logical volumes.

# vgs - Display information about volume groups
sudo vgs # <=> sudo vgdisplay -C

# -v : verbose mode showing additionals column

# vgdisplay - Display volume group information
sudo vgdisplay # vgname

sudo vgdisplay -v # display PV associated

# vgscan - Search for all volume groups
sudo vgscan

# Create volume Group
sudo vgcreate vg01 /dev/sda1 /dev/sda2

# Extend a volume group with a new PV
sudo vgextend vg01 /dev/loop3

```

#### Logical Volume

The LV (Logical Volume) is a chunk of a VG; if we keep the VG-as-disk analogy, the LV compares to a partition. The LV appears as a block device with an entry in `/dev`, and it can be used as any other physical partition can be (most commonly, to host a filesystem or swap space).

The `lvcreate` command carves storage capacity from a VG.

    lvcreate -L size -n lvname vgname


> new LV device can be designated as
  * `/dev/<vg>/<lv>`  for LV operations
  * `/dev/mapper/<vg>-<lv>` for filesystem operations

```bash

# LV : Logical volume commands will present the volume groups as partitions so that your operating system can use the designated space.

# lvs - Display information about logical volumes
sudo lvs # <=> sudo lvdisplay -C

# -v : verbose mode showing additionals column

# --segments use default columns that emphasize segment information.
sudo lvs --segments

# lvdisplay - Display detailed information about a logical volume
sudo lvdisplay

# -v verbose -m maps : sudo lvdisplay /dev/vg01/lvol1 -vm
sudo lvdisplay /dev/vg01/lvol1 -vm

# lvscan - List all logical volumes in all volume groups
sudo lvscan

# Create Logical Volume
lvcreate -n lvol1 -L 130M vg01  # create LV /dev/vg01/lvol1  or /dev/mapper/vg01-lvol1

# use -l n%VG to specifiy a size in percentage of the VG
lvcreate --extents 100%FREE --name root vol_e27

# remove logical volume
sudo lvremove /dev/vg01/lvol1
```

lvcreate size options:
 * `-L|--size Size[m|UNIT]`
     Specifies the size of the new LV. The --size and --extents options are alternate methods of specifying size.
     UNIT  bBsSkKmMgGtTpPeE.
     * b|B is bytes,
     * s|S is sectors of 512 bytes,
     * k|K is kilobytes,
     * m|M is megabytes,
     * g|G is gigabytes,
     * t|T is terabytes,  
     * p|P is petabytes,
     * e|E is exabytes.


 * `-l|--extents Number[PERCENT]`<br>
     Specifies the size of the new LV in logical extents.<br>
     the suffix `%FREE` the remaining free space in the VG.<br>
     the suffix `%VG` denotes the total size of the VG,

```bash
-L 100G # specify total amount
-l 100%VG # specify all the VG size
--extents 100%FREE # specify all remaining free space of the VG

# NB use + symbol before size to specify extra amount, when resizing or extending for sample
-l +100%FREE
-L +10G
```

**Resizing**

Live Resizing LVM on Linux<br>
http://sirlagz.net/2016/01/20/live-resizing-lvm-on-linux/

RHEL8: Adding a second drive to LVM<br>
http://jhurani.com/linux/2019/10/09/rhel8-adding-a-drive-to-lvm.html

How to Delete/Remove LVM (Logical) Volume in Linux<br>
https://www.2daygeek.com/how-to-delete-remove-lvm-logical-volume-in-linux/


Sizing ops:
 * resize – can shrink or expand physical volumes and logical volumes but not volume groups
 * extend – can make volume groups and logical volumes bigger but not smaller
 * reduce – can make volume groups and logical volumes smaller but not bigger


```bash
# lvresize can be used for both shrinking and/or extending while lvextend can only be used for extending.
lvresize -L 50G /dev/rhel/var  # total size : resize to 50G
lvresize -L +10G /dev/rhel/var  # extra size : add 10G

# extend logical volume  NB use + symbol before size to specify extra amount, e.g -L +50G or -l +100%FREE
lvextend -L +120M /dev/vg01/lvol1  # extend with extra 120M
lvextend -L8G /dev/vgpool/lvstuff  # extend to 8G

```

Monter une partition LVM en mode secours<br>
https://documentation.online.net/fr/dedicated-server/rescue/mount-lvm-partition


## device Labels and ids

Persistent naming methods

There are four different schemes for persistent naming: by-label, by-uuid, by-id and by-path
The directories in `/dev/disk/` are created and destroyed dynamically, depending on whether there are devices in them.

 * ` /dev/disk/by-label/`
 * ` /dev/disk/by-uuid/`
 * ` /dev/disk/by-id/`
 * ` /dev/disk/by-path/`


### UUID

7 Methods to identify Disk Partition/FileSystem UUID in Linux (10/2019)<br>
https://www.2daygeek.com/check-partitions-uuid-filesystem-uuid-universally-unique-identifier-linux/

**UUID** stands for Universally Unique Identifier which helps Linux system to identify a hard drives partition instead of block device file.
 * It’s a 128 bit number used to identify information in computer systems.
 * UUIDs are represented as 32 hexadecimal (base 16) digits, displayed in five groups separated by hyphens, in the form 8-4-4-4-12 for a total of 36 characters (32 alphanumeric characters and four hyphens)

> FAT, exFAT and NTFS filesystems do not support UUID, but are still listed in /dev/disk/by-uuid/ with a shorter UID (unique identifier):

#### list uuid devices

The directory `/dev/disk/by-uuid` contains UUID and real block device files, UUIDs were symlink with real block device files.

```bash
ls -l /dev/disk/by-uuid
```


```bash
set -v
ls -l /dev/disk/by-uuid
set +v
```

    ls -l /dev/disk/by-uuid
    total 0
    lrwxrwxrwx 1 root root 10 mars   3 21:41 [0m[01;36m2fda2bc5-7ec9-4210-9801-eb112dec55a9[0m -> [40;33;01m../../dm-0[0m
    lrwxrwxrwx 1 root root 15 mars   4 23:49 [01;36m32C8-A0AE[0m -> [40;33;01m../../nvme0n1p1[0m
    lrwxrwxrwx 1 root root 10 mars   3 21:41 [01;36m62fd387f-b313-41d2-9433-60a58edda8d8[0m -> [40;33;01m../../dm-1[0m
    lrwxrwxrwx 1 root root 15 mars   4 23:49 [01;36m86df7174-005a-41e3-bc19-0beefc2d94b3[0m -> [40;33;01m../../nvme0n1p5[0m
    lrwxrwxrwx 1 root root 15 mars   4 23:49 [01;36mEA9C122B9C11F335[0m -> [40;33;01m../../nvme0n1p4[0m
    lrwxrwxrwx 1 root root 10 mars   3 21:41 [01;36mee2ae4e9-c67e-4c0c-9e62-d748907b38c7[0m -> [40;33;01m../../dm-2[0m
    set +v


#### using blkid

`blkid` - locate/print block device attributes

blkid device : display device info including UUID
 * `-U, --uuid uuid` : Look up the device that uses this filesystem uuid.
 * `-L, --label label` : Look up the device that uses this filesystem label
 * `-k, --list-filesystems` : List all known filesystems and RAIDs and exit.


```bash
# list all labels and uuid of partitions listed in /proc/partitions
sudo blkid

# list all known FS
sudo blkid -k

sudo blkid /dev/nvme0n1p5

sudo blkid -U 4dbd54b9-d391-4c86-b3a2-7d41d7cb0f17


```


#### using lsblk

```bash
# list block devices showing uuid column
lsblk -o name,mountpoint,size,uuid

```

### Labels

Linux Partition HOWTO - Labels<br>
https://www.tldp.org/HOWTO/Partition/labels.html

In linux, hard drives are referred to as devices, and devices are pseudo files in /dev. For example, the first partition of the second lowest numbered SCSI drive is `/dev/sdb1`. If the drive referred to as `/dev/sda` is removed from the chain, then the latter partition is automatically renamed `/dev/sda1` at reboot.
Volume labels make it possible for partitions to retain a consistent name regardless of where they are connected, and regardless of whatever else is connected.


#### list devices label


Almost every file system type can have a label. All your volumes that have one are listed in the `/dev/disk/by-label` directory.

```bash
# list device's label
ls -l /dev/disk/by-label
```


```bash
set -v
ls -l /dev/disk/by-label
set +v
```

    ls -l /dev/disk/by-label
    total 0
    lrwxrwxrwx 1 root root 15 mars   4 23:49  [0m[01;36mSYSTEM[0m -> [40;33;01m../../nvme0n1p1[0m
    lrwxrwxrwx 1 root root 15 mars   4 23:49 [01;36m'Windows\x20RE\x20Tools'[0m -> [40;33;01m../../nvme0n1p4[0m
    set +v


#### using blkid


```bash
set -v

# list LABEL using blkid
sudo blkid | grep ' LABEL='

# get device ustin blkid
sudo blkid -L SYSTEM
set +v
```


    # list LABEL using blkid
    sudo blkid | grep ' LABEL='
    /dev/nvme0n1p1:[01;31m[K LABEL=[m[K"SYSTEM" UUID="32C8-A0AE" TYPE="vfat" PARTLABEL="EFI system partition" PARTUUID="333c0f85-c8e4-4909-807b-f8a7c498dc08"
    /dev/nvme0n1p4:[01;31m[K LABEL=[m[K"Windows RE Tools" UUID="EA9C122B9C11F335" TYPE="ntfs" PARTLABEL="Basic data partition" PARTUUID="843937cc-97f3-4d14-adc5-8b3d1c64c214"

    # get device ustin blkid
    sudo blkid -L SYSTEM
    /dev/nvme0n1p1
    set +v


#### Creating or changing labels

Commands for changing or modifying Partition Name/Label are dependent on type of filesystem on that partition with exception of some general commands.

 * `tune2fs` : command used for changing label of ext2, ext3 and ext4 type partitions
 * `e2label` : command used for changing label of ext2, ext3 and ext4 type partitions
 * `ntfslabel` : command used for changing label of NTFS partitions.
 * `mlabel` : make an MSDOS volume label
 * `fatlabel` : fatlabel - set or get MS-DOS filesystem label
 * `exfatlabel` : command used for changing the label of exFAT formatted partition.
 * `xfs_admin` : to change label on xfs partition
 * `swaplabel` : command used for changing label of SWAP partition.

How to Change Linux Partition Label Names on EXT4 / EXT3 / EXT2 and Swap<br>
https://www.tecmint.com/change-modify-linux-disk-partition-label-names/

The commands `e2label` or `tune2fs` used for changing label of ext2, ext3 and ext4 type partitions.

```bash
# using tune2fs to set label
tune2fs -L BOOT /dev/sda1
```

`e2label` - Change the label on an ext2/ext3/ext4 filesystem<br>
e2label will display or change the volume label on the ext2, ext3, or ext4 filesystem located on device.

```bash
# show label for specified
e2label /dev/sda6
# assign label
e2label /dev/sda6 MAIN
# assign label on LVM partition
sudo e2label /dev/vg-sys/root root
```

```bash

# change label on an exfat partition
exfatlabel /dev/sda3 EX_PART

# change label on a xfxs partition
xfs_admin -L media /dev/sdb1

# Change the label of swap partition.
swaplabel -L SWAP_PART /dev/sda5
```


## Partition schemes

Partitioning consists in dividing the available space on the hard drives (each subdivision thereof being called a “partition”) according to the data to be stored on it and the use for which the computer is intended.

Partitioning define the various portions of the disks (or “partitions”) on which the Linux filesystems and virtual memory (swap) will be stored.

### partitioning methods

 * All files in one partition. The entire Linux system tree is stored in a single filesystem, corresponding to the root / directory.

 * Separate /home partition, is similar, but splits the file hierarchy in two: one partition contains the Linux system (/), and the second contains “home directories” (meaning user data, in files and subdirectories available under /home/).

 * Separate /home, /var, and /tmp partitions, is appropriate for servers and multi-user systems. It divides the file tree into many partitions: in addition to the root (/) and user accounts (/home/) partitions, it also has partitions for server software data (/var/), and temporary files (/tmp/). These divisions have several advantages. Users can not lock up the server by consuming all available hard drive space (they can only fill up /tmp/ and /home/). The daemon data (especially logs) can no longer clog up the rest of the system.


A Quick Guide to Linux Partition Schemes (11/2020)<br>
https://www.maketecheasier.com/quick-guide-to-linux-partition-schemes/


 * `/boot` : Boot loader files (e.g., kernels, initrd).
 * `/boot/efi` : EFI system partition (esp)
 * `/` : Primary hierarchy root
 * `/home` : user accounts home directories,
 * `/var` : server software data
 * `/tmp` : temporary files

### filesystem organisation

FHS : Filesystem Hierarchy Standard

Episode 12 - Crash Course on the Filesystem Hierarchy Standard <br>
https://sysadmincasts.com/episodes/12-crash-course-on-the-filesystem-hierarchy-standard

Filesystem Hierarchy Standard<br>
https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard

Understanding the Linux Virtual Directory Structure<br>
https://www.maketecheasier.com/linux-virtual-directory-structure/


### boot partition


Do You Need a Boot Partition in Linux?<br>
https://www.maketecheasier.com/do-you-need-boot-partition-linux/


In Linux, the boot partition contains files like the kernel itself, which is the operating system’s ticking heart and brain. It’s also where you will find initrd, which loads a temporary root system in the computer’s memory, and GRUB, the bootloader that loads the operating system.

In the past, the boot and the system partitions were separate. They first contained everything needed to load the operating system, and the second contained the operating system itself.

> NB Today, a single partition usually contains everything needed to load and use the operating system and acts as both a boot and system partition.

It’s worth noting that, just like with Windows and BitLocker, if you’re using an encrypted file system for your Linux-based operating system, or other complicated storage schemes like LVM or software RAID, you may still need a separate boot partition.

> NB grub2 support lvm management => don't have to create external boot partition

**separate** /boot is only required if root partition is encrypted (and boot is obviously in clear)



### encryption

#### Glossary

dm-crypt : https://en.wikipedia.org/wiki/Dm-crypt

**dm-crypt** is a transparent disk encryption subsystem in Linux kernel versions 2.6 and later

dm-crypt is implemented as a device mapper target and may be stacked on top of other device mapper transformations. It can thus encrypt whole disks (including removable media), partitions, software RAID volumes, logical volumes, as well as files. It appears as a block device, which can be used to back file systems, swap or as an LVM physical volume.

At least two frontends (CLI) are currently available: cryptsetup and cryptmount.

LUKS : https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup

The Linux Unified Key Setup (LUKS) is a disk encryption specification

#### How to

How to Encrypt Your Partitions on Linux with dm-crypt (12/2018)<br>
https://www.maketecheasier.com/encrypt-linux-partitions-dm-crypt/

How to encrypt partition in Linux (05/2021)<br>
https://linuxconfig.org/partition-encryption


How to Encrypt Your Hard Disk in Ubuntu (01/2017)<br>
https://www.maketecheasier.com/encrypt-hard-disk-in-ubuntu/

How to Encrypt Linux Partitions with VeraCrypt on Ubuntu (05/2019) <br>
https://vitux.com/how-to-encrypt-linux-partitions-with-veracrypt-on-ubuntu/

### partition in RAM

How To Mount A Temporary Partition In RAM In Linux<br>
https://www.ostechnix.com/how-to-mount-a-temporary-partition-in-ram-in-linux/

`tmpfs` is a temporary filesystem that is created in memory or swap partition(s). In Linux and Unix systems, some directories like `/tmp` and `/var/run` are mounted on this filesystem. Anything saved in these folders will be automatically cleared upon reboot. We can also use tmpfs filesystem for programs or tasks that requires a lot of read/write operations to improve their performance.

```bash
mount -t tmpfs tmpfs /mnt -o size=100m
```

How To Write Log Files In RAM Using Log2ram In Linux
https://www.ostechnix.com/how-to-write-log-files-in-ram-using-log2ram-in-linux/

Running Linux from RAM
https://dreamlayers.blogspot.com/2012/10/running-linux-from-ram.html


## Filesystems

https://en.wikipedia.org/wiki/Comparison_of_file_systems


A file system is the way in which files are named, stored, retrieved as well as updated on a storage disk or partition; the way files are organized on the disk.

There are many filesystem formats, corresponding to many ways of physically storing data on disks.

There are also network filesystems, such as NFS, where data is not stored on a local disk. Instead, data is transmitted through the network to a server that stores and retrieves them on demand.



### filesystem types

 * `ext2` : the default file system used in earlier Linux systems
 * `ext3` : the most common file system on Linux. improved capability to recover from crashes
 * `ext4` :  journaling file system for Linux,
 * `proc` : file system interface to the linux kernel
 * `squashfs`: a compressed read-only filesystem for Linux. http://www.admin-magazine.com/HPC/Articles/Read-only-File-Compression-with-SquashFS
 * `exFAT` : is a modern replacement for FAT32 and more devices and operating systems support it than NTFS,<br>   but it’s not nearly as widespread as FAT32.
 * `fat32` : FAT32 is the oldest of the three file systems available to Windows. <br>Individual files on a FAT32 drive can’t be over 4 GB
 * `ntfs` : New Technology File System is a proprietary file system developed by Microsoft.
 * `xfs` :  is a high-performance 64-bit journaling file system created by Silicon Graphics (NB XFS filesystems are not actually shrunk)
 * `zfs` : ZFS merges the traditional volume management and filesystem layers
 * `btrfs` : B-tree filesystem is a filesystem and volume manager rolled into one
 * `nfs` : Network file system basically used to mount the file systems on other Linux machines
 * `cifs` : Common internet file system (smb / samba)

#### list know filesystems

```bash
# list known filesystem types
cat /proc/filesystems

# blkid -k, --list-filesystems :  List all known filesystems and RAIDs and exit.
blkid -k
```


#### exfat

ExFAT is that it's cross-platform (Windows, Mac, and many portable devices use it), and it's designed without the overhead of file permissions. You can attach a drive formatted as ExFAT to any computer, and all files are available to anyone. Whether that's good or bad depends on your use case, but for portable media, that's often exactly the intent.

> symbolic links of any type are not supported in exFAT at all.

> require  exfat-fuse exfat-utils packages installation


What’s the Difference Between FAT32, exFAT, and NTFS<br>
https://www.howtogeek.com/235596/whats-the-difference-between-fat32-exfat-and-ntfs/

How to mount an exFAT drive on Linux<br>
https://www.xmodulo.com/mount-exfat-drive-linux.html

Mounting exFAT Drive on CentOS<br>
https://linuxize.com/post/how-to-mount-an-exfat-drive-on-centos-7/

#### btrfs

Getting started with btrfs for Linux<br>
https://opensource.com/article/20/11/btrfs-linux

The Beginner’s Guide to Btrfs<br>
https://www.maketecheasier.com/the-beginners-guide-to-btrfs/

Convert your filesystem to Btrfs<br>
https://fedoramagazine.org/convert-your-filesystem-to-btrfs/

#### zfs

ZFS 101—Understanding ZFS storage and performance<br>
https://arstechnica.com/information-technology/2020/05/zfs-101-understanding-zfs-storage-and-performance/

Canonical adds ZFS on root as experimental install option in Ubuntu<br>
https://www.theregister.co.uk/2019/08/12/canonical_zfs_ubuntu/

### Determine the File System Type

7 Ways to Determine the File System Type in Linux (Ext2, Ext3 or Ext4) (03/2017)<br>
https://www.tecmint.com/find-linux-filesystem-type/

13 Methods to Find the File System Type on Linux (Ext3, Ext4 or XFS) (10/2019)<br>
https://www.2daygeek.com/find-identify-check-determine-linux-file-system-type/


```bash
# display filesystem type for instance on lvm logical volume
blkid /dev/mapper/vg--data-vm

# -f option, it prints file system type on partitions
lsblk -f /dev/mapper/vg--data-vm

# -T print file system type
df -hT /dev/vg-data/vm

# -N disables checking of file system for errors
fsck -N /dev/vg-data/vm

# -s flag enables reading of block or character files and -L enables following of symlinks
file -sL /dev/vg-data/vm
```


```bash
df -Th | grep "^/dev"
```


### formating filesystem

 In any case, a filesystem must be prepared on a disk before it can be mounted and this operation is known as **formatting**.

Using mkfs command in Linux to Format the Filesystem on a Disk or Partition<br>
https://linuxhandbook.com/mkfs-command/


Is it ok to mkfs without partition number?<br>
https://unix.stackexchange.com/questions/346826/is-it-ok-to-mkfs-without-partition-number
A: Creating a filesystem on a whole disk rather than a partition is possible, but unusual.


`mkfs` - build a Linux filesystem<br>
mkfs is simply a front-end for the various filesystem builders (mkfs.fstype) available under Linux.

    mkfs [options] [-t type]  device <=> mkfs.type [options] device

`mke2fs` alias `mkfs.ext4` - create an ext2/ext3/ext4 filesystem
  + `-L new-volume-label` : Set the volume label for the filesystem to new-volume-label


```bash

# ext4 fs
mkfs -t ext4 -L root /dev/vg-sys/root

# Fat32
/sbin/mkfs.vfat -F32 -n "$label" $device

# exfat
sudo apt-get install --reinstall -y exfat-fuse  exfat-utils
# or use exfatprogs instead of exfat-utils
sudo mkexfatfs -n MonEXFAT /dev/sdXN # mkfs.exfat
sudo mkfs.exfat -n "DATA-EASY2BOOT" /dev/sdb2

```


### checking filesystem


Fsck Command in Linux (Repair File System)<br>
https://linuxize.com/post/fsck-command-in-linux/

`fsck` - check and repair a Linux filesystem<br>
`fsck [OPTIONS] [FILESYSTEM]`
 + `-V` : explain what is being done
 + `-M` : do not check mounted filesystems
 + `-N` : do not execute, just show what would be done

fsck is used to check and optionally repair Linux file systems, it can also print the file system type on specified disk partitions.

When no FILESYSTEM is provided as an argument, fsck checks the devices listed in the fstab file.

**Never run fsck on mounted partitions as it may damage the file system**. Before attempting to check or repair file systems always unmount it first.


#### ext4 filesystem

`e2fsck` - check a Linux ext2/ext3/ext4 file system
 + `-p` : Automatically repair ("preen") the file system.
 + `-v` : Verbose mode.
 + `-y` : Assume an answer of `yes' to all questions
 + `-f` : Force checking even if the file system seems clean.

        e2fsck -f -t fstype -v -y # check linux ext2/ext3/ext4 file system


`tune2fs` - adjust tunable filesystem parameters on ext2/ext3/ext4 filesystems
 + `-l` :  List  the  contents  of  the filesystem superblock, including the current values of the parameters that can be set via this program.


```bash
sudo tune2fs -l /dev/sda1
sudo tune2fs -l /dev/mapper/vg--sys-root


sudo tune2fs -l /dev/sdc1 | grep -i 'last checked\|mount count'
```


#### exfat filesystem


> exfatprogs is a re-branding of exfat-utils by SAMSUN

 * exfat-utils: exfatfsck or fsck.exfat
 * exfatprogs: fsck.exfat

`exfatfsck` - check an exFAT file system
 + `-a` : Automatically repair the file system. No user intervention required.
 + `-n` : No-operation mode: non-interactively check for errors, but don't write anything to the file system.




### resizing filesystem

Not all filesystems can be resized online; resizing a volume can therefore require unmounting the filesystem first and remounting it afterwards.

The `ext3`, `ext4` and `xfs` filesystems can be grown online, without unmounting; shrinking requires an unmount. The `reiserfs` filesystem allows online resizing in both directions. The venerable `ext2` allows neither, and always requires unmounting.

#### ext filesystem

`resize2fs` - ext2/ext3/ext4 file system resizer<br>
resize2fs device
 * `-f` : overriding some safety checks
 * `-p` : Prints  out  a percentage completion bars

```bash
# resize by default to the size of the partition
resize2fs -f -p /dev/vg01/lvol1
```

#### xfs filesystem


`xfs_growfs` -  expand an XFS filesystem<br>
`xfs_growfs /mount_point`
The mount-point argument is the pathname of the directory where the filesystem is mounted
 +  `-d | -D size` : Specifies  that  the data section of the filesystem should be grown. If the -D size option is given, the data section is grown to that size (expressed in filesystem blocks), otherwise the data section is grown to the largest size possible with the -d option or without any option.
```bash
# Increasing the Size of an XFS File System
xfs_growfs /mount/point
```



### mounting filesystem

The mount point is the directory tree that will house the contents of the filesystem on the selected partition. Thus, a partition mounted at `/home/` is traditionally intended to contain user data.


`mount` - mount a filesystem<br>
`mount [-t type] [-o options] /path/to/device /path/to/mount_point`
 * `-l, --show-labels` : Add the labels in the mount output.
 * `-L, --label label` : Mount the partition that has the specified label.
 * `-a, --all` : Mount  all  filesystems (of the given types) mentioned in fstab (except for those whose line contains the noauto keyword).
 * `-U, --uuid uuid` : Mount the partition that has the specified uuid.
 * `-t, --types fstype` : the fstype argument is used to indicate the filesystem type. NB thes parameter is optional, wihtout this mount will try to guess the desired  type
 * `-o, --options opts` : Use the specified mount options.  The opts argument is a comma-separated list.
     * `loop` : mount via the loop device. If no explicit loop device is mentioned then mount will try to find some unused loop device.


```bash
# mounting an iso  via the loop device
sudo mkdir /media/iso/
sudo mount -o loop /path/to/ubuntu.iso /media/iso

mount -t iso9660 -o loop image_cd.iso /media/cd

# mount a system image skiping partition table with given offset
sudo mount -o loop,offset=1048576 ~/mint.img /mnt/vsf


# inside guest VM : mount virtual box shared folder
# mount -t vboxsf  Name  /mount/alias [-o umask=0022,uid=id,gid=id]
mount -t vboxsf -o uid=$(id -u vagrant),gid=$(id -g vagrant) var_www /var/www


# mount block device
mount -t ext3 /dev/hdc1 /mnt/tmp/

# mount samba network file system
mount -t smbfs //nom_serveur/repertoire_partagee /media/partage -o username=user password=mdp
sudo mount.cifs //myhostname/share /some/directory -o username=xxxx,password=yyyy

```



#### Using UUID or Label

Most of the Linux systems are mount the partitions with `UUID`

Using label for mounting partition
```bash
# Mount the partition that has the specified label.
mount -L USBDISK <mount_point>

or in fstab use LABEL=<name>
```

#### Bind mount operation

What is a bind mount? cf Use cases Read-only view<br>
https://unix.stackexchange.com/questions/198590/what-is-a-bind-mount

A bind mount is an alternate view of a directory tree. Classically, mounting creates a view of a storage device as a directory tree. A bind mount instead takes an existing directory tree and replicates it under a different point. The directories and files in the bind mount are the same as the original. Any modification on one side is immediately reflected on the other side, since the two views show the same data.


```bash
# mount a directory to another directory:
mount --bind {{path/to/old_dir}} {{path/to/new_dir}}

```

From man mount:

```
   Bind mount operation
       Remount part of the file hierarchy somewhere else.  The call is:

              mount --bind olddir newdir

       or by using this fstab entry:

              /olddir /newdir none bind

       After this call the same contents are accessible in two places.
       ...
```

#### unmounting

`umount`- unmount file systems
The umount command detaches the mentioned file system(s) from the file hierarchy.  
 * `-a, --all` : All of the filesystems described in `/proc/self/mountinfo` are unmounted, except the `proc`, `devfs`, `devpts`, `sysfs`.
 * `-f, --force` : Force an unmount (in case of an unreachable NFS system).
 * `-l, --lazy` : Lazy  unmount.

> NB The `umount` command will automatically detach loop device previously initialized by mount
```bash
umount /mnt/test
```

**NB** Unmounting a file system can only be done if no process is accessing it at the same time.
If the unmount is refused by the system, it means that there are still processes using the Filesystem.

The `fuser` command lists the processes using the mounted FS.

```bash
fuser -vm /dev/sdb1
```

**Lazy unmount**

Use the -l (--lazy) option to unmount a busy file system as soon as it is not busy anymore.

```bash
umount -l DIRECTORY
```

#### checking mounted filesystems

```bash
# lists all mounted filesystem
mount
# <=> cat /etc/mtab
# <=> cat /proc/mounts

# list ext4 mounted fs
mount -t ext4

# list mounted partitions and labels
mount -l

# lists all mounted filesystem
cat /proc/mounts

# use mountpoint command to test specified directory
mountpoint /var  && echo /var is a mountpoint

```

`mountpoint` - see if a directory or file is a mountpoint
`mountpoint directory|file` return zero if the directory or file is a mountpoint, non-zero if not.


#### Permanently mounting a filesystem


Understanding the /etc/fstab File in Linux (07/2019)<br>
https://www.2daygeek.com/understanding-linux-etc-fstab-file/

The `/etc/fstab` file gives a list of all possible mounts that happen either automatically on boot or manually for removable storage devices.

The file `/etc/fstab` may contain lines describing what devices are usually mounted where, using which options.

fstab line format:

 * `file_system_spec` : specify the block special device or remote filesystem to be mounted (e.g. `/dev/sda1` or `LABEL=<label>` or `UUID=<uuid>` may be given instead of a device name)
 * `mount_point` : describes the mount point (target) for the filesystem.
 * `type` : describes the type of the filesystem. e.g. `ext3`, `vfat`, `ntfs`, `reiserfs`, `xfs`
 * `fstab options` : describes the mount options associated with the filesystem
   * _`defaults<`_  use default options: `rw`, `suid`, `dev`, `exec`, `auto`, `nouser`, and `async`<br>NB can change individual option adding modified option after e.g. `defaults,nosuid,user`
   * `noauto` :  do not mount when "mount -a" is given (.e.g at boot time).
   * `user` : allow a user to mount
   * `owner` : allow device owner to mount
   * `exec/noexec` : Permit or not execution of binaries
   * `suid/nosuid` : Allow set-user-ID or set-group-ID bits to take effect.
   * `ro/rw` : Mount the filesystem read-only/Mount the filesystem read-write.
   * `sync/async` : All I/O to the filesystem should be done synchronously/asynchronously.  (Sync e.g. some flash  drives)
   * `dev` :  Interpret character or block special devices on the filesystem.
   * `noatime` : Do not update inode access times on this filesystem
 * `dump` : determine which filesystems need to be dumped. It indicates backup utility dump is enabled or not for the file system
    * `0`: dump will ignore the file system backup (NB the default if not present). This field is almost always set to 0.
    * `1`: If 1, dump will make a backup. It tells the dump tool that the partition contains data that is to be backed up.
 * `pass` : fsck reads the pass number and determines in which order the file systems should be checked. Possible entries are 0, 1, and 2. It indicates the sequence of the file system checks. This last field  indicates whether the integrity of the filesystem should be checked on boot, and in which order this check should be executed.
   * `0`: fsck check doesn’t performed on filesystems.
   * `1`: It’s a highest priority and should be assigned to root file system.
   * `2`: All other file systems you want to have checked should get a 2.

```bash
# fs_spec   mount_point     type    options     dump  pass

# swap
/dev/sda2       none            swap    sw                       0       0
# virtualbox host shared folder
ShareName /home/user/Extern  vboxsf   umask=0022,uid=id,gid=id  0  0

# nfs
arrakis:/partage /partage nfs defaults 0 0
# sshfs
sshfs#id@gist:/home/user /mnt fuse port=22,user,noauto,noatime 0 0
# smb / cifs
//nom_serveur/repertoire_paratage /media/remote smbfs username=user1,password=pwd   0   0

```

How to Mount Windows Share on Ubuntu Linux (cifs /smb)<br>
https://linoxide.com/linux-how-to/mount-windows-share-on-ubuntu-linux/





### filesystem information

How to Check Disk Space Usage in Linux Using the df Command?<br>
https://www.2daygeek.com/linux-check-disk-space-usage-df-command/

How To Check Free Disk Space on Linux<br>
https://devconnected.com/how-to-check-free-disk-space-on-linux/

`df` - report file system disk space usage
 + `-h, --human-readable` : print sizes in powers of 1024 (e.g., 1023M)
 + `-l, --local` : limit listing to local file systems
 + `-t, --type TYPE` : limit listing to file systems of type TYPE
 + `-x, --exclude-type=TYPE` : limit listing to file systems not of type TYPE
 + `-T, --print-type` : print file system type
 + `-i, --inodes` : list inode information instead of block usage

```bash
# report filesystem using human readable size and showing file system type
df -hT

# report fs of type ext4
df -t ext4

# report specfic partition
df -h  /dev/mapper/vg--sys-root

# In order to check inode usage on Linux
df -i
df -i -h

# Display the file system and its disk usage containing the given file or directory:
df path/to/file_or_directory

```

**Using stat on mount point**
`stat` command can also be used to fetch details about a file system instead of a file.
 +  `-f, --file-system` : display file system status instead of file status

```bash
stat -f /boot
stat -f /

```


How To List Filesystems In Linux Using Lfs (rust lfs tools)<br>
https://ostechnix.com/how-to-list-filesystems-in-linux-using-lfs/

lfs - List your filesystems. All units are SI.
```bash
lfs

lfs -a

```

## Partition and filesystem use cases

How to Save Space with Symlinks and Mount Points:<br>
https://linuxconfig.org/how-to-save-space-with-symlinks-and-mount-points

How to resize active root partition in Linux (using gparted) <br>
https://www.2daygeek.com/linux-resize-active-primary-root-partition-gparted/

### Create a new partition table, partition and filesystem

```bash
# sample creating partition and lvm LV
sudo apt-get install -y parted
# create gpt table partition
sudo parted /dev/sdb mklabel gpt --script
# create a first primary partition
sudo parted --align optimal /dev/sdb mkpart primary 0% 100% --script
# set flag lvm on first partition
sudo parted /dev/sdb set 1 lvm on --script
# create physical volume using first partition
sudo pvcreate /dev/sdb1
# create volume group
sudo vgcreate data-vg /dev/sdb1
sudo lvcreate -n data -l 100%VG data-vg
sudo mkfs -t ext4 -L data /dev/data-vg/data
echo '/dev/mapper/data--vg-data /data   ext4  defaults 0  2' | sudo tee -a /etc/fstab
sudo bash -c 'mount /data && chown user: /data'
```
### Resize partition

```bash
# 1. resize existing disk and resize partition => ok if it is the last partition
parted /dev/sda unit B print free
parted /dev/sda resizepart 2 <freesize-end>
pvresize /dev/sda2
lvresize -l +100%FREE /dev/rhel/root
xfs_growfs /dev/mapper/rhel-root  # or use mount dir => xfs_growfs /


# 2. vm resize existing disk (/dev/sda) and add a new partition e.g. /dev/sda3
parted /dev/sda unit B print free
parted /dev/sda mkpart primary <freesize-start> 100% --script
pvcreate /dev/sda3
vgextend rhel /dev/sda3
lvextend -l 100%VG /dev/mapper/rhel-root
xfs_growfs /dev/mapper/rhel-root

# 3. vm add a new disk , create partition

parted /dev/sdb mkpart primary 0% 100% --script
pvcreate /dev/sdb1
vgextend rhel /dev/sdb1
lvextend -l 100%VG /dev/mapper/rhel-root
xfs_growfs /dev/mapper/rhel-root


```

## Moving partition


Moving /var, /home to separate partition<br>
https://unix.stackexchange.com/questions/131311/moving-var-home-to-separate-partition

How to move or relocate /var folder to a new partition in Linux<br>
https://www.suse.com/support/kb/doc/?id=000018399

on redhat with selinux
```bash
restorecon -vr /var
or
touch /.autorelabel
reboot
# or
sudo chcon -R -t var_t /var
sudo restorecon -R /var
```


## swap management

### swap space

Virtual memory allows the Linux kernel, when lacking sufficient memory (RAM), to free a bit of memory by storing the parts of the RAM that have been inactive for some time on the swap partition of the hard disk.
To simulate the additional memory, Windows uses a swap file that is directly contained in a filesystem. Conversely, Linux uses a partition dedicated to this purpose, hence the term “swap partition”.

Swap space can be added , either by creating a swap file or by creating a disk partition dedicated to swap.
There are no performance improvements in using a swap file rather than creating a file partition. Swap files are just easier to manage because the file size can be easily adjusted. Changing the partition size for swap can be trickier than changing the file size.


### swap sizing

How Much Swap Should You Use in Linux?<br>
https://itsfoss.com/swap-size/

 * If RAM is more than 1 GB, swap size should be at least equal to the square root of the RAM size and at most double the size of RAM
 * If hibernation is used, swap size should be equal to size of RAM plus the square root of the RAM size

> NB When your computer hibernates, this will entail moving out the physical memory (RAM) content to the swap space before the machine turns off. In this situation the swap partition would need to be at least as big as the main memory


### swapiness

Swappiness refers to the kernel parameter responsible for how much and how often that the system moves data from RAM to swap memory.

The default value for swappiness is 60; however, you can manually set it anywhere between 0-100. <br>
Small values cause little swapping to occur, whereas high values can cause very aggressive swapping.

```bash
# check your current swappiness setting by running the following command:

$ cat /proc/sys/vm/swappiness

# Change current swappiness setting
sudo sysctl vm.swappiness=30

```

To make this parameter persistent across reboots append the following line to the `/etc/sysctl.conf` file:
```

vm.swappiness=10

```


```bash
set -v
cat /proc/sys/vm/swappiness
set +v

```

    cat /proc/sys/vm/swappiness
    60
    set +v


### Checking swap

Checking existing swap partitions using `swapon` command

`swapon` - enable devices and files for paging and swapping
swapon is used to specify devices on which paging and swapping are to take place
 + `--show[=column...]` : Display a definable table of swap areas
 + `-s, --summary` : Display  swap  usage  summary  by  device.   Equivalent to "cat /proc/swaps"
 + `--noheadings` : Do not print headings when displaying --show output.
 + `--raw` : Display --show output without aligning table columns.
 + `--bytes` : Display swap size in bytes in --show output instead of in user-friendly units.


```bash
# Display swap areas
sudo swapon --show

cat /proc/swaps # <=> swapon -s

# show used swap memory
free -m | grep ^Swap

# show label, uuid  of specified swap space
sudo swaplabel /swapfile

# show label, uuid
sudo blkid /swapfile

```

### Adding swap


How To Add Swap Space on Debian 10 Buster<br>
https://devconnected.com/how-to-add-swap-space-on-debian-10-buster/

How To Add Swap Space on Debian 10 Linux<br>
https://linuxize.com/post/how-to-add-swap-space-on-debian-10/


`mkswap` - set up a Linux swap area<br>
`mkswap [options] device`
 + `-L, --label label` : Specify a label for the device, to allow swapon by label.


`swapon` - enable devices and files for paging and swapping<br>
`swapon [options] [specialfile...]`
> The device or file used is given by the specialfile parameter.  It may be of the form -L label or -U uuid to indicate a device by label or uuid.

 + `-L label` :  Use the partition that has the specified label.
 + `-U uuid` : Use the partition that has the specified uuid.


#### swap partition

Creating a disk partition dedicated to swap (disk partition or LVM volume)

```bash
# Create a regular disk partition (/dev/sda5) or LVM volume (/dev/mint-vg/swap_1)
# for instance with a regular disk partition
parted /dev/sda mkpart primary linux_swap begin end --script
parted /dev/sda2 set swap on # set swap flag
# format swap and assign swap label
mkswap -L swap /dev/sda2
swapon -L swap
swapon --show
swaplabel  /dev/sda2
SWAP_UUID=$(swaplabel /dev/sda2 | sed -E 's/UUID:\s+([^\s]+)/\1/')
# Make your swap space permanent : edit /etc/fstab adding
echo "UUID=$SWAP_UUID   none   swap  defaults   0   0" >> /etc/fstab

```

#### swap file

Creating a Swap File

```bash
# create 2GB swap file
cd /
sudo dd if=/dev/zero of=swapfile bs=1MiB count=$((2*1024))
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon --show
# Make your swap space permanent : edit /etc/fstab adding
/swapfile none swap defaults 0 0
```



### Resizing swap

`swapoff` - disable devices and files for paging and swapping<br>
`swapoff [-va] [specialfile...]`
 + '-a, --all` : All devices marked as swap in /etc/fstab  

Managing swap in the modern Linux system<br>
https://www.redhat.com/sysadmin/managing-swap


```bash
# resize swap
# Disable:
swapoff -v /dev/rhel/swap
# Resize:
lvresize /dev/rhel/swap -L +2G (to increase swap by 2G)
# Format:
mkswap -L swap /dev/rhel/swap
# Enable:
swapon -v /dev/rhel/swap
```

How to increase swap space in Linux (cf How much swap is needed ? & swapiness adjustment)<br>
https://net2.com/how-to-increase-swap-space-in-linux/


###  Clearing swap

How to clear swap memory in Linux
https://www.redhat.com/sysadmin/clear-swap-linux


To clear the swap memory on your system, you simply need to cycle off the swap. This moves all data from swap memory back into RAM. It also means that you need to be sure you have the RAM to support this operation.

```bash
### clear swap
# Check space:
free -m
# Disable swap:
swapoff -a

# Wait approx 30 sec (use free -m to see the amount of swap used/available decrease over time)
# Enable swap:
swapon -a

```



### Disabling swap

How to Permanently Disable Swap in Linux<br>
https://www.tecmint.com/disable-swap-partition-in-centos-ubuntu/

Disable swap in systemd  ( # if using systemd and having swap partition : systemctl mask dev-sdXX.swap )<br>
https://rageagainstshell.com/2019/12/disable-swap-in-systemd/

#### systemd swap

cf `man systemd.swap` <br>
Swap units may either be configured via unit files, or via /etc/fstab.
Swaps listed in /etc/fstab will be converted into native units dynamically at boot and when the configuration of the system manager is reloaded.
If a swap device or file is configured in both /etc/fstab and a unit file, the configuration in the latter takes precedence.

```bash

# systemctl stop unit.swap  required ? or use swapoff

systemctl status swap.target

systemctl --type swap # swap.unit files
systemctl status *.swap


```

Disabling swap using systemd configuration

```bash
# mask swap target
systemctl mask swap.target

# or mask swap unit file
systemctl mask swapfile.swap

```


#### Removing a Swap File

To deactivate and remove the swap file, perform the steps below:

```bash
# Deactivate the swap space by running:
sudo swapoff -v /swapfile

# Open the /etc/fstab file with your text editor and remove the swap file entry:  
# /swapfile swap swap defaults 0 0.

# Finally, delete the actual swapfile file with the rm command:
sudo rm /swapfile

```

#### Disabling swap partition

```bash
# Deactivate the swap space by running:
sudo swapoff -va

# open /etc/fstab file, search for the swap line and comment the entire line by adding a # (hashtag) sign in front of the line
sed -i -E 's/([^#].*?\sswap\s+.*)/# \1/' /etc/fstab

```


# ISOs

ManipulatingISOs<br>
https://wiki.debian.org/ManipulatingISOs

## List ISO partitions

```bash
# print partitions of iso
parted image.iso print


#  Prints information about iso and all partitions on iso.
wipefs image.iso
```

## mount an iso



```bash
# mounting an iso  via the loop device
sudo mkdir /media/iso/
sudo mount -o loop /path/to/ubuntu.iso /media/iso

# Mount a CD-ROM device (with the filetype ISO9660) to /cdrom (readonly):
mount -t iso9660 -o ro /dev/cdrom /cdrom

# optionally specify filesystem type
mount -t iso9660 -o loop image_cd.iso /media/cd

# mount a system image skiping partition table with given offset
sudo mount -o loop,offset=1048576 ~/mint.img /mnt/vsf

```

> **NB**
  ```
    mount -o loop image.iso /mnt/path
    # <=>
    losetup -f image.iso
    losetup --list --associated=image.iso
    mount /dev/loopx /mnt/path
    umount /mnt/path  --detach-loop
  ```



## extract iso

3 Ways to Extract and Copy Files from ISO Image in Linux (2018)<br>
https://www.tecmint.com/extract-files-from-iso-files-linux/

How to extract an ISO image<br>
https://sleeplessbeastie.eu/2014/08/26/how-to-extract-an-iso-image/

### using mounter iso

```bash
sudo mkdir /mnt/iso
sudo mount -o loop ubuntu-16.10-server-amd64.iso /mnt/iso
ls /mnt/iso/
cd /mnt/iso
sudo cp md5sum.txt /tmp/
```


### using 7zip

Extract ISO Content Using 7zip Command

```bash
sudo apt-get install p7zip-full

# extract image.iso ISO image to the extracted_image directory
7z x -oextracted_image image.iso
```

> !!! p7zip does not like spaces between switches and its arguments.

# USB

What’s the Difference Between FAT32, exFAT, and NTFS<br>
https://www.howtogeek.com/235596/whats-the-difference-between-fat32-exfat-and-ntfs/


## devices information

USB Descriptors<br>
https://www.beyondlogic.org/usbnutshell/usb5.shtml

How To Find USB Device Bandwidth Usage On Linux (using usbtop)<br>
https://www.ostechnix.com/how-to-find-usb-device-bandwidth-usage-on-linux/


`lsusb` - list USB devices<br>
Display information about USB buses and devices connected to them.
 + `-v, --verbose` : Tells lsusb to be verbose and display detailed information about the devices shown.
 + `-s [[bus]:][devnum]` : Show only devices in specified bus and/or devnum.
 + `-d [vendor]:[product]` : Show  only  devices  with the specified vendor and product ID.

> **NB**
  Verbose option display bcdUSB field (`2.00`, `3.00`, `3.10`) that indicates the version of the USB specification<br>
  `-t` option list devices by usb hierarchy (=> can find usb 3.0 devices under root usb 3.0 controller)


```bash
# List all the USB devices available
lsusb   # NB show bus, devnum, vendor id for each device

# List the USB hierarchy as a tree : Display information about USB buses and devices connected to them.
lsusb -tv

# -s [[bus]:][devnum]  Show only devices in specified bus and/or devnum,
# -v List verbose information about USB devices
lsusb -s 001:002 -v

# List devices with a specified vendor and product id {{vendor}}:{{product}} only:
lsusb -d  1d6b:0003 -v

# List detailed information about a USB device: /dev/bus/usb/<bus>/<devnum>
lsusb -D /dev/bus/usb/001/001  # Bus 1, device 1


```


`usbrip`

Show USB Devices Event History Using Usbrip In Linux<br>
https://ostechnix.com/show-usb-devices-event-history-using-usbrip-in-linux/



## Check and Test USB Flash Drives

```bash

### Example syntax, for a USB stick enumerated as /dev/sdz,
# outputting progress information, with a data-destructive write test and error log written to usbstick.log
sudo badblocks -w -s -o usbstick.log /dev/sdz

```

`f3` - Fight Flash Fraud

f3 is a simple tool that tests flash cards capacity and performance to see if they live up to claimed specifications. It fills the device with pseudorandom data and then checks if it returns the same on reading.
https://fight-flash-fraud.readthedocs.io/en/stable/introduction.html#the-extra-applications-for-linux


`H2testw`
https://sumtips.com/software/test-integrity-and-size-of-usb-flash-drives-and-hard-drives/


## Create a bootable usb drive


Create Bootable Debian 10 USB Stick on Linux<br>
https://linuxize.com/post/create-bootable-debian-10-usb-stick-on-linux/

### using dd

`# copy iso to usb device /dev/sdn
sudo dd bs=4M if=input.iso of=/dev/sd<?> conv=fdatasync
`

```bash
umount /dev/sd<x>

sudo dd bs=4M if=/path/to/debian-10.0.0-amd64-netinst.iso of=/dev/sdx status=progress oflag=sync

# or
sudo dd bs=4M if=ubuntu-20.04-desktop-amd64.iso of=/dev/sdx && sync
```

### etcher

*etcher* : https://www.balena.io/etcher/#download

How to Create Bootable Linux USB Drive<br>
https://linuxize.com/post/how-to-create-a-bootable-linux-usb-drive/

Create Bootable USB Using Etcher in Linux<br>
https://trendoceans.com/create-bootable-usb-using-etcher-in-linux/

How to create bootable Ubuntu 20.04 on windows 10<br>
https://trendoceans.com/how-to-create-bootable-ubuntu-20-04-on-windows-10/


### unetbootin

*unetbootin* -  - https://unetbootin.github.io/

UNetbootin allows you to create bootable Live USB drives for Ubuntu, Fedora, and other Linux distributions without burning a CD.

### rufus

http://rufus.akeo.ie/

Rufus: The Reliable USB Formatting Utility
Rufus is a utility that helps format and create bootable USB flash drives.


How to create a Bootable USB using Rufus for Linux Distributions<br>
https://trendoceans.com/how-to-create-a-bootable-usb-using-rufus-for-linux-distributions/


## usb multiboot

### ventoy

`Ventoy` is a free, open source and cross-platform program to create multiboot USB drives in Linux and MS Windows.

*Ventoy Features*
 * Support UEFI Secure Booting
 * Support Linux Persistence
 * MBR and GPT Partition Support
 * Directly boot from ISO files, No need to extract


`Usage:  Ventoy2Disk.sh CMD [ OPTION ] /dev/sdX`<br>
  `CMD:`<br>
   `-i`  install ventoy to sdX (fail if disk already installed with ventoy)<br>
   `-u`  update ventoy in sdX<br>
   `-I`  force install ventoy to sdX (no matter installed or not)<br>
<br>
  `OPTION: (optional)`<br>
   `-r SIZE_MB`  preserve some space at the bottom of the disk (only for install)<br>
   `-s`          enable secure boot support (default is disabled)<br>
<br>

```bash
# retrieve ventoy archive
curl -L https://github.com/ventoy/Ventoy/releases/download/v1.0.14/ventoy-1.0.14-linux.tar.gz \
 | tar -xmpz

# install ventoy on usb device
cd ventoy-1.0.14 && sudo sh Ventoy2Disk.sh -I /dev/sdc

# copy isos to boot on ventoy main partition
rsync *.iso /media/$USER/ventoy/ --progress -ah

```

> Two partitions will be created on the device
 * the first will take almost all the available space on the device, and will be formatted as `exfat`
 * the second will be used as esp, and formatted as `vfat`

How To Create Multiboot USB Drives With Ventoy In Linux<br>
https://www.ostechnix.com/how-to-create-multiboot-usb-drives-with-ventoy-in-linux/

Create Persistent Bootable USB Using Ventoy In Linux<br>
https://www.ostechnix.com/create-persistent-bootable-usb-using-ventoy-in-linux/

How to create a multiboot USB with Ventoy<br>
https://linuxconfig.org/how-to-create-a-multiboot-usb-with-ventoy

Easily Create a Multiboot USB with Ventoy<br>
https://trendoceans.com/easily-create-a-multiboot-usb-with-ventoy/




## Create a new usb drive

### Remove all existing partitions

`wipefs` - wipe a signature from a device


```bash
# Delete any previous partition table:

USB=sda
# overwrite the first 33 sectors (MBR/GPT table)
sudo dd if=/dev/zero of=/dev/$USB bs=512 count=33


# or using wipefs to Wipe all available signatures for specified device (-a | --all)
sudo wipefs -a /dev/$USB

```

### Create a new partition table

```bash
USB=sda

sudo parted /dev/$USB # create interactively

sudo parted /dev/$USB -s mktable gpt  # or msdos
sudo parted /dev/$USB unit s print free

# NB 32 sectors are reserved for gpt table, 512b * 32
# the GPT provides data duplication; the header and partition table are stored both at the start and the end of the disk.
# NB 1 sector is reserved for msdos table
```

### Create partition

```bash
USB=sda

# Using newer disk drives, GPT is recommended which uses 2048 as start sector by default.
# => aligns to 1MiB boundaries.
# Most partitioning tools today (late 2012) align partitions on 1MiB (2048-sector) boundaries by default.
# Advanced Format feature (4096-byte physical sectors and 512-byte logical sectors) require partition start sectors to be multiples of 8 in order to get optimal performance.
# 40 (multiple of 8) is the smallest start sector for a GPT disk with a standard partition table size that works well with Advanced Format disks.

sudo parted /dev/$USB mkpart primary 0% 100%
# Using % parted will determine automatically optimal alignement, starting at 2048s for gpt partition

sudo parted /dev/$USB align-check opt 1

# WARNING using gpt partition on windows requires to set msftdata flag
sudo parted /dev/$USB set 1 msftdata on

```

### Format and mount partition

```bash
# Using fat
USB=sda1

sudo mkfs.vfat /dev/$USB

# Using fat32
apt-get install dosfstools
sudo mkdosfs -F 32 -I /dev/$USB
#<=>
sudo mkfs.fat -F32 -v -I /dev/$USB

# Using exfat
# centos : sudo yum install exfat-utils fuse-exfat
# debian
sudo apt-get install --reinstall -y exfat-fuse  exfat-utils
# or install exfat-fuse exfatprogs
sudo mkexfatfs -n MonEXFAT /dev/sdXN # <=> mkfs.exfat

sudo mount -t exfat /dev/sdc1 /media/exfat
```

# Backup/Restore

How to restore your Ubuntu Linux system to its previous state (using systemback)<br>
https://vitux.com/how-to-restore-your-ubuntu-linux-system-to-its-previous-state/

Best Linux Backup Software For Desktops And Servers  (04/2021)
http://www.linuxandubuntu.com/home/best-linux-backup-software

## desktop

**Timeshift**

Timeshift is one of the most popular backup software for Linux. It comes pre-installed with many popular Linux distributions such as Linux Mint and Manjaro. Timeshift is a very handy tool and easy-to-use application that helps you to create a restore point in your system.

## server

**Bacula** (For enterprise)

Bacula is a popular backup solution focused on enterprise-level backup solutions.

# Data Recovery



Essential System Tools: ddrescue – Data recovery software<br>
https://www.linuxlinks.com/essential-system-tools-ddrescue-data-recovery-software/

Recover deleted files in Debian with TestDisk<br>
https://vitux.com/recover-deleted-files-in-debian-with-testdisk/

How to Use Foremost to Recover Deleted Files in Linux<br>
https://www.maketecheasier.com/use-foremost-recover-deleted-files-linux/

# Data Wiping



Shred Command - Securely Delete Files in Linux<br>
https://www.putorius.net/shred-command-securely-delete-files.html


```bash

```
