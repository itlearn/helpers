# Introduction to Kubernetes on Edge with K3s

 * [Course Overview](#course-overview)
 * [Chapter 1. The Case for Edge Compute](#chapter-1-the-case-for-edge-compute)
 * [Chapter 2. The Edge Compute Landscape](#chapter-2-the-edge-compute-landscape)
 * [Chapter 3. Scaling Down and System on a Chip Devices](#chapter-3-scaling-down-and-system-on-a-chip-devices)
 * [Chapter 4. K3s: What Is and Why Is It Needed?](#chapter-4-k3s:-what-is-and-why-is-it-needed?)
 * [Chapter 5. Setting Up Your Own Lab Environment](#chapter-5-setting-up-your-own-lab-environment)
 * [Chapter 6. Deploying Real Workloads with the Kubernetes API](#chapter-6-deploying-real-workloads-with-the-kubernetes)
 * [Chapter 7. Functions at the Edge](#chapter-7-functions-at-the-edge)
 * [Chapter 8. Command & Control and Remote Access](#chapter-8-command--control-and-remote-access-incomplete-section)
 * [Chapter 9. Deployment Strategies for Applications at the Edge](#chapter-9-deployment-strategies-for-applications-at-the-edge)
 * [Chapter 10. Challenges with the Edge](#chapter-10-challenges-with-the-edge)
 * [Chapter 11. Continue Your Journey](#chapter-11-continue-your-journey)


**Reference**

LinuxFoundationX LFS156x

https://training.linuxfoundation.org/training/introduction-to-kubernetes-on-edge-with-k3s-lfs156x/
https://www.edx.org/course/introduction-to-kubernetes-on-edge-with-k3s


There is a growing interest in running software at the edge. This course takes a deep dive into the use cases and applications of Kubernetes at the edge using examples, labs, and a technical overview of the K3s project and the cloud native edge ecosystem.

## Course Overview

This course is designed for those interested learning more about Kubernetes, as well as in deploying applications or embedded sensors in edge locations. While learners do not need a Kubernetes certification for this course, experience with a Linux operating system and shell scripting will be beneficial. Programming experience is also not strictly required. Learners will need to be able to run Docker on their computer

This course will enable developers to learn about the growing impact the cloud native movement is having on modernizing edge deployments. They will also learn the challenges of deploying Kubernetes on the edge through a concrete example via the k3s project.

### **What you’ll learn**

In this course, you will learn the use cases for running compute in edge locations and about various supporting projects and foundations such as LF Edge and CNCF. The course covers how to deploy applications to the edge with open source tools such as K3s and k3sup, and how those tools can be applied to low-power hardware such as the Raspberry Pi. You will learn the challenges associated with edge compute, such as partial availability and the need for remote access. Through practical examples, students will gain experience of deploying applications to Kubernetes and get hands-on with object storage, MQTT and OpenFaaS. It also introduces the fleet management and GitOps models of deployment, and helps you understand messaging, and how to interface with sensors and real hardware.

 * Use cases for running compute in edge locations
 * How to deploy applications to the edge with open source tools such as K3s and k3sup
 * How open source tools can be applied to low-power hardware such as the Raspberry Pi
 * The challenges associated with edge compute, such as partial availability and the need for remote access
 * Gain experience of deploying applications to Kubernetes and get hands-on with object storage, MQTT and OpenFaaS
 * Fleet management and GitOps models of deployment
 * Understand messaging and how to interface with sensors and real hardware


### At a glance

 * Institution: LinuxFoundationX
 * Subject: Computer Science
 * Level: Introductory
 * Prerequisites:

    * You should be familiar with the Linux Operating System and how to use common CLI commands to pass arguments, make use of configuration files, and to configure networking.

    * A basic understanding or some prior experience with deploying applications to Kubernetes would be helpful to you.

    * You will need to be able to run Docker on your computer.

 * Language: English


### **Course Syllabus**

 * Welcome
 * Ch 1. The Case for Edge Compute
 * Ch 2. The Edge Compute Landscape
 * Ch 3. Scaling Down and System-on-Chip Devices
 * Ch 4. What Is K3s and Why Is It needed?
 * Ch 5. Setting Up Your Lab Environment
 * Ch 6. Kubernetes API Primitives
 * Ch 7. Functions at the Edge
 * Ch 8. Command & Control and Remote Access
 * Ch 9. Deployment Strategies for Applications at the Edge
 * Ch 10. Challenges with Edge
 * Ch 11. Further Resources
 * Final Exam (verified track only)


### **About the instructors**

Alex Ellis
Founder of OpenFaas and inlets
OpenFaaS Ltd

### **Course Details**

 * Length: 10 Weeks of Free Access to Online Course / 14-16 Hours of Course Material 
 * Effort: 2-3 hours per week
 * Level: Beginner
 * Price: Free (certificate optional)


----

## Chapter 1. The Case for Edge Compute

### **Overview and Learning Objectives**

In this first chapter, we will be setting the scene for what is to come in the course. We begin by setting expectations, exploring what edge is and why it matters, and we will briefly touch on some of the expected challenges with this form of computing.

By the end of this chapter, you will have answers to the following questions:

 * What can you expect from this course?
 * What is "the edge" and why should you care?
 * What use cases make sense for the edge?
 * What are the challenges with working at the edge?


### **Learn the Case for Edge Compute**


#### == **What Can You Expect from This Course?** ==

This course aims to give you a working knowledge of a category of compute called edge - by defining the term, introducing the use cases so that you know what makes a good fit, and what it is useful for. We will also explore the broader landscape of projects and foundations which are exploring this area. There is also a hands-on element to the course, which gets into how you can leverage Kubernetes in edge locations and on constrained devices.

The content will focus primarily on the infrastructure level - hardware and underlying software, but in several chapters, you will also build an understanding of how to deploy applications to Kubernetes using K3s, a distribution of Kubernetes that is well-suited for edge.

So, what is considered a non-goal for the course? Whilst the course will cover various elements of running Kubernetes on a Raspberry Pi, it is not a complete guide, and you will be much better served by resources we include in the final chapter.

#### == **What Is "The Edge" and Why Should You Care?** ==

As we attempt to define the edge, let’s first look at what it is not. Recent trends have seen companies migrate traditional workloads from on-premises equipment and data centers, to cloud providers such as AWS and Azure. In some instances, bespoke software and solutions, which used to require custom development and support, can be replaced by managed services which make moving to a cloud a compelling option.

An example may be an accounting or legal agency which had its own secure document storage product for sharing confidential information with clients. This software may have been deployed to servers managed by the company’s own data center and engineers, using custom software such as a database for the storage of the files. When migrating to a cloud vendor, a similar managed product may be available, which could mean making considerable cost savings by removing the maintenance and hosting costs. It could also be that the company decides to migrate the front-end of their application, and to use a managed storage service instead of their relational database, saving on the associated costs.

Cloud providers tend to offer compute and managed services in multiple regions, but these are not always extensive, and in some cases, companies need data to be kept within a region for compliance. This is the first type of edge computing - a local datacenter for compliance reasons.

The second type of edge compute is an embedded system most commonly found in automotive and industrial industries. Here, an embedded computer may gather information from directly attached sensors and use that information to control hardware. An example would be within an agricultural silo used for drying grain, where the humidity levels need to be carefully monitored and adjusted. Another example may be in the automotive industry, where a feed from a video camera may be used to help an onboard computer make lane changes or to assist with parking.

Embedded systems are also found within smart homes, where a light bulb may provide an Application Programmable Interface (API) for control via a smartphone app.

The third category is a hybrid of the first and second category - where processing needs to be brought closer to the end user of a service due to latency requirements. This can be referred to as the last mile. An example of "last mile" compute may be a Content Delivery Network (CDN) which provides a high-speed cache of data within a local region. A streaming video service may launch a new series in Europe and make the data available within an edge location to enable faster response times and streaming.

The type of edge compute is not always segmented, and can be a mix of all three broad categories.

To recap, we can have the following types of edge:

 * Local datacenter for compliance
 * Embedded systems for industrial, automotive and smart home applications
 * Low-latency applications for last mile.


#### == **What Are the Use Cases of Edge?** ==

Many homes in Europe and US now have smart meters installed to measure the consumption of utilities such as electricity, water, and gas. The smart meters can be accessed via a smartphone app or a separate read-out to make informed decisions about usage, and to enable more accurate billing for suppliers. A custom-built device will be built, usually with a general-purpose microcontroller that is programmed with a Read-Only Memory (ROM) and designed to run for many years without any intervention or updates. It is likely to communicate using radio waves, rather than being attached to your home network’s WiFi or telephone line, to make it more reliable.

Your Internet Service Provider (ISP) probably issued you with a router running a Linux operating system. This is a more general-purpose type of controller, and usually has a way to update its software and firmware through a web UI. This is a type of edge device which connects your home to a larger network.

If you have ever got a fine from parking your car in a restricted zone, a similar system may be running, but this time a more general purpose computer chip may be used, such as one running Linux and a machine-learning model, which scans registration plates and sends the plate number over the Internet to a central datacenter, perhaps running in the cloud. Within a few days, an operator may review the image, look up your car’s details and issue a fixed-penalty fine.

In the previous section we touched on the idea of an on-premises datacenter for low-latency applications, and on the industrial and automotive sectors.

The use case for edge compute usually comes down to three needs:

 * Being tied to the real-world through sensor inputs or outputs (industrial, automotive, and smart home)
 * Needing to be close to the end users of a system (last mile)
 * Local and on-premises datacenters for compliance.

This course will primarily focus on the need for self-hosted applications needing to be close to users. We will also touch briefly on the IoT use case, and talk about messaging protocols and hardware available to accelerate the use of machine learning models.

#### == **What Are the Challenges with Working at the Edge?** ==

There are several challenges present in all applications of edge computing.

Since most edge devices need to be built once, and run for a very long time without user intervention, a large amount of time and money is required to test, harden and roll-out solutions compared to centrally-hosted services. In the industrial sector, strict safety and certification requirements need to be met.

An example may be the Raspberry Pi Foundation, that wanted to send a $35 micro-computer into space for astronauts aboard the International Space Station (ISS) to operate as part of their [AstroPi program](https://astro-pi.org/about/).


 * As part of British ESA, astronaut Tim Peake’s Principia mission (2015–2016) on the ISS, two space-hardened Raspberry Pi computers, called Astro Pis, equipped with environmental sensors, were sent to the ISS and then used to run students’ and young people’s programs, with ISS crew support.
 * 17,000 young people participated in 2019-2020, including participants from 25 countries.

The foundation had to create a case made from 6063 grade aluminium, which is standard for aerospace applications. This came at a great cost, which then had to be certified and put through rigorous testing (you can learn more from the [Astro Pi flight case](https://www.raspberrypi.org/blog/astro-pi-flight-case/) blog article). Whilst this may be a special case, it goes to show what challenges can be present in deploying edge compute into the real world.

If compliance and regulation are not an issue, then there are other problems to content with, such as partial connectivity. A microservice or API deployed in a managed cloud is likely to have close to 99.9% uptime and connectivity to the network, whereas devices deployed in homes and further away from the core power grid and network may encounter fluctuations.

It may also be costly and inefficient to have devices connected to the Internet at all times, meaning that new messaging techniques and resilience has to be built into any controller software that works with edge applications.

Deploying updates to edge devices can also be a challenge. Many home security cameras have been found to be vulnerable to hacking, so much so that a website called Shodan took to popularity. [Shodan](https://www.shodan.io/) is described as “The world’s first search engine for Internet connected devices”.

If devices are not managed centrally, or require firmware updates to be applied by users, then two problems need to be overcome: distribution of the said updates, and communication to the end users. The more powerful, and general purpose computers, such as those found within an electric car or a smartphone, can receive Over The Air (OTA) updates, which helps with this issue.

----

## Chapter 2. The Edge Compute Landscape


### **Overview and Learning Objectives**

In the previous chapter we aimed to understand edge computing concepts and use cases. In this chapter, we will look specifically at software, foundations and initiatives aiming to lower the barrier to entry for edge applications.

By the end of this chapter, you should be able to:

 * Discuss the various projects and foundations of edge computing.
 * Understand at a high level what each project can offer.
 * Review the edge compute landscape: 
   * LF Edge: Eve and EdgeX Foundry
   * BalenaOS from Balena
   * Azure Akri
   * K3s from the Cloud Native Computing Foundation (CNCF).

### **Foundations and Projects for Edge Compute**

#### == **The Linux Foundation and Its Initiatives in the Edge Compute Landscape** ==

[The Linux Foundation](https://www.linuxfoundation.org/) is dedicated to building sustainable ecosystems around open source projects to accelerate technology development and industry adoption. Founded in 2000, the Linux Foundation provides unparalleled support for open source communities through financial and intellectual resources, infrastructure, services, events, and training. Working together, the Linux Foundation and its projects form the most ambitious and successful investment in the creation of shared technology. The Linux Foundation is the umbrella for many critical open source projects that power corporations today, spanning all industry sectors.

Foundations aim to level the playing field between companies that may otherwise be competitors, and to bring various efforts together. The two main foundations that are important for you to know are the Cloud Native Computing Foundation (CNCF) and the LF Edge, both of them under the Linux Foundation umbrella.

#### == **Cloud Native Computing Foundation** ==

The [Cloud Native Computing Foundation (CNCF)](https://www.cncf.io/) is the home of many open source projects designed for running applications in the cloud and on modern infrastructure. The first project that joined the CNCF was Kubernetes, which was donated by Google in order to attract more contributors and users. The various projects within the CNCF have varying tiers:

 * Sandbox
  
   These are projects that are just starting, and need a lot of input and change before becoming more mature.
 
 * Incubation
 
   These projects are considered suitable for production, and show growing signs of traction.
 
 * Graduated
 
   These projects are heavily used in production, with independent maintainers from several companies and organizations. 

There are many other projects which are not part of the CNCF, but which are tracked in the [CNCF cloud native interactive landscape](https://landscape.cncf.io/) and partitioned into various categories like networking, security, runtime and more.

#### == **LF Edge** ==

[LF Edge](https://www.lfedge.org/) is an umbrella organization that aims to establish an open, interoperable framework for edge computing independent of hardware, silicon, cloud, or operating system. LF Edge is a Linux Foundation initiative and was [launched in January of 2019](https://www.linuxfoundation.org/en/press-release/the-linux-foundation-launches-new-lf-edge-to-establish-a-unified-open-source-framework-for-the-edge/); at launch, LF Edge was comprised of five projects that aimed to support emerging edge applications in the area of non-traditional video and connected things that require lower latency, faster processing and mobility. At launch, it had the support of 60 global organizations as founding members and new project contributions.

"We are fostering collaboration and innovation across the multiple industries including industrial manufacturing, cities and government, energy, transportation, retail, home and building automation, automotive, logistics and health care — all of which stand to be transformed by edge computing."

The [LF Edge Interactive Landscape web page](https://landscape.lfedge.org/card-mode?category=lf-edge-member-company&grouping=category&style=logo) provides a good representation of the organizations supporting the LF Edge initiatives.

#### == **LF Edge: Project Eve** ==

[Project EVE](https://www.lfedge.org/projects/eve/) is building EVE-OS, a universal, open Linux-based operating system for distributed edge computing, designed to run appliances such as network firewalls and routers inside a virtualized environment. With EVE-OS, a company can consolidate several hardware devices into one physical appliance. Supporting Docker containers, Kubernetes clusters and virtual machines, EVE-OS provides a flexible foundation for distributed edge deployments with choice of any hardware, application and cloud.

As a real-world use case example, Project EVE is used for predictive maintenance for oil rigs, where AI can help detect when a drill has come to the end of its useful life, or when it is about to fail. A similar use case would be with Heating, Ventilation, and Air Conditioning (HVAC), where an industrial cooling unit may be monitored continually for signs of failure or wear. An accurate prediction can mean the difference between a unit failing, and being serviced to prevent a failure.

Project Eve Logo


#### == **LF Edge: EdgeX Foundry** ==

[EdgeX Foundry](https://www.edgexfoundry.org/) is a highly flexible and scalable open source software framework that facilitates interoperability between devices and applications at the IoT Edge.

EdgeX Foundry is split into four primary layers: device, core, supporting, and application. It is a collection of more than a dozen microservices that are deployed to provide a minimal edge platform capability.

![EdgeX Foundry Platform Layers](LFS156-EdgeX_Foundry_Platform_Layers.png)


EdgeX Foundry uses Hashicorp's Consul project for service discovery and registration.

![Consul Registry Check](LFS156-EdgeX_GettingStartedUsrConsul.png)

Consul Registry Check (retrieved from the EdgeX Foundry Documentation)

#### == **Balena: Balena Cloud and BalenaOS** ==

[BalenaCloud](https://balena.io/) is a device deployment and management infrastructure platform aimed at deploying IoT applications and managing fleets of IoT devices. [BalenaOS](https://www.balena.io/docs/reference/OS/overview/2.x/) is the operating system deployed to each of the IoT devices which make up a larger fleet: its tagline is: "Run Docker containers on embedded devices".

![Balena Cloud](LFS156-Balena_Cloud.png)

BalenaCloud (retrieved from balena.io)

BalenaOS can be used as a self-hosted operating system, or connected to Balena’s cloud. It uses the open source [Moby project](https://mobyproject.org/) created at Docker to schedule workloads as containers on devices.

As of April 2021, over 65 different devices are supported by Balena Cloud; each must be able to run a Linux operating system and have virtualization capabilities. Balena’s (optional) cloud platform has built-in tools for provisioning, secure tunnels, and distributing updates efficiently.

Balena has a built-in mechanism to build and deploy new versions of code, triggered by pushing code to a git repository. You can see a number of demos for the Raspberry Pi, like running an SSH server, controlling GPIO, or accessing a camera.

#### == **Akri** ==

[Akri](https://github.com/deislabs/akri) was designed for "building a connected edge with Kubernetes"; the project was developed by the Microsoft Azure team and [open sourced in October 2020](https://cloudblogs.microsoft.com/opensource/2020/10/20/announcing-akri-open-source-project-building-connected-edge-kubernetes/).

> Akri lets you easily expose heterogeneous leaf devices (such as IP cameras and USB devices) as resources in a Kubernetes cluster, while also supporting the exposure of embedded hardware resources such as GPUs and FPGAs. Akri continually detects nodes that have access to these devices and schedules workloads based on them."

So, it seems that Akri is less about managing edge Kubernetes clusters, and more about using Kubernetes as a conduit, and data plane for accessing remote devices such as GPUs and IP cameras.

![Akri Architecture](LFS156-akri-architecture.png)

Akri Architecture (retrieved from Akri's GitHub repository)

Akri's focus is discovering and managing edge Kubernetes clusters. Akri does this is by connecting edge clusters back to a larger centrally-managed cluster through two new components: the Akri Controller and Akri Agent.

#### == **CNCF: K3s** ==

The CNCF provided a home for the Kubernetes project, originally created at Google, which was then open sourced and given a neutral home, where other software vendors could get a seat at the table and feel sure that the project was not controlled by any one given corporation.

The [K3s](https://k3s.io/) project was created by Rancher Labs in an attempt to reduce the memory footprint of Kubernetes, and to simplify the onboarding experience. It is especially suited to edge applications and was donated to the CNCF as a sandbox project, which has the same benefit for potential users and adopters as when Kubernetes joined the foundation.

K3s can provide similar capabilities to EVE through other CNCF and cloud native projects, and, when built as part of a complete architecture, can resemble a complete system more like EdgeX Foundry. We chose K3s as the core of this course for its flexibility and suitability to constrained devices. It also offers a very low barrier to entry for learning and experimentation.


----

## Chapter 3. Scaling Down and System on a Chip Devices

### **Overview and Learning Objectives**


In this chapter, we shift focus from software to hardware again, and look at what kind of devices are commonly used for running edge workloads. We will introduce the idea of System on a Chip (SoC) and talk about some of the challenges associated with the architecture. Then, we will cover what it looks like to scale up and down at the edge.

By the end of this chapter, you should be able to:

 * Explain what is System on a Chip (SoC).
 * Discuss the differences between SoC and traditional CPU architecture.
 * Discuss the pros and cons of scaling down.


### **Scaling Down and SoCs**

#### == **What Is a System on a Chip (SoC)?** ==

Depending on when you started using computers, you may remember the days when home users had two options for getting a new PC. The first was going to a local store to buy off-the-shelf, and the second option involved buying parts and assembling a system yourself.

You would source a case or chassis, followed by a hard disk for storage, a CD-ROM drive for loading an operating system and applications, a network card, a modem, a number of cables, a CPU, a cooler for the CPU, a Power Supply Unit (PSU), and finally, a motherboard into which all of the above would be plugged.

Fast forward to 2021, and all of the above can be miniaturized into a smartphone that fits in your pocket, and potentially has more computing power than a custom-built PC from 10 years ago.

How is this possible? The core idea is that rather than making memory, storage, motherboard, and a CPU swappable and interchangeable, they could all be built into the same chip, to create a much smaller and more efficient device.

According to [Wikipedia](https://en.wikipedia.org/wiki/System_on_a_chip), "A system on a chip is an integrated circuit that integrates all or most components of a computer or other electronic system."

You may have other SoCs already like the Raspberry Pi, a microcomputer capable of running Linux and Kubernetes.

![Raspberry Pi 4](LFS156-raspberry-pi-4.png)


The Raspberry Pi uses a Broadcom chipset, which was originally designed for a mobile phone, so for this reason, it has typically been underpowered when compared to a traditional PC or the Apple M1. Its cost is, however, very competitive for a computer capable of running a full Linux operating system and K3s.

#### == **How Does SoC Compare to Traditional CPU Architecture?** ==

A System On Chip (SoC) architecture combines several systems into one chip along with the CPU. A traditional architecture would have separate discrete components that can be exchanged and updated separately. An advantage of building a SoC is that the overall size can be reduced, at the cost of not being able to upgrade any of the components.

If you’ve heard of the new generation of Apple hardware called M1, this is a good example of a SoC architecture, which goes even further to add in a security chip, GPU (Graphics Processing Unit), machine-learning accelerator and much more. On the other hand, the storage, RAM and CPU are no longer upgradeable. This is less of a concern with an edge compute device, but with a consumer-facing laptop, it means the only way to increase storage or speed is to buy an entirely new Apple M1 computer.

See the following diagram from [TechRadar](https://www.techradar.com/news/apple-m1-chip) of the M1 architecture:

![Apple M1](LFS156-Apple_M1.jpeg)

#### == **Pros and Cons of Scaling Down** ==

Both the Raspberry Pi and the Apple M1 chips are based upon an ARM architecture, which is low-cost, and power-efficient. This is in contrast to traditionally Intel-compatible chips, which tend to consume much more power.

The primary drawback of ARM chips is that software developed for Intel machines is not directly compatible, and requires "porting" and re-building. Fortunately, when it comes to Kubernetes, the hobbyist community has spent a great deal of time preparing the way.

![Ampere Altra Arm Server](LFS156-Ampere_Alta_ARM_Server.png)

Ampere Altra ARM Server (retrieved from amperecomputing.com)

Several manufacturers like Ampere Computing and AWS have industrial-grade ARM chips and servers which retain the benefits of lower power consumption and a SoC architecture, but deliver better performance by adding more CPU cores. So, whilst a Raspberry Pi may have 4 cores, an Ampere Altra server may come with 160 CPU cores and the ability to host many dozens of gigabytes of RAM. This kind of scale is known as Vertical Scaling, where a single device is made more powerful.

When it comes to the Raspberry Pi (RPi), Horizontal Scaling is most popular for overcoming the limitations of its specification. Whilst a single RPi may deliver 4 cores and up to 8 GB of RAM, 40 of them can offer similar specifications (on paper) to an industrial-grade server.

If you’d like to learn more about the journey and specific challenges with supporting Kubernetes on Raspberry Pi and ARM chips, see my talk from KubeCon 2020: [The Past, Present, and Future of Kubernetes on Raspberry Pi](https://www.youtube.com/watch?v=jfUpF40--60).



----

## Chapter 4. K3s: What Is and Why Is It Needed?

### **Overview and Learning Objectives**

In this chapter, we return to software for edge, specifically Kubernetes and the K3s distribution originally created at Rancher Labs. You’ll learn how it started off as an experiment, how it gained traction, its own ecosystem, and how became GA. You’ll also understand what the component parts of the project are and how it differs from upstream Kubernetes.

By the end of this chapter, you should be able to:

 * Discuss how K3s started off, and its evolution.
 * Compare K3s to Kubernetes.
 * Discuss the specific projects created by K3s and how are they used.
 * Understand why K3s use is suited at the edge.

### **What Is K3s and Why Is It Needed?**

#### == **How Did K3s Come to Be?** ==

In September 2018, Darren Shepherd, Chief Architect at Rancher Labs had a brainwave and created a project called K3s, to scratch a personal itch. It turned out to be much more popular than he initially expected, and K3s is now considered a production-ready version of Kubernetes.

However, several years prior to that, a number of companies had created their own bespoke solutions to manage containers across clusters. Darren’s offering was called Cattle and was very popular within the Rancher community. Docker created their Swarm product, and Mesosphere created Mesos, another tool to manage microservices across multiple nodes.

Around the same time, the Kubernetes project emerged from Google and became the first project of the CNCF. This was the beginning of a period of time known as the container wars, where Docker, Mesosphere, Rancher and Google fought tooth and nail to dominate the space. Both Docker Swarm and Cattle were simple and easy to manage, but the writing was on the wall, and Kubernetes had won the battle. Customers only cared about Kubernetes, and Rancher started deprecating their orchestrator.

Unfortunately, this move left a gap in the market for users who found Kubernetes too hard to set up, too resource-intensive and too complex to configure. Darren’s tinkering led him to create a patch that removed 8 million lines of code from Kubernetes for the various features he felt weren’t needed for his user base.

The [K3s: Kubernetes without the features I don't care about](https://news.ycombinator.com/item?id=18080390) article that was published in the Hacker News had 88 comments and 171 upvotes, showing that there was an underlying desire to make Kubernetes more accessible. This is what Darren said on the thread:

"I created this (I'm the Chief Architect and Co-founder of Rancher Labs, I experiment with stuff like this all the time). [...] I never had any intention to support it as a standalone project, but hey if somebody is interested who knows. 

All of the features removed are based off of two basic ideas

1) If there are X ways to do something, I choose the best one (obviously subjective).

2) The features are not commonly used or really shouldn't be in k8s to begin with.

The end goal of what I'm working on is basically to use k8s code as a library to do orchestration. Step one was to reduce the footprint of k8s (I'm 80% there with this project), step two is to completely rework the state management such that you no longer need to persist state in k8s. This is a much larger goal. I've fooled around a lot with the persistence layer in k8s (this project is running on sqlite3) so I'm basically doing a lot of work of figuring what state I can throw away. The theory is that all desired state comes from your yaml files. Actual state is actual (what really exists). Everything else in k8s should be purely derived and not important."

The name K3s comes from K8s, just with a smaller number to show how the project has shrunk. Three years later, Mirantis went one step further and released their own K3s-like distribution of Kubernetes and called it K0s.

Over time, the 8 million line patch went away, and Darren found ways to configure, bundle, and add code and get a very similar result.

#### == **How Does K3s Compare to Kubernetes?** ==

When comparing it with Kubernetes, K3s:

 * Exposes the same REST API and kubectl CLI.
 * Uses the same YAML files and API objects.
 * Uses the same architecture and components.
 * Uses the same SDKs and client components.


![How K3s Works](LFS156-how-it-works-k3s.png)


Where it differs is primarily in bundling and in bootstrapping. Core components that run and manage containers on a host like [containerd](https://containerd.io/) (container runtime) and [runc](https://github.com/opencontainers/runc) are built directly into the K3s binary. So, rather than installing these tools separately, followed by K3s, everything required can be distributed in a single binary. The bootstrapping is simplified through the use of an HTTP tunnel, which allows the nodes and masters to communicate together.

K3s also remaps some terminology: a Kubernetes master is called a server, and a node is called an agent.

There are several personas or users of Kubernetes. Next, let's discuss how using K3s may affect each of them.

 * **Application developers** create code and declarative configuration files written in YAML that describe how to deploy their applications. For them, K3s presents no obvious difference to upstream Kubernetes.
 * **Platform engineers** create internal tooling and help to support application developers. They still get to work with the same Kubernetes API, however, some choices are already decided for them - like, what networking will be used, and how to configure High Availability (HA).
 * **SecOps engineers and SREs** - this group of users are responsible for the uptime of a cluster and for hardening its boundaries. Their experience with upstream Kubernetes will be of use here, however, there are very specific decisions taken by the K3s project, which means they will have to spend additional time learning how to build a reliable system.
 * **Home users and hobbyists** present an entirely valid and growing group of users who are learning, testing, and developing applications or deploying applications built by others. They may be building Raspberry Pi clusters on the weekends, or after working hours. For them, creating a highly-available cluster with K3s is made much simpler through the cluster bootstrap experience, which is simplified when compared to  traditional tools like **kubeadm**.


#### == **Why Is K3s Suited to the Edge?** ==

At the time of writing (April 2021), the K3s website describes the project as "lightweight Kubernetes", which captures Darren’s original vision for K3s.

Over the past two years, Darren’s 8-million line patch has been evolving from a maintenance nightmare, to a set of purpose-built components that can be reassembled into a platform that has a low footprint, and is easy to use. We will cover some of those components in the next section.

The official positioning of K3s is "the certified Kubernetes distribution built for IoT & Edge computing". But how can it claim that, if it is compatible with the unmodified version of Kubernetes?

With the upstream project, a point was reached when Raspberry Pi just wasn't a priority, or even tenable as a runtime target. At one point in 2018, 2GB of RAM was required to even bootstrap a Kubernetes cluster; this ruled out the Raspberry Pi 3, which was current at the time with its 1GB of RAM.

From the first release of the project, Darren made sure that K3s would run on ARM servers and SoCs like the Raspberry Pi. K3s cut that memory requirement down to just 500MB per server and 50MB per agent. Now, at runtime, it clearly requires more memory than that, and Rancher have written up some guidelines on allocating memory. For more details, see also the [Resource Profiling](https://rancher.com/docs/k3s/latest/en/installation/installation-requirements/resource-profiling/) section in the K3s documentation.

The single binary required to bootstrap K3s was also a selling point for Edge and IoT, where access to servers and devices can be difficult to manage. A simple solution was for K3s to bundle and make default choices about networking and state management, which we will see in the next section.

#### == **Projects Created by K3S and How Are They Used** ==

Next, let's discuss some of the projects created by K3s and how these projects are used.

**Kine**

The primary difference with the first version of K3s was that it was not highly available; it could not tolerate the failure of a Kubernetes master. This deficiency was also its killer feature. The core components of Kubernetes are stateless and rely on a key-value store called etcd to maintain information about nodes and workloads. So what did the team do? They developed an interface called Kine that looked and responded like etcd, but was actually driven by SQLite underneath. [SQLite](https://www.sqlite.org/index.html) is an embedded database which is primarily used in smartphone apps due to its small size and high performance.

In retrospect, the K3s team added back an embedded version of etcd, which can be configured by K3s, without needing the traditional bootstrapping and configuration overheads. This allows users to decide between a single K3s node which cannot survive a failure, or a redundant system managing many worker nodes.

For more details, check out the [Kine GitHub documentation](https://github.com/k3s-io/kine).

**Binary Embedding**

K3s can be installed through a single binary, which packages a compatible version of containerd, runc, kubectl, and a number of other tools required by Kubernetes. This binary is a bit like a self-extracting archive, and means K3s can be updated just by downloading a new version. This is important at the edge, where a bad upgrade could mean sending out an engineer to a customer site to restore a device.

**Add-ons and Defaults**

K3s makes the choice to use Flannel as the networking driver, and whilst this can be changed, K3s has specific options and binary code built-in to make this a preferred choice.

K3s also bundles the **metrics-server** to get runtime statistics on pods and nodes, and Traefik 1.x, an ingress controller to control incoming network traffic. In some cases, you will want to disable the built-in add-ons, such as when you use Kong or Nginx as your ingress controller.

**Cluster Join Tokens**

The bootstrap process for K3s involves installing a server, and then joining agents to it. The initial installation can be configured by downloading the k3s binary, which contains both the client and the server.

![K3s architecture with a single server](LFS156-k3s-architecture-single-server.png)

A more popular method is to use a helper shell script and curl, such as:

```bash
curl -sfL https://get.k3s.io | sh -
```

Then, the user can find the join token on the server, and use it with the same script to join an agent:

```bash
# On a different node run the below. NODE_TOKEN comes from /var/lib/rancher/k3s/server/node-token

# on your server

sudo k3s agent --server https://myserver:6443 --token ${NODE_TOKEN}
```

The installation script has many options, which can be overridden through environment variables to customize the installation. For example, you may want to enable SQLite, or embedded etcd. You may want to install a specific version of K3s or enable/disable an add-on like Traefik.

A common issue for new users is that K3s stores its configuration file in a non-standard location, with root ownership, so it can be a struggle for first time users.

On the server, the `kubeconfig` is stored at `/etc/rancher/k3s/k3s.yaml`, so if you do log in to the host, you’ll need to use `sudo` to run `kubectl`.


#### == **Introducing K3sup (“ketchup”)** ==

A community tool named [k3sup](https://k3sup.dev/), pronounced "ketchup", makes the bootstrap even easier, and can be run over SSH. 

The main two commands for the k3sup CLI are:

 * **install** 
  
   Form a single server, or multi-server HA cluster.

 * **join**
  
   Add agents into a pre-existing cluster.

So why does k3sup need to exist? With its two commands, it makes it very quick and easy to discover the installation options, and then to go on to build out a working cluster in a short period of time.

At the time of writing (April 2021), k3sup has 3000 GitHub stars and many community blog posts written about it. The K3s team is supportive of the tool and the team members have said that they use it themselves.

Here is an example of how to build a three-node cluster:

```bash
k3sup install --host $AGENT1
k3sup join --host $AGENT1 --server-host $SERVER
k3sup join --host $AGENT2 --server-host $SERVER
```

After the install command, k3sup fetches the kubeconfig file so that you can access your cluster via kubectl.

```bash
export KUBECONFIG=𝑝𝑤𝑑/kubeconfig
kubectl get node

NAME      STATUS   ROLES    AGE     VERSION
k3sup-1   Ready    master   73s     v1.18.6+k3s1
k3sup-2   Ready    <none>   2m31s   v1.18.6+k3s1 
k3sup-3   Ready    <none>   14s     v1.18.6+k3s1
```

k3sup aims to make the various options you may need easily accessible through a CLI:

```bash
$ k3sup install --help
Install k3s on a server via SSH.

Usage:
  k3sup install [flags]

Examples:
  k3sup install --ip 192.168.0.100 --user root
  k3sup install --ip 192.168.0.100 --k3s-channel latest
  k3sup install \
  --host ec2-3-250-131-77.eu-west-1.compute.amazonaws.com \
  --ssh-key ~/ec2-key.pem --user ubuntu

Flags:
      --cluster                Form an etcd cluster
      --context string         Set the name of the kubeconfig context. (default "default")
      --datastore string       Optional: connection-string for the k3s datastore to enable HA, i.e. "mysql://username:password@tcp(hostname:3306)/database-name"
  -h, --help                    help for install
      --host string             Public hostname of node on which to install agent
      --host-ip string          Public hostname of an existing k3s server
      --ip ip                   Public IP of node (default 127.0.0.1)
      --k3s-channel string      Optional release channel: stable, latest, or i.e. v1.19 (default "v1.19")
      --k3s-extra-args string   Optional extra arguments to pass to k3s installer, wrapped in quotes (e.g. --k3s-extra-args '--no-deploy servicelb')
      --k3s-version string      Optional: set a version to install, overrides k3s-channel
      --local                   Perform a local install without using ssh
      --local-path string       Local path to save the kubeconfig file (default "kubeconfig")
      --merge                   Merge the config with existing kubeconfig if it already exists.
                                Provide the --local-path flag with --merge if a kubeconfig already exists in some other directory
      --no-extras               Disable "servicelb" and "traefik"
      --skip-install            Skip the k3s installer
      --ssh-key string          The ssh key to use for remote login (default "~/.ssh/id_rsa")
      --ssh-port int            The port on which to connect for ssh (default 22)
      --sudo                    Use sudo for installation. e.g. set to false when using the root user and no sudo is available. (default true)
      --tls-san string          Optional: defaults to server IP, unless provided
      --user string             Username for SSH login (default "root")
```

k3sup can also update the configuration file for `kubectl` by creating and merging different clusters into your `KUBECONFIG` file.


#### == **Introducing K3sup (Continued)** ==

In the following example, we install two single-node Raspberry Pi clusters and merge them into our `kubeconfig` file, so we can switch to either one by name:

```bash
k3sup install --user pi \
  --ip 192.168.0.101 \
  --context pi-cluster1 \
  --kubeconfig $HOME/.kube/config \
  --merge

k3sup install --user pi \
  --ip 192.168.0.102 \
  --context pi-cluster2 \
  --kubeconfig $HOME/.kube/config \
  --merge
```

You can then use `kubectl config use-context pi-cluster1` or `kubectl config use-context pi-cluster2` to access either of the two single node Raspberry Pi clusters.

#### == **High-Availability Clusters** ==

K3s has options to set up a cluster with highly available servers through the use of an external datastore. This can be either the embedded etcd option, or SQL such as Postgresql or Mysql.

![K3s architecture with highly available server](LFS156-k3s-architecture-ha-server.png)


In the diagram above, due to the consensus required, an odd number of servers is needed, such as 3 or 5.

The bash installation script for the K3s can be complex when setting up an SQL or etcd backend for a HA cluster. k3sup makes this easier through the use of flags. In a later chapter you will see some examples of this, but you can also skip ahead to the [k3sup README](https://k3sup.dev/).

Now that you have a clear idea of what K3s is and why it’s suited for the Edge, let’s move into the hands-on section of the course.


----

## Chapter 5. Setting Up Your Own Lab Environment

### **Overview and Learning Objectives**

In the previous chapter, we touched on some of the components of K3s and options for setting it up. In this chapter, we will review options for where you can deploy K3s in your own lab environment. We will also cover a Bill of Materials (BoM) for building a Raspberry Pi cluster. Once we have explored options for deploying a lab environment, we will go ahead and deploy a single-node K3s cluster and start exploring it.

By the end of this chapter, you should be able to:

 * Review the options for where to deploy your lab environment.
 * Review a Bill of Materials for a Raspberry Pi cluster.
 * Configure a single-node K3s cluster using K3sup and SSH.
 * Explore your new cluster.


### **Setting Up the Lab Environment**


#### == **Options for Deploying Your Lab Environment** ==

There are currently several options for deploying your lab environment; and in 6 months, there may be even more options, as K3s gains in popularity.

**You can run K3s in a Docker container**

 * Pros

      This is the most flexible option, because you can then run K3s anywhere where you can run Docker or Docker Desktop. So that means the same tooling will work on MacOS, Windows and Linux.
 * Cons

      Running in a container requires special steps to expose services and to gain access to the cluster.
 * Tool

      This can be done using [K3d](https://k3d.io/).

**You can install K3s with Multipass**

 * Pros

      Multipass creates a full VM, with its own operating system and networking stack. It works the same way on Linux, Windows and MacOS. If using an Intel machine, you don’t need to rebuild any projects for ARM support.
 * Cons

      Multipass only supports Ubuntu Linux. Running Multipass and Docker Desktop at the same time means running two virtual machines.

 * Tool

      This is done using [Multipass](https://multipass.run/).

**You can install K3s on a Raspberry Pi**

 * Pros

      You get a dedicated host and access to bare-metal networking. The cost is relatively low - starting at $35 for the Raspberry Pi 4 with 2GB of RAM. You can build a real cluster, which has very low power requirements and can be run 24/7 in your home.
 
 * Cons

      ARM support is required for any tools you want to use. The Raspberry Pi’s I/O channels are limited, so it will be slower than using your PC.

**You can use cloud infrastructure**

 * Pros

      You can install K3s to a cloud VM and closely simulate cloud-based Kubernetes.

 * Cons
      You will be paying for the VMs you create, and may need to delete them to stop recurring charges.

Our choice for this training course is to use K3d, which provides the best balance overall. We will already be running Docker Desktop to build container images, so it means there are no additional costs and no additional pieces of software to install.

We also want to give you a quick overview of what a Raspberry Pi cluster should look like, and what it may cost to build one of your own.


#### == **Bill of Materials for a Raspberry Pi Cluster** ==

We will list four builds for a range of budgets. Whatever you go for, you will also need a gigabit ethernet switch for networking. Kubernetes requires dedicated networking, and is not suitable for use over WiFi. WiFi has partial availability and high latency, which will cause issues for your build.

**The Shoe-String Budget Build**

If you are on a tight budget, then you can experiment with K3s using a single Raspberry Pi. You will need a minimum of one Raspberry Pi to get started, along with an SD card and a power supply:

 * Get the most RAM that you can afford - 4 or 8GB.
 * Buy an official power supply.
 * Get a 32-64GB SD card.
 * Get a case with a built-in fan, or a fan add-on.

**The Machine Learning Build**

If you are interested in machine learning, then you should consider the [Nvidia Jetson Nano Developer Kit](https://developer.nvidia.com/embedded/jetson-nano-developer-kit) which comes with hardware acceleration through its GPU. There is very detailed documentation for this, and like the shoe-string budget, you only need one to get started.

You can add machine learning capabilities to the Raspberry Pi through the use of the [Google Coral USB accelerator](https://coral.ai/products/), and the author has used one of these to detect objects in images from a Raspberry Pi camera.

**The Hobbyist Build**

If you have a modest budget, then you can buy between 3-6 Raspberry Pis and house them in a stackable case using one of the options listed below.

 * Buy 3-6 Raspberry Pis with 2-4GB of RAM each.
 * Get 32-64GB class 10 SD cards, which will be used to run the Operating System and the containers in the cluster.
 * Make sure you buy individual official power supply adapters, because most multi-chargers are unable to supply the amount of power required.
 * Buy a multi-layer acrylic case from Amazon or eBay, where each Raspberry Pi is stacked and has its own fan.

![Pi Rack Case for Raspberry Pi 4 Model B with 4 Layers](LFS156-Pi_Rack_Case_for_Raspberry_Pi_4_Model_B_with_4_Layers.jpg)



![GeeekPi Raspberry Pi Cluster Case](LFS156-GeeekPi_Raspberry_Pi_Cluster_Case.jpg)

**Production-Quality Build**

If you are looking to build an industrial quality cluster which you can install on-site for a client, or to simulate a production build, there are a couple of options available.

The one that comes to mind is built by BitScope, a company based in Australia.

The BitScope Blade edge cluster can take 8 Raspberry Pi 4s and has built-in fans, along with support for a wide range of voltages. Industrial equipment like the cluster blade also offers the ability to power-cycle each Raspberry Pi remotely. The alternative could be having to pay an engineer or contractor to travel to the site.

![BitScope Cluster Blade](LFS156-BitScope_Cluster_Blade.jpeg)


With a system like the BitScope blade, you may find it a better option to use network storage for the filesystem of each Raspberry Pi. Network filesystems can provide better durability and speed than the SD card.

The Raspberry Pi 4 supports net-booting, and there are tutorials which cover how to configure a system like this.

The hands on part of this course should not be carried out on a Raspberry Pi. However, you can follow a tutorial on how to install K3s to a Raspberry Pi and how to configure its operating system to support containers. For more details, see: [Walk-through - install Kubernetes to your Raspberry Pi in 15 minutes](https://alexellisuk.medium.com/walk-through-install-kubernetes-to-your-raspberry-pi-in-15-minutes-84a8492dc95a).


#### == **Create a Single-Node K3s Cluster Using K3d** ==

Install Docker or Docker Desktop on your computer.

Now, install the K3d project using the instructions on its [homepage](https://k3d.io/).

Next, create a cluster run:

```bash
k3d cluster create
```

> Note that you can simulate a larger cluster by using the following options:

```bash
k3d cluster create \
  --servers 1 \
  --agents 1
```

After the tooling has downloaded a container image for K3s, it will start K3s and save the `kubeconfig` file into your local Kubernetes context.

If you’ve already got a cluster, you can run `k3d cluster delete` to remove it.

```
INFO[0000] Network with name 'k3d-k3s-default' already exists with ID '76cf51a817603da23857b30a2c36d76f0972d040dab02a9f542d9a834ba4d82c'
INFO[0000] Created volume 'k3d-k3s-default-images'
INFO[0001] Creating node 'k3d-k3s-default-server-0'
INFO[0006] Creating LoadBalancer 'k3d-k3s-default-serverlb'
INFO[0007] (Optional) Trying to get IP of the docker host and inject it into the cluster as 'host.k3d.internal' for easy access
INFO[0010] Successfully added host record to /etc/hosts in 2/2 nodes and to the CoreDNS ConfigMap
INFO[0010] Cluster 'k3s-default' created successfully!
INFO[0010] You can now use it like this:
kubectl cluster-info
```

List clusters you can access (there should be one if this is your first cluster):

```bash
kubectl config get-clusters
NAME
K3d-k3s-default
```

**Be warned:** kubectl can be configured to access multiple clusters. Always use the command below to check which cluster you are pointing at before running any commands that may destroy data or install applications.

```bash
kubectl config current-context
k3d-k3s-default 
```

#### == **Explore Your New Cluster** ==


Let’s explore the cluster.

Find out which nodes you have available:

```bash
kubectl get nodes
NAME                      STATUS  ROLES   AGE    VERSION
k3d-k3s-default-server-0  Ready   master  4m14s  v1.19.4+k3s1
```

Now change the output flag to see additional data:

```bash
kubectl get nodes --output wide
NAME                      STATUS  ROLES   AGE    VERSION       INTERNAL-IP  EXTERNAL-IP  OS-IMAGE  KERNEL-VERSION    CONTAINER-RUNTIME
k3d-k3s-default-server-0  Ready   master  4m20s  v1.19.4+k3s1  172.19.0.2   <none>       Unknown   5.4.0-65-generic  containerd://1.4.1-k3s1
```

You can also output as JSON, and use a tool like .jq to query specific fields.

```bash
kubectl get nodes --output json

{
    "apiVersion": "v1",
    "items": [
        {
            "apiVersion": "v1",
            "kind": "Node",
            "metadata": {
                "annotations": {
….
}
```

If there is a specific field you want to find, such as the CPU architecture, to determine if a node is Intel or ARM, you can run:

```bash
kubectl get node k3d-k3s-default-server-0 --output jsonpath="{.status.nodeInfo.architecture}"
```

You should see: amd64; if you see anything else, please see the note we gave in the previous chapter about not using a Raspberry Pi for this course. If you see "" (empty string), then check to ensure your cluster context is set correctly.

And on a Raspberry Pi:

```bash
kubectl get node
NAME      STATUS  ROLES        AGE   VERSION
1b9a8834  Ready   etcd,master  119m  v1.19.7+k3s1
1befbd15  Ready   etcd,master  118m  v1.19.7+k3s1
3c104aa8  Ready   etcd,master  117m  v1.19.7+k3s1
7feb56e7  Ready   <none>.      89m   v1.19.7+k3s1
```


You can see that three of the nodes are running in an HA configuration, where there are three taking on the master role, and one is an agent. In K3s, all nodes can run workloads by default. This differs from traditional Kubernetes clusters, where the masters run with a taint to prevent scheduling.

You can see that the CPU architecture is showing as ARM. It is possible to have a cluster with mixed architecture; this is known as a heterogeneous cluster.

```bash
kubectl get node/1b9a8834 --output jsonpath="{.status.nodeInfo.architecture}"
arm
```

K3s ships with a project called [metrics-server](https://github.com/kubernetes-sigs/metrics-server) built into it, which can be used to find the resources consumed by each pod and node in the cluster.

Earlier, I installed OpenFaaS to this cluster and requested 5 replicas of the NodeInfo function. They are running a Node.js webserver, and are taking up around 8MB of RAM each.

```bash
kubectl top pod --all-namespaces
NAMESPACE    NAME                                    CPU(cores) MEMORY(bytes)
kube-system  coredns-66c464876b-85nxw                10m         5Mi
kube-system  local-path-provisioner-7ff9579c6-2f7lj  3m          6Mi
kube-system  metrics-server-7b4f8b595-4jrhq          9m         11Mi

openfaas     alertmanager-74d75c96c7-fpfpf           1m          6Mi
openfaas     basic-auth-plugin-6c7b45c8f7-wfklx      1m          3Mi
openfaas     gateway-5f85f8bd66-88sn4                5m         15Mi
openfaas     gateway-5f85f8bd66-sjt4j                6m         14Mi
openfaas     gateway-f6b4745c5-sqlds                 6m         13Mi
openfaas     gateway-f6b4745c5-tfmwl                 8m         10Mi
openfaas     nats-56f45c48ff-kkt86                   4m          4Mi
openfaas     prometheus-6bb5d7f9bf-r7tp4             9m         24Mi
openfaas     queue-worker-597cd66c6d-lqvtz           1m          1Mi

openfaas-fn  nodeinfo-5599c56476-55x6d               2m          8Mi
openfaas-fn  nodeinfo-5599c56476-5ct8k               2m          8Mi
openfaas-fn  nodeinfo-5599c56476-87fhc               2m          8Mi
openfaas-fn  nodeinfo-5599c56476-96ffr               2m          8Mi
openfaas-fn  nodeinfo-5599c56476-9ccsc               2m          8Mi
```

Each unit is measured in "millis" - which is 1/1000th of a unit. The metrics-server pod is using 11Mi, which corresponds to 11MB of RAM.

You can also check the memory and CPU pressure across each node using the following:

```bash
kubectl top node
NAME       CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
1b9a8834   703m         17%    915Mi           47%
1befbd15   446m         11%    715Mi           36%
3c104aa8   506m         12%    732Mi           37%
7feb56e7   187m         4%     370Mi           18%
```

You can find out whether any nodes are blocked from scheduling new pods due to being overloaded by running:

```bash
kubectl describe node

Conditions:
  Type                 Status   LastHeartbeatTime
  ----                 ------   -----------------
  NetworkUnavailable   False    Mon, 08 Mar 2021 18:48:21
  MemoryPressure       False    Mon, 15 Mar 2021 16:27:02
  DiskPressure         False    Mon, 15 Mar 2021 16:27:02
  PIDPressure          False    Mon, 15 Mar 2021 16:27:02
  Ready                True     Mon, 15 Mar 2021 16:27:02

Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource   Requests     Limits
  --------   --------     ------
  cpu        652m (32%)   102m (5%)
  memory     810Mi (51%)  654Mi (41%)
```

The output is very verbose, but you will see whether there are any warnings present about low disk space, loss of network, too many processes (PIDs) or any other resources, like RAM running out.

My Raspberry Pi cluster has 4x 2GB nodes, which are netbooted and running in the BitScope Cluster blade we mentioned in the previous section.



----

## Chapter 6. Deploying Real Workloads with the Kubernetes 


### **Overview and Learning Objectives**


### **Deploying Real Workloads with the Kubernetes API**

In this chapter, we will start deploying real workloads to your lab environment. To begin with, we explore what role container images play in Kubernetes, and then construct an HTTP microservice using Python. The rest of the chapter is hands-on: we will learn the basic primitives of Kubernetes and apply them to our microservice.

By the end of this chapter, you should be able to:

 * Understand what role a container image plays, and how to build one.
 * Build a container image for a Python microservice.
 * Learn the Kubernetes API primitives to deploy and access the microservice.


#### == **Container Images: Their Role and How to Build One** ==

The basic primitive for Kubernetes workloads is the container image. It packages operating system packages and libraries, along with your code, and provides a self-contained execution environment.

At runtime, the container image is downloaded, extracted, and then run as a container. The inner workings of containers are beyond the scope of this course; however, it is important to say that traditionally, a container is a Linux kernel construct for isolating workloads. It is not a primitive in itself, but made up of network and process isolation. For this reason, MacOS and Windows users who want to build and run containers on their machines need to create a small Virtual Machine (VM) and run Linux inside it.

If you are using MacOS or Windows, the easiest way to build a container image is through Docker Desktop by Docker, Inc., which will start a Linux VM for you and run a Docker daemon inside it. If you are a Linux user, you can install the Docker CE package to get access to Docker.

There are alternative tools you can install to build container images, such as Podman from Red Hat. Some cloud providers also offer SaaS-style container builders, like Google Cloud Build, which can be used as part of a Continuous Integration (CI) pipeline.


#### == **Building a Container Image for a Python Microservice** ==

Let’s build a small microservice using Python and the Flask HTTP framework. It will accept incoming traffic on TCP port 5000 using the HTTP protocol.

To begin with, you need to create an app.py file, which will be executed upon start-up:

```python
from flask import Flask, request
from waitress import serve

import logging
logger = logging.getLogger('waitress')
logger.setLevel(logging.INFO)

app = Flask(__name__)

@app.route("/", methods=["GET"])
def home():
    logger.info("/ invoked")
    return "Hello world"

if __name__ == '__main__':
    port = 5000
    serve(app, host='0.0.0.0', port=port)
```

Next, create a requirements.txt file to instruct the build which Python modules are required:

```
flask
waitress
```

The Flask project recommends using a separate package for serving HTTP traffic. Waitress provides a suitable alternative here.

The most complex part of the container image will be the Dockerfile, which instructs Docker what steps to take to build the final container image:

```
FROM python:3.7-alpine
RUN addgroup -S app && adduser app -S -G app

WORKDIR /home/app/

COPY requirements.txt .

USER root
RUN pip install -r requirements.txt
COPY app.py .

RUN chown -R app:app ./

USER app

CMD ["python3", "./app.py"]
```

The `Dockerfile` is using Alpine Linux as the basis, since it has a small overall footprint. Debian, Ubuntu, and CentOS are also available, but all three tend to be a lot larger, given the additional packages they supply.

It is important to note that whilst Alpine Linux is suitable for pure Python packages, many modules require a native toolchain to build C/C++ code for use at runtime. In these cases, it is a better option to move to a Debian-based image which uses a standard build toolchain. Database, encryption and image manipulation packages tend to use preexisting C/C++ libraries, and aren’t necessarily tested or supported with Alpine Linux and its toolchain.

The next step adds a non-root user, so that the container can run as a non-privileged user. Many new users to containers skip this step and are running an unnecessary risk, so **do not skip this**.

The `pip install` command reads the `requirements.txt` file we created above. So, if you need an additional module like the `requests` package, to make outbound HTTP requests from your microservice, you can add it here.

Then, the `CMD` instruction tells the container what process to start at runtime.

To test this, simply run:

```bash
docker build -t flask-app:0.1.0 .
docker run --name flask-app -p 5000:5000 flask-app:0.1.0
```

Then, invoke the service with:

http://127.0.0.1:5000


You will see the text "Hello world" printed.

Stop the container with Control + C followed by

```bash
docker rm -f flask-app
```

Now edit the text in app.py and rebuild the container, and start it up again. This time, change the 0.1.0 to 0.1.1 to build a new image.


#### == **Deploying the Microservice with Kubernetes API Primitives** ==

The smallest compute unit in Kubernetes is a container. However, the API requires a [pod](https://kubernetes.io/docs/concepts/workloads/pods/) to be created to schedule and execute the container. The concept of a pod comes from the natural world, and is the name for a group of whales and a hat-tip to Docker’s logo. A pod usually contains one container, but, as the name implies, they can also support multiple containers joined together.

Containers in pods can share the same network namespace and storage volumes. A popular example would be an HTTP service plus a reverse proxy that adds a TLS certificate. Another example would be a pod containing a background job along with a logging component to capture any output and forward it to a log aggregation system.

In our example, we need a single container, and we will define the pod using YAML, then use the CLI to schedule the microservice.

There are three ways to access a Pod or service within a Kubernetes cluster. We will use the port-forwarding command available in `kubectl` because it works with any Kubernetes or K3s cluster without additional configuration:

 * Port-forwarding use of `kubectl`
 * Exposing a LoadBalancer using a cloud service or software addition like [MetalLB](https://metallb.universe.tf/) or [inlets-operator](https://github.com/inlets/inlets-operator)
 * Exposing a NodePort.

Each of the three examples can then be combined with [Kubernetes Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) to serve traffic on different hostnames or paths.

You can read more about the options in the [Kubernetes docs](https://kubernetes.io/docs/concepts/services-networking/service/).

The way that we reference the image we built with Docker is to push it to a remote registry. We can then use the full URL of the registry server, repository and tag in the **image** field.

First, register for an account with the [Docker Hub](https://hub.docker.com/), create an Access Token from Account Settings, and log in. Now build a new version of the image and push it to the registry.

 ```bash
$USER=mydocker_account
docker login --username $USER
# Paste the Access Token as your password

docker build -t docker.io/$USER/flask-app:0.1.1 .
docker push $USER/flask-app:0.1.1
```

#### == **A Pod** ==


Create a file called `pod.yaml`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: flask-app
  labels:
    app: flask-app
spec:
  containers:
    - name: flask-app
      image: amon/flask-app:0.1.1
      imagePullPolicy: Always
      ports:
        - name: http
          containerPort: 5000
          protocol: TCP
```


Note that the pod is made up of a **metadata** section, which provides a name and a spec for how to run the workload.

A TCP port of 5000 is also provided to let Kubernetes and any other users know how to access this container.

The `imagePullPolicy` is set to `Always`, which means that if you happen to push a new version of your code without updating the tag (version), then the image will be pulled again. You can save bandwidth by setting this to `IfNotPresent`, which first checks the host’s local image library before deciding whether to pull in an image.

Apply the configuration to your cluster with:

```bash
kubectl create -f pod.yaml
pod/flask-app created
```
The pod will appear in the default namespace:

```bash
kubectl get pods
NAME       READY  STATUS   RESTARTS  AGE
flask-app  1/1    Running  0         95s
```

We can send traffic to the pod by forwarding its TCP port to our local computer:

```bash
kubectl port-forward flask-app 5000:5000

curl http://127.0.0.1:5000 ;echo
Hello world
```

Now, check the container logs:

```bash
kubectl logs pod/flask-app
INFO:waitress:/ invoked 
```

#### == **Creating a Service for the Microservice** ==

The pod has an IP address, which can change over time. This makes it unstable because it will change if the workload is restarted or moved to another node. So, if we want to access it from other services or external systems, we need a stable IP or a way of looking it up by name (also known as resolution).

Services are designed to solve this problem. They come with a name and a stable IP.

Query the IP of our Pod, then recreate it and see it get a new IP:

```bash
kubectl get pod/flask-app --output go-template="{{.metadata.name}} {{ .status.podIP }}" ; echo;
flask-app 10.244.0.11
```

Now delete it:

```bash
kubectl delete -f pod.yaml
```

And re-create it:

```bash
kubectl apply -f pod.yaml

kubectl get pod/flask-app --output go-template="{{.metadata.name}} {{ .status.podIP }}" ; echo;

flask-app 10.244.0.13
```

See how the IP address is different the second time it is created?

Create a service.yaml file:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: flask-svc
spec:
  selector:
    app: flask-app
  ports:
    - protocol: TCP
      port: 5000
      targetPort: 5000
```

```bash
kubectl apply -f service.yaml
```

We can send traffic to the pod by forwarding its service instead:

```bash
kubectl port-forward service/flask-svc 5000:5000

curl http://127.0.0.1:5000 ;echo
Hello world
```

The result appears the same, but now we are forwarding traffic to any pods with the label `flask-app`, whatever their name happens to be.

Create another pod with a different name (pod-b.yaml):

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: flask-app-b
  labels:
    app: flask-app
spec:
  containers:
    - name: flask-app
      image: amon/flask-app:0.1.1
      imagePullPolicy: Always
      ports:
      - name: http
        containerPort: 5000
        protocol: TCP
```

```bash
kubectl apply -f pod-b.yaml
```

Now delete the original pod named flask-app:

```bash
kubectl delete -f pod.yaml
```

Stop the kubectl port-forward command, and run it again:

```bash
kubectl port-forward service/flask-svc 5000:5000
```

And see that you can still send traffic to the service:

```bash
curl http://127.0.0.1:5000 ;echo
```

#### == **A Deployment** ==

A deployment is very similar to a pod, but is used to scale a pod, so that it has a stable name, and can have more than one replica of itself. Having more than one replica means that pods can be scaled horizontally.

Deployments also allow for changes to be rolled out in a controlled manner. They have built-in strategies for rollout and rollback of changes.

The `replicas` field specifies how many copies of the pod should be created, so when working with pods, for 3 replicas, we may need three `pod-*.yaml` files and a service to forward traffic to them. With a deployment (`deployment.yaml`), we can manage one file, and one service:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: flask-app
  labels:
    app: flask-app
spec:
  replicas: 3
  selector:
    matchLabels:
      app: flask-app
  template:
    metadata:
      labels:
        app: flask-app
    spec:
      containers:
      - name: flask-app
        imagePullPolicy: Always
        image: amon/flask-app:0.1.1
        ports:
        - containerPort: 5000
```

You will notice that the `template.spec` entry looks very similar to our pod.yaml file, but the rest of the file looks different and has lots of repetition. Unfortunately, Kubernetes YAML files tend to err on the side of complexity and verbosity. On the other hand, once you have everything as you need it, you will usually only need to change the `image` field.

Delete your pods and apply the deployment instead:

```bash
kubectl delete -f pod.yaml,pod-b.yaml
```

Now apply the deployment:

```bash
kubectl create -f deployment.yaml
deployment.apps/flask-app created
```

Now, port-forward the original service. It doesn’t need to be changed; invoke it a few times:

```bash
curl http://127.0.0.1:5000 ;echo
curl http://127.0.0.1:5000 ;echo
curl http://127.0.0.1:5000 ;echo
...
```

Not many people know this, but you can find the logs for the pods created by your deployment by using the following command:

```bash
kubectl logs deploy/flask-app
INFO:waitress:/ invoked
INFO:waitress:/ invoked
INFO:waitress:/ invoked
INFO:waitress:/ invoked
INFO:waitress:/ invoked
```

You can also search out the generated name for the pod, and use that. Note that this will be different for you because the pod suffixes are random strings. Alter the command as required:

```bash
kubectl get pods
NAME                        READY  STATUS   RESTARTS  AGE
flask-app-6c74fb9c7c-hr4s7  1/1    Running  0         54s
flask-app-6c74fb9c7c-kzt9c  1/1    Running  0         54s
flask-app-6c74fb9c7c-sb5f6  1/1    Running  0         54s
```

Now get the logs of each pod:

```bash
kubectl logs pod/flask-app-6c74fb9c7c-hr4s7
kubectl logs pod/flask-app-6c74fb9c7c-kzt9c
kubectl logs pod/flask-app-6c74fb9c7c-sb5f6
```

Given the way that kubectl port-forward works, you will see all the traffic to go to one replica. However, when external Ingress is configured, the replicas will be load-balanced.

To demonstrate this, create an Ingress record using the built-in Traefik IngressController (`ingress.yaml`):

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: flask-app
  annotations:
    ingress.kubernetes.io/rewrite-target: /
    kubernetes.io/ingress.class: traefik
spec:
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: flask-svc
            port:
              number: 5000
```

```bash
kubectl apply -f ingress.yaml
ingress.networking.k8s.io/flask-app created
```

Now, port-forward Traefik itself:

```bash
kubectl port-forward -n kube-system service/traefik 8080:80
```

Now, curl the service 6 times:

curl http://127.0.0.1:8080

And check the logs of the pods you have:

```bash
$ kubectl get pods
NAME                        READY  STATUS   RESTARTS  AGE
flask-app-6c74fb9c7c-2dzmw  1/1    Running  0         105s
flask-app-6c74fb9c7c-vssw5  1/1    Running  0         105s
flask-app-6c74fb9c7c-dcmxj  1/1    Running  0         105s

$ kubectl logs pod/flask-app-6c74fb9c7c-2dzmw
INFO:waitress:/ invoked
INFO:waitress:/ invoked

$ kubectl logs pod/flask-app-6c74fb9c7c-vssw5
INFO:waitress:/ invoked
INFO:waitress:/ invoked

$ kubectl logs pod/flask-app-6c74fb9c7c-dcmxj
INFO:waitress:/ invoked
INFO:waitress:/ invoked
```

You can see that the requests are being load-balanced through the use of the service.

The IngressController can also be used to obtain a TLS certificate and add encryption to the service, and to map various different domain names to different services within the cluster.

We configured the IngressController to send all traffic to one service. However, we can add a DNS host name to the Ingress record and then have multiple services and domain names (ingress-host.yaml):

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: flask-app
  annotations:
    ingress.kubernetes.io/rewrite-target: /
    kubernetes.io/ingress.class: traefik
spec:
  rules:
  - host: "flask-app.example.com"
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: flask-svc
            port:
              number: 5000
```

```bash
kubectl apply -f ingress-host.yaml
ingress.networking.k8s.io/flask-app created
```

You’ll now see that any connections without the specific host header return an error:

```bash
curl http://127.0.0.1:8080
404 page not found
```

Now add the host header and run curl again:

```bash
curl http://127.0.0.1:8080 --header "Host: flask-app.example.com"
Hello world
```

If you had another microservice written in Go, or Node.js, you would simply repeat all the steps we have run through so far and have a different name for each resource created. If you want to try that as an extra task, it may help cement the chapter’s exercises for you.

#### == **A CronJob** ==

The Kubernetes CronJob is a flexible definition that can be used to run an ad-hoc task on a schedule. The task could even be to run curl against an existing endpoint in the cluster, like the Flask app we built previously, or to run some code or a maintenance task (`cronjob.yaml`):

```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: hello
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: hello
            image: busybox
            imagePullPolicy: IfNotPresent
            command:
            - /bin/sh
            - -c
            - date; echo Hello from the Kubernetes cluster
          restartPolicy: OnFailure
```

This example from the Kubernetes docs runs the bash built-in date command in an Alpine Linux container, then goes ahead and prints an echo message. If it fails to work, the container is restarted.

To generate CronJob schedule expressions, you can also use web tools like [crontab.guru](https://crontab.guru/).

You can read more in the [Kubernetes docs](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/).


----

## Chapter 7. Functions at the Edge

### **Overview and Learning Objectives**

In this chapter, you’ll learn how to deploy a Functions As A Service (FaaS) framework at the end, and how it can reduce repetition. You’ll then invoke a function via the Message Queuing Telemetry Transport (MQTT) and learn what metrics come pre-bundled for monitoring.

By the end of this chapter, you should be able to:

 * Discuss how a Functions as a Service (FaaS) framework can reduce repetition and management of Kubernetes primitives.
 * Trigger a function with simulated sensor data from Message Queuing Telemetry Transport (MQTT).
 * Monitor the Rate, Error and Duration (RED) metrics for your functions.
 * Discuss how combining functions with a [Tensor Processing Unit](https://en.wikipedia.org/wiki/Tensor_Processing_Unit) (TPU) or [Graphics Processing Unit](https://en.wikipedia.org/wiki/Graphics_processing_unit) (GPU) can help analyze data and save on bandwidth.
 * Understand other frameworks and how to go further with the Introduction to Serverless on Kubernetes (LFS157x) course.



### **Functions at the Edge**

#### == **Reducing Repetition with FaaS** ==

The concept of a FaaS framework is to make it easy to bundle and manage functions. The term *serverless* comes from the idea of having management, automation, and scaling, however functions are just like any other unit of compute, like VMs and containers.

OpenFaaS is one of the open source FaaS frameworks listed on the CNCF Landscape and it can be installed to a cluster running K3s or Kubernetes. It also works on edge devices like a Raspberry Pi.

Functions are simply event handlers, where the event could be an HTTP call or an asynchronous event triggered from a broker or source.

By using a function, we can reduce the management overhead by using templates and an API gateway:

 * The API gateway allows for functions to be monitored and scaled in the same way.
 * The templates contain Dockerfiles and boiler-plate code which doesn’t have to be repeated for each function.

The OpenFaaS CLI has a template store for pre-made functions, most of which are multi-arch, meaning they run on Intel and Arm clusters.

If you were to create your own function in Python for example, then it may look like this:

```python
def handle(req):
    """handle a request to the function
    Args:
        req (str): request body
    """

    return req
```

This code is overlaid with a template to produce a container image, which is then pushed into a container registry. When the function is deployed, the target cluster will schedule a pod onto a node, and then pull the container image from the registry. Fortunately, container images can be decomposed into different layers, making them efficient to push and pull.

#### == **Triggering a Function via MQTT** ==

This is an example of a function that receives sensor data over MQTT, and then stores it as a file using object storage. Object storage is used to store individual files using the S3 protocol. The S3 protocol was created at AWS, and now open source projects also implement the interface, such as MinIO.

![Edge Location with K3s: Conceptual Architecture](LFS156-edge-to-cloud-mqtt-sm.png)


You can try the function out by installing OpenFaaS, and then deploying the solution from a GitHub repository. It is beyond the scope of the course to explain the code line-for-line, however, you can read the source code and customize it if you wish.

Install OpenFaaS using the arkade tool:

```bash
curl -sLS https://get-arkade.dev | sh
```

Install the CLI for OpenFaaS:

```bash
arkade get faas-cli
```

Now move the arkade binary into your $PATH, or run sudo sh instead:

```bash
arkade install openfaas
```

Now clone the example function and follow all the instructions in the GitHub repository to:

            Set up Minio
            Deploy the functions

```bash
git clone https://github.com/alexellis/mqtt-s3-example
```

From here, we will be using the content in the Git repository. First, we will install the required software for the exercise, then deploy the functions, and finally test them out end-to-end to broadcast a temperature reading. You will see that message sent to MQTT and then stored in S3.

Open the `README.md` file in the repository and follow the Installation instructions to deploy everything required.

Next, follow the *Testing the functions instructions* to run the function and verify the results are uploaded to the S3 bucket.

**Summing up**: You have now deployed a function with OpenFaaS, that can be triggered by MQTT. It is up to you to decide what to do with the data and how to process it, but you now have a reasonable simulation of what an edge use case may be using K3s.

#### == **Monitoring Rate Error and Duration for Functions** ==

One of the other ways a FaaS can reduce duplication is through built-in monitoring of the rate (throughput), error (400/500 HTTP codes) and duration (length of execution for requests) [RED] of functions. The OpenFaaS project uses [Prometheus](https://prometheus.io/), an open source time-series database to collect and query this information. You can interact with Prometheus using its query language called PromQL.

![Prometheus showing the rate of invocations over a 15s window](LFS156-Prometheus_showing_the_rate_of_invocations_over_a_15s_window.png)

Port-forward Prometheus to your local computer and open it in a web browser:

```bash
kubectl port-forward -n openfaas svc/prometheus 9090:9090 & http://127.0.0.1:9090
```

Try out the following queries to view the data for your functions.

Monitor the rate of execution over a 15 second window, for all HTTP codes:

```
rate(gateway_function_invocation_total[15s])
```

> **Note**: If your graph is flat (rate is zero), too much time has passed since you sent data to the function or you did not send enough requests prior in the lab environment. Refer to the Testing your function section in the GitHub repository again to send requests and trigger function invocations.

Monitor a single function’s 200 (success codes):

```
rate(gateway_function_invocation_total{
  function_name="mqtt-s3.openfaas-fn",
  code="200"
}[1m])
```

This query may be useful for finding bugs or tracking down issues with your functions or their event sources. Timeouts may also show here: for instance, if S3 or Minio is down or unreachable.

Check out the average execution duration, by taking the total execution time and dividing it by the amount of executions seen over a period of time:

```
gateway_functions_seconds_sum / gateway_functions_seconds_count
```

You can also plot the values according to how they have changed over a window such as a 1-minute moving window:

```
rate(gateway_functions_seconds_sum[1m]) /
rate(gateway_functions_seconds_count[1m])
```

These PromQL queries can be pasted into a [Grafana](https://grafana.com/) dashboard, so that you and your team can monitor the status of functions and other K3s endpoints without having to interact with Prometheus directly. The OpenFaaS project offers a dashboard that can be downloaded and comes pre-populated with useful queries.


#### == **Understanding How a TPU or GPU Can Help Save on Data** ==

A GPU is a Graphics Processing Unit, a specialized equipment to process tasks that require graphics and calculations. A TPU is a Tensor Processing Unit, a specialized device designed to accelerate machine learning and inference.

GPUs and TPUs are commonly used for two purposes at the edge:

 * To capture sensor data and train a model, also known as deep learning.
 * To capture sensor or camera data and make predictions, also known as inference.

K3s is compatible with both GPUs and TPUs; however, it will require some special setup for your device, which is beyond the scope of this course.

Here are two use cases and devices you may want to look into.

The [Jetson Nano Developer Kit](https://developer.nvidia.com/embedded/jetson-nano-developer-kit) is a specialized device made by Nvidia which has GPU capabilities, and it is also capable of running K3s.

![Jetson Nano Developer Kit](LFS156-Jetson_Nano_Developer_Kit.jpeg)

With the Jetson Nano, you can deploy a range of machine learning models, and then make predictions by attaching a sensor to it. By filtering and processing data at the edge, cost savings can be made on bandwidth, since the data doesn’t have to be sent to a cloud region for processing. Only results or values that trigger an alert will need to be sent to a cloud service.

An example would be in a commercial Heating, Ventilation, and Air Conditioning (HVAC) unit, sensing any changes in data being monitored, which may indicate a failure of a component is about to occur. Instead of sending each individual sensor reading, the edge device can filter them and only send a message when absolutely necessary.

Another example would be a camera in a parking lot which scans the registration plates of vehicles and automatically sends out a fine if the customer has not bought a ticket within 15 minutes. This is an example of a real-world use case. Given that the camera will be processing hundreds of frames per second, these can be analyzed within the device, and will save on bandwidth and processing power.

![Google Coral products](LFS156-Google_Coral_products.png)


The [Google Coral accelerator](https://coral.ai/) can be plugged into a Raspberry Pi or Jetson Nano using USB, and can then be used with machine learning models that can recognize automotive plates.

So, building on the previous section, where we used MQTT to store sensor data in S3, you can perhaps start to imagine a workflow where a device running K3s with a GPU or TPU is inspecting data, storing records in an S3 bucket, and perhaps emitting an event via MQTT for a central cloud control plane to take some action, like looking up an automobile’s plate and then sending a fixed penalty notice.

![Edge Cluster](LFS156-Edge_Cluster.png)

Edge K3s cluster making predictions upon images of automotive plates from a camera feed, using MQTT to connect to a central cloud cluster and issue a fixed penalty notice

#### == **Learning More about FaaS on Kubernetes** ==

The best place to learn more about OpenFaaS would be on the [project homepage, blog and documentation site](https://openfaas.com/).

Alternatively, the edX course [Introduction to Serverless on Kubernetes](https://www.edx.org/course/introduction-to-serverless-on-kubernetes) gives a broad overview of the landscape, various projects, and then takes a deeper dive into building functions in Python with OpenFaaS.

In this chapter, we explored K3s and OpenFaaS for running functions at the edge. The [faasd project](https://github.com/openfaas/faasd) is another option for running OpenFaaS that is well suited for the edge. It deploys largely the same stack, but without the need for Kubernetes or K3s. By removing clustering for functions, the entire stack can function within less than 512MB of RAM and has fewer moving parts than Kubernetes. This makes management easier and requires fewer updates once deployed at the edge.

----

## Chapter 8. Command & Control and Remote Access, Incomplete section


### **Overview and Learning Objectives**

In this chapter, we will explore the concepts of push and pull-based synchronization, along with a number of technologies that can be used for remote administration and access of a K3s cluster.

By the end of this chapter, you should be able to:

 * Understand why remote access is required and what are the tradeoffs of push vs. pull.
 * Discuss technologies that can be used for remote administration and access of a K3s cluster and their trade-offs: SSH, VPN, and tunnels with inlets.


### **Command & Control and Remote Access**


#### == **Remote Access: Why Is It Required and What Are the Tradeoffs of push vs. pull** ==

Once a device is deployed at an edge location, it can be fairly expensive and slow to send an engineer out to perform maintenance or to update software. For this reason, systems should be made as resilient as possible, and in some cases, be run with redundancy, so that if one of the devices fails, a second can take over.

Remote access is simply avoiding the need to access a device physically. In a push-based scenario, the devices need to be accessible over a network, so that a central control plane can send commands and query the state of the device and system. This can be difficult in practice, because it depends upon a reliable, always-connected network, which could be expensive. It also tends to require that the devices expose endpoints on a public network, such as the Internet.

With a pull-based scenario, there is no direct connection to the device from a control plane or administrator, but the device will connect to a remote endpoint to download its desired configuration or state.

An example of both approaches will be explored in the next chapter, when we look at GitOps and agent-based synchronization of Kubernetes workloads.


#### == **Technologies and Their Trade-offs** ==

One of the most common approaches for remote administration is to set up a Virtual Private Network (VPN). A VPN means that each edge device joins a private network along with the control plane, and must have its own IP address and routing tables.

VPNs use varying levels of encryption and authorization rules to secure the network, and prevent unauthorized access to the edge device.

Unfortunately, VPNs do not integrate well with Kubernetes since they are not designed to run in containers, and each container or Kubernetes pod tends to be run in a restricted mode that prevents new networks being established.

SSH provides a simpler alternative to a VPN, where any TCP traffic can be forwarded from a remote network to the edge device, or from the edge device to the remote network. SSH is well understood by many IT administrators, and is available for most Linux distributions. Whilst SSH is well suited to system administration, it wasn’t designed to work well in containers or pods. At scale, it requires additional configuration, from moving from public/private key pairs to certificates, which can be revoked and rotated centrally.

Another option is the open source [inlets](https://inlets.dev/) project, which was written from the ground-up to run well in containers and Kubernetes. It can provide a VPN-style remote connection, where an edge device can make its services available in a remote cluster. Alternatively, it can be used to provide a public endpoint that can be accessed by a control plane. Inlets also has a first-class integration with Kubernetes through its [inlets-operator](https://github.com/inlets/inlets-operator) to create LoadBalancers for ingress, and a Helm chart for running individual tunnels.

![Inlets in the public mode, where the private API within the cluster is available on the Internet](LFS156-inlets_in_the_public_mode__where_the_private_API_within_the_cluster_is_available_on_the_Internet.png)


![Inlets in the "VPN-style" use-case, where a private database at an edge location, is made available in a central control plane cluster](LFS156-inlets_in_the__VPN-style__use-case__where_a_private_database_at_an_edge_location__is_made_available_in_a_central_control-plane_cluster.png)


Whether using push or pull-based administration, realistic testing is important. You should consider the following:

 * Are there any requirements around latency?
 * How much data will be transferred and how often?
 * What happens if and when a link is unavailable for a period of time?

----

## Chapter 9. Deployment Strategies for Applications at the Edge


### **Overview and Learning Objectives**

In the previous chapter, we looked at several options for remote administration, networking and command & control. In this chapter, we look at specific strategies and projects for running applications upon K3s edge devices.

By the end of this chapter you should be able to:

 * Understand how to deploy K3s in an air-gapped environment (no Internet access).
 * Discuss GitOps and related CNCF projects.
 * Discuss fleet management.

### **Deployment Strategies for Applications at the Edge**

#### == **Deploy K3s in an Air-Gapped Environment (No Internet Access)** ==

An air-gapped environment is where there is either no network access at all, or just no Internet access. These environments are usually regulated, or may be in a remote location with no network connectivity.

When installing K3s using K3sup or the bash script available on the K3s homepage, a number of binaries are downloaded from the Internet. Once the k3s process starts, it will cause Kubernetes to download a number of container images.

There are two approaches recommended by the K3s authors for deploying into an air-gapped environment:

 * Download the binaries and container images to archive files, then make them available directly on the device. The container images should be placed at `/var/lib/rancher/k3s/`.
 * Manually download the binaries and mirror the container images to a local registry on the local network.

You can follow instructions from the K3s project in the [documentation](https://rancher.com/docs/k3s/latest/en/installation/airgap/).

#### == **GitOps and Related CNCF Projects** ==

The term GitOps was coined by [Alexis Richardson of Weaveworks](https://twitter.com/monadic). His team wanted a way to restore a customer’s cluster back to its desired state after a failure, and decided that keeping configurations in a Git repository was the way to go.

Git has been used for storing code and configurations for many years, so why is GitOps not just Ops? GitOps specifies that configuration needs to be stored in Git, but also that an agent should run inside the cluster and detect any drift from the desired state of the cluster and the said configuration, then make adjustments.

GitOps also provides for an audit log, because all changes made to the cluster must first go through a Git repository, and are stored in the commit log, showing who made a change and when. The GitOps tooling can sometimes also make changes to update new versions of software as your team builds them.

Within the CNCF there are two projects that provide a mature GitOps solution

[ArgoCD](https://github.com/argoproj/argo-cd) was originally developed by [Intuit](https://www.intuit.com/), an American business that specializes in financial software. It ships with a UI, and is popular in the enterprise.

![ArgoCD Architecture](LFS156-argocd_architecture.png)

[FluxCD](https://github.com/fluxcd/flux) was originally developed by Weaveworks and was subsequently donated to the CNCF, where it has received adoption from companies like Microsoft Azure, and makes up part of the Azure Arc managed product. Alibaba and AWS are also listed as users. FluxCD does not ship with a UI or API, but works through YAML files stored within a configuration Git repository. To have FluxCD make a change, users edit configuration files in the said repository, then the agent component will detect a drift, and apply the change.

FluxCD: Flux can detect new container images in your registry, and then automatically bump the revision version for your config repository

FluxCD (retrieved from GitHub)

In Continuous Deployment (CD), new releases are deployed to your cluster automatically when they are detected. Whilst this may sound good in theory, in practice, there may be reasons such as the need for a database migration, why you cannot upgrade without manual actions or testing. [Semantic Versioning (semver)](https://semver.org/) is a strategy for describing the types of changes that are present in a new release.

Flux can use semver so that it will only upgrade new container images which match a given expression. Users may only wish to upgrade services which have a minor version upgrade, which is usually considered harmless.

The team is working on [Flux V2](https://fluxcd.io/), which is described as a "toolkit".


#### == **Fleet Management** ==

Rather than running a highly available K3s cluster with 3-5 nodes, the idea of fleet management is that an organization may instead want to run a large number of single-node K3s clusters. Rancher created the [Fleet](https://rancher.com/docs/rancher/v2.x/en/deploy-across-clusters/fleet/) project to manage large numbers of clusters and [achieved 1 million clusters](https://rancher.com/blog/2020/scaling-fleet-kubernetes-million-clusters).

![Fleet Architecture](LFS156-Fleet_deployment_model_using_pull-based_synchronisation.png)

&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;**Fleet deployment model using pull-based synchronization** (retrieved from rancher.com)

How does Fleet differ from ArgoCD or FluxCD? Fleet is a specialized piece of software designed for running massive amounts of edge devices, whereas FluxCD and ArgoCD are designed to run within individual clusters.

As with networking tools, it is recommended that you try each solution to see what fits your needs, and then get to know the tools well before putting them into production.

----


## Chapter 10. Challenges with the Edge

### **Overview and Learning Objectives**

In this chapter, we will explore some of the challenges such as cost, complexity and connectivity that can affect edge devices.

By the end of this chapter, we will cover the following:

 * Partial connectivity and bandwidth
 * High Availability
 * Access to storage
 * Mean time to recover from a failure.


### **Challenges with the Edge**

#### == **Partial Connectivity and Bandwidth** ==

For devices at the edge, there may be no Internet or cellular connection available at all, which means that data can only be collected by visits from an engineer to a site. An example of this would be a data-logger by a riverside, there to detect flow and water levels.

Where devices have cellular modems that access a 3G network, bandwidth is charged per MB, so being always connected could be expensive, especially at scale. In this instance, the device will need to call home or connect to the network on an interval to save on costs.

Cloud software doesn’t tend to be developed to cope with unreliable or partially available network access, but a prime example would be a mobile phone running an operating system like Android. During a flight, or an underground train journey, you will have no access to the network, but when you are able to connect again, the device can re-synchronize.

#### == **Options for High Availability** ==

High Availability (HA) means that the cluster has enough servers running to tolerate a failure of at latest one of them. K3s has two options for running in HA mode.

The first option is connecting to an external SQL database such as MariaDB, MySQL or Postgresql. In this scenario, all servers synchronize their state here, instead of in the etcd database traditionally used in Kubernetes clusters. Whilst easy to set up, the latency may be high if using an SQL service in the cloud and an edge device in a different region. Another downside is that databases themselves also need to be run in HA mode to tolerate a failure, which just moves the problem.

The second option is more recent, but considered production-ready in v1.19.6 and onwards. This is to run a copy of etcd on every server and have those etcd instances form a cluster and share state. You will need an odd number of servers because the Raft consensus protocol is used to decide which server is the leader, this means a minimum of 3 or 5 nodes.

High-availability isn’t strictly the only option; having a single device which can fail, but be rebuilt in a short period of time could also be desirable and save a considerable amount of money.

The k3sup project has commands to help you set up an HA cluster.

High Availability (HA) means that the cluster has enough servers running to tolerate a failure of at latest one of them. K3s has two options for running in HA mode.

The first option is connecting to an external SQL database such as MariaDB, MySQL or Postgresql. In this scenario, all servers synchronize their state here, instead of in the etcd database traditionally used in Kubernetes clusters. Whilst easy to set up, the latency may be high if using an SQL service in the cloud and an edge device in a different region. Another downside is that databases themselves also need to be run in HA mode to tolerate a failure, which just moves the problem.

The second option is more recent, but considered production-ready in v1.19.6 and onwards. This is to run a copy of etcd on every server and have those etcd instances form a cluster and share state. You will need an odd number of servers because the Raft consensus protocol is used to decide which server is the leader, this means a minimum of 3 or 5 nodes.

High-availability isn’t strictly the only option; having a single device which can fail, but be rebuilt in a short period of time could also be desirable and save a considerable amount of money.

The k3sup project has commands to help you set up an HA cluster.

![K3s HA cluster running with etcd, with three servers and two agents](LFS156-K3s_HA_cluster_running_with_etcd__with_three_servers_and_two_agents.png)

Here is an example of what this would look like for a 3-node HA cluster using etcd, adapted from a [tutorial written for Rancher Labs](https://blog.alexellis.io/bare-metal-kubernetes-with-k3s/), the original author of K3s:

```bash
export SERVER1="192.168.0.101"
export SERVER2="192.168.0.102"
export SERVER3="192.168.0.103"

k3sup install \
  --ip $SERVER1 \
  --cluster \

k3sup join \
  --ip $SERVER2 \
  --server \
  --server-ip $SERVER1

k3sup join \
  --ip $SERVER3 \
  --server \
  --server-ip $SERVER1
```

Now, the first, second or third server could be shut down or stopped, and the cluster would recover from that one failure.

An alternative to running in an HA mode is running in a mode where the recovery from a failure is cheap and easy. If you take the example of a restaurant running a K3s cluster to store records, it could be as simple as sending a new device over a 24-hour courier, or having some spare devices in the stock room. It may also be that the device running K3s doesn’t need to be replaced, but can be reset, and then it can reconfigure itself in some automated way.


#### == **Access to Storage** ==

Why would you want storage for edge devices?

 * For the system drive which runs and operating system and K3s
 * For any data samples or data which is logged
 * To store container images that are run inside the K3s cluster.

There are concerns when it comes to storage at the edge:

 * Latency - is it fast enough?
 * Capacity - will it run out?
 * Reliability - is it going to tolerate a failure?

On top of this, cost is also a factor. A consumer-grade Network Attached Storage (NAS) could cost in the region of $500-1000 USD when populated with three drives running in a RAID-5 configuration. RAID gives redundancy, which can mean the loss of a drive could be tolerated, but is not a substitute for backups.

Backups are usually done with the 3-2-1 method. Three backups are made, 2 are kept on-site on different media, and one is taken off-site, so that in the event of a catastrophic event on-site, the off-site backup can be used to recover the data.

The Raspberry Pi’s SD card for instance has very high latency storage with low capacity when compared to an NVMe (Non-Volatile Memory Express) drive attached over USB.

The Raspberry Pi can also be net-booted over a NAS, which increases reliability and capacity, but the latency also increases compared to a local USB drive.

The other option that can provide high capacity and reliability is remote, network-based storage such as object storage. Events could also be cached on local storage, until there is a chance to upload them, or use MQTT to broadcast them to a central network.

----


## Chapter 11. Continue Your Journey


### **Overview and Learning Objectives**

In this chapter, we will provide resources that you can use to continue your journey, learning Kubernetes, K3s, edge and Raspberry Pi:

 * Edge compute recap
 * Where to find out more about Raspberry Pi clusters
 * Build an HA K3s cluster for production
 * Functions as a Service
 * Additional links for Kubernetes.


### **Continue Your Journey**

#### == **Recap on Edge Compute** ==

In the first few chapters we discussed other projects such as Balena and Eve, which can provide edge computing capabilities. For example, Balena can provide a completely managed option, where the only customization users make is the application they choose to run. Eve can offer advanced virtualization to help consolidate existing VMs and appliances into one system.

So, whilst Kubernetes is not the only option available, it does have the advantage of offering a homogenous platform, if you adopt it at the edge and in your datacenter. K3s makes it even easier to deploy Kubernetes at the edge, and the two are interoperable. Technologies like GitOps and Rancher’s Fleet project can make it easier to manage large numbers of devices or clusters.

#### == **More on Raspberry Pi Clusters** ==

The following are good resources to use:

 * Raspberry PI Foundation: [products and hardware](https://www.raspberrypi.org/products/)
 * BitScope: [Cluster Blade product for industrial applications](https://www.bitscope.com/blog/JK/?p=JK38B)
 * Pimoroni: [sensors and add-ons for Raspberry Pi](https://pimoroni.com/).

The following are complementary resources from Alex Ellis:

 * Watch a video from KubeCon: [The Past, Present, and Future of Kubernetes on Raspberry Pi](https://www.youtube.com/watch?v=jfUpF40--60)
 * Follow a 15-minute guide for K3s on Raspberry Pi with applications: [Walk-through — install Kubernetes to your Raspberry Pi in 15 minutes](https://alexellisuk.medium.com/walk-through-install-kubernetes-to-your-raspberry-pi-in-15-minutes-84a8492dc95a)


#### == **Build K3s Clusters for Production Use** ==

The following are some of the resources that you can use for self-study:

 * Learn more about [K3s in the project documentation](https://k3s.io/)
 * Learn about k3sup as used in the course - [k3sup on GitHub - install K3s over SSH](https://k3sup.dev/)
 * Learn about [ArgoCD](https://argoproj.github.io/argo-cd/)
 * Learn about [Flux](https://fluxcd.io/)

The following are complementary resources from Alex Ellis:

 * Learn to set up K3s on clouds without load-balancers: [Bare-metal Kubernetes with K3s](https://blog.alexellis.io/bare-metal-kubernetes-with-k3s/)
 * Set up K3s using a database and load-balancer: [Set up Your K3s Cluster for High Availability on DigitalOcean](https://rancher.com/blog/2020/k3s-high-availability)


#### == **More about Functions as a Service** ==

Learn everything you need to know about Serverless on Kubernetes with the Edx course: [Introduction to Serverless on Kubernetes (LFS157x)](https://www.edx.org/course/introduction-to-serverless-on-kubernetes).

[faasd](https://github.com/openfaas/faasd) offers a way to run [OpenFaaS](https://www.openfaas.com/) without also having to manage Kubernetes, and may provide a suitable alternative, especially with resource constrained devices.

#### == **Learn about Kubernetes** ==

There are numerous resources to learn more about Kubernetes:

 * Browse the [CNCF Landscape](https://landscape.cncf.io/)
 * Check out the Kubernetes [documentation](https://kubernetes.io/docs/home/) and [tutorials](https://kubernetes.io/docs/tutorials/)
 * Take a Kubernetes course offered by The Linux Foundation
  * Gain a Kubernetes certification: [Certified Kubernetes Administrator (CKA)](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/), [Certified Kubernetes Application Developer (CKAD)](https://training.linuxfoundation.org/certification/certified-kubernetes-application-developer-ckad/), [Certified Kubernetes Security Specialist (CKS)](https://training.linuxfoundation.org/certification/certified-kubernetes-security-specialist/)
 * Learn more about [K3s](https://k3s.io/).
