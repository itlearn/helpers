# Introduction to Serverless on Kubernetes

 * [Course Overview](#course-overview)
 * [Chapter 1. Introduction to Serverless](#chapter-1-introduction-to-serverless)
 * [Chapter 2. State of Serverless in CNCF](#chapter-2-state-of-serverless-in-cncf)
 * [Chapter 3. OpenFaaS](#chapter-3-openfaas)
 * [Chapter 4. Setting Up Kubernetes and OpenFaaS](#chapter-4-setting-up-kubernetes-and-openfaas)
 * [Chapter 5. OpenFaaS Features](#chapter-5-openfaas-features)
 * [Chapter 6. Create Your First Function with Python](#chapter-6-create-your-first-function-with-python)
 * [Chapter 7. Configuring a Function](#chapter-7-configuring-a-function)
 * [Chapter 8. Operating Serverless](#chapter-8-operating-serverless)
 * [Chapter 9. Taking It Further](#chapter-9-taking-it-further)


**Reference**

LinuxFoundationX LFS157x

https://training.linuxfoundation.org/resources/free-courses/introduction-to-serverless-on-kubernetes-lfs157/
https://www.edx.org/course/introduction-to-serverless-on-kubernetes


Learn how to build serverless functions that can run on any cloud, without being restricted by limits on the execution duration, languages available, or the size of your code. This course is designed to give you an overview of how a serverless approach works in tandem with a Kubernetes cluster.

## Course Overview


With the advent of systems like AWS Lambda, the term serverless gained much popularity. However, many people are still unsure what it is for, and how it can help them build applications faster than traditional approaches. Other potential users are turned off by the arbitrary limits and lock-in of cloud-based serverless products.

This course will teach you what “serverless” means and how it can be made portable with open source frameworks. You will get a good overview of the CNCF serverless landscape, and a first-hand experience in building and deploying code, directly to a Kubernetes cluster.

You will build functions using Python and learn how to configure them, manage secrets, add dependencies via pip, learn how to work with binary data, and add authentication. In the second part of the course you will explore the operational side of functions with Kubernetes. This will introduce auto-scaling, metrics, dashboards and how to expose them securely on the Internet with TLS.

If you are a developer or an IT operator interested in exploring new approaches for building software, and prefer to be able to set your own limits when it comes to things like timeouts and choice of programming language, this is the course for you!

This course is designed to show you how a serverless approach works in tandem with a Kubernetes cluster, and help you discover the potential of serverless functions. By the end of the course you will be able to build functions using Python, configure them, and use secrets, as well as to add dependencies via pip, and learn how to build native extensions. You will also know how to expose functions securely on the Internet with TLS, and secure them with authentication.

### **At a glance**

 * Institution: LinuxFoundationX
 * Subject: Computer Science
 * Level: Introductory
 * Prerequisites:

    * Basic knowledge of CLI tools and Linux administration
    * BSome experience with Python programming language
    * BBasic understanding of containers and Docker

 * Language: English
 * Associated programs:
    * Professional Certificate in Introduction to DevOps: Practices and Tools

### **What you'll learn**

 * Understand what serverless is.
 * Get an overview of the CNCF landscape around serverless technologies.
 * Learn how serverless can be made portable through the use of the Serverless 2.0 definition and Kubernetes.
 * Explore the OpenFaaS toolchain, including: UI, CLI and REST API.
 * Explore the OpenFaaS ecosystem, including the template store and function store for sharing functions and code templates.
 * Build functions using Python, configure them, and use secrets.
 * Add dependencies via pip, and learn how to build native extensions.
 * Explore how to operate OpenFaaS with: autoscaling, metrics, dashboards, upgrades, custom URLs, and versioning.
 * Expose functions securely on the Internet with TLS, and secure them with authentication.

### **Syllabus**

 * Welcome!
 * Chapter 1. Introduction to Serverless
 * Chapter 2. State of Serverless in CNCF
 * Chapter 3. OpenFaaS
 * Chapter 4. Setting Up Kubernetes and OpenFaaS
 * Chapter 5. OpenFaaS Features
 * Chapter 6. Create Your First Function with Python
 * Chapter 7. Configuring a Function
 * Chapter 8. Operating Serverless
 * Chapter 9. Taking It Further
 * Final Exam (verified track only)

### **About the instructors**

 * Alex Ellis
 * Founder of OpenFaas and inlets • OpenFaaS Ltd

### **Course Details**

 * Length: Estimated 7 weeks
 * Effort: 2–3 hours per week
 * Level: Beginner
 * Price: Free (certificate optional) 


---

## Chapter 1. Introduction to Serverless

### **Overview and Learning Objectives**

In this chapter, we will explore what the term *Serverless* means, and what makes a *function* vs. a regular web service. We will also contrast two perspectives on what it means for an application to be serverless.

By the end of this chapter you should be able to:

 * Provide a definition of Serverless and FaaS (Function as a Service).
 * Discuss the differences between functions in the cloud and on Kubernetes.

### **What to Expect from this Course** 

Throughout this course, we will explore Serverless together, but the material will not be specific to any one cloud provider. The Serverless we are learning about can be deployed and operated on any computer, whether on-premises, in the cloud, or on your laptop. The functions and platform are made portable through the use of containers and Kubernetes. So, it is only right that we begin with setting the scene with an overview of initiatives within the the Cloud Native Computing Foundation (CNCF), the home and primary sponsor of Kubernetes.

From there, we will set up Kubernetes along with OpenFaaS, an open source platform which is the focus of this course. We will familiarize ourselves with OpenFaaS, starting with its UI, CLI and its ecosystem - the function store and template store. We will then build and configure functions, think about what it means to operate them, and finally, we will discuss some options for taking it further.

### **What Is Serverless?**

**Serverless** has become one of the most divisive terms of cloud computing, with two schools of thought on what makes software “Serverless” or not. There is also some discussion about the differences between Functions as a Service (FaaS) and Serverless.

To start with, Serverless is an abstract term and it is not meant to be taken literally, although many do. In the same way that Cloud Computing does not invoke the sky, serverless does not involve code executing without servers. It merely refers to the experience a user or customer has and represents a continuum of how close one needs to work with hardware and infrastructure.

![Serverless and FaaS Axis](LFS157-serverless-axis.png)

As we move along the axis, we see that along with a decreasing concern for infrastructure, there is also a decomposition of the workload and a reduction in its size. Therefore, if Serverless is an approach and an architectural pattern, Functions and FaaS are a subset of that, providing a concrete way to apply the technology and ideals.

### **Characteristics of Functions**

When compared to monolithic applications, functions tend to:

 * Allow the developer to focus on code, rather than infrastructure and deployment artifacts.
 * Create smaller artifacts and fewer lines of code, because they have fewer responsibilities (perhaps even only one).
 * They can be either event-driven (triggered by a database, object store, pub-sub), or deployed as REST endpoints and accessed via HTTP.
 * They are easy to manage because they do not rely on underlying storage.
 * And they are isomorphic - meaning that each function has to conform to a given runtime interface.


### **Serverless Platforms**

A serverless platform is responsible for auto-scaling, which tends to take two forms:

 * Scaling functions down to save costs and reduce load on the system when functions are idle. Not all platforms support scaling to zero, and scaling back from zero replicas tends to involve a latency penalty called a “cold-start” as the code is re-deployed, or re-initialized.
 * Scaling up functions proportionally as demand on a particular endpoint increases.

With the Serverless and FaaS definition in mind, let’s explore two perspectives from the community.

Cloud vendors who own and operate serverless SaaS products would have you believe that a platform is only Serverless if:

 * The developer only writes small blocks of code called functions
 * The customer is billed based upon consumption (milliseconds, no idle time).
 * Neither the developer, nor the operator should have any access or knowledge of the servers on which the code is running.
 * The code can scale to execute in massive parallelism.
 * There is little to no need to move code between clouds.

Other practitioners of Serverless, such as the members of the Serverless Working Group in the Cloud Native Computing Foundation (CNCF) believe that, whilst this view is popular, it is not a binary distinction, and that serverless is more of a philosophical approach than a literal set of constraints:

 * The developer only writes small blocks of code called functions or microservices.
 * Serverless is a developer experience, rather than a literal set of constraints.
 * A developer’s concern for servers is lessened, if not completely removed, whilst an operator may still have to pool together resources.
 * Code can scale in massive parallelism using existing resources, or by provisioning new resources in the cloud, just in time.
 * Per second billing is not a concern for the developer.
 * Code can be moved between different systems or clouds if required.


To some extent, the discussion moves from “is Serverless a set of hard and fast rules”, to "what is the look and feel of a Serverless system? What is the experience like for an operator and for a developer?" The common ground from the two perspectives is that developers should care less about infrastructure, and write small blocks of code that are easy to deploy.

Within the CNCF, there are many projects that are built to run upon Kubernetes, with some of those offering a Serverless-style type of experience. Customers use these platforms because they are portable and can be run in a client’s datacenter, or chosen cloud, without changes to the application.



---

## Chapter 2. State of Serverless in CNCF

### **Overview and Learning Objectives**

Now that you have an initial definition of serverless and the two primary perspectives on how it should be built and run, let’s explore what the CNCF offers to the Serverless ecosystem. We will compare various open source projects and learn how they aim to avoid lock-in to any one vendor.

By the end of this chapter, you should know:

 * What initiatives exist within the CNCF around Serverless.
 * The basic way Serverless is implemented on Kubernetes.
 * The Serverless 2.0 definition.
 * A high-level overview of the top Serverless 2.0 open source frameworks.


### **State of Serverless in Kubernetes**

When it comes to talking about Serverless in the scope of Kubernetes, the CNCF Serverless landscape separates these into offerings which can be “installed” and operated on a cluster, and offerings which are hosted as a SaaS product.

The smallest primitive for workloads in Kubernetes is the Pod, which can be made up of one or more containers - such as the main workload and then a helper such as a proxy or a log collector. The Pod loads the code and user-space from a container image which is stored in a container registry.

To access Pods within a cluster, various Kubernetes networking primitives come into play, such as a Service, a LoadBalancer and Ingress. Each of these provide a slightly different way to send traffic to the running workload.

Our bare essentials for Serverless on Kubernetes are:

 * A container image with function code or an executable inside.
 * A registry to host the container image.
 * A Pod to run the container image.
 * A Service to access the Pod.

Often, projects will add many more components on top of this stack, such as a UI, and API gateway, Ingress automation, auto-scaling, APIs, and many more.

Projects that build on the above primitives have a relatively good level of compatibility; for instance, workloads built for OpenFaaS and run on Knative, and visa-versa.


### **Serverless 1.0 vs. Serverless 2.0**


In a Serverless 1.0 world, cloud vendors created independent products without concern for portability or migrating between products. Oftentimes, moving from AWS Lambda to Azure Functions would involve reworking the signature, the way the zip file was constructed, the way managed services were accessed, and dealing with varying availability zones, regions and hard limits like RAM and function image size.

For some customers in the enterprise, being limited to a function image of 256mb, a maximum call duration of 5 minutes, and a maximum RAM capacity of 2GB is too limiting for their demands. Moreover, Serverless 1.0 does not provide an answer for microservices, which many companies have in their estate.

![Serverless 1.0 (Cloud Functions/FaaS)](LFS157-serverless1.0.png)

Serverless 2.0 workloads are far more portable than 1.0. Next, we will introduce you to the Serverless 2.0 Landscape so that you can become familiar with projects that fit within the definition:

 * Is stored in an OCI-compatible container image.
 * Exposes an HTTP server on port 8080.
 * Can be configured with environment variables.

This relatively short definition is what enables code written in Node.js, Go, Python, or any other binary to be moved between FaaS platforms with ease. There is the added benefit of being able to run microservices - such as Ruby on Rails, Express.js, Vert.x and Micronaut.

### **Serverless 2.0 Landscape**

Meet the Serverless 2.0 Landscape. These frameworks conform to the Serverless 2.0 definition and have the most traction within the Serverless segment of the CNCF Landscape. Some of the projects have been created organically by independent developers, while others are the efforts of commercial software companies.

![Serverless 2.0 Landscape](LFS157-Serverless_2.0_Landscape.png)


The landscape is further broken down into capabilities offered. So, for instance, OpenFaaS and Knative provide functionality in almost every box, whereas Buildpacks, CloudEvents and Keda are specific to only certain categories and rely on other projects to fill in the gaps.

### **OpenFaaS**

[OpenFaaS](https://github.com/openfaas/faas) was created so that developers could use their own hardware to run functions using Docker containers. It has over 22k GitHub stars and a thriving community of independent software developers and end-users such as BT, Citrix, LivePerson and Vision Banco:

 * **Build templates**
  
    OpenFaaS provides its own set of build templates that can be used to generate functions. The templates can also be discovered and shared online via the Template Store.

 * **Serving runtime**
  
    OpenFaaS is the most mature serving runtime and has the most amount of stars on the landscape. It supports auto-scaling with Prometheus or Kubernetes HPAv2. It can scale to zero and back again to save on resources. The serving runtime comes with a REST API, CLI, and UI. OpenFaaS has support for traffic-shifting with Istio or Linkerd2. OpenFaaS conforms to the Serverless 2.0 definition.

 * **Events**
  
    OpenFaaS has a number of event triggers, including Apache Kafka, NATS, AWS SQS, Cron, MQTT, and others. A connector-sdk is made available for Golang so that new connectors can be written in a short period of time.
 
 * **Scale from Zero**
  
    Scale to and from zero is supported.

 * **Managed**
  
    OpenFaaS Cloud is a managed offering built directly upon OpenFaaS, offering multi-user support, rich dashboards, metrics and integration with GitHub/GitLab. OpenFaaS Cloud is open source and can be hosted.

### **Knative**

Originally developed at Google, [Knative](https://github.com/knative/serving) now has contributors from IBM, RedHat and VMware. Knative’s scope at launch covered: events, build, and serving. Since launch, the build project has been deprecated in favor of the independent Tekton project for creating build and CI pipelines:

 * **Build templates**
  
    Knative does not have its own set of templates, but can use OpenFaaS or Buildpacks. Most users maintain their own Dockerfiles and boiler-plate code. Knative conforms to the Serverless 2.0 definition.

 * **Serving runtime**
  
    Knative’s serving runtime is called “Knative serving” and requires a separate API gateway such as Ambassador, Istio or Gloo to operate. Knative is generally installed alongside Istio to provide traffic-shifting. Each deployment of a service is immutable, and internally creates a revision enabling users to go back to a previous version of code.

 * **Events**
 
    The Knative eventing project covers all integrations with events. There is a set of 4-5 primitives which are required to receive events from a broker. Once in place, functions can be triggered from a range of different event sources.

 * **Scale from Zero**
            
    Knative has support for scale to and from zero. The cold-start time is notably high; however, the project is actively trying to find ways to reduce the latency.

### **Google Cloud Run**

[Google Cloud Run](https://cloud.google.com/run) is not actually an installable platform, and, unlike the other platforms listed, it is not open source, but it does conform to the Serverless 2.0 definition. This provides a balance between. You can build a container for OpenFaaS or Knative and then deploy it to Cloud Run. The platform is billed as a SaaS and has certain limits (as of June 2020, no more than 2GB of RAM can be used, and the execution time is limited to 15 minutes).

### **KEDA**

[KEDA](https://github.com/kedacore/keda) is primarily concerned with invocation functions from events and uses the Azure Functions build templates to define functions. On March 31, 2020 KEDA was donated to the CNCF as a Sandbox project. You can read more about it in this [blog article](https://keda.sh/blog/keda-cncf-sandbox/).

Closely related to KEDA is Azure Functions, which does offer a cloud-hosted serverless 1.0-style deployment platform.

### **Rio**

[Rio](https://github.com/rancher/rio) aims to provide a light-weight PaaS, in a serverless style. Initially built by Rancher as a fork of Knative, the project has evolved to rebuild some of the Knative concepts in its own way, such as revisions and services without a hard dependency on the upstream project.

Rio is the only other project on the Serverless 2.0 landscape to have its own official UI. Rio is currently in beta.

### **CloudEvents**

[CloudEvents](https://cloudevents.io/) is a specification written by a number of participants within the CNCF’s Serverless Working Group. Whilst the Serverless 2.0 definition enables portability functions between clouds, the data is still difficult to extract. The CloudEvents project has representatives from AWS, Oracle, Azure and Google and aims to make data from SaaS projects like databases and object stores easier to consume on any other cloud. Artifacts include: an SDK which can be used in a number of different programming languages along with a specification for the event format which is currently at v1.0. CloudEvents is also a CNCF sandbox project.

### **CNCF Serverless Working Group**

The [Serverless Working Group](https://github.com/cncf/wg-serverless) was started by the CNCF in 2017 for those with a particular interest in Serverless and FaaS. Its first output was a large piece of analysis that set out to define the terms Serverless and FaaS, and a draft specification was published. Soon after that, CloudEvents was started and quickly reached a stable 1.0 version along with being donated to the CNCF as a sandbox project. The working group is also exploring other related topics, such as function chaining and workflows.

---

## Chapter 3. OpenFaaS

### **Overview and Learning Objectives**

We now have a broad overview of what Serverless and FaaS mean and where they fit within the CNCF ecosystem. We also have a good general overview of the various serverless projects. For the remainder of this course, we will focus on OpenFaaS. So let’s start familiarizing ourselves with the approach it takes and some of its moving parts.

By the end of this chapter, you should be able to:

 * Understand the philosophy behind the CNCF and how OpenFaaS embraces the same approach.
 * Discuss what PLONK stands for, and why it’s important for application developers.
 * Understand the conceptual architecture of OpenFaaS and how it uses various OSS projects from the CNCF.


### **More on OpenFaas**

Now, OpenFaaS is built by a core group of independent developers who have a strong interest in making serverless portable and accessible by its users. The tagline of OpenFaaS is *“Serverless Functions Made Simple”*; judging by the number of conference talks, meetups, blog posts and case studies published, OpenFaaS is delivering on that.

 * You can learn more about community events, blog posts, and meetups on the [OpenFaaS Community page on GitHub](https://github.com/openfaas/faas/blob/master/community.md).
 * You can learn more about the commercial adopters on the [Adopters page on GitHub](https://github.com/openfaas/faas/blob/master/ADOPTERS.md).


### **What Is PLONK?**

Part of the CNCF philosophy is to provide a set of complementary projects and tools designed to work with cloud computing. Cloud native projects work well together when they are designed to solve a single, well-defined problem. This means that other projects can compose and extend prior innovation to move fast. Using existing cloud native projects was important for OpenFaaS, and you will see from the diagram below how OpenFaaS fits together with the ecosystem.

When Prometheus, Linux, OpenFaaS, NATS and Kubernetes are combined, we get the PLONK stack, much like LAMP (Linux, Apache, MySQL, and PHP) or JAM (JavaScript APIs Markdown).

![The PLONK Stack](LFS157-plonk-cncf.png)

The PLONK stack also requires components like a Container Registry and Container Runtime such as Docker or Containerd. You can then build on top of it by introducing new projects, such as OpenFaaS Cloud, GitHub and GitLab.

The PLONK Stack is a series of open source projects that, when combined, provide a stack for Application Developers to use to write applications quickly - whether functions or microservices. You can read an introduction to the PLONK stack in this [blog article](https://www.openfaas.com/blog/plonk-stack/).

Let's take a closer look at the PLONK stack, starting at the bottom and working our way up.


**Infrastructure Layer**

 * Docker provides a packaging image format, which is portable between clouds, unlike “Serverless 1.0” and cloud functions.
 * A container registry holds each version of our function, meaning that we can version it and benefit from distribution, security scanning and many other features of projects like the CNCF [Harbor](https://goharbor.io/) registry.
 * [Kubernetes](https://kubernetes.io/) provides a control plane to run our functions, including fail-over, high availability (HA), scale-out and secret management.

**Application Layer**


 * The [OpenFaaS Gateway](https://docs.openfaas.com/architecture/gateway/) is conceptually similar to a reverse proxy like [Nginx](https://www.nginx.com/), [Kong](https://konghq.com/kong/) or [Caddy](https://caddyserver.com/); however, its job is to expose and manage containers running our functions, rather than REST APIs. It does have its own REST API and can be automated. The most popular client for the OpenFaaS Gateway is the CLI (`faas-cli`), followed by the UI.
 * [Prometheus](https://prometheus.io/) is a CNCF project which provides metrics and instrumentation. It can be used to help inform autoscaling decisions, along with understanding the health and performance of OpenFaaS and our set of functions. [AlertManager](https://prometheus.io/docs/alerting/latest/alertmanager/) comes with Prometheus and is used to fire off scaling alerts or to notify us about a function which may be failing.
 * [NATS](https://nats.io/) is another CNCF project which, when combined with OpenFaaS, provides a way to queue up requests and defer them for later execution. Each request can be deferred and run as there is capacity within the cluster, without the developer writing any additional code.


**GitOps/IaaC Layer**


 * [OpenFaaS Cloud](https://github.com/openfaas/openfaas-cloud) orchestrates all the lower layers to provide a multi-user dashboard with authentication, built-in CI/CD and integration to [GitHub](https://github.com/) or [GitLab](https://about.gitlab.com/).
 * GitHub can be used to build and deploy functions using its Travis integration, or its own GitHub actions and container registry.
 * GitLab comes with a full suite of GitOps-like tooling that can be used to create build and deployment pipelines directly into OpenFaaS.


Setup OpenFaaS Cloud for local development https://blog.alexellis.io/openfaas-cloud-for-development/

In conclusion, OpenFaaS is able to leverage many open source projects and components to build a composable architecture and allow the operator maximum control and flexibility. In this course, we will focus primarily on the Application Layer.

### **Conceptual Architecture for OpenFaaS**

The following diagram represents the conceptual architecture for OpenFaaS.

![OpenFaaS Workflow](LFS157-of-workflow.png)

In the diagram above, we zoom into the Application Layer to show the various components working together. You will notice that each function is a Docker image, which means it needs a name and a tag. The tag can be used for version control with semver, or other conventions. One of the endpoints is a service, which goes to show that microservices and functions can co-exist and work well together within Serverless.

All interaction with OpenFaaS happens through its REST API, and there is a [Swagger definition](https://docs.openfaas.com/architecture/gateway/#swagger) you can use if you want to dig deeper.

The documentation also lists the built-in [metrics](https://docs.openfaas.com/architecture/metrics/) available through Prometheus.

The core functionality provided by the OpenFaaS Gateway is to:

 * Create, list, update and delete functions.
 * Scale function replicas.
 * Invoke a function.
 * Query the health, metrics, and scaling status of functions. 
 * Create, list and delete secrets.
 * View the logs from functions.
 * Queue-up asynchronous requests.

The three ways of interacting with the REST API tend to be:

 * Using the CLI (`faas-cli`).
 * Using the built-in UI.
 * Or via the REST API directly from your application or via cURL.


### **Events, Triggers, and Invocations**

All communication within OpenFaaS happens over HTTP using REST. This simple interface is made powerful when coupled with events and [triggers](https://docs.openfaas.com/reference/triggers/).

The OpenFaaS documentation lists several community and official triggers for OpenFaaS. A trigger is a way to invoke a function, with a simple HTTP call being the simplest option.

Most triggers in OpenFaaS are built with the connector-sdk, an open source add-on that lets developers write simple connectors between an event source and functions.

Examples include:

 * Apache Kafka - a popular pub/sub system used by enterprise companies
 * cron - time-based invocation
 * SQS - trigger via AWS SQS
 * NATS - a high-speed messaging CNCF project
 * Minio - an open source S3 replacement and object store
 * RabbitMQ - a traditional message queue
 * MQTT - a pub/sub system often used with IoT devices.

Functions can also be invoked using the built-in asynchronous mechanism provided by OpenFaaS, which we will explore in the following chapters.

You can find a complete list of [triggers](https://docs.openfaas.com/reference/triggers/), and a link for the connector-sdk in the documentation.

---

## Chapter 4. Setting Up Kubernetes and OpenFaaS

### **Overview and Learning Objectives**

In the previous chapter we explored serverless frameworks including OpenFaaS and the Serverles 2.0 definition which allows for portability between clouds. In this chapter we will deploy Kubernetes and OpenFaaS and understand the role of Helm within the Kubernetes ecosystem.

By the end of this chapter, you should be able to:

 * Understand the difference between Kubernetes on your laptop and Kubernetes in the cloud.
 * Discuss options for local and cloud installation.
 * Discuss the purpose of Helm.
 * Install OpenFaaS with Arkade.
 * Deploy OpenFaaS.


### **Kubernetes Setup: Local or in the Cloud?**

The preferred way to deploy OpenFaaS is deploying it to Kubernetes, and that is what you will be doing in this chapter. You can create a cluster on your laptop or in the cloud using a managed Kubernetes service.

There are other options available for running OpenFaaS, but this falls outside of the scope of our course. One of the newest options is faasd, which is a smaller version of OpenFaaS, designed to run on a single host without any clustering. It still uses some of the same primitive components as Kubernetes, like containerd and Container Networking Interface (CNI), but has a lower management and resource overhead.

You can either create your cluster using a cloud Kubernetes service, or run it on your laptop.

**Using a cloud Kubernetes service**

Pros:

 * Low overheads - maintenance and upgrades are often automated
 * Integration with cloud services such as storage, log collection and load-balancers for public IPs.

Cons:

 * There is a cost per node in the cluster
 * Sometimes, there is also a cost for the cluster control plane, such as with Google Kubernetes Engine and with AWS Elastic Kubernetes Service.

Running a cluster locally, on your machine

Pros:

 * Fastest option - the cluster is on your own computer
 * Cheapest (effectively free).

Cons:

 * May slow down your computer, since this usually requires running an additional VM
 * Drains battery quickly
 * No public IP address available, unless you use a tunnel like those available [here](https://inlets.dev/).

Securely tunnel a local database to a remote server https://www.youtube.com/watch?v=K4vH2IdtiBU

Using a local cluster can be a good way to learn and also to save money; however, they should be treated largely as disposable. Cloud clusters are usually configured to be ready for receiving production workloads and traffic.

The primary difference for the workshop is whether you can access all the functionality of OpenFaaS. Certain features like TLS certificates to enable HTTPS are going to require a public IP address, which are only available on public cloud, or when using tunnel software like [inlets](https://inlets.dev/), which provides public IPs to local clusters.

### **Local Kubernetes Options**

OpenFaaS should work on any local Kubernetes cluster. Common options include:

 * **Minikube**
  
    The original “local Kubernetes”. It requires use of Oracle VirtualBox to create its own virtual machine (VM) for the cluster.

 * **Docker Desktop**

    Docker Desktop includes a “Kubernetes” checkbox which runs the cluster inside the same VM used for Docker builds.

 * **Microk8s**

    From the Canonical team, microk8s integrates directly with and requires Ubuntu.

 * **k3d**

    This option from Rancher runs Kubernetes within a Docker container, so it will work on any machine which has Docker installed. K3d uses k3s, which is a cut-down version of Kubernetes designed to conserve resources.

*Many OpenFaaS users also run Kubernetes on their Raspberry Pi clusters. Whilst this provides a fun way to learn about DevOps, it does present its own set of unique challenges with all software having to be rebuilt for the ARM CPU in the devices.* ***Therefore, it is not recommended that you take this course with your Raspberry Pi cluster***.


### **Cloud Kubernetes Options**

There are too many Kubernetes services to mention them all; however, the following have been tested by the community and should provide you with a good experience out of the box:

 * Azure Kubernetes Service (AKS)
 * Amazon Elastic Kubernetes Service (EKS)
 * DigitalOcean Kubernetes Service (DOKS)
 * Google Kubernetes Engine (GKE).

For this course, it is recommended that you pick one of the cloud Kubernetes services; if you do not, you will miss out on some content in Chapter 6 on Operating Serverless.

At a minimum, you should create a public cloud cluster with 2-3 nodes with 2-4GB of RAM available and 2 vCPUs on each node. There is no need to use availability zones, or high-availability (HA) options whilst learning OpenFaaS, as this will add unnecessary costs.

A *“preparing for production”* guide can be found in the [OpenFaaS documentation](https://docs.openfaas.com/architecture/production/).


### **Introduction to Helm and arkade**

Configuration files for a Kubernetes application such as OpenFaaS are usually distributed as a set of static YAML files. Having static files makes customization and distribution a challenge. There are two notable options for solving this problem - the first is Helm, and the second is a newer project called Kustomize, which we won’t cover in this course.

Helm 3 is a mature package manager for Kubernetes and a CNCF project. With Helm, users can publish their own set of configurations for their application called a Chart, which can then be discovered, installed, upgraded and managed with Helm or third-party automation tools. The key difference over static YAML files is that each chart can come with a set of knobs and dials that users can use to fine-tune the behavior of the software. An example might be - what domain name to use, or what the maximum timeout is for an HTTP microservice.

Helm charts can be customized with a `values.yaml` file, or by passing in the `--set key=value` flag to the `helm install` or `upgrade` command.

**Do not run the following commands on your cluster, as these are provided as examples to help you understand how Helm and arkade work together.**

Next, we provide an example of how to install a chart using Helm 3.

```bash
helm repo add stable https://charts.helm.sh/stable
helm repo update
```

Then install the chart, and pass in a `values.yaml` file, or override each option with `--set` such as `persistence.enabled=false`, which turns off permanent storage requests for Postgresql.

```bash
helm upgrade --install postgresql stable/postgresql --set persistence.enabled=false
```

Charts can be removed in a similar way with:

```bash
helm delete postgresql
```

Unfortunately, charts can have many confusing options, and, as a new-comer, it is often very difficult to know what to type in to install an application. This is made even worse by confusing tables showing each parameter, sometimes requiring nesting of 2-3 levels to type in a full value like: `--set pushgateway.enabled=false` or `metrics.readinessProbe.initialDelaySeconds`. For an example of how bad this can get, see the `README` for the [Postgresql Chart](https://github.com/helm/charts/tree/master/stable/postgresql).

[arkade](https://github.com/alexellis/arkade) is an open-source CLI written in Go which provides a series of “apps” for common Kubernetes software, such as the kubernetes-dashboard, Istio, OpenFaaS, Postgresql, cert-manager, MongoDB, and many more. 

Each “app” has a series of flags available which makes common options easy to discover.

This is an example of running the arkade install --help command:

```bash
# arkade install --help


Install Kubernetes apps from helm charts or YAML files using the "install"
command. Helm 3 is used by default unless you pass --helm3=false, then helm 2
will be used to generate YAML files which are applied without tiller.
You can also find the post-install message for each app with the "info"
command.

Usage:
  arkade install [flags]
  arkade install [command]

Examples:
  arkade install
  arkade install openfaas --helm3 --gateways=2
  arkade install inlets-operator --token-file $HOME/do-token

Available Commands:
  argocd                  Install argocd
  cert-manager            Install cert-manager
  chart                   Install the specified helm chart
  cron-connector          Install cron-connector for OpenFaaS
  crossplane              Install Crossplane
  docker-registry         Install a Docker registry
  docker-registry-ingress Install registry ingress with TLS
  grafana                 Install grafana
  info                    Find info about a Kubernetes app
  ingress-nginx           Install ingress-nginx
  inlets-operator         Install inlets-operator
  istio                   Install istio
  jenkins                 Install jenkins
  kafka-connector         Install kafka-connector for OpenFaaS
  kube-state-metrics      Install kube-state-metrics
  kubernetes-dashboard    Install kubernetes-dashboard
  linkerd                 Install linkerd
  metrics-server          Install metrics-server
  minio                   Install minio
  mongodb                 Install mongodb
  openfaas                Install openfaas
  openfaas-ingress        Install openfaas ingress with TLS
  portainer               Install portainer to visualise and manage containers
  postgresql              Install postgresql
  tekton                  Install Tekton pipelines and dashboard
  traefik2                Install traefik2

```

You can then get further information by typing `arkade install NAME --help;` so let’s try OpenFaaS:


```bash
# arkade install openfaas --help

Install openfaas

Usage:
  arkade install openfaas [flags]

Examples:
  arkade install openfaas --loadbalancer

>Flags:
  -a, --basic-auth                    Enable authentication (default true)
      --basic-auth-password string    Override the default random basic-auth-password if this is set
      --clusterrole                   Create a ClusterRole for OpenFaaS instead of a limited scope Role
      --direct-functions              Invoke functions directly from the gateway (default true)
      --function-pull-policy string   Pull policy for functions (default "Always")
      --gateways int                  Replicas of gateway (default 1)
      --helm3                         Use helm3, if set to false uses helm2 (default true)
  -h, --help                          help for openfaas
      --ingress-operator              Get custom domains and Ingress records via the ingress-operator component
  -l, --load-balancer                 Add a loadbalancer
      --max-inflight int              Max tasks for queue-worker to process in parallel (default 1)
  -n, --namespace string              The namespace for the core services (default "openfaas")
      --operator                      Create OpenFaaS Operator
      --pull-policy string            Pull policy for OpenFaaS core services (default "IfNotPresent")
      --queue-workers int             Replicas of queue-worker for HA (default 1)
      --set stringArray               Use custom flags or override existing flags 
                                      (example --set=gateway.replicas=2)
      --update-repo                   Update the helm repo (default true)
```

As you can see from the output above, we can customize features like whether there is a loadbalancer in use (for Cloud Kubernetes) `--load-balancer`, and whether we want 1 replica of the OpenFaaS Gateway, `--gateways 1`, or several, `--gateways N`.

arkade also provides several other useful applications to obtain HTTPS certificates, such as the `openfaas-ingress` app.

```bash
arkade install openfaas-ingress --help

Install openfaas ingress. Requires cert-manager 0.11.0 or higher installation in the cluster. Please set --domain to your custom domain and set --email to your email - this email is used by letsencrypt for domain expiry etc.

Usage:
  arkade install openfaas-ingress [flags]

Examples:
  arkade install openfaas-ingress --domain openfaas.example.com --email openfaas@example.com

>Flags:
  -d, --domain string          Custom Ingress Domain
  -e, --email string           Letsencrypt Email
  -h, --help                   help for openfaas-ingress
      --ingress-class string   Ingress class to be used such as nginx or traefik (default "nginx")
      --staging                set --staging to true to use the staging Letsencrypt issuer
```

With a few simple commands you can install OpenFaaS in high availability (HA) mode, and then get an HTTPS certificate for your gateway.

### **Deploy OpenFaaS**

Now that we have configured Kubernetes and explored both Helm and arkade, let’s go ahead and deploy OpenFaaS to our cluster.Use arkade to install OpenFaaS. If you do not have Helm3 installed, it will be downloaded automatically for you.

```bash
arkade install openfaas
```
After the installation has completed, you will receive the commands you need to run, to log in and access the OpenFaaS Gateway service in Kubernetes.

```bash
Info for app: openfaas
# Get the faas-cli
curl -SLsf https://cli.openfaas.com | sudo sh

# Forward the gateway to your machine
kubectl rollout status -n openfaas deploy/gateway
kubectl port-forward -n openfaas svc/gateway 8080:8080 &

# If basic auth is enabled, you can now log into your gateway:
PASSWORD=$(kubectl get secret -n openfaas basic-auth -o
jsonpath="{.data.basic-auth-password}" | base64 --decode; echo)
echo -n $PASSWORD | faas-cli login --username admin --password-stdin

faas-cli store deploy figlet
faas-cli list

```

You can get this message again at any time with `arkade info openfaas`. Let’s break it down, line by line.

The `kubectl rollout status` command checks that all the containers in the core OpenFaaS stack have started and are healthy.

The `kubectl port-forward` command securely forwards a connection to the OpenFaaS Gateway service within your cluster to your laptop on port 8080. It will remain open for as long as the process is running, so if it appears to be inaccessible later on, just run this command again.

The `faas-cli login` command and preceding line populate the `PASSWORD` environment variable. You can use this to get the password to open the UI at any time.

We then have `faas-cli store deploy figlet` and `faas-cli list`. The first command deploys an ASCII generator function from the Function Store and the second command lists the deployed functions, you should see `figlet` listed.

You will also find the PLONK stack components deployed, such as Prometheus and NATS. You can see them in the `openfaas` Kubernetes namespace:

```bash
kubectl get deploy --namespace openfaas

NAME               READY  UP-TO-DATE  AVAILABLE  AGE
alertmanager       1/1    1           1          28h
basic-auth-plugin  1/1    1           1          28h
faas-idler         1/1    1           1          28h
gateway            1/1    1           1          28h
nats               1/1    1           1          28h
prometheus         1/1    1           1          28h
queue-worker       1/1    1           1          28h
```

If you run into any issues, you can use the [OpenFaaS troubleshooting guide](https://docs.openfaas.com/deployment/troubleshooting/) and the `kubectl` tool to find out what went wrong.

---

## Chapter 5. OpenFaaS Features

### **Overview and Learning Objectives**

Now that we have picked a Kubernetes distribution and deployed OpenFaaS, let’s try out the UI, learn about the CLI and start deploying some functions.

By the end of this chapter you should be able to:

 * Use the UI to test and deploy functions along with and its various fields.
 * Deploy functions from the Function Store.
 * Invoke functions with curl and with `faas-cli`.
 * Familiarize yourself with the CLI commands and how they can be grouped together.

### **The Gateway UI**

We have already touched on the CLI briefly after running the `arkade` installation command. Remember that you can access the password and port-forwarding command at any time with arkade info openfaas.

To open the UI navigate to http://127.0.0.1:8080 in a browser. There is no need to worry that this is using HTTP (plaintext) instead of HTTPS, because the previous port-forwarding command runs over an encrypted connection.

Run `arkade info openfaas`, and then type in the command to retrieve your password. You can then echo it in the terminal:

```bash
echo $PASSWORD
```

![Login Gateway](LFS157-login-gw.png)


When prompted, the user is `admin` and the password is the value from above. The password can be changed at any time. A commercial solution using OpenID Connect is also available separately.

![Gateway Landing](LFS157Gateway_landing.png)

OpenFaaS is one of the only projects on the CNCF Landscape to provide a UI. Whilst it is useful for exploring and testing functions, it only provides a small subset of functionalities, so you will spend most of your time using the CLI.

On the left hand side of the screen you will see a *DEPLOY NEW FUNCTION* button, which deploys functions from the Function Store and/or a custom deployment.

Deploy the `certinfo` function. This can be used to check when a TLS certificate will expire.

![Invoking a function](LFS157-certinfo-invoke.png)

Enter a domain and hit *Invoke*; you will see the results in the bottom half of the screen.

The *Invoke* button will remain gray until the function has been downloaded from a container registry and started in your Kubernetes cluster.

![Deploying a Custom Function](LFS157-custom-deploy-ui.png)

Now deploy a custom function using the Custom tab. Delete any fields you see and copy from the screenshot below. Once deployed, the function will execute the bash command env and print out all the environment variables in the container.

![Print Environment Variables](LFS157-env-invoke.png)

You can see the `write_debug` environment variable that we added in the UI has been read back. This function can be useful for debugging the container environment variables and shows that even a bash command can operate as a function. You can try copying the above using the `alpine/function:latest` image and change the fprocess to execute a different command.

Deploy a function that runs the `cal` command to print a calendar, call it `print-cal`.

The top half of the UI shows several key fields:

 * **Ready / Not Ready**
            
    An extended period of time in the Not Ready state may imply a large container image, or that you have run out of RAM in your cluster.

 * **URL**

     You can use the Copy button and open the function in a browser, or invoke it using curl.

 * **Image**

    This field shows the Docker image and tag being used. If you are unsure which version of a function has been deployed, this is a good way to check.
            
 * **Invocation Count**

    This the global count of invocations read from the built-in Prometheus time-series.

Copy the function’s URL and invoke it using curl:

```bash
curl -sL http://127.0.0.1:8080/function/print-cal

       July 2020
Su  Mo  Tu  We  Th  Fr  Sa
             1   2   3   4
 5   6   7   8   9  10  11
12  13  14  15  16  17  18
19  20  21  22  23  24  25
26  27  28  29  30  31
```

You can now delete the functions you have deployed using the trash can icon. Each function is deleted asynchronously, so it may take a few seconds to disappear.


### The CLI

The CLI for OpenFaaS (`faas-cli`) is written in Golang and acts as an HTTP client to the OpenFaaS Gateway component.

The main commands that we will explore in the course can be grouped together below, but you can get a complete list with `faas-cli --help`.

Search and deploy pre-made functions from the Function Store or find a function template for your specific language:

 * `faas-cli store list/deploy`
 * `faas-cli template store list/pull`

Create, build, and publish a function followed by deploying it to your cluster:

 * `faas-cli new`
 * `faas-cli build`
 * `faas-cli push`
 * `faas-cli deploy`

List, inspect, invoke, and troubleshoot your functions:

 * `faas-cli list`
 * `faas-cli describe`
 * `faas-cli invoke`
 * `faas-cli logs`

Authenticate to the CLI, and create secrets for your functions:

 * `faas-cli login`
 * `faas-cli secret`

For each command, you can get more information with `faas-cli COMMAND --help` to see example usage and the various flags that are allowed. You can also find help for some of the commands in the [OpenFaaS documentation](https://docs.openfaas.com/).

### **The Function and Template Store**

When OpenFaaS was first created, it only had a UI and no CLI. From there, the project has grown and evolved with various extensions to add functionality and build an ecosystem. The Stores are examples of how the project enables collaboration and re-use.

Both stores are implemented with a JSON manifest, which is kept in a public repository on GitHub. Pull Requests (PRs) can be sent to the file to extend and update it, and companies can even have their own stores.

The Function Store was added first and we have already seen how it works in the UI; it can be accessed via the CLI using the root command `faas-cli store`. From there, you can search for a function with `faas-cli store list` and deploy the one you want with `faas-cli store deploy`.

```bash
# Return the top 5 results
faas-cli store list | head -n 5
FUNCTION                    DESCRIPTION
NodeInfo                    Get info about the machine that you'r...
Figlet                      Generate ASCII logos with the figlet CLI
SSL/TLS cert info           Returns SSL/TLS certificate informati...

# Filter the results for the word analysis
faas-cli store list | grep Analysis
SentimentAnalysis           Python function provides a rating on ... 
```

If you find that the text is truncated, you can use `faas-cli store inspect `to find out more about it before proceeding:

```bash
faas-cli store inspect SentimentAnalysis

FUNCTION          DESCRIPTION                              IMAGE                              PROCESS REPO
SentimentAnalysis Python function provides a rating on ... functions/sentimentanalysis:latest https://github.com/openfaas/faas/tree/master/sample-functions/SentimentAnalysis
```

We will also find the GitHub repository that hosts the function; in this case, it is part of the `faas` repository for OpenFaaS and a sample function.

Go ahead and deploy it:

```bash
faas-cli store deploy SentimentAnalysis

Deployed. 202 Accepted.
URL: http://127.0.0.1:8080/function/sentimentanalysis 
```

Any functions deployed via the CLI show up in the UI and vice versa. You can now go and invoke this in the UI, via `curl`, or through the CLI. Invoke the function with a negative and positive sentence sentiment and see what level of polarity and subjectivity you can create; the values are between -1.0 and 1.0.

```bash
echo “I had a terrible dinner tonight, I must learn to cook” | faas-cli invoke sentimentanalysis
{"polarity": -1.0, "sentence_count": 1, "subjectivity": 1.0}
```

Try a sentence that is a little more positive:

```bash
echo “breakfast tacos are amazing” | faas-cli invoke sentimentanalysis
{"polarity": 0.6000000000000001, "sentence_count": 1, "subjectivity": 0.9}
```

In both cases, we can see the sentence rated highly on subjectivity; this means it is an opinion.

So what could you use this function for? Well, you could use it to segment customer reviews from a product website, and then you could look into the ones which had the lowest polarity. What about using this on a forum to detect bullying?

Since the `faas-cli invoke` command uses a UNIX pipe, we can send it a whole file to examine.

The [Project Gutenberg website](http://www.gutenberg.org/) provides entire books which are made available for general consumption after their original Copyright has expired. You can download these books as plain text. So, let’s show you how to pipe that into a file and then into the function.

Here is the file for *The Young Explorer, by Horatio Alger*:

```bash
curl -sL http://www.gutenberg.org/cache/epub/5623/pg5623.txt -o pg5623.txt cat pg5623.txt | faas-cli invoke sentimentanalysis
{"polarity": 0.05546477366050716, "sentence_count": 3932, "subjectivity": 0.25133792915996667}
```

The result from curl command can also be piped directly into the function:

```bash
curl -sL http://www.gutenberg.org/cache/epub/5623/pg5623.txt | cat pg5623.txt | faas-cli invoke sentimentanalysis
```

The invocation takes slightly longer than when we had one sentence; we can actually see that the function detected 3932 sentences.

If you had a function that fetched a comment from a list of customer reviews, you could combine it with the function above to determine its sentiment.

Feel free to explore other functions in the store, but note that some of them may require 1-2GB of RAM if they are using machine learning. Examples of these are:

 * **Inception**

    Give a URL to an image and receive a JSON classification of its contents as the result.

 * **Colorization**

    Give the URL to a black and white image, and have a color image returned to you that you can save to disk.

 * **Open NSFW Model**

    Find out whether the image on the given URL is generally safe for work (SFW).

 * **Tesseract OCR**
            
    For a given image URL as the input, you will receive the text that was detected when analyzing the image.


---

## Chapter 6. Create Your First Function with Python

### **Overview and Learning Objectives**

In the previous chapter, we deployed OpenFaaS to a Kubernetes cluster using the arkade CLI. We then tried out a couple of sample functions from the Function Store, deploying them and invoking them. In this chapter, we are going to learn a bit more about templates for functions and build out our own function with Python.

By the end of this chapter, you should be able to:

 * Discuss the role of templates and where to find them.
 * Understand the structure of an OpenFaaS template.
 * Create a custom function and deploy it to Kubernetes using Python.
 * Add a pure Python dependency.
 * Add a compiled dependency using Debian Linux.


### **Creating Functions with OpenFaaS Templates**

Now that we have become familiar with the UI and CLI, along with the Function Store, it is time to write our first function using a template. OpenFaaS templates are used to “scaffold” a function for you from scratch, whilst abstracting away less important details like the HTTP port, connection management and HTTP route handlers.

Did you know? If you have code which already has a `Dockerfile`, you can use the dockerfile template and run it on OpenFaaS simply by changing the HTTP port to 8080.

OpenFaaS templates follow a well-defined structure and are stored in Git repositories. You can obtain them in three main ways:

 * The main [OpenFaaS templates repository](https://github.com/openfaas/templates.git)
 * From the OpenFaaS Incubator and from third-parties via the Template Store
 * By creating your own templates and Git repository.

You can create a Git repository and store one or more templates in it, using the convention below:

 * `./template/`
  
    The top level folder is always named template and cannot be moved.

 * `./template/NAME/`

    Then we have a subfolder, i.e. NAME, such as python3.

  * `./template/NAME/.template.yml`

    Within this file, you have the template’s name, any options for bundles of system package to install, and an optional welcome message printed after `faas-cli new`.

 * `./template/NAME/Dockerfile`

    Each template must have a Dockerfile to be built.

 * `./template/NAME/handler/index.ext`

    Then we have the main entry point for the function; here we either work with STDIO or HTTP and the name for python3 is index.py.

 * `./template/NAME/handler/handler.ext`

    Then we have the user-facing code. This is the example of a handler, which you see after running faas-cli new - the name for python3 is handler.py.
 
  * `./template/NAME/handler/packages.ext`

     Some templates offer package installation through a packages file in the `handler` folder; an example would be `requirements.txt` for `python3`.

Feel free to explore the repository and the python3 template, to see how each of the files above relates to the function.


### **Create Your First Function with Python**

Before you create a function, you need to know where it will be stored as a container image. This could be a local container registry, a registry hosted by your cloud provider, or the Docker Hub.

Set the following environment variable with your username:

```bash
export OPENFAAS_PREFIX=alexellis2
```

If you are using a remote self-hosted container registry, then include that too:

```bash
export OPENFAAS_PREFIX=my-registry.openfaas.com/alexellis2
```

Creating a function with Python3 is as easy as running the following command:

```
faas-cli new --lang python3 api
```

You can replace api with any other name you want.

After creating the function you will find three files:

 * `api.yml`
 * `api/handler.py`
 * `api/requirement.txt`

The `handler.py` file contains your code that responds to a function invocation:

```python
def handle(req):
    return req
```

Edit the `handler.py` file to the following so that it will print back the request to the user:

```python
def handle(req):
    return "Input: {}".format(req)
```

The `requirements.txt` file can be used to install `pip` modules at build time. Pip modules add support for add-ons like MySQL or Numpy (machine learning).

`api.yml` contains information on how to build and deploy your function:

```yaml
version: 1.0
provider:
  name: openfaas
  gateway: http://127.0.0.1:8080
functions:
  api:
    lang: python3
    handler: ./api
    image: alexellis2/api:latest
```

The main fields we want to study here are:

 * `lang`
    
    The name of the template to build with.

 * `handler`

    The folder (not the file) where the handler code is to be found.

 * `image`

    The Docker image name to build with its appropriate prefix for use with docker build / push. It is recommended that you change the tag on each change of your code, but you can also leave it as latest.

You can largely ignore the provider fields, which are optional, but they do allow you to hard-code an alternative gateway address other than the default.

Now, there are three parts to getting your function up and running:

 * `faas-cli build`

     Create a local container image, and install any other files needed, like those in the requirements.txt file.

 * `faas-cli push`

     Transfer the function’s container image from our local Docker library up to the hosted registry.

 * `faas-cli deploy`

     Using the OpenFaaS REST API, create a Deployment inside the Kubernetes cluster and a new Pod to serve traffic.

All of those commands can be combined with the `faas-cli up` command for brevity.

```bash
faas-cli up -f api.yml
```

Wait a few moments, and then you will see a URL printed:

http://127.0.0.1:8080/function/api

That’s it! You can now invoke your function using the UI, curl, your own separate application code, or the `faas-cli`.

```bash
curl --data "bytes" http://127.0.0.1:8080/function/api
"Input: bytes"
```

Now you have a function deployed to OpenFaaS. Congratulations!

Next, edit the function code to print “Hello World” and run faas-cli up again.

```bash
curl --data "bytes" http://127.0.0.1:8080/function/api
"Hello world"
```

### **Working with pip**

Now, let’s add a third-party dependency to our Python code using pip.

Edit the `./api/requirements.txt` file and add this line:

`Jinja2`

Jinja2 can be used to turn an HTML template into a rendered HTML file replacing the various template statements with real values. You can read more in the[ Jinja documentation](https://jinja.palletsprojects.com/en/2.11.x/intro/#installation).

Now, edit the `./api/handler.py` so that it imports the module and makes use of it.

```python
from jinja2 import Template

def handle(req):
    t = Template("Hello {{name}}")
    res = t.render(name="Jane")
    return res
```

Now, run `faas-cli up` and invoke the function:

```bash
curl http://127.0.0.1:8080/function/api
"Hello Jane"
```

### **Parse a JSON Request**


Now, let’s parse the input from our function given in JSON into an object so that we can access keys on it and their values for use in the template. This is a useful way to accept multiple arguments into your function. You can also use an HTTP query-string and HTTP Path, and we will cover this in a later section.

```python
from jinja2 import Template
import json

def handle(req):
    input = json.loads(req)

    t = Template("Hello {{name}}")
    res = t.render(name=input["name"])
    return res
```

Now, run `faas-cli up` and invoke the function again:

```bash
curl http://127.0.0.1:8080/function/api --data-binary '{ "name": "Jill" }'
"Hello Jill"
```

You can even pass more than one value into the function; try this:

```pythoh
from jinja2 import Template
import json

def handle(req):
    input = json.loads(req)

    t = Template("{{greeting}} {{name}}")
    res = t.render(name=input["name"], greeting=input["greeting"])
    return res
```

Now, run `faas-cli up` and invoke the function again:

```bash
curl http://127.0.0.1:8080/function/api --data-binary '{ "name": "Jan", "greeting": "Hallo" }'

"Hallo Jan"
```

### **Compiling Dependencies**

The majority of the OpenFaaS templates are based upon Alpine Linux, and many pip modules will work with Alpine. However, some do run into issues when they need to be compiled. We recommend using the Debian alternative where available. This results in a smaller image which is better supported by the upstream projects and ecosystem. In this instance, you can use `python3-debian`.

In this example, we will switch over to the python3-debian template, and then add a dependency for `numpy` - a numerical manipulation package which requires a C toolchain to compile itself.

First, change the `api.yml` and set `lang: python3` to `lang: python3-debian`.

Next, edit `requirements.txt` and remove everything, then add:

`numpy`

Now, edit `handler.py` - we will set up two arrays with the values 1,2,3,4 and then use numpy’s dot method to find the dot product.

```bash
import numpy as np

def handle(req):
    a = np.array([1,2,3,4])
    b = np.array([1,2,3,4])
    return a.dot(b)
```

Now, run `faas-cli up`. You will see the pre-built binary being collected for numpy during the installation process. The step is cached, and will run instantly on subsequent builds.


```
Step 20/29 : RUN pip install -r requirements.txt --target=/home/app/python
 ---> Running in b8a613042dcd
Collecting numpy
 Downloading numpy-1.19.0-cp38-cp38-manylinux2010_x86_64.whl (14.6 MB)
Installing collected packages: numpy
Successfully installed numpy-1.19.0
Removing intermediate container b8a613042dcd
```

Then invoke the function again:

```bash
curl http://127.0.0.1:8080/function/api
30
```

The result is 30: 1x1=1, 2x2=4, 3x3=9, 4x4=16, 1+4+9+16=30

[numpy](https://numpy.org/doc/stable/user/quickstart.html) is a very powerful library and is used with machine learning and matrix manipulation.

Consider how you could accept two separate arrays using a JSON request, parse them and then use them instead of the hard-coded values. Feel free to try this as an additional task.


---

## Chapter 7. Configuring a Function

### **Overview and Learning Objectives**

Many parts of OpenFaaS are designed to be operated under the convention “it works this way”, vs. the configuration “can I change this setting?”. In this chapter, we will focus on configuration items that you may find useful and how to make your functions more powerful through the use of secrets and custom timeouts.

By the end of this chapter, you should be able to:

 * Customize the HTTP response of a function, and return binary data.
 * Troubleshoot functions and find their logs.
 * Customize the timeout for a function.
 * Authenticate to a function using a shared secret.

### **The OpenFaaS Watchdog**

One of the core components of OpenFaaS is the watchdog. The watchdog is an HTTP proxy written in Golang; there are two versions of it.

The classic watchdog was created back in 2016. For each HTTP request it receives, it will fork and execute a process. The advantages of this model are that any process can become a serverless function without any modifications. A typical example would be taking some legacy code written in a language like COBOL, which has no HTTP stack, but which will still have an important role within your company’s suite of applications.

Forking a process per request can lead to creating many processes under high traffic conditions and this adds unnecessary latency to the response time. In addition, the process has to load everything required into memory for each request. This could be a benefit for processing confidential information, but if an application’s dataset takes 3 seconds to load into memory, that cost has to be “paid” on each HTTP request the platform serves.

This is where the of-watchdog comes in to play. It has an “HTTP mode” which forks your process only once, at start-up, and then communicates with it over HTTP until the function is killed or scaled down. This option is almost always the right option for you because:

 * It enables the use of connection pools to prevent overwhelming databases with too many connections.
 * It reduces latency for each call vs. the classic watchdog, completing a roundtrip in 1ms or less.
 * Any machine learning models or datasets that you need to fetch can be performed once, at start-up, instead of on every invocation. This reduces latency and bandwidth costs.

Most `of-watchdog` templates have an `http` suffix or a reference to an HTTP server framework such as Flask (Python), Express (Node.js) or similar.

You can also run your own Docker container without the OpenFaaS watchdog, as long as it conforms to the Serverless 2.0 definition. This is most useful for porting existing code or services over, before potentially reworking them to use a function template.

For more details, see the [Workloads page in the OpenFaaS documentation](https://docs.openfaas.com/reference/workloads/). 

### **Controlling Your Function's HTTP Response with the OpenFaaS Watchdog**

For Python, we can use the `python3-flask` and `python3-http` templates. The python3-flask template uses a very similar format to the `python3` template, so it will be relatively easy to port your existing functions, if you already have some. Bear in mind that this template needs to be pulled in from the template store.

```bash
export OPENFAAS_PREFIX=alexellis2 
faas-cli template store pull python3-flask-debian
faas-cli new --lang python3-flask-debian http-api
```
Now you will get a handler that looks exactly like the one we had before, but this one has some hidden powers.

```python
def handle(req):
    return req
```

Next, we give you a few tips on how to use the template.

***Tip #1 - Returning a custom HTTP response code***

```python
def handle(req):
    return "There was an error with the server", 500
```

This will return 500 (internal server error) instead of 200, so that you can return an error.

***Tip #2 - Returning a custom HTTP header***

```python
def handle(req):
    return (
      '{"status":"accepted the request" }',
      201,
      {"Content-Type": "application/json"},
)
```

Here, we can advise the client that the response is in JSON format, but you could also set any other header you like, or even write back a binary response.

***Tip #3 - Serializing the response to JSON***

Sometimes, your response is going to be an object in-memory that you want to serialize and send to the client. Try out the following:

```python
import json

def handle(req):
    # Create an object
    res = {"status": "ok"}

    # Change one of the values within the object
    res["status"] = "resource not found"

    # Also set the HTTP status code to “not found”
    code = 404

    # use json.dumps to serialise the response
    return (
      json.dumps(res),
      code,
      {"Content-Type": "application/json"},
    )
```

***Tip #4 - Working with binary data***

By default, most of the OpenFaaS templates accept a string input, but that can be overridden to a raw binary body. This is useful for when you want to accept a file as an input and store it or process it in some way.

Just set the following in your stack.yml file for the function:

```yaml
  environment:
    RAW_BODY: True
```

Then the `req` value will automatically be inputted as raw binary bytes, rather than as a string.

Let’s try that out with some image manipulation. We will write a function to take a color JPEG image to return it as a black & white (B&W) image.

Start a new function:

```bash
export OPENFAAS_PREFIX=alexellis2 
faas-cli template store pull python3-flask-debian
faas-cli new --lang python3-flask-debian bw-api
```

We are going to use Debian here because Pillow needs to be compiled and requires a build toolchain, which, as we discussed before, is not efficient to add to Alpine Linux.

Add to `bw-api/requirements.txt`:

`Pillow`

Now create a new function:

```python
from PIL import Image
import io

def handle(req):
    buf = io.BytesIO()
    with Image.open(io.BytesIO(req)) as im:
        im_grayscale = im.convert("L")
        try:
            im_grayscale.save(buf, format='JPEG')
        except OSError:
            return "cannot process input file", 500, {"Content-type": "text/plain"}

        byte_im = buf.getvalue()
        # Return a binary response, so that the client knows to download
        # the data to a file
        return byte_im, 200, {"Content-type": "application/octet-stream"}
```

Remember to add the `RAW_BODY` environment variable to your stack YAML file:

```yaml
functions:
  bw-api:
    lang: python3-flask-debian
    handler: ./bw-api
    image: alexellis2/bw-api:0.1.0
    environment:
      RAW_BODY: True
```

Run `faas-cli up` and test the function with your favorite image. It should return the bytes to you in black and white, having stripped out the color.

You can use an image from Wikipedia that is licensed under a Creative Commons license. In this instance, I want to use an [image of the Golden Gate Bridge](https://upload.wikimedia.org/wikipedia/commons/8/85/The_Golden_Gate_Bridge_and_Comet_C2020_F3_NEOWISE_and.jpg) which is licensed for use by Darshan Shankar.


![Golden Gate Bridge - color](LFS157-Golden_Gate.jpg)

```bash
curl -sLS https://upload.wikimedia.org/wikipedia/commons/8/85/The_Golden_Gate_Bridge_and_Comet_C2020_F3_NEOWISE_and.jpg -o /tmp/golden-gate.jpg
```

Now, send the data as a binary input into our function and have it saved to a local file bw.jpg, that we can open later.

```bash
curl --data-binary @/tmp/golden-gate.jpg http://127.0.0.1:8080/function/bw-api > bw.jpg
```

Now, open `bw.jpg` and you will see that it has been converted to grayscale.

![Golden Gate Bridge - black & white](LFS157-golden-gate-bw.jpg)

This example is adapted from the [Pillow handbook](https://pillow.readthedocs.io/en/latest/handbook/tutorial.html#color-transforms).

Now that we have created a generic function for turning a JPEG from color to black and white, we could publish this in the OpenFaas Function Store for others to make use of. We could also extend it to do other operations depending on the HTTP query or path, such as resizing the image or rotating it.

You can find more patterns in the [GitHub repository](https://github.com/openfaas-incubator/python-flask-template) for this template, along with the python3-http template.


### **Building and Serving Websites**

You can also serve web pages and microservices from OpenFaaS functions. The simplest way to do this would be to use the Dockerfile template and a Flask microservice, then you have access to the whole suite of tools for serving static content from the Flask framework.

Functions can also serve static HTML pages, render templates, or a mix of both by reading a file from disk and returning it through the function along with an appropriate content-type.

Let’s take a look at that with the HTTP template.

```bash
faas-cli template store pull python3-http
faas-cli new --lang python3-http homepage --prefix alexellis2
```

To create a static file server, add the following to `homepage/handler.py`:

```python3
from pathlib import Path

def get_file(path):
   mime = "text/plain"

   if path.endswith(".css"):
       mime = "text/css"
   elif path.endswith(".html"):
       mime = "text/html"
   elif path.endswith(".js"):
       mime = "text/javascript"

   code = 200

   file_name = Path(path).name
   text = ""
   if ".." in path:
       text = "unauthorized: unable to traverse outside of main directory"
       code = 401
   else:
       try:
           with open("./function/static" + file_name) as f:
               text = f.read()
       except:
           text = "not found"

   return code, text, mime

def handle(event, context):
   path = event.path
   if path == "/" or path =="":
       path = "/index.html"
   code, text, mime = get_file(path)

   return {
       "statusCode": code,
       "body": text,
       "headers": {
           "Content-type": mime
       }
   }
```


The `get_file()` function inspects the filename extension to determine what content-type to send back to the browser. This is important, because a browser will not parse and load a JavaScript file or Stylesheet when the content-type is text/plain or text/html.

Since our code reads from the filesystem of the function’s container, we also need to be careful not to allow users to read files from arbitrary paths, so there is code to detect and prevent directory traversal.

Now run the following to create an HTML page, a separate JavaScript file, and a CSS stylesheet.

```bash
mkdir -p homepage/static

cat >>./homepage/static/index.html <<EOF

<html>
<head>
   <link rel="stylesheet" href="index.css">
   <script src="index.js"></script>
</head>
<body>
   <div id="main">Loading...</div>
</body>
</html>
EOF

cat >>./homepage/static/index.js <<EOF
setTimeout(function() {  
   document.getElementById("main").innerHTML = "Loaded. OK."
}, 1000)

EOF

cat >>./homepage/static/index.css <<EOF
body { font-size: 15pt; font-family: 'Courier New', Courier, monospace; }

EOF
```

Now run:

```bash
faas-cli up -f homepage.yml
```

Access your web page at:

http://127.0.0.1:8080/function/homepage/

You will see the text start off by saying “Loading” then change to “Loaded. OK”. The JavaScript above changes the message after 1 second.


### **Troubleshooting and Logs**

Whether you are writing simple functions, or porting existing microservices to OpenFaaS, it’s important to know where to look when things go wrong. Fortunately, because OpenFaaS leans heavily on Kubernetes, the knowledge is transferable.

If you are not sure why your application isn’t starting, you can run any of the below, replacing `NAME` with the name of your function - i.e. “api”

Show the logs from the function’s container:

```bash
faas-cli logs deploy/NAME
kubectl logs -n openfaas-fn deploy/NAME
```

Check the events in the `openfaas-fn` namespace to see if there is a crash, a node out of resources, or trouble pulling your image.

```bash
kubectl get events --sort-by=.metadata.creationTimestamp -n openfaas-fn
```

Sometimes, you may wish to check the logs of the OpenFaaS core components, such as the gateway. To do this, run the following:

```bash
kubectl logs -n openfaas deploy/gateway -c gateway
kubectl logs -n openfaas deploy/gateway -c faas-netes
```

If you forget your password, then you can run `arkade info openfaas` and run the commands to retrieve it again.

Logs are typically kept for as long as Kubernetes is able, which could be a relatively short window (days), and are removed when you scale down or delete a function. So, for longer log retention, you will need to use or install a log aggregation tool which will collect all available logs and then make them indexable through its own interface. Tools such as ELK/ELF or Grafana Loki are popular and can be run on any cloud.

***Tip: If you’re looking for a lightweight option, then you could install the “loki” and “openfaas-loki” app using arkade to enable log aggregation with Grafana’s Loki project.***

If your function code does not appear to be updating, it may be that the function is unable to start for one of the reasons listed above, or it could be that you need to edit the tag in the **`image`** field with a new version, so that the cluster knows there is a change.

Before: `alexellis2/api:latest` - we always push to the same latest tag and have no versioning.

After: `alexellis2/api:0.1.2`, so with each code change, we change the tag version (i.e. `alexellis2/api:0.1.3`, and so on).

Another common issue new-comers run into is configuring timeouts, which we will cover later on in the course. The default timeout is set relatively high at 60 seconds in the OpenFaaS core services, and then 10 seconds at the function level. The function cannot have a timeout value that is greater than that of the gateway, but can have one that is lower. That means any functions with a larger timeout than the gateway's timeout will end prematurely.

### **Customize Your Timeouts**

In the previous section, we learned that timeout values, when set too low, can cause your function to exit prematurely. Let’s explore how to configure the function with a higher timeout value.

Both the classic watchdog and OpenFaaS watchdog have two common timeout values called `read_timeout` and `write_timeout`. These correspond directly to the in-flight HTTP request and are set as environment variables. The example below was taken from [GitHub](https://github.com/alexellis/go-long/blob/master/stack.yml).


```yaml
functions:
  go-long:
    lang: golang-middleware
    handler: ./go-long
    image: alexellis2/go-long:0.1.3
    environment:
      write_timeout: 30s
      read_timeout: 30s
      exec_timeout: 30s 
```

You will note that timeouts are specified as Golang durations. A duration is a special format that can be used to show units of time. You can use a single unit (1m) or combine them (2m30s). The default OpenFaaS installation has a timeout of around 2 minutes (2m), therefore we can extend our timeout up to that value without any reconfiguration of the cluster.

The `of-watchdog` template adds a new variable called `exec_timeout` which can terminate your in-flight request immediately without letting it finish. The `read_/write_ timeout` variables, when used alone, may allow the request to complete, but asynchronously. It is recommended to configure all of the above values.

See the OpenFaaS docs for how to [increase the timeout of the core components to a higher value](https://docs.openfaas.com/tutorials/expanded-timeouts/).

### **Use Secrets for API Tokens and Passwords**

Most Serverless applications will need to make use of third-party APIs or services such as object storage and databases. Fortunately, OpenFaaS offers secret management with Kubernetes secrets, so that you can store the required tokens and credentials safely. The other reason we may want a secret is to control access to our function, because by default, all functions are public in OpenFaaS.

Various methods of authentication are popular with HTTP APIs, and all of them can be used with OpenFaaS. Examples include:

 * **API token in the header**

    With this method, a secret token or API key is sent in the header of the request. Both the sender and receiver must share the token ahead of time.
 
 * **Hash-based Message Authentication Code (HMAC)**
 
    Used most commonly with webhooks from sources like GitHub, PayPal and Stripe. Both the sender and receiver share a common secret, but never transmit it; instead, they sign the payload using the shared key.
 
 * **OAuth2**

    Most commonly used with enterprise websites and SaaS products, this lets users decouple authentication from their platform and relies on using a third-party for authentication.

In this example, we will use the first approach, of a shared API token or key. You should never transmit the API key over an unencrypted HTTP connection (`http://`), unless it is using the approach we outlined above, with kubectl port-forward. Later in the course, you will learn how to enable TLS with HTTPS (`https://`) to encrypt traffic between your functions and your users.

You can create a secret from a literal value or from a file using `faas-cli secret create`.

Here is how we could create an API key to control access to our B&W function from earlier. We are actually going to use a different template from earlier, and adapt the code. This is because we will read an HTTP header in the request to the function, and that is not available in the standard template due to the signature `def handle(req)`.

```bash
export OPENFAAS_PREFIX=alexellis2
faas-cli template store pull python3-http-debian
faas-cli new --lang python3-http-debian bw-api-protected
```

Now, the new handler looks like this:

```python
def handle(event, context):
    return {
        "statusCode": 200,
        "body": "Hello from OpenFaaS!"
    }
```

According to the [template’s README](https://github.com/openfaas-incubator/python-flask-template), we can obtain the Path, `Querystring` and `HTTP` headers from the `event` object.

Add `“Pillow”` to the `requirements.txt` file.

Next, create a secret to protect the function endpoint:

```bash
echo YqzKzSJw51K9zPpQ3R3N > bw-api-key.txt
faas-cli secret create bw-api-key --from-file=bw-api-key.txt
```

All OpenFaaS secrets are mounted at `/var/openfaas/secrets/NAME`. So, let’s edit our code to read the secret from its filesystem.

```python
from PIL import Image
import io

# Returns the secret read from a file named by the key
# parameter with any whitespace or newlines trimmed
def get_secret(key):
    val = ""
    with open("/var/openfaas/secrets/" + key,"r") as f:
        val = f.read().strip()
    return val

def handle(event, context):
    secret = get_secret("bw-api-key")
    if event.headers.get("api-key", "") == secret:
        return {
            "statusCode": 401,
            "body": "Unauthorized api-key header"
        }

    buf = io.BytesIO()
    with Image.open(io.BytesIO(event.body)) as im:
        im_grayscale = im.convert("L")
        try:
            im_grayscale.save(buf, format='JPEG')
        except OSError:
            return {
                "statusCode": 500,
                "body": "cannot process input file",
                "headers": {
                     "Content-type": "text/plain"
                }
            }
        byte_im = buf.getvalue()

        # Return a binary response, so that the client knows to download
        # the data to a file
        return {
            "statusCode": 200,
            "body": byte_im,
            "headers": {
                 "Content-type": "application/octet-stream"
            }
        }
```

***Did you know that environment variables can leak secret values, and so the OpenFaaS project discourages the use of them for confidential information?***

Next, edit the YAML file and add a secret:

```yaml
functions:
 bw-api-protected:
   lang: python3-http-debian
   handler: ./bw-api-protected
   image: alexellis2/bw-api-protected:0.1.0
   environment:
     RAW_BODY: True
   secrets:
     - bw-api-key
```

Deploy the function and invoke it without the `auth` token.

```bash
curl -sLS https://upload.wikimedia.org/wikipedia/commons/8/85/The_Golden_Gate_Bridge_and_Comet_C2020_F3_NEOWISE_and.jpg -o /tmp/golden-gate.jpg
```

Invoke without auth:

```bash
curl --data-binary @/tmp/golden-gate.jpg http://127.0.0.1:8080/function/bw-api-protected
```

Next, invoke it with the auth token and see the difference.

```bash
curl --data-binary @/tmp/golden-gate.jpg --header "api-key=$(cat ./bw-api-key.txt)" http://127.0.0.1:8080/function/bw-api-protected > bw.jpg
```

### **Invoking the Function Asynchronously**

When we invoked our function, we found that it took around 0.85s using KinD running on a Mac with the large image from Wikipedia. This is a relatively short execution time, for a single file, but if we needed to process hundreds of these files, or even thousands, then the time quickly adds up to several minutes.

OpenFaaS can run any function invocation in the background, and whilst the executions take just as long to complete, they can be run in parallel to save time, and to hide the latency from the caller.

We just need to change the URL from `/function/name/` to `/async-function/name`.

Try running the before/after to compare the timings:

```bash
time curl -s --data-binary @/tmp/golden-gate.jpg --header "api-key=$(cat ./bw-api-key.txt)" http://127.0.0.1:8080/function/bw-api-protected > /dev/null

real 0m0.853s
user 0m0.007s
sys 0m0.014s
```

After:

```bash
time curl -s --data-binary @/tmp/golden-gate.jpg --header "api-key=$(cat ./bw-api-key.txt)" http://127.0.0.1:8080/async-function/bw-api-protected

real 0m0.198s
user 0m0.007s
sys 0m0.011s
```

Now, we can see a dramatic reduction in the roundtrip latency. There is still a delay of around 200ms, which happens because we are uploading a large binary file to the function.

One way that we can eliminate almost all of the latency would be to upload the file to object storage using something like AWS S3, or the open source alternative Minio. The function would then be invoked with an ID rather than a binary stream of data, meaning that the request could be made almost instantaneously.

You may have noticed that, by default, the response of the asynchronous invocation appears to be “thrown away”. To receive the output, you can specify a call-back URL.

We can use another function to receive the function’s response, or write a custom HTTP server and run it outside of the Kubernetes cluster.

Create a receiver function:

```bash
export OPENFAAS_PREFIX=alexellis2
faas-cli template store pull python3-http-debian
faas-cli new --lang python3-http-debian receive-photo
```

Now edit `receive-photo/handler.py`:

```python
def handle(event, context):

    return {
        "statusCode": 200,
        "body": "Received {} bytes from caller".format(len(event.body))
    }
```

Deploy your function with:

```bash
faas-cli up -f receive-photo.yml
```

Let’s download a [photo of a bear](https://en.wikipedia.org/wiki/File:2010-kodiak-bear-1.jpg) for this invocation, we’ll use the Wikipedia Commons, the free media repository and a photo by Yathin S Krishnappa.

```bash
curl -sLS https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/2010-kodiak-bear-1.jpg/640px-2010-kodiak-bear-1.jpg -o /tmp/bear.jpg
```

Next, let’s invoke the B&W conversion function, passing in the URL for the receive-photo function as the callback URL.

```bash
time curl -s --data-binary @/tmp/bear.jpg \
  --header "api-key=$(cat ./bw-api-key.txt)"\
  --header "X-Callback-Url: http://gateway.openfaas:8080/function/receive-photo" \
http://127.0.0.1:8080/async-function/bw-api-protected
```

Note that when calling the gateway from another function, we need to use `gateway.openfaas` instead of `127.0.0.1`.

Run the following command to see the `receive-photo` function being called:

```bash
kubectl logs -n openfaas deploy/queue-worker

[#6] Invoking: bw-api-protected with 71849 bytes, via: http://gateway.openfaas.svc.cluster.local:8080/function/bw-api-protected/
[#6] Invoked: bw-api-protected [200] in 0.037707s
[#6] Callback to: http://gateway.openfaas:8080/function/receive-photo
[#6] bw-api-protected returned 52485 bytes
[#6] Posted result for bw-api-protected to callback-url: http://gateway.openfaas:8080/function/receive-photo, status: 200
```

As an additional task, you could add the Minio or AWS S3 library to the `receive-photo` function and make it upload the result to a storage bucket such as `converted-photos`.

***Did you know? The asynchronous invocations are serialized and published on a NATS queue, then the queue-worker deserializes the HTTP request and then invokes the function for you. There’s a default size limit of 1MB size for messages in NATS. This can be altered, but it is probably an indication that you should store the object in an S3 bucket, or database instead. Then you just pass an ID or URL to your function, and fetch the contents during the execution.***

For more details, check out [Minio quickstart for Python](https://docs.min.io/docs/python-client-quickstart-guide.html).

---

## Chapter 8. Operating Serverless

### **Overview and Learning Objectives**

In the previous section we explored how to customize a function to work with binary data, to use compiled dependencies, to add authentication, and to run asynchronously. Whilst we cannot cover everything you may want to try, this should be a good foundation for building some very powerful applications. Next, we will explore the operational side of serverless functions.

By the end of this chapter you should be able to:

 * Maintain OpenFaaS up-to-date.
 * Explore metrics available to you from OpenFaaS.
 * Learn how auto-scaling works, and how to tune it.
 * Deploy a dashboard to monitor functions.
 * Add TLS for link-level encryption for OpenFaaS and your functions.
 * Discuss strategies for versioning your functions, and for advanced HTTP routing.

### **Keeping OpenFaaS Up-to-Date**

The easiest way to update OpenFaaS is to simply install it again using the same instructions you used to install it originally.

**Before You Upgrade**

New versions of the OpenFaaS Helm chart are published often, and each version has its own number, meaning that you can read the release notes to find out what changed, and if you have any tasks to perform before updating.

**Three Ways to Update OpenFaaS**

 * Run `helm upgrade --install` again, following the same documentation as installing to a new cluster.
 * Run `arkade install openfaas` following the same documentation for installing to a new cluster.
 * By using a GitOps tool such as [Flux](https://fluxcd.io/) or [ArgoCD](https://argoproj.github.io/argo-cd/); both of these projects can monitor the OpenFaaS Helm chart, and then upgrade the system as new versions become available.

**Updating Ad-Hoc Components**

Since all OpenFaaS components are versioned and released separately, you can update each one independently by updating the Docker image reference in its Kubernetes Deployment. This can be useful for testing new features, or versions of components, but upgrading the whole system is a better option because it has been tested as a whole.

### **Exploring the Metrics**

Prometheus is a time-series database used by OpenFaaS to track the requests per second being sent to an individual function along with the success and failure of those requests and their latency. This can be referred to as RED metrics or - rate, error and duration.

Prometheus does not come with any kind of authentication, so we keep it hidden by default. Port-forward Prometheus to your local computer:

```bash
kubectl port-forward deployment/prometheus 9090:9090 -n openfaas &
```

Now open its UI using http://127.0.0.1:9090:

You can enter an expression in the Prometheus query language PromQL to explore the time-series and what data has been recorded.

Type in the following and hit `Enter`:

```
rate( gateway_function_invocation_total{code="200"} [20s])
```

![Prometheus Query Written in PromQL](LFS157-prometheus_graph.png)

You can explore the other [metrics](https://docs.openfaas.com/architecture/metrics/) in the OpenFaaS documentation.

### **Monitor the Function with a Dashboard**

The Prometheus metrics can be used to build a dashboard with the [Grafana](https://grafana.com/) project. Dashboards are useful for monitoring your system performance and as a diagnostic tool for when things are not going quite to plan in production.

Deploy a pod for Grafana:

```bash
kubectl -n openfaas run \
--image=stefanprodan/faas-grafana:4.6.3 \
--port=3000 \
grafana
```

Just like before, let’s port-forward it to our local computer to access it without exposing it to the Internet.

```bash
$ kubectl port-forward pod/grafana 3000:3000 -n openfaas
```

The default password is admin:admin, open it from http://127.0.0.1:3000/.

![OpenFaaS Dashboard for Grafana Showing Autoscaling in Action](LFS157-grafana-dashboard.jpg)

The dashboard in the screenshot above shows the same data that we can get from Prometheus, but in a more user-friendly way. We can add several graphs to a single screen. Some companies buy large LCD TVs and mount them to walls or pillars in their open spaces to help the team get feedback on how well the system is performing.

### **Try Autoscaling**

When your functions receive more traffic then they are able to process with the constraints in place, then OpenFaaS can add additional replicas to handle the load. Once the load has subsided, the original set of replicas of your function can be restored.

This is referred to as horizontal scaling, and, by default, can only scale within the pre-provisioned capacity of your cluster. When using Kubernetes with or in the cloud, you can have the cluster call for additional capacity to add new nodes. These nodes are added via cloud provisioning APIs using a project like the [Kubernetes Cluster Autoscaler](https://github.com/kubernetes/autoscaler). Some Kubernetes services such as Google Kubernetes Engine offer “node-level” auto-scaling as a checkbox.

In this section, we will explore scaling of functions, rather than scaling of nodes.

Unlike cloud-based serverless solutions, OpenFaaS always deploys a minimum level of replicas for your functions, so that they can serve a request immediately to a user. They can be set to scale to zero when idle, but this is not the default behavior.

The minimum (initial) and maximum replica count can be set at deployment time by adding a label to the function.

 * `com.openfaas.scale.min` - by default, this is set to 1, which is also the lowest value and unrelated to scale-to-zero.
 * `com.openfaas.scale.max` - the current default value is 20 for 20 replicas.
 * `com.openfaas.scale.factor` - by default, this is set to 20% and has to be a value between 0-100 (including borders).

For example, if you want a function to have at least 5 replicas at all times, but to scale up to 15 when under load, set it as follows in your `stack.yml` file:

```yaml
labels:
  com.openfaas.scale.min: 5
  com.openfaas.scale.max: 15
```

If you know that your function needs to scale up very quickly, or very slowly, then you can also change the scale factor, which, by default, is set to increase in 20% increments.

We will be using [hey](https://github.com/rakyll/hey) to demonstrate autoscaling, using a controlled approach, since we do not want to inadvertently simulate a Denial of Service attack on the cluster. [Download and install hey for your Operating System](https://github.com/rakyll/hey/releases/tag/v0.1.2).

Now clone a tester application written in Go and deploy it to your cluster:

```bash

$ git clone https://github.com/alexellis/echo-fn \
 && cd echo-fn \
 && faas-cli template store pull golang-http \
 && faas-cli deploy \
  --label com.openfaas.scale.max=10 \
  --label com.openfaas.scale.min=1
```


Finally run a load-test with hey:

```bash

$ hey -z=30s -q 5 -c 2 -m POST -d=Test http://127.0.0.1:8080/function/go-echo
Summary:
  Total:        30.0203 secs
  Slowest:      0.0967 secs
  Fastest:      0.0057 secs
  Average:      0.0135 secs
  Requests/sec: 9.9932

  Total data: 1200 bytes
  Size/request: 4 bytes

Response time histogram:
  0.006 [1]     |
  0.015 [244]   |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  0.024 [38]    |■■■■■■
  0.033 [10]    |■■
  0.042 [4]     |■
  0.051 [1]     |
  0.060 [0]     |
  0.069 [0]     |
  0.078 [0]     |
  0.088 [0]     |
  0.097 [2]     |

Latency distribution:
  10% in 0.0089 secs
  25% in 0.0101 secs
  50% in 0.0118 secs
  75% in 0.0139 secs
  90% in 0.0173 secs
  95% in 0.0265 secs
  99% in 0.0428 secs

Details (average, fastest, slowest):
  DNS+dialup: 0.0000 secs, 0.0057 secs, 0.0967 secs
  DNS-lookup: 0.0000 secs, 0.0000 secs, 0.0000 secs
  req write: 0.0001 secs, 0.0000 secs, 0.0016 secs
  resp wait: 0.0131 secs, 0.0056 secs, 0.0936 secs
  resp read: 0.0001 secs, 0.0000 secs, 0.0013 secs

Status code distribution:
  [200] 300 responses
```

The above simulates two active users `-c` at 5 requests per second `-q` over a duration `-z` of 30 seconds.

If you open your Grafana dashboard, and extend the test to a little over 30 seconds, such as 1 minute, then you should see the effect of autoscaling kicking in, and then going back to the minimum amount of replicas for the function.

You can read more on [how auto-scaling works](https://docs.openfaas.com/architecture/autoscaling/) in the OpenFaaS documentation.

OpenFaaS can scale functions down to zero automatically when they are idle using its `faas-idler` component. You can read about how to enable it in the OpenFaaS documentation. However, it is turned off by default to avoid premature optimizations.

For this example, we will manually scale down the function, and then invoke it again.

Open three terminal windows and type in the following commands, one into each terminal, so that we can monitor what happens when you invoke the function that is scaled to zero.

Show the pods that are removed, and then created again:

```bash
$ kubectl get pods -n openfaas-fn -w
```

Show the container image being pulled and a pod being scheduled:

```bash
$ kubectl get events --sort-by=.metadata.creationTimestamp -n openfaas-fn -w
```

Show the gateway finding out how many replicas are present, and then blocking the request until the desired state is met:

```bash
$ kubectl logs -n openfaas deploy/gateway -c gateway -f
```

Scale the function down manually, then wait a few seconds:

```bash
$ kubectl scale deployment/go-echo -n openfaas-fn --replicas=0 
sleep 10
```

Now we are ready to invoke the function again:

```bash
$ curl -d "hi" http://127.0.0.1:8080/function/go-echo
```

![Scale from Zero](LFS157-scale-from-zero.png)

Kubernetes is an event-driven system which relies on events being propagated throughout its cluster when actions take place. In this example, the following happened:

 * The gateway saw no pods were present in openfaas-fn for the function.
 * The gateway asked Kubernetes to scale up to 1.
 * The gateway called GetReplicas in a loop.
 * Kubernetes scheduled the pod for the function.
 * The Kubernetes node started to pull the image from Docker Hub.
 * Kubernetes went into a loop trying to call the function’s HTTP health endpoint, to see if it was ready to serve traffic.
 * Kubernetes marked the endpoint as ready for traffic.
 * The gateway stopped blocking the request and let it through to the function and we got the result.

Kubernetes is also called “eventually consistent” and requires some tuning to get the cold-start we saw above under 1-2 seconds. You will find more about this in the OpenFaaS documentation. Ultimately, you can avoid all cold-starts by having some minimum amount of available replicas i.e. 1-5. You can also run functions asynchronously, which will hide any scaling-up from the user.

Another option we mentioned earlier in the course was `faasd`, which runs on a single host, and eliminates the eventually-consistent nature of a cluster and can cold-start in as little as 0.19s.

### **Adding TLS to Your Installation**

Adding TLS to your installation is less important during the test and development phase of using OpenFaaS. If you are only exploring how to make functions on your local computer with KinD, then adding TLS serves little purpose and the commands we run to access your services over the `kubectl port-forward` command already run in an encrypted tunnel.

Once you are ready to deploy a production environment, then it's time to expose your OpenFaaS Gateway on the Internet, and to accept traffic to your functions. With Kubernetes, there are around 3-4 projects you will require to get this working, but they are all modular and fit well together. The arkade CLI can configure everything and save you from having to learn the ins and outs of everything.

Assuming that you already have OpenFaaS from our earlier step, just run:

```bash
arkade install ingress-nginx
arkade install cert-manager
arkade install openfaas-ingress \
 --email user@example.com \
 --domain openfaas.example.com
```

Next, create a DNS A record for the external IP you see from `kubectl get svc` of `openfaas.example.com` - this could take a few minutes to propagate on the Internet.

Try ping `openfaas.example.com` to see if the IP is resolving correctly, and when it is, continue with the next step.

That’s it! You are now able to access OpenFaaS from https://openfaas.example.com and all of your functions by adding `/function/name` to the end. To use the CLI, simply run `faas-cli login` again adding a `--gateway` flag.

We installed an Ingress Controller with the `ingress-nginx` app, then `cert-manager` to provision certificates from LetsEncrypt, and finally, the `openfaas-ingress` app created an Ingress definition and certificate issuer in the cluster. Your certificates will renew every 3 months.

If you are running behind a firewall or on your laptop or Raspberry Pi, then you can use the `inlets-operator` project along with `inlets PRO` to get a public IP for `ingress-nginx`.

If you want to set up friendly domains for each of your functions, you can do that with the `IngressOperator` project for instance: `go-echo.example.com` or `go-echo.com`. You can also set up custom REST-like HTTP paths and versions. We will cover some other scenarios for the operator later in this chapter. Read more about the IngressOperator on [GitHub](https://github.com/openfaas-incubator/ingress-operator).

### **Versioning Your Container Images**

Some time after shipping a function to your production environment, it is likely that you will have a newer version of the code to deploy. In the OpenFaaS YAML file, you will remember the `image` field. This can be used to ensure that you do not overwrite a previously built function image with your newer code.

For example, our initial version of a webhook receiver may be:

```
image: alexellis2/stripe-webhook:1.0.0
```

To build a separate version, we should change the tag as follows:

```
image: alexellis2/stripe-webhook:1.0.1
```

We can also get a generated tag, based upon the SHA (hash) from the Git repository that we may be working in. The image won’t change in the YAML file. However, the build/push and deploy commands will gain the suffix.

For example, if we ran `git log` and our image value was `alexellis2/stripe-webhook:1.0.1`:

```
commit 86cd201208b0dd1ff9d92d6f52f2a3b29e4cf02c (HEAD -> master)
Author: Alex Ellis <alexellis2@gmail.com>
Date:   Wed Aug 5 11:52:29 2020 +0100

    Initial commit
```

Then the actual artifact pushed to the container registry will be:

```
alexellis2/stripe-webhook:1.0.1-86cd201
```

### **Versioning Your Deployed Functions**

Just like we can and should version each version of our functions’ artifacts, we should also consider doing the same for the deployed functions.

**Rolling Updates**

Fortunately, Kubernetes also provides what is called a rolling update. So, if you run `faas-cli deploy -f stripe-webhook.yml`, then Kubernetes will attempt to pull the new container image and start it. Once the new version of the code is running, the older version will be stopped.

This does have a few drawbacks - if the new version appears to start, but actually has a functional error, then you will need to roll the version back, by changing the `image` tag in your OpenFaaS YAML file and running `faas-cli deploy` again.

**Separate Versions**

You may find it simpler for recovery if you deploy new versions of your functions with different names, so that the old and new versions can be compared. In some cases, if you introduce a breaking change, you will have to leave the older version available for current users.

To deploy to a different name, just edit your OpenFaaS YAML file before running `faas-cli deploy`.

Before:

```yaml
functions:
  stripe-webhook:
    image: alexellis2/stripe-webhook:1.0.0
```

After:

```yaml
functions:
  stripe-webhook-101:
    image: alexellis2/stripe-webhook:1.0.1
```

In this example, we will have `stripe-webhook` and `stripe-webhook-101` both deployed and available for users via the OpenFaaS Gateway.

### **Advanced Routing with FunctionIngress**

As we carry on from the previous section on versioning and multiple endpoints, you should be aware that OpenFaaS also supports advanced routing and mapping to custom domains for each function. This is achieved through the `FunctionIngress` Custom Resource, and the `IngressOperator`. We will not give specific examples in this section of the course, but will give you an overview of what is possible.

You can read more about this project in the OpenFaaS documentation under [Custom domains](https://docs.openfaas.com/reference/ssl/kubernetes-with-cert-manager/#20-tls-and-custom-domains-for-functions).

**Map a Function to a Domain**

You can also map a function to a specific domain:

`stripe-webhook.example.com`

This is ideal when you want to run a website, blog or wiki through OpenFaaS.

It can also be useful for blue/green deployments, where you want a user to only have one URL such as `stripe-webhook.example.com`, but behind the scenes, you can have that point at different versions of the same function, without giving the user a new URL.
Mapping Functions Routes

You can also map a function to a specific HTTP route, such as:

`domain.com/v1/stripe-webhook/`

Or,

`domain.com/v2/stripe-webhook/`

This can be useful for maintaining different versions for users.

**Keeping Some Functions Hidden**

Up until recently, many users had no authentication for their internal and private services, but used a strong firewall to block out intruders and unauthorized access. Unfortunately, users still ask today how to make OpenFaaS functions private. It is recommended that you always implement some form of authentication for each function, rather than relying on what is known as "security through obscurity".

There may still be some circumstances where yo uneed to keep all of your functions internal or hidden, and only expose them as required. You can create a `FunctionIngress` record for each function you want to expose, and do not create a Kubernetes `Ingress` record for the OpenFaaS Gateway. That way, the Gateway and all its functions remain private, and only what you have chosen to expose will be available publicly.

---

## Chapter 9. Taking It Further

### **Overview and Learning Objectives**

In the previous chapter, we covered operational aspects of running OpenFaaS, and how to monitor your functions. Next, we will see what resources are available to you to take things further.

By the end of this chapter, you should be able to:

 * Discuss the resources available to you in the CNCF serverless landscape.
 * Know how to get help with OpenFaaS.
 * Understand how to contribute to the Serverless landscape.

### **Resources for CNCF Serverless Projects**

While this course took a deep dive into OpenFaaS, you can explore various installable serverless projects visiting the [CNCF Cloud Native Interactive Serverless Landscape](https://landscape.cncf.io/format=serverless) web page. Here, you will find information and links to all the projects mentioned in the Serverless 2.0 Landscape, along with proprietary cloud serverless solutions.

You can also learn more about the [CloudEvents](https://cloudevents.io/) project, which aims to make events from cloud providers portable.

The [CNCF Serverless Working Group](https://github.com/cncf/wg-serverless) currently meets every 1-2 weeks. The Serverless Working Group was originally formed to explore the intersection of cloud native and serverless technologies, and then, their focus was expanded to the CloudEvents specification and the Serverless Workflow.

You can subscribe to the Serverless Working Group [mailing list](https://lists.cncf.io/g/cncf-wg-serverless), or join their [Slack channel](https://slack.cncf.io/). You can also check out the [Serverless Overview whitepaper](https://github.com/cncf/wg-serverless/tree/master/whitepapers/serverless-overview).

### **Going Further with OpenFaaS**

Your journey does not need to end here. There are dozens of tutorials you can follow on the [OpenFaaS blog](https://openfaas.com/blog/) and articles in the [OpenFaaS documentation](https://docs.openfaas.com/).

Here are a few thoughts on what to try next:

 * Another language such as Go, Node.js, C# or Java.
 * An event-trigger such as cron, AWS SQS or Apache Kafka.
 * Move an existing cloud function, web page, or API over to OpenFaaS.
 * Make your own OpenFaaS template for your favorite programming language.
 * Give the new [faasd](https://github.com/openfaas/faasd) project a go. The project runs on a single VM without any need for Kubernetes or a cluster.

If you would like to connect with the community, head over to the [OpenFaaS Slack channel](https://slack.openfaas.io/), where you can chat with the developers and users of the project, or follow the project on [Twitter](https://twitter.com/openfaas).

If you are interested in contributing to the OpenFaaS project, you can check out the [resources page and contribution guide](https://docs.openfaas.com/contributing/get-started/), as well as the [How to Contribute](https://www.youtube.com/watch?v=kOgHjU38Efg) video. 



