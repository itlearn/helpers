
# Introduction to GitOps (LFS169)

 * [Course Introduction](#course-introduction)
 * [GitOps in Action](#gitops-in-action)
 * [GitOps Concepts](#gitops-concepts)
 * [Course Completion](#course-completion)


## Course Introduction

### **Course Information**

#### == **Course Introduction** ==

GitOps refers to an emerging set of principles, practices and tools which help you build an operational framework for cloud native applications primarily running on Kubernetes. It offers not only a developer-centric approach to deploy applications, but also brings in the familiar and effective workflows built around Git to the operations fold, such as rolling out with pull requests and rollbacks with git reverts.

GitOps provides a simple, fast, yet secure way to run operational activities on platforms such as Kubernetes, including continuous delivery, rolling out infrastructure components and policies, and a quick remediation in case of failure.

This course builds a conceptual foundation on GitOps by explaining the key principles, practices and the tools involved. By the end of this course, you should be familiar with the need for GitOps, learn about the two different reconciliation patterns and three different implementation options so that you could get started with your own GitOps journey with confidence.

#### == **Course Learning Objectives** ==

By the end of this course, you should:

 * Be familiar with key principles and practices of GitOps.
 * Learn how a GitOps deployment pipeline to Kubernetes looks like.
 * See how you could roll out infrastructure components and applications.
 * Appreciate how fast GitOps can help you recover in case of a disaster.
 * Choose the tool of the trade from the two different reconciliation approaches.
 * And more...

#### == ** Meet Your Instructor: Gourav Shah** ==

**Gourav Shah** is a corporate trainer, author, consultant and course creator with deep expertise on topics such as implementing DevOps and containers technologies. Under the brand name of School of DevOps, he has trained professionals from leading corporations, including Walmart, Cisco, Mercedes, Adobe, GE, Schnider, Rakuten, Citrix, Ericsson, Visa, and many more.... In addition, he has touched the careers of more than 75k tech professionals from 140+ countries, who have subscribed to his video-based courses and has helped them get started with DevOps technologies. He has a published book, and is a Linux Foundation course author. Gourav is actively involved in, organizes and speaks at local DevOps meetups and events. He has been running a Youtube channel on DevOps topics (School of DevOps) and recently has started hosting a podcast by the name of Being DevOps.

#### == **Course Audience and Requirements** ==

**Audience**

This course is for software developers interested in learning how to easily deploy their cloud native applications to Kubernetes; quality assurance engineers interested in understanding what a continuous delivery pipeline on Kubernetes looks like with GitOps; site reliability engineers looking for a simple, easy and secure solution to set up automated and continuous applications, infrastructure, and policy rollouts with an ability to do quick roll backs when needed; and anyone looking to understand the landscape of GitOps and learn how to choose and implement the right tools.

**Knowledge/Skills Requirements**

To make the most of this course, you should have a good understanding of:

 * Kubernetes and Git-based workflows.
 * CI/CD processes.

### **Before You Begin**

#### == **Course Support** ==

If you have questions regarding your course enrollment, you can reach out to us via our Customer Support system. You will be required to login with your LF Account, which will help us to quickly locate your account and respond to your request. This will also allow you to track your support request through to resolution, and create an ongoing record of your support requests.

The Linux Foundation Training & Certification Customer Support system also offers enhanced functionality, such as:

 * Knowledge Base Articles - to help you find a quick response to your commonly asked questions
 * Service Request Forms - asking the right questions so that you can get the right answers.

#### == **Course Timing** ==

This course is entirely self-paced; there is no fixed schedule for going through the material. You can go through the course at your own pace, and you will always be returned to exactly where you left off when you come back to start a new session. However, we still suggest you avoid long breaks in between periods of work, as learning will be faster and content retention improved.

**You have unlimited access to this course for 12 months from the date you registered, even after you have completed the course.**

The chapters in the course have been designed to build on one another. It is probably best to work through them in sequence; if you skip or only skim some chapters quickly, you may find there are topics being discussed you have not been exposed to yet. But this is all self-paced, and you can always go back, so you can thread your own path through the material.

#### == **Copyright** ==


Copyright 2021, The Linux Foundation. All rights reserved.

The training materials provided or developed by The Linux Foundation in connection with the training services are protected by copyright and other intellectual property rights.

Open source code incorporated herein may have other copyright holders and is used pursuant to the applicable open source license.

Although third-party application software packages may be referenced herein, this is for demonstration purposes only and shall not constitute an endorsement of any of these software applications.

All The Linux Foundation training, including all the material provided herein, is supplied without any guarantees from The Linux Foundation. The Linux Foundation assumes no liability for damages or legal action arising from the use or misuse of contents or details contained herein.

Linux is a registered trademark of Linus Torvalds. Other trademarks within this course material are the property of their respective owners.

If you believe The Linux Foundation materials are being used, copied, or otherwise improperly distributed, please email info@linuxfoundation.org or call +1-415-723-9709 (USA).

### **The Linux Foundation**

#### == **The Linux Foundation** ==

[The Linux Foundation](https://www.linuxfoundation.org/) provides a neutral, trusted hub for developers to code, manage, and scale open technology projects. Founded in 2000, The Linux Foundation is supported by more than 1,000 members and is the world’s leading home for collaboration on open source software, open standards, open data and open hardware. The Linux Foundation’s methodology focuses on leveraging best practices and addressing the needs of contributors, users and solution providers to create sustainable models for open collaboration.

The Linux Foundation hosts Linux, the world's largest and most pervasive open source software project in history. It is also home to Linux creator Linus Torvalds and lead maintainer Greg Kroah-Hartman. The success of Linux has catalyzed growth in the open source community, demonstrating the commercial efficacy of open source and inspiring countless new projects across all industries and levels of the technology stack.

As a result, the Linux Foundation today hosts far more than Linux; it is the umbrella for many critical open source projects that power corporations today, spanning virtually all industry sectors. Some of the technologies we focus on include big data and analytics, networking, embedded systems and IoT, web tools, cloud computing, edge computing, automotive, security, blockchain, and many more.

#### == **Continuous Delivery Foundation** ==


The Continuous Delivery Foundation (CDF) was formed on March 12, 2019 by the Linux Foundation and over 20 founding members. It serves as the vendor-neutral home for the most important open source projects related to Continuous Integration and Continuous Delivery (CI/CD), which include:

 * [Jenkins](https://jenkins.io/)
 * [Jenkins X](https://jenkins-x.io/)
 * [Spinnaker](https://www.spinnaker.io/)
 * [Tekton](https://github.com/tektoncd).

​CDF fosters collaboration between the industry's top developers, end users and vendors to evangelize CI/CD and DevOps methodologies, further CI/CD best practices, and grow and sustain projects that are part of the broad and growing CD ecosystem. By creating training materials and providing an OSS-based, open development model it supports software development teams around the world to deliver code changes faster and more reliably.

#### == **The Linux Foundation Events** ==

Over 85,000 open source technologists and leaders worldwide gather at Linux Foundation events annually to share ideas, learn and collaborate. Linux Foundation events are the meeting place of choice for open source maintainers, developers, architects, infrastructure managers, and sysadmins and technologists leading open source program offices, and other critical leadership functions.

These events are the best place to gain visibility within the open source community quickly and advance open source development work by forming connections with the people evaluating and creating the next generation of technology. They provide a forum to share and gain knowledge, help organizations identify software trends early to inform future technology investments, connect employers with talent, and showcase technologies and services to influential open source professionals, media, and analysts around the globe.

The Linux Foundation hosts an increasing number of events each year, including:

 * Open Source Summit North America, Europe, and Japan
 * Embedded Linux Conference North America and Europe
 * Open Networking & Edge Summit
 * KubeCon + CloudNativeCon North America, Europe, and China
 * Automotive Linux Summit
 * KVM Forum
 * Linux Storage Filesystem and Memory Management Summit
 * Linux Security Summit North America and Europe
 * Linux Kernel Maintainer Summit
 * The Linux Foundation Member Summit
 * Open Compliance Summit
 * And many more.

You can learn more about the [Linux Foundation events](https://events.linuxfoundation.org/) online.

#### == **Training Venues** ==

The Linux Foundation's training is for the community, by the community, and features instructors and content straight from the leaders of the Linux developer community.

The Linux Foundation offers several types of training:

 * Classroom
 * Online
 * On-site
 * Events-based.

Attendees receive Linux and open source software training that is distribution-flexible, technically advanced and created with the actual leaders of the Linux and open source software development community themselves. The Linux Foundation courses give attendees the broad, foundational knowledge and networking needed to thrive in their careers today. With either online or in-person training, The Linux Foundation classes can keep you or your developers ahead of the curve on open source essentials.

#### == **The Linux Foundation Training Offerings** ==

Our current course offerings include:

 * Linux Programming & Development Training
 * Enterprise IT & Linux System Administration Courses
 * Open Source Compliance Courses.

To get more information about specific courses offered by the Linux Foundation, including technical requirements and other logistics, visit the [Linux Foundation training](https://training.linuxfoundation.org/) website.

#### == **The Linux Foundation Certifications** ==

The [Linux Foundation certifications](https://training.linuxfoundation.org/certification/) give you a way to differentiate yourself in a job market that's hungry for your skills. We've taken a new, innovative approach to open source certification that allows you to showcase your skills in a way that other peers will respect and employers will trust:

You can take your certification exam from any computer, anywhere, at any time:

 * The certification exams are performance-based​
 * The exams are distribution-flexible
 * The exams are up-to-date, testing knowledge and skills that actually matter in today's IT environment.

Click on each card to learn about certifications offered by the Linux Foundation and its collaborative projects.

#### == **Training/Certification Firewall** ==


The Linux Foundation has two separate training divisions: Course Delivery and Certification. These two divisions are separated by a firewall. 

The curriculum development and maintenance division of the Linux Foundation Training department has no direct role in developing, administering, or grading certification exams. 

Enforcing this self-imposed firewall ensures that independent organizations and companies can develop third party training material, geared towards helping test takers pass their certification exams. 

Furthermore, it ensures that there are no secret "tips" (or secrets in general) that one needs to be familiar with in order to succeed. 

It also permits the Linux Foundation to develop a very robust set of courses that do far more than teach the test, but rather equip attendees with a broad knowledge of the many areas they may be required to master to have a successful career in open source system administration.

## GitOps in Action

Demonstrating the Power of GitOps in Action

### Chapter Introduction

### Scenario
### Demo 1: Setting Up a Common Kubernetes Infrastructure

Starting situation : 
You had an application repository, where your source code war for the application. Along with that, you had written some manifests and the Helm charts, which were all stored in the same repository.
You also had a continuous integration environment, and basically had a bunch of steps which ended up in building an image and publishing it to the Docker Hub registry. You also had a Kubernetes environment and you basically did not have a proper way to deploy to this environment. So the missing piece of the puzzle was clearly the deployment system to Kubernetes. And you wanted a continuous automated deployment system. 

Helm can't solve all the problems, Helm is definitely a good packaging system, and you can put that code in the deployment repository, but how do you take that from that deployment repository and actually run it in the Kubernetes environment it's still an unsolved problem here.
Helm does'nt give you an automated deployment

At that time, you were running a combination of kubectl commands and Helm to deploy to this kubernetes environment.

You are even considering using the CI to deploy to Kubernetes from, but it's not only asking too much from your CI, but it would also be a security risk because anyone who has an access to the CI system would have access to your clusters and could do anything and deploy.
That's definitely a security risk.

So you needed a streamlined CD process to deploy to Kubernetes, and you also wanted to experiment with the canaries and progressibe canaries, blue/green, etc...

To Implement a continuous delivery solution on top of the existing CI
 * take out the deployment code of the application repository (it is a clean solution to keep deployement code separate from)
 * set up a simple gitops solution called Flux (WeaveWorks). it has its own repository called fluxfleet and is multi-tenant. Flux has an agent (reconciler) that keeps on checking for the changes in the fleet repository. It would know how to onboard other projects and also know how to go and sync the code from those repositories and applies it to the API server.

### Demo 2: Onboarding an Application Project

setup 3 repositories

 * one for the applicaiton code : https://github.com/devopsfoo/instavote
 * the second for the deployment code : https://github.com/devopsfoo/instavote-deploy contains deplouyment code including Kustomize overlays as well as helm charts and flux code
 * the third for Flux  : https://github.com/initcron/flux-fleet contains the cluster configuration and common infrastructure componentts 

Flux basically just extends the features of Kubernetes by implementing its own resources.


### Demo 3: Deploying All Microservices
### Demo 4: Adding A Release Strategy
### Demo 5: End to End CD Workflow with GitOps
### Demo 6: Disaster Recovery with GitOps
### Chapter Summary

##  GitOps Concepts

GitOps Concepts Overview

### Chapter Introduction
### What Is GitOps?
### Gitops Use Cases - What can you do with GitOps?
### Principles and Practices of GitOps
### Reconciliation Models - Pull vs Push
### Tools of the Trade - Which One to Pick?
### The Story of GitOps: Flux and Argo
### Key Benefits of GitOps
### Chapter Summary


## Course Completion

### Feedback

Thank you for taking this course!

We would love to hear your feedback.
All responses are read and taken seriously, as they are going to help us improve the course.

Take the Survey

 
### Course Completion

Congratulations! You have reached the end of this course! There is one last question to answer to complete the course!