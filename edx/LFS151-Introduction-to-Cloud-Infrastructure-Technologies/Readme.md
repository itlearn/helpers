# Introduction to Cloud Infrastructure Technologies

 * [Course Overview](#course-overview)
 * [Chapter 1. Virtualization](#chapter-1-virtualization)
 * [Chapter 2. Infrastructure as a Service (IaaS)](#chapter-2-infrastructure-as-a-service-iaas)
 * [Chapter 3. Platform as a Service (PaaS)](#chapter-3-platform-as-a-service-paas)
 * [Chapter 4. Containers](#chapter-4-containers)
 * [Chapter 5. Containers: Micro OSes for Containers](#chapter-5-containers-micro-oses-for-containers)
 * [Chapter 6. Containers: Container Orchestration](#chapter-6-containers-container-orchestration)
 * [Chapter 7. Unikernels](#chapter-7-unikernels)
 * [Chapter 8. Microservices](#chapter-8-microservices)
 * [Chapter 9. Software-Defined Networking and Networking for Containers](#chapter-9-software-defined-networking-and-networking-for-containers)
 * [Chapter 10. Software-Defined Storage and Storage Management for Containers](#chapter-10-software-defined-storage-and-storage-management-for-containers)
 * [Chapter 11. DevOps and CI/CD](#chapter-11-devops-and-cicd)
 * [Chapter 12. Tools for Cloud Infrastructure I (Configuration Management)](#chapter-12-tools-for-cloud-infrastructure-i-configuration-management)
 * [Chapter 13. Tools for Cloud Infrastructure II (Build & Release)](#chapter-13-tools-for-cloud-infrastructure-ii-build-release)
 * [Chapter 14. Tools for Cloud Infrastructure III (Key-Value Pair Store)](#chapter-14-tools-for-cloud-infrastructure-iii-key-value-pair-store)
 * [Chapter 15. Tools for Cloud Infrastructure IV (Image Building)](#chapter-15-tools-for-cloud-infrastructure-iv-image-building)
 * [Chapter 16. Tools for Cloud Infrastructure V (Debugging, Logging, and Monitoring for Containerized Applications)](#chapter-16-tools-for-cloud-infrastructure-v-debugging-logging-and-monitoring-for-containerized-applications)
 * [Chapter 17. Service Mesh](#chapter-17-service-mesh)
 * [Chapter 18. Internet of Things (IoT)](#chapter-18-internet-of-things-iot)
 * [Chapter 19. Serverless Computing](#chapter-19-serverless-computing)
 * [Chapter 20. Distributed Tracing](#chapter-20-distributed-tracing)
 * [Chapter 21. How to Be Successful in the Cloud](#chapter-21-how-to-be-successful-in-the-cloud)


**Reference**

LinuxFoundationX LFS151.x

https://training.linuxfoundation.org/resources/free-courses/introduction-to-cloud-infrastructure-technologies/
https://www.edx.org/course/introduction-to-cloud-infrastructure-technologies

Learn the fundamentals of building and managing cloud technologies directly from The Linux Foundation, the leader in open source.

## Course Overview

New to the cloud and not sure where to begin? This introductory course, taught by cloud experts from The Linux Foundation, will help you grasp the basics of cloud computing and comprehend the terminology, tools and technologies associated with today’s top cloud platforms.

Understanding cloud technologies tops the list of most important skills for any developer, system administrator or network computing professional seeking a lucrative career in technology. However, getting started and researching all things cloud can be complicated and time consuming. This course maps out the entire cloud landscape and explains how various tools and platforms fit together.

Experts from The Linux Foundation can help guide you step-by-step as you begin to navigate the cloud. They host some of the world's leading open source cloud projects and provide training and networking opportunities to educate a talent pool to support those projects, and is a respected, neutral, non-profit education source to provide training for anyone learning how to build and manage cloud infrastructure.

This course gives you a primer on cloud computing and the use of open source software to maximize development and operations. 

### Topics covered include

* Next-generation cloud technologies: Learn about cloud and container technologies like Docker, Cloud Foundry, Kubernetes and OpenStack, as well as the tooling around them.
* Scalable and performant compute, storage and network solutions: Get an overview of software defined storage and software defined networking solutions.
* Solutions employed by companies to meet their business demands: Study up on DevOps and continuous integration practices, as well as the deployment tools available to architects to meet and exceed their business goals.

No previous cloud experience is required for this course. "Introduction to Cloud Infrastructure Technologies" gives you the knowledge and tools to make smart decisions about which cloud services and applications to use depending on your nIeeds.

### What you'll learn

 * Basics of cloud computing
 * Characteristics of the different cloud technologies
 * Working knowledge on how to choose the right technology stack for your needs


### Course Details

    Length: 10 weeks
    Effort: 3-4 hours per week
    Level: Beginner
    Price: Free (certificate optional)

### Instructors

Chris Pokorni Instructor NQB8 Cloud Tech Consulting
Chip Childers Chief Technology Officer Cloud Foundry Foundation
Neependra Khare Founder and Principal Consultant CloudYuga Technologies

### Welcome

#### Before You Begin

Welcome to LFS151x - Introduction to Cloud Infrastructure Technologies!

We strongly recommend that you review the course syllabus before jumping into the content. It provides the most important information related to the course, including:

 * Course overview
 * Instructors biographies and targeted audience
 * Course prerequisites and length
 * Course learning objectives and the outline
 * edX platform guidelines
 * Discussion forums, course timing, and learning aids
 * Grading, progress, and course completion
 * Professional Certificate Program
 * Audit and verified tracks
 * The Linux Foundation's history, events, training, and certifications.

----

## Chapter 1. Virtualization

### Introduction and Learning Objectives


#### == Introduction to Cloud Computing and Technologies ==

Historically, the term *cloud* was used as a metaphor to represent the Internet, while a cloud-shaped figure depicting the Internet was used in computer network diagrams. To find out more about the origin of the cloud, you can explore its history on [Wikipedia](https://en.wikipedia.org/wiki/Cloud_computing#History). 

*Cloud computing* can be referred to as the remote allocation of resources on the cloud. According to [NIST (National Institute of Standard and Technology)](https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-145.pdf) , the formal definition of cloud computing is the following: 

"Cloud computing is a model for enabling ubiquitous, convenient, on-demand network access to a shared pool of configurable computing resources (e.g. networks, servers, storage, applications, and services) that can be rapidly provisioned and released with minimal management effort or service provider interaction".

Cloud computing providers offer various services built on top of basic provisioning and releasing of resources. Most of these service models fall into one of the following categories: 

 * Infrastructure as a Service (IaaS)
 * Platform as a Service (PaaS) 
 * Software as a Service (SaaS).
 
We will discover service and deployment models together with key characteristics throughout the course. Cloud services providers offer web interfaces in the form of consoles and dashboards, aimed to improve the user experience on the platform and ease the building, configuration and monitoring of the desired services stack. Another benefit of the cloud service model is the pay-as-you-go model adopted by cloud providers, allowing users to pay only for the cloud resources used during a set time frame. Such an approach to treat computing resources as a utility eliminates the need for high up-front investments in hardware, software, and specialized staff. 


#### == **Key Features of Cloud Computing** ==

Users benefit most from the following features of cloud computing:

 * **Speed and Agility** : Cloud resources are provisioned with a few clicks, saving time and providing agility. They also scale up or down with ease, by increasing or decreasing the number of running instances of a particular resource as a response to current demand or based on projected future demand. An additional characteristic is elasticity, allowing some cloud resources instances to increase or decrease in size, also demand-driven.
 * **Cost** : By reducing the up-front cost to set up the infrastructure, it allows users to focus on applications supporting the business processes. Another beneficial feature of cloud providers is a cost estimator, which assists users in better resource planning.
 * **Easy Access to Resources** : Users can access the cloud services from any place and device, as long as there is connectivity to the cloud service provider.
 * **Maintenance** : All the lights-on maintenance work to keep resources up, running, and up-to-date is offloaded onto the provider, and it is no longer the user's concern.
 * **Multi-tenancy** : Multiple users sharing the same pool of resources, which helps to drive down the costs incurred by each user.
 * **Reliability** : Resources can be hosted in different data center locations, to provide increased reliability, resiliency, and high-availability.


#### == **Cloud Deployment Models** ==

Primarily, a cloud is deployed under the following models: 

 * *Private Cloud* : It is designated and operated solely for one organization. It can be hosted internally or externally and managed by internal teams or a third party. A private cloud can be built using a software stack like OpenStack.
 * *Public Cloud* : It is open to the public and anybody can use it, after swiping a credit card, of course. Amazon Web Services, Google Cloud Platform, and Microsoft Azure are examples of public clouds.
 * *Hybrid Cloud* : Public and private clouds can be bound together to offer a hybrid cloud. Such hybrid clouds are beneficial for:
    * Storage of sensitive information on the private cloud, while offering public services based on that information from a public cloud.
    * Meeting temporary resources during peak or times of high demand by scaling to the cloud, when such temporary resources cannot be met on the private cloud.
 

![Cloud Computing Types](LFS151-Cloud_computing_types.svg)

Cloud Computing Types
(by Sam Johnston, licensed under CC BY-SA 3.0, retrieved from Wikipedia)

Additional cloud deployment models may include: 

 * *Community Cloud* : Formed by multiple organizations sharing a common cloud infrastructure.
 * *Distributed Cloud* : Formed by distributed systems connected to a single network.
 * *Multicloud* : One organization uses multiple public cloud providers to run its workload, typically to avoid vendor lock-in.
 * *Poly Cloud* : One organization uses multiple public cloud providers to leverage specific services from each provider.


#### == **Virtualization** ==

According to Wikipedia,

"In computing, virtualization refers to the act of creating a virtual (rather than actual) version of something, including virtual computer hardware platforms, operating systems, storage devices, and computer resources". 

Virtualization can be achieved at different hardware and software layers, such as the Central Processing Unit (CPU), storage (disk), memory (RAM), filesystems, etc. In this chapter, we will explore a few tools that allow us to create Virtual Machines (VMs) after emulating essential hardware components that support the installation of a guest Operating System (OS) on them. A VM represents an isolated collection of emulated resources, behaving like an actual physical system.  

Virtual Machines are created with the help of a *hypervisor*, that runs on a host machine. The hypervisor is a piece of software capable of creating multiple isolated virtual operating environments, each composed of emulated resources that are then made available to guest systems. Hypervisors are classified into two main categories:

**Type-1 hypervisor** (native or bare-metal) runs directly on top of a physical host machine's hardware, without the need for a host OS. Typically, they are found in enterprise settings. Examples of type-1 hypervisors:

 * IBM z/VM
 * Microsoft Hyper-V 
 * Nutanix AHV 
 * Oracle VM Server for SPARC
 * Oracle VM Server for x86
 * VMware ESXi
 * AWS Nitro
 * Xen.

**Type-2 hypervisor** (hosted) runs on top of the host's OS. Typically, they are for end-users, but they may be found in enterprise settings as well. Examples of type-2 hypervisors:

 * VirtualBox
 * VMware Player
 * VMware Workstation 
 * Parallels Desktop for Mac.

And then there are the exceptions - hypervisors matching both categories, which can be listed redundantly under hypervisors of both type-1 and type-2. They are Linux kernel modules that act as both type-1 and type-2 hypervisors at the same time. Examples are:

 * KVM
 * bhyve.

Hypervisors enable the virtualization of computer hardware such as CPU, disk, network, RAM, etc., and allow the installation of guest VMs on top of them. We can create multiple guest VMs with different Operating Systems on a single hypervisor. For example, we can use a native Linux machine as a host, running on bare-metal, and after setting up a type-2 hypervisor we can create multiple guest machines with different OSes, commonly Linux and Windows. 

Hardware virtualization is supported by most modern CPUs, and it is the feature that allows hypervisors to virtualize the physical hardware of a host system, thus sharing the host system's processing resources with multiple guest systems in a safe and efficient manner. In addition, most modern CPUs also support *nested virtualization* (https://en.wikipedia.org/wiki/Virtualization#Nested_virtualization), which allows us to have VMs inside a VM.

Next, let's explore a few methods of creating VMs on top of different hypervisors.


#### == Learning Objectives ==

By the end of this chapter, you should be able to:

 * Explain the concept of virtualization.
 * Explain the role of a hypervisor in the creation of Virtual Machines.
 * List hypervisor types. 
 * Create and configure Virtual Machines with hypervisors, such as KVM and VirtualBox.
 * Explore methods to automate the creation of VMs with Vagrant.


### **KVM**

#### == **Introduction to KVM** ==

According to [linux-kvm.org](http://www.linux-kvm.org/), 

"KVM (Kernel-based Virtual Machine) is a full virtualization solution for Linux on x86 hardware".

KVM is a loadable virtualization module of the Linux kernel and it converts the kernel into a hypervisor capable of managing guest Virtual Machines. Specific hardware virtualization extensions supported by most modern processors, such as Intel VT and AMD-V, have to be available and enabled for processors to support KVM. Although originally designed for the x86 hardware, it has also been ported to FreeBSD, S/390, PowerPC, IA-64, and ARM as well.

![A High-Level Overview of the KVM/QEMU Virtualization Environment](LFS151-Kernel-based_Virtual_Machine.svg)
A High-Level Overview of the KVM/QEMU Virtualization Environment
(by V4711, licensed under CC BY-SA 4.0, retrieved from Wikipedia)

#### == **Features** ==

KVM is an open source software. It provides hardware-assisted virtualization that supports [various guest OSes](http://www.linux-kvm.org/page/Guest_Support_Status), such as Linux distributions, Windows, Solaris, etc. 

KVM allows for device abstraction, however, the processor is not emulated. It exposes the /dev/kvm interface that can be used by an external userspace host for emulation. [QEMU](https://en.wikipedia.org/wiki/QEMU) is an example of such a host. 

KVM supports [nested guests](https://www.linux-kvm.org/page/Nested_Guests), which allow VMs to run within VMs. It also supports hotpluggable devices such as CPUs and PCI devices. [Overcommitting](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/) is possible as well for the allocation of additional virtualized resources that may not be available on the system. To achieve overcommitting for a VM, KVM dynamically swaps resources from another guest that is not using the type of resource needed.

#### **Demo: Creating a Virtual Machine Instance on the KVM Hypervisor**

In this video, we will show how you can create a virtual machine instance on the KVM hypervisor using virt-manager.

Creating a Virtual Machine Instance on the KVM Hypervisor


#### == **Benefits of Using KVM** ==

Some of the benefits of using KVM are:

 * It is an open source solution, and, as such, free to customize.
 * Using KVM is efficient from a financial perspective as well, due to the lower costs associated with it.
 * It provides efficient hardware-assisted virtualization for an array of guest OSes, such as Linux, BSD, Solaris, Windows, Mac OS, ReactOS, and Haiku.
 * It provides para-virtualization of Ethernet cards, disk I/O controllers, and graphical interfaces for a set of guest OSes, such as Linux, OpenBSD, FreeBSD, and Windows. 
 * It is highly scalable.
 * KVM employs advanced security features, utilizing SELinux. It provides MAC (Mandatory Access Control) security between Virtual Machines. KVM has received awards for meeting common government and military security standards and for allowing open virtualization for homeland security projects.


### **VirtualBox**

#### == **Introduction to VirtualBox** ==

[VirtualBox](https://www.virtualbox.org/) is an x86 and AMD64/Intel64 virtualization product from Oracle, running on Windows, Linux, Mac OS X, and Solaris hosts and supporting [guest OSes](https://www.virtualbox.org/wiki/Guest_OSes) from Windows, Linux families, and others, such as Solaris, FreeBSD, and DOS. However officially, only a select few of the most common guest OSes are supported and optimized to run on VirtualBox.

It is an easy-to-use multi-platform type-2 hypervisor. Although not part of the mainline Linux kernel it can be used by compiling and inserting the respective kernel module. 

VirtualBox is distributed under the [GNU General Public License (GPL) version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).


#### **Demo: Creating a Virtual Machine Instance on VirtualBox**

In this video, we will show you how to create a virtual machine instance on VirtualBox.

Creating a Virtual Machine Instance on VirtualBox


#### == **Benefits of Using VirtualBox** ==

Some of the benefits of using VirtualBox are:

 * It is an open source solution.
 * It is free to use.
 * It runs on Linux, Windows, Mac OS X, and Solaris.
 * It provides two virtualization choices: software-based virtualization and hardware-assisted virtualization.
 * It is an easy-to-use multi-platform type-2 hypervisor.
 * It provides the ability to run virtualized applications side-by-side with normal desktop applications.
 * It provides teleportation - live migration.


### **Vagrant** 

#### == **Introduction to Vagrant** ==

Using Virtual Machines in a development environment has numerous benefits:

 * Reproducible environment
 * Management of multiple projects, each in its isolated and restricted environment
 * Sharing the environment with other teammates
 * Keeping the development and deployment environments in sync
 * Running consistently the same VM on different OSes leveraging hypervisors such as VirtualBox or KVM. 

Configuring and sharing one VM is easy, but, when we have to manage multiple VMs for the same project, performing all the build and configuration steps manually can be tiresome. [Vagrant](https://www.vagrantup.com/) by HashiCorp helps us automate VMs management by providing an end-to-end lifecycle management utility - the vagrant command line tool. Vagrant is a cross-platform tool, as it can be installed on Linux, Mac OS X, and Windows.

One of the key features of Vagrant is extensibility. By default, Vagrant is shipped with a limited amount of features, but, thanks to various plugins we can extend its support for custom providers, provisioners, commands, and hosts. 

Vagrant has recently added support for Docker, allowing it to manage Virtual Machines and containers alike.

#### == **Managing Virtual Machines with Vagrant** ==

Let's explore some of the key components used by Vagrant to manage Virtual Machines:

 * **Vagrantfile** : The [Vagrantfile](https://www.vagrantup.com/docs/vagrantfile/) describes how Virtual Machines should be configured and provisioned. It is a text file with the Ruby syntax, which has all the information about configuring and provisioning a set of machines. It may include the machine type, image, networking, provider-specific information, and provisioner details. While it is portable, there should be only one Vagrantfile per project. We provide a sample Vagrantfile below:

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
   # Every Vagrant development environment requires a box. You can search for 
   # boxes at https://app.vagrantup.com/boxes/search
   config.vm.box = "centos/7"

   # Create a private network, which allows host-only access to the machine

   # using a specific IP.
   config.vm.network "private_network", ip: "192.168.33.10"

   # config.vm.synced_folder "../data", "/vagrant_data"

   config.vm.provider "virtualbox" do |vb|
      # Customize the amount of memory on the VM:
      vb.memory = "1024"
   end

   config.vm.provision "shell", inline: <<-SHELL
         yum install vim -y
   SHELL
end
```

The *vagrant* command reads the configuration given in the configuration file and does different operations, like up, ssh, destroy, etc. The vagrant command also has sub-commands like box to manage Box images, rdp to connect to VMs using Remote Desktop Protocol (RDP), etc. A detailed list of commands is available at its [documentation](https://www.vagrantup.com/docs/cli/).

 * **Boxes** : Boxes are the package format for the Vagrant environment. The Vagrantfile requires an image, which is then used to instantiate Virtual Machines. In the example above, we have used centos/7 as the base image. If the image is not available locally, then it can be downloaded from a central image repository such as [Vagrant Cloud box repository](https://app.vagrantup.com/boxes/search), provided by HashiCorp. Box images can be versioned and customized to specific needs, simply by updating the Vagrantfile accordingly. 

 * **Providers** : [Providers](https://www.vagrantup.com/docs/providers/) are the underlying engines or hypervisors used to provision VMs or containers. Although the default Vagrant provider is VirtualBox, it also supports Hyper-V, and Docker out of the box. Custom providers such as KVM, AWS, may be installed as well.

 * **Synced Folders** : With the [Synced Folder](https://www.vagrantup.com/docs/synced-folders/) feature, we can sync a directory on the host system with a VM, which helps the user manage shared files/directories easily. In our example, if we un-comment the line describing the synced folder attribute, then the ../data folder from the current working directory of the host system would be shared with the /vagrant_data folder of the VM.

```ruby
# config.vm.synced_folder "../data", "vagrant_data"
```

 * **Provisioning** :  [Provisioners](https://www.vagrantup.com/docs/provisioning/) allow us to automatically install software and make configuration changes after the machine is booted. It is part of the vagrant up process. There are many types of provisioners available, such as File, Shell, Ansible, Puppet, Chef, Docker, and Salt. In the example below, we used Shell as the provisioner to install the vim package.

```ruby
config.vm.provision "shell", inline: <<-SHELL
              yum install vim -y
   SHELL
```

 * **Plugins** : We can use [plugins](https://www.vagrantup.com/docs/plugins/) to extend the functionality of Vagrant.

 * **Networking** : Vagrant provides high-level [networking](https://www.vagrantup.com/docs/networking/) options for port forwarding, network connectivity, and network creation. These networking options represent an abstraction that enables cross-provider portability. That is, the same Vagrantfile used to provision a VirtualBox VM could be used to provision a VMware machine.

 * **Multi-Machine** : A project's Vagrantfile may describe [multiple VMs](https://www.vagrantup.com/docs/multi-machine/), which are typically intended to work together or may be linked between themselves. 

#### **Demo: Automating the Creation and Deletion of VMs with Vagrant**


In this video, we will see how we can automate the creation and deletion of virtual machines, using Vagrant.

Automating the Creation and Deletion of VMs with Vagrant

#### == **Benefits of Using Vagrant** ==

Some of the benefits of using Vagrant are:

 * It automates the setup of one or more VMs, which results in saved time, increased productivity, and lower operational costs.
 * It introduces consistency in infrastructure provisioning through Vagrantfile.
 * It is a flexible cross-platform tool.
 * It provides support for Docker, thus helping manage Docker containers in addition to VMs.
 * It is easy to install and to configure.
 * It is very useful in multi-developer teams.

----

## Chapter 2. Infrastructure as a Service (IaaS)

### Introduction and Learning Objectives

#### == **Introduction** ==

**Infrastructure as a Service (IaaS)** is a cloud service model that provides on-demand physical and virtual computing resources, storage, network, firewall, and load balancers. To provide virtual computing resources, IaaS uses hypervisors, such as Xen, KVM, VMware ESXi, Hyper-V, or Nitro.

Infrastructure as a Service is the backbone of all other cloud services, providing computing resources. After the provisioning of the computing resources, other services are setup on top.

Example:
Let's say that you want to have a set of ten Linux systems with 4GB RAM each, and two Windows systems with 8GB each to deploy your software. You can go to any of the IaaS providers and request these systems. Generally, an IaaS provider creates the respective VMs in the background, puts them in the same internal network, and shares the credentials with you, thus allowing you to access them. Other than VMs, some IaaS providers offer bare-metal machines for provisioning.

In this chapter, we will take a closer look at some of the IaaS providers and their features. We will also provide a demo video for each one of them.

#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Explain the concept of Infrastructure as a Service (IaaS).
 * Distinguish between different IaaS providers.
 * Provision Virtual Machine instances with different IaaS providers.

### **Amazon Elastic Compute Cloud**

#### == **Introduction to Amazon EC2** ==


[Amazon Web Services](https://aws.amazon.com/) (AWS) is one of the leading cloud services providers. Its cloud services, whether Platform, Software, DB, Containers, Serverless, Machine Learning, to name a few, rely heavily on its Infrastructure. Part of Amazon's Infrastructure as a Service (IaaS) model is the [Amazon Elastic Compute Cloud](https://aws.amazon.com/ec2/) (Amazon EC2) service . Amazon EC2 service allows individual users and enterprises alike to build a reliable, flexible, and secure cloud infrastructure for their applications and workloads on Amazon's platform. AWS offers an easy to use [Console](https://aws.amazon.com/console/), which is a web interface for cloud resources management.

Amazon EC2 instances are in fact Virtual Machines. When provisioning EC2 instances on the AWS infrastructure, we are provisioning VMs on top of hypervisors that run directly on Amazon's physical infrastructure. In addition to the web console, AWS also offers a [command line interface](https://aws.amazon.com/cli/) (CLI), a tool to manage resources scriptically if necessary. 

At the heart of Amazon EC2 service, are various type-1 hypervisors, such as Xen, KVM, and [Nitro](https://aws.amazon.com/ec2/nitro/) (a newer KVM-based lightweight hypervisor that delivers close to bare-metal performance).

It comes with no surprise that the Amazon EC2 is a paid service, but, new users are encouraged to try the AWS platform for free through [AWS Free Tier](https://aws.amazon.com/free/free-tier-faqs/) offering. While not all of its services qualify for the free tier, the ones that are available for free do come with some limitations. However, users are still encouraged to take advantage of the free tier offering to become familiar with the most popular services the AWS platform has to offer.


#### == **Features and Tools** ==

When provisioning Amazon EC2 instances, users are able to manage different instance aspects, such as hardware profile, operating system image with software package, additional storage, the network attached to the instance, and firewall rules. 

[Amazon Machine Images](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html) (**AMI**) are preconfigured images with the information needed to launch EC2 instances. They include an Operating System and a collection of software packages. AMIs are stored in an AWS repository and are used to quickly launch instances. There are default images created and maintained by AWS, the user community, or custom images we can create ourselves.

[Instance Types](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-types.html) determine the virtualized hardware profile of the instance to be launched. Each instance type is preconfigured with different compute, memory, and storage capabilities. They are categorized in instance families such as:

 * General purpose instances
 * Optimized instances for:
    * Compute (for CPU intensive applications)
    * GPU
    * Memory (RAM)
    * Machine Learning (ML)
    * Storage (SSD).

When you launch an instance, select an instance type based on the requirements of the application or software that you plan to run on your instance. The following are just a few examples of general purpose instance types, together with their high-level hardware profile information:

 * t2.micro: 1 vCPUs, 2.5 GHz, Intel Xeon Family, 1 GiB memory, EBS only
 * t3a.large: 2 vCPUs, 2.2 GHz, AMD EPYC 7571, 8 GiB memory, EBS only
 * m5a.16xlarge: 64 vCPUs, 2.5 GHz, AMD EPYC 7571, 256 GiB memory, EBS only.

Additional configurable aspects of EC2 instances, related services, and tools:  

 * [Security Groups](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_SecurityGroups.html) for network access rules
 * [Amazon Elastic Block Store](https://aws.amazon.com/ebs/) (**EBS**) for persistent storage attachment
 * Dedicated hosts to provision instances on a physical machine reserved for our use
 * Elastic IP to remap a Static IP address automatically
 * [Virtual Private Cloud](https://aws.amazon.com/vpc/) (VPC) for network isolation
 * [CloudWatch](https://aws.amazon.com/cloudwatch/) for monitoring resources and applications
 * [Auto Scaling](https://aws.amazon.com/cloudwatch/) to dynamically resize resources.



#### **Demo: Creating and Destroying an Instance using Amazon EC2 Compute Service**

Demo: Creating and Destroying an Instance using Amazon EC2 Compute Service

Let's take a look at the following demo, which illustrates an example of how to create and destroy an instance on AWS, using the Amazon EC2 compute service.

Creating and Destroying an Instance using Amazon EC2 Compute Service

#### == **Benefits of Using Amazon EC2** ==

Key benefits of using Amazon EC2 service are:

 * It is an easy-to-use IaaS solution.
 * It is flexible and scalable.
 * It provides a secure and robust functionality for your compute resources.
 * It enables automation.
 * It is cost-effective: only pay for the time and resources used.
 * It is designed to work in conjunction with other AWS components, while it integrates with third-party automation tools.
 * It promises 99.99% uptime.
 * It provides specialized instances for workloads, such as floating-point operations, high graphics capability (GPU), high input/output (I/O), High Performance Computing (HPC), Machine Learning (ML).

### **Azure Virtual Machine**

#### == **Introduction to Azure Virtual Machine** ==


[Microsoft Azure](https://azure.microsoft.com/) is a leading cloud services provider, with products in different domains, such as compute, web and mobile, data and storage, Internet of Things (IoT), and many others. The [Azure Virtual Machine](https://docs.microsoft.com/en-us/azure/virtual-machines/) service allows individual users and enterprises alike to provision and manage compute resources, both from a web [Portal](https://azure.microsoft.com/en-us/features/azure-portal/) or the [Azure Cloud Shell](https://docs.microsoft.com/en-us/azure/cloud-shell/overview), a flexible command line utility configurable to use either Bash or PowerShell.

Azure cloud services are enabled by the Azure Hypervisor, a customized version of Microsoft Hyper-V type-1 hypervisor. The Azure Hypervisor promises high efficiency, performance, and scalability over a small footprint thanks to the optimization of the custom Hyper-V hypervisor coupled with its tight integration with the physical infrastructure. 

The Azure Virtual Machine is a paid service, but, there an [Azure free account](https://azure.microsoft.com/en-us/free/) offering aimed to attract new users and encourage them to explore the cloud services the Azure platform has to offer. 

#### Features and Tools

During the VM provisioning process, Azure users are able to manage various instance aspects such as the Operating System, VM Size, storage, networking, and firewall rules. 

The VM [Image](https://azuremarketplace.microsoft.com/en-us/marketplace/apps?filters=virtual-machine-images) field defines the base Operating System or the application for the VM. Both [Linux VMs](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-cli) and [Windows VMs](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/quick-create-powershell) can be launched from images available through the [Azure Marketplace](https://azuremarketplace.microsoft.com/en-us). 

 VM [Size](https://docs.microsoft.com/en-us/azure/cloud-services/cloud-services-sizes-specs) determines the type and capacity of the compute, memory, storage and network resources for the VM to be launched. VMs are grouped in families based on their intended usage:

 * General purpose
 * Optimized VMs for:
    * Compute (for CPU intensive applications)
    * GPU
    * Memory (RAM)
    * High Performance Compute (HPC)
    * Storage.

Additional configurable aspects of VMs:  

 * Network security groups to manage network traffic
 * SSD or HDD for persistent storage attachment, with optional encryption
 * Dedicated hosts to provision VMs on a physical machine reserved for our use
 * Accelerated networking for low latency and high throughput
 * Virtual network for network isolation
 * Monitoring resources and applications
 * Resource Manager templates for VM deployment
 * Seamless hybrid connections
 * Automated backups.

#### **Demo: Creating and Destroying an Instance using Amazon EC2 Compute Service**

Let's take a look at the following demo, which illustrates an example of how to create and destroy an instance on AWS, using the Amazon EC2 compute service.

Creating a Virtual Machine Instance on Microsoft Azure

#### == **Benefits of Using Azure Virtual Machine** ==

key benefits of using Azure Virtual Machine service:

 * It is an easy-to-use IaaS solution.
 * It is flexible and scalable.
 * It provides a secure and robust functionality for your compute resources.
 * It enables automation.
 * It is cost-effective: only pay for the time and resources used.
 * It is designed to work in conjunction with other Azure services.

### **DigitalOcean Droplet**

#### == **Introduction to DigitalOcean Droplet** ==


[DigitalOcean](https://www.digitalocean.com/) is a leading cloud services provider, aiming its cloud platform at both individual users and enterprises. DigitalOcean helps you create a simple cloud quickly, as it promises IaaS virtual instances launched in as little as 55 seconds. DigitalOcean cloud services enable application deployments and scaling on a cloud infrastructure available worldwide, leveraging network and storage flexibility together with security and monitoring tools.

In the DigitalOcean ecosystem, the virtual compute instances are [Droplets](https://www.digitalocean.com/products/droplets/), and they are launched on top of the KVM type-1 hypervisor, with SSD (Solid-State Drive) as their primary storage disk.

DigitalOcean offers paid cloud services, but users can take advantage of an introductory credit towards cloud services part of a free tier offering aimed to encourage users to explore and become familiar with its offerings. 


#### == **Features and Tools** ==

DigitalOcean Droplets are easy to configure, users being able to manage the resource profile, guest Operating System, application server, security, backup, monitoring, and more.  

Resource profiles are directly associated with DigitalOcean cost plans and are categorized in Shared CPU and Dedicated CPU plans.

The Shared CPU plan includes the **Standard Droplets** with burstable vCPU and memory that can be configured to support the running of web servers, forums, and blogs.

The Dedicated CPU plan includes dedicated vCPUs and a specific amount of memory per each vCPU that further determines the Droplet type:

 * **General Purpose** with 4GB of memory for each vCPU, to support high traffic web servers, e-commerce, and SaaS.
 * **CPU-Optimized** with 2GB of memory for each vCPU, to support Machine Learning, CI/CD, and video encoding.
 * **Memory-Optimized** with 8GB of memory for each vCPU to support high-performance DBs, and real-time big data processing.

The Droplet guest OS can be picked from a list of Linux distribution images, such as CentOS, CoreOS, Debian, Fedora, and Ubuntu. While custom images can be built and used, Droplets can be pre-configured to run applications such as Docker, LAMP, MongoDB, MySQL, and Node.js.

Users can manage other aspects of Droplets:

 * Monitoring to collect performance metrics.
 * Cloud Firewalls to secure the network infrastructure.
 * Backups that can be automated, allowing for easy Droplet restores or to launch new pre-configured Droplets.
 * Snapshots to be used as restore points after a failed upgrade or configuration change.
 * Team Management for collaboration. 
 * Block Storage for Droplet storage.
 * Spaces for scalable and secure storage solutions aimed to store and deliver data.
 * Load Balancers for traffic distribution.
 * Floating IPs for flexibility when assigning IPs to Droplets and to release them when no longer needed.
 * APIs for programmatic Droplet launching.
 * Networking features, such as DNS, IPv6, Private networking.

#### **Demo: Creating a Virtual Machine on DigitalOcean**

Let's take a look at the following demo, which illustrates how we can create a virtual machine instance on Microsoft Azure platform.

Creating a Virtual Machine on DigitalOcean

#### == **Benefits of Using DigitalOcean** ==

Key benefits of using DigitalOcean are:

 * It allows you to configure a cloud in as little as 55 seconds.
 * It is flexible and scalable.
 * It provides a high level of security by using KVM virtualized Droplets.
 * It enables automation.
 * It is cost-effective: only pay for the time and resources used.
 * It is focused on providing a simple, user-friendly experience.
 * It uses high-performance Solid State Disks (SSD).
 * It offers a one-click installation of a multitude of applications and application stacks, such as CloudBees Jenkins, LAMP,  * Docker, Kubernetes, NGINX, and WordPress.

### **Google Compute Engine**

#### == **Introduction to Google Compute Engine** ==


[Google Cloud Platform](https://cloud.google.com/) (GCP) is one of the leading cloud services providers. Its infrastructure is the foundation for all other cloud services, whether Platform, Software, Containers, Serverless, Artificial Intelligence, Machine Learning, to name a few. Part of Google's Infrastructure as a Service (IaaS) model is the Compute Engine service. [Google Compute Engine](https://cloud.google.com/compute) (GCE) service allows individual users and enterprises alike to build a reliable, flexible, and secure cloud infrastructure for their applications and workloads on Google's platform. GCP offers an easy to use [Console](https://console.cloud.google.com/), which is a web interface for cloud resources management.

GCE instances are in fact Virtual Machines. When provisioning GCE instances on the GCP infrastructure, we are provisioning VMs on top of hypervisors that run directly on Google's physical infrastructure. In addition to the web Console, GCP also offers a command line interface (CLI), a tool to manage resources scriptically if necessary. 

GCE services are enabled by the KVM type-1 hypervisor, running directly on Google's physical infrastructure and allowing for VMs to be launched with Linux and Windows guest Operating Systems.

GCE is a paid service, but, new users are encouraged to try the GCP platform for free through [GCP Free Tier](https://cloud.google.com/free) offering. The Free Tier offers new users a chance to become familiar with the most popular services the Google Cloud Platform has to offer.

#### == **Features and Tools** ==

Google Compute Engine service allows users to configure many of the VM instance characteristics, such as machine profile, image, storage, network, security, and access control. 

GCE [Machine Types](https://cloud.google.com/compute/docs/machine-types) determine the virtualized hardware configuration for VMs to be provisioned. Based on available configurations, VMs are categorized in various machine type families aimed to support specific types of applications and workloads:

 * **General-purpose** machines offering the best price-performance ratio. This family includes machine types N1, N2, N2D, E2.
 * **Memory-optimized** designed for memory-intensive workloads.
 * **Compute-optimized** for compute-intensive workloads.
 * **Shared-core** for cost-effective light-weight applications. Several machine types from the N1 and E2 machine families share a physical core to minimize costs.

GCE [Images](https://cloud.google.com/compute/docs/images) determine the guest Operating System of the VMs. While there are several public images available to launch VMs, users can create custom Images as well.

Additional configurable aspects of GCE VMs, and related services:  

 * **Storage** Disks for VM attached storage volumes.
 * **Networking VPC and Firewalls** for network isolation and security.
 * **Snapshots** for fast GCE persistent disk backups and recovery.
 * **Cloud Security Scanner** to scan applications for security vulnerabilities.
 * **Health Checks** to probe VMs health.
 * **Sole-tenant Nodes** for dedicated physical Compute Engines.
 * **Network Endpoint Group** representing collections of IP addresses for load balancing, firewalls, and logging purposes.


#### Demo: Creating and Destroying an Instance on Google Compute Engine

Let's take a look at the following demo, illustrating how we can create and destroy an instance on Google Compute Engine.

Creating and Destroying an Instance on Google Compute Engine

#### Benefits of Using Google Compute Engine

Key benefits of using Google Compute Engine are:

 * It is flexible and allows you to scale your applications easily.
 * Fast boot time.
 * It is very secure, encrypting all data stored.
 * It enables automation.
 * It is cost-effective: only pay for the time and resources used.
 * It supports custom machine types.
 * It supports Virtual Private Cloud, Load Balancers, etc.


### **IBM Cloud Virtual Servers**

#### == **Introduction to IBM Cloud Virtual Servers** ==

[IBM Cloud](https://www.ibm.com/cloud) is one of the leading cloud services providers for enterprises and academic institutions. It includes the typical service models, the Infrastructure as a Service (IaaS), Software as a Service (SaaS), and Platform as a Service (PaaS) offered through public, private, and hybrid cloud delivery models. Part of the IBM Cloud IaaS model is the [IBM Cloud Virtual Servers](https://www.ibm.com/cloud/virtual-servers) service, also known as Virtual Machines, one of the many services providing cloud Compute services. IBM Cloud Virtual Servers provide interoperability between virtual and bare-metal servers by being deployed on the same VLANs as the physical servers. When you create an IBM Cloud Virtual Server, you can choose between multi-tenancy or single-tenancy environments, and also high-performance local disks or enterprise SAN storage.

IBM Cloud is the successor of two joined technologies - the IBM mainframe and virtualization. IBM treats virtualization with some level of flexibility. While it uses IBM z/VM and IBM PowerVM, two powerful hypervisors, to manage its own virtual workload, the users of IBM Cloud are allowed to choose between XenServer, VMware, and Hyper-V hypervisors when managing bare-metal instances. This level of hypervisor flexibility is one of the advantages of IBM Cloud, and one of the top reasons why users pick IBM Cloud over another cloud service provider.

IBM Cloud services are available at a cost, however, the IBM Cloud Free Tier offer allows users to explore cloud services for free in combination with a predetermined amount of free credits towards specific cloud resources.

#### == **Features and Tools** ==

When provisioning IBM Cloud Virtual Servers, users are able to manage different server aspects, such as profile, image, software package add-ons, attached storage, network interface bandwidth, internal and external firewall rules, IP addresses, and VPN. Most of these server aspects are available for the four supported types of virtual servers: 

 * **Public** for multi-tenants
 * **Dedicated** for single-tenants
 * **Transient** for ephemeral multi-tenants
 * **Reserved** for multi-tenants committed for a pre-specified term.

Profiles specify the size of the virtual server and are associated with predefined resource amounts that the servers get launched with. While there are popular profiles specifying a balanced collection of compute, memory, and storage resources, there are optimized profiles aimed at specific workload types:

 * **Balanced** for common cloud workloads requiring a balance between performance and scalability, with network-attached storage (NAS).
 * **Balanced local storage** for medium to large databases that require high I/O, with local HDD storage.
 * **Compute** for compute-intensive deployments such as moderate to high traffic webservers.
 * **Memory** for caching and analytics workloads.
 * **Variable compute** for workloads without constant high-CPU performance.
 * **GPU** for high-performance deployments.

Virtual server images are preconfigured with the information needed to launch IBM Cloud Virtual Servers. They include an Operating System such as a Linux distribution or Windows, and optional software package add-ons.

#### **Demo: Creating and Destroying an Instance using IBM Cloud Virtual Servers Service**

Let's take a look at the following demo, which illustrates an example of how to create and destroy an instance on IBM Cloud, using the Virtual Servers compute service.

Creating and Destroying an Instance using IBM Cloud Virtual Servers Service

#### **Benefits of Using IBM Cloud Virtual Servers**

Key benefits of using IBM Cloud Virtual Servers are:

 * It is an easy-to-use IaaS solution.
 * It is flexible and scalable.
 * It provides a secure and robust functionality for your compute resources.
 * It enables automation.
 * It is cost-effective: only pay for the time and resources used.
 * It is designed to work in conjunction with other IBM Cloud components and services.

### **Oracle Cloud Compute Virtual Machines**

#### == **Introduction to Oracle Cloud Compute Virtual Machines** ==

Oracle is a leading cloud services provider through its [Oracle Cloud](https://www.oracle.com/cloud/) offering. Oracle's IaaS cloud service model is implemented by a wide range of [Cloud Infrastructure](https://www.oracle.com/cloud/products.html) products. A commonly used infrastructure product is Cloud [Compute](https://www.oracle.com/cloud/compute/), which includes several options to support various types of I/O intensive workloads, high-performance computing (HPC), and artificial intelligence (AI). These options are represented by Cloud Compute Instances that include Virtual Machines (VMs), Bare Metal Servers, GPU optimized VMs and Bare Metal Servers, alongside Autonomous Linux instances, and instances optimized for Container Registries and Container Engines for Kubernetes.

[Oracle Cloud Infrastructure Compute VMs](https://www.oracle.com/cloud/compute/virtual-machines.html) are offered in many shapes with Oracle Compute Units (OCPUs) to support a wide range of workloads and software platforms, allowing for storage support customization from remote block storage to high local SSD storage capacity. 

At the core of Oracle Cloud Infrastructure are Bare Metal Servers capable of supporting Nested Virtualization when paired with a hypervisor such as KVM.

Oracle Cloud is also a paid offering, however, similar to several other cloud providers, Oracle too has a [Free Tier](https://www.oracle.com/cloud/free/?source=:ow:o:s:po:0917CloudSEEDIF&intcmp=:ow:o:s:po:0917CloudSEEDIF) offering. The free tier is composed of a set of Always Free services, coupled with free credit towards additional eligible infrastructure services, available for a limited time upon signing up. The free tier encourages users to navigate the Oracle Cloud services and to become familiar with the most popular options and features.

#### == **Features and Tools** ==

Oracle Cloud Infrastructure VMs offer flexible performance paired with strong isolation thanks to cloud-optimized hardware, software, and networking, all at an advantageous cost. 

Infrastructure VMs are offered in several shapes, which are determined by the virtual hardware profile of the instance:

 * Standard, or general-purpose instances, offering a balance between compute, memory, and networking resources, suitable for most common use cases. 
 * Dense IO, or high-performance instances, paired with large local non-volatile memory express (NVMe) SSD storage, suitable for large database applications.

When provisioning Cloud VMs, users are able to manage additional instance aspects and related services:  

 * Flexible image management by allowing users to choose between images based on enterprise Linux distributions or Windows Server, to bring their own custom guest operating systems, or to choose an image from an Oracle partner
 * Low latency block storage for boot volumes, for increased performance and reliability, and to facilitate backups and restores
 * Secure and flexible network through fully customizable private Virtual Cloud Networks (VCN)
 * High availability by distributing application deployments in multi-regions, multi-availability domains or multi-fault domains, ensuring fault isolation, and low latency across availability domains.

#### **Demo: Creating and Destroying an Instance using Oracle Cloud Compute Service**

Let's take a look at the following demo, which illustrates an example of how to create and destroy an instance on Oracle Cloud using the Cloud Infrastructure Compute service.

Creating and Destroying an Instance using Oracle Cloud Compute Service

#### == **Benefits of Using Oracle Cloud Compute Virtual Machines** ==

Key benefits of using Oracle Cloud Compute service are:

 * It is an easy-to-use IaaS solution.
 * It is flexible, reliable, and scalable.
 * It provides a secure and robust functionality for your compute resources.
 * It enables automation.
 * It is cost-effective: only pay for the time and resources used.
 * It is designed to work in conjunction with other Oracle Cloud components and services.
 * It provides specialized instances for workloads, high graphics capability (GPU), high input/output (I/O), high performance computing (HPC), and artificial intelligence (AI).


### **OpenStack**

#### == **Introduction to OpenStack** ==

Earlier in this chapter, we have seen examples for consuming the services of different cloud providers to provision our infrastructure. What if we want to become a cloud provider and offer cloud computing services?

With [OpenStack](https://www.openstack.org/), we can build a cloud computing platform for public and private clouds. OpenStack was started as a joint project between [Rackspace](https://www.rackspace.com/) and [NASA](http://www.nasa.gov/) in 2010. In 2012, a non-profit corporate entity, the OpenStack Foundation, was formed and it is managing it since then while it receives the support of more than 500 organizations. OpenStack is an open source software platform released under an [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0).

In addition to providing an IaaS solution, OpenStack has evolved over time to provide other services, such as Database, Storage, and Networking.

#### == **Components and Features** ==

The modular nature of OpenStack allows users to design and implement components for specific features or functionality. OpenStack components provide APIs for accessing infrastructure resources by cloud end users. The major components of OpenStack are:

  * [**Nova**](https://www.openstack.org/software/releases/train/components/nova)
    Compute service that implements scalable and on-demand access to compute resources, including bare metal, virtual machines, and containers.
  * [**Ironic**](https://www.openstack.org/software/releases/train/components/ironic)
    Bare metal provisioning service part of Hardware lifecycle services.
  * [**Swift**](https://www.openstack.org/software/releases/train/components/swift)
    Object store part of Storage services provides a highly available, distributed, eventually consistent object/blob store.
  * [**Cinder**](https://www.openstack.org/software/releases/train/components/cinder)
    Block storage part of Storage services provides an abstraction layer over the management of block storage devices through a self-service API.
  * [**Manila**](https://www.openstack.org/software/releases/train/components/manila)
    Shared filesystem part of Storage services, provides coordinated access to shared or distributed file systems.
  * [**Neutron**](https://www.openstack.org/software/releases/train/components/neutron)
    Networking service, a Software Defined Networking (SDN) delivering networking as a service (NaaS) in virtual compute environments.
  * [**Octavia**](https://www.openstack.org/software/releases/train/components/octavia)
    Load balancer part of Networking services delivers on-demand and horizontal scaling load balancing as a service (LBaaS) to fleets of VMs, containers, or bare-metal serves.
  * [**Keystone**](https://www.openstack.org/software/releases/train/components/keystone)
    Identity service part of Shared services, provides authentication, service discovery, and authorization. It supports LDAP, OAuth, OpenID Connect, SAML, and SQL.
  * [**Glance**](https://www.openstack.org/software/releases/train/components/glance)
    Image service part of Shared services, provides VM image discovery, registration, and retrieval.
  * [**Searchlight**](https://www.openstack.org/software/releases/train/components/searchlight)
    Indexing and search service part of Shared services, provides near real-time indexing and flexible search capability, including authentication and RBAC authorization for data protection. 
  * [**Heat**](https://www.openstack.org/software/releases/train/components/heat)
    Orchestration service provides orchestration of infrastructure resources for cloud applications that also supports autoscaling.
  * [**Senlin**](https://www.openstack.org/software/releases/train/components/senlin)
    Clustering service part of Orchestration services eases the orchestration of clusters of similar objects. 
  * [**Magnum**](https://www.openstack.org/software/releases/train/components/magnum)
     Container orchestration engine provisioning service part of Workload provisioning services provisions Docker Swarm, Kubernetes or Apache Mesos to run on a cluster of VMs or bare-metal servers.
  * [**Freezer**](https://www.openstack.org/software/releases/train/components/freezer)
    Backup, Restore, and Disaster Recovery service part of Application lifecycle services, provides efficient and flexible block-based backups, file-based incremental backups, synchronized backups over multiple nodes, while it supports multiple OSes (Linux, Windows, Mac OS-X). 
  * [**Horizon**](https://www.openstack.org/software/releases/train/components/horizon)
    Dashboard service part of Web frontend services provides an extensible web-based user interface to manage OpenStack services.
  * [**Ceilometer**](https://www.openstack.org/software/releases/train/components/ceilometer)
    Metering and data collection service part of Monitoring tools, provides efficient data collection, normalization, and transformation for telemetry purposes.
  * [**Monasca**](https://www.openstack.org/software/releases/train/components/monasca)
    Monitoring service part of Monitoring tools, provides highly-scalable, fault-tolerant monitoring as a service solution (MaaS), together with high-speed metrics processing, querying, alarm, and notification engines.
  * [**Cloudkitty**](https://www.openstack.org/software/releases/train/components/cloudkitty)
    Billing and chargeback service part of Billing and Business logic services introduces rating as a service (RaaS) to convert rating metrics into service pricing. 
  * [**Tricircle**](https://www.openstack.org/software/releases/train/components/tricircle)
    Networking automation for multi-region deployments part of Multi-region tools, for high-availability, traffic isolation, and ISP redundancy across Neutron.

#### **Demo: Deploying an Instance with OpenStack**

In the following example, we will show how you can deploy an instance with OpenStack.

Deploying an Instance with OpenStack

#### == **Benefits of Using OpenStack** ==

Key benefits of using OpenStack are:

 * It is an open source solution.
 * It is a cloud computing platform for public and private clouds.
 * It offers a flexible, customizable, vendor-neutral environment. 
 * It provides a high level of security.
 * It provides high availability.
 * It supports Artificial Intelligence (AI) and Machine Learning (ML).
 * It facilitates automation throughout the stages of the cloud lifecycle.
 * It is cost-effective, achieved by reducing system management overhead and avoiding vendor lock-in.

----

## Chapter 3. Platform as a Service (PaaS)

### **Introduction and Learning Objectives**

#### == **Introduction** ==

**Platform as a Service** (PaaS) is a cloud service model representing a class of cloud computing services that allow users to develop, run, and manage applications while concealing the tasks involved in the management of the underlying infrastructure. With PaaS, users are able to concentrate their efforts on building their applications, which is a great benefit to developers.

Users have a choice between managed and self-managed PaaS solutions. Users can either use managed PaaS solutions hosted by cloud computing providers like Amazon AWS, Microsoft Azure, Google Cloud Platform (GCP), IBM Cloud, and Oracle Cloud or deploy an on-premise PaaS as a self-managed solution, using a platform such as [Red Hat OpenShift](https://www.openshift.com/products).

PaaS can be deployed on top of IaaS, or, independently on VMs, bare-metal servers, and containers.

In this chapter, we will take a closer look at some of the PaaS providers and their features. We will also provide a demo video for each one of them.

#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Explain the concept of Platform as a Service (PaaS).
 * Distinguish between different PaaS providers.
 * Deploy an application on top of different PaaS providers: Cloud Foundry, OpenShift, Heroku.

### **Cloud Foundry**



#### == **Introduction to Cloud Foundry** ==

[Cloud Foundry](https://www.cloudfoundry.org/) (CF) is an open source Platform as a Service (PaaS) framework aimed at developers, that is portable to any cloud, highly interoperable, has an architecture supporting any programming language, and integrates easily with various cloud and on-premise tools. In addition to supporting any type of application, it provides means for these applications to access external resources through application services gateway.

CF can be deployed on-premise or on [IaaS](https://www.cloudfoundry.org/thefoundry/#providers), such as Amazon AWS, Microsoft Azure, Google Cloud Platform (GCP), IBM Cloud, OpenStack, and VMware vSphere. Cloud Foundry is available through several certified commercial [distributions](https://www.cloudfoundry.org/thefoundry/#cert-distros) as well, such as IBM Cloud Foundry, SAP Cloud Platform, and VMware Tanzu (formerly known as Pivotal Cloud Foundry).

CF aims to address challenges faced by developers through a collection of customized technologies such as KubeCF, App Runtime, Container Runtime, BOSH, Quarks, Eirini, and Open Service Broker API.

#### Features

The following are key characteristics of Cloud Foundry:

 * Application portability
 * Application auto-scaling
 * Application isolation
 * Centralized platform management
 * Centralized logging
 * Dynamic routing
 * Application health management
 * Role-based application deployment
 * Horizontal and vertical scaling
 * Security
 * Support for different IaaS platforms.

#### Cloud Foundry BOSH

Cloud Foundry (CF) utilizes two runtimes to separately manage and run applications and containers. These are the **Cloud Foundry Application Runtime** and **Cloud Foundry Container Runtime**. 

The tool that manages both runtimes for CF is [Cloud Foundry BOSH](https://www.cloudfoundry.org/bosh/). Although it works in the background and developers may not be exposed to the runtime management tool, BOSH is a cloud-agnostic open source tool for release engineering, deployment, and lifecycle management of complex distributed systems.

#### Cloud Foundry Application Runtime (CFAR)

[Cloud Foundry Application Runtime](https://www.cloudfoundry.org/application-runtime/) (CFAR) allows developers to run applications written in any language or framework on the cloud of their choice. 

CFAR includes a command-line tool that enables the interaction with the Application Runtime. It also uses [*buildpacks*](https://docs.cloudfoundry.org/buildpacks/) to provide the framework and runtime support for applications. Buildpacks are programming language-specific and include information on how to download dependencies and configure specific applications. Below are some of the languages supported by well-defined buildpacks:

 * Java
 * Python
 * Go
 * Ruby
 * .Net
 * Node.js
 * PHP.

In addition to system buildpacks, developers may build new buildpacks or customize the existing ones to meet architectural requirements. CFAR is also responsible for the build and running phases of the application, together with incoming network traffic routing, identity management, monitoring, logging, and integration with external services.

As presented in the graphic below, the CFAR platform can manage the entire workflow and lifecycle of an application, from routing to logging. In addition, by using the Open Service Broker API, the application may be connected with external services like databases or third-party SaaS providers. 

![The CF Application Runtime Platform Architecture](LFS151-cf_architecture_block.png)

ref from https://docs.cloudfoundry.org/concepts/architecture/

#### Cloud Foundry Container Runtime (CFCR)


[Cloud Foundry Container Runtime](https://www.cloudfoundry.org/container-runtime/) (CFCR) offers Cloud Foundry users the alternative to deploy cloud-native, developer-built, pre-packaged applications in containers. The CFCR platform manages containers on a Kubernetes cluster, which in turn is managed by Cloud Foundry BOSH. Kubernetes is a container orchestrator that operates in conjunction with a container runtime and automates deployment, scaling, and management of containerized applications. CF BOSH adds high availability, scaling, VM healing, and upgrades support to the Kubernetes cluster.

![The CF Application Runtime Platform Architecture](LFS151-CF_Container_Runtime.png)

#### KubeCF


[KubeCF](https://www.cloudfoundry.org/kubecf/) is a Kubernetes native distribution of Cloud Foundry. That is a distribution of the Cloud Foundry Application Runtime (CFAR) optimized for Kubernetes. It allows developers on CFAR to also enjoy the benefits Kubernetes has to offer, just as developers using CF Container Runtime do. Thus it bridges classic features of CF available through CFAR with emerging cloud-native workload and infrastructure management offered by Kubernetes.

KubeCF operates in conjunction with two incubating projects of CF: Project Quarks and Project Eirini.

[Project Quarks](https://www.cloudfoundry.org/project-quarks/) deploys and manages built releases. It packages CFAR as containers rather than VMs, thus allowing for effortless deployments to Kubernetes. As a result, users experience reduced infrastructure capacity which translates into cost savings. 

[Project Eirini](https://www.cloudfoundry.org/project-eirini/) allows the use of Kubernetes as the underlying container scheduler for users of CFAR, with no effect on the user experience.

#### Demo: Deploying an Application on Cloud Foundry

In the following video we will show how you can deploy an application on Cloud Foundry.

NOTE: The Pivotal Cloud Foundry (PCF) Dev environment installer, used in the following demo video, is found at https://network.pivotal.io/products/pcfdev.

Deploying an Application on Cloud Foundry


#### Benefits of Using Cloud Foundry

Key benefits of using Cloud Foundry are:

 * It is an open source platform, but there are also many commercial Cloud Foundry providers. 
 * It offers centralized platform management.
 * It enables horizontal and vertical scaling.
 * It provides infrastructure security.
 * It provides multi-tenant compute efficiency.
 * It offers support for multiple IaaS providers.
 * It supports the full application lifecycle: development, testing, and deployment, thus enabling a continuous delivery strategy. It also provides integration with CI/CD tools.
 * It allows for application virtualization using containers for application isolation.
 * It leverages Kubernetes' workload management capabilities to optimize application deployments in containers. 
 * It is a simple and flexible solution, supported by an extensive community of developers.
 * It reduces the chance of human errors.
 * It is cost-effective, reducing the overhead for Ops teams.


### **OpenShift**

#### == **Introduction to OpenShift** ==


[OpenShift](https://www.openshift.com/) is an open source PaaS solution provided by Red Hat. It is built on top of the container technology orchestrated by [Kubernetes](https://kubernetes.io/). OpenShift can be deployed in the cloud or locally on a full-fledged Linux OS or on a Micro OS specifically designed to run containers and Kubernetes. 

Red Hat offers several [products](https://www.openshift.com/products) under OpenShift :

  * **OpenShift Online**
        Deploy and host your applications on the OpenShift public platform managed by Red Hat. You can deploy up to eight services for free under the Starter plan or subscribe to the Pro plan and pay for resources used in addition to the base subscription price.
  * **OpenShift Dedicated** 
        Take advantage of your own private OpenShift cluster on AWS and GCP, which is a hosted service supported by Red Hat. Two additional fully managed dedicated services are: Azure Red Hat OpenShift, jointly engineered, operated and supported by Microsoft and Red Hat, and Red Hat OpenShift on IBM Cloud supported by Red Hat together with IBM.
  * **OpenShift Container Platform** 
        Create your own self-managed, private, secure, consistent PaaS across cloud and on-premise.
  * **OpenShift Kubernetes Engine**
        An entry-level solution promoting advantages of OpenShift over other Kubernetes solutions, supported by Red Hat.

All the upstream development on OpenShift happens on [GitHub](https://github.com/openshift/origin) and it is referred to as [OpenShift Origin](https://github.com/openshift/okd).

#### == **Features** ==

OpenShift allows developers to deploy containerized applications. Features such as application images and QuickStart application templates allow for one-click application deployments.

Thanks to Kubernetes, OpenShift receives all the features offered by the container orchestration platform, such as adding or removing nodes at runtime, persistent storage, and auto-scaling.

OpenShift uses the *Source-to-Image* (S2I) framework that enables users to build container images from the source code repository allowing for fast and easy application deployments.

OpenShift integrates well with Continuous Deployment tools to deploy applications as part of the CI/CD pipeline.

Applications are managed with ease through command line (CLI) tools, web user interface (UI), and integrated development environment (IDE) available for OpenShift.

#### == **Installing OpenShift** ==

Red Hat engineering and support teams are available to assist with installation and configuration issues whether opting for the OpenShift Online Pro offering or one of the hosted service offerings such as OpenShift Dedicated, Azure OpenShift, or OpenShift on IBM Cloud. OpenShift is hosted by the major cloud services providers and can be easily setup on Amazon AWS, Google Cloud Platform (GCP), Microsoft Azure, and IBM Cloud.

Detailed instructions for installing and configuring OpenShift products are given in the official [documentation]( https://docs.openshift.com/).

#### **Demo: Deploying an Application on OpenShift**

In the following demo, we will illustrate how you can deploy an application in the OpenShift environment. 

Deploying an Application on OpenShift

#### == **Benefits of Using OpenShift** ==

Key benefits of using OpenShift are:

 * It is an open source PaaS solution.
 * It uses Kubernetes for workload management.
 * It can scale applications easily and quickly.
 * It provides integration with CI/CD tools.
 * It is a simple and flexible solution, supported by an active community of developers.
 * It enables developers to be more efficient and productive, allowing them to quickly develop, host, and scale applications in the cloud in a streamlined and standardized manner.
 * It enables application portability, meaning that any application created on OpenShift can run on any platform that supports Docker.
 * OpenShift users have the choice to deploy their applications on top of physical or virtual infrastructures, as well as on public, private or hybrid clouds.
 * With OpenShift, you can easily build applications with integrated service discovery and persistent storage.
 * You can use the web console and the CLI to build and monitor applications.
 * OpenShift integrated Docker registry, automatic edge load balancing, cluster logging, and integrated metrics.


### **The Heroku Platform**

#### == **Introduction to Heroku** ==

[Heroku](http://www.heroku.com/) is a fully-managed container-based cloud platform, with integrated data services and a strong ecosystem. Heroku is used to deploy and run modern applications. It is a [*Salesforce*](https://www.salesforce.com/) company.

Heroku offers multiple products, however, its core remains the [Heroku Platform](https://www.heroku.com/platform), a PaaS platform used to deploy applications. The Heroku Platform supports the following popular languages and frameworks:

 * Node.js
 * Ruby
 * Python
 * Go
 * PHP
 * Clojure
 * Scala
 * Java,

but it can be easily extended to other languages and frameworks through custom buildpacks.

#### == **Heroku Core Concepts** ==

A strict Heroku-centric development and deployment workflow needs to be followed once the decision has been made to use Heroku. However, the workflow is quite developer-friendly. Below are several core concepts of the workflow:

 * Applications should contain the source code, its dependency information and the list of named commands to be executed to deploy it, in a file called [*Procfile*](https://devcenter.heroku.com/articles/procfile).
 * For each supported language, it has a pre-built image which contains a compiler for that language. This pre-built image is referred to as a [*buildpack*](https://devcenter.heroku.com/articles/buildpacks). [Multiple buildpacks](https://devcenter.heroku.com/articles/using-multiple-buildpacks-for-an-app) can be used together. We can also create a custom buildpack.
 * While deploying, we need to send the application's content to Heroku, either via Git, GitHub, or via an API. Once the application is received by Heroku, a buildpack is selected based on the language of preference.
 * To create the runtime which is ready for execution, we compile the application after fetching its dependency and configuration variables on the selected buildpack. This runtime is often referred to as a [slug](https://devcenter.heroku.com/articles/slug-compiler).
 * We can also use third-party [add-ons](https://elements.heroku.com/addons) to enable access to value-added services like logging, caching, monitoring, etc.
 * A combination of slug, configuration variables, and add-ons is referred to as a release, on which we can perform upgrade or rollback.
 * Depending on the process-type declaration in the Procfile, a virtualized UNIX container is created to serve the process in an isolated environment, which can be scaled up or down, based on the requirements. Each virtualized UNIX container is referred to as a [dyno](https://devcenter.heroku.com/articles/dynos). Each dyno gets its own ephemeral storage.
 * Dyno Manager manages dynos across all applications running on Heroku.


#### == **Features** ==

The Heroku Platform allows users to deploy, run, and manage applications written in multiple programming languages or frameworks, from source code that may also include dependencies. 

By encapsulating the application in a dyno, which is a lightweight and secure container, the application can be scaled based on demand.

Application configuration can be decoupled from the application source code, allowing for application customization based on the environment where the application is deployed. Configuration customization is achieved through [config vars](https://devcenter.heroku.com/articles/config-vars), which combined with the application slug, produce a [release](https://devcenter.heroku.com/articles/releases).

It is a very rich ecosystem, and it allows us to extend the functionality of an application with [add-ons](https://devcenter.heroku.com/articles/add-ons) available in the [Elements Marketplace](https://elements.heroku.com/addons). Add-ons allow us to easily integrate our applications with other fully-managed cloud services such as storage, database, monitoring, logging, pipelines, email, message queue, security, and networking.

Applications deployed on the Heroku Platform can be easily integrated with Salesforce.

#### **Demo: Deploying an Application on the Heroku Platform**

In the following video, we will show how you can deploy a sample application on the Heroku platform.

Deploying an Application on the Heroku Platform

#### **Benefits of Using Heroku**

Key benefits of using the Heroku platform are:

 * It enables a development-friendly workflow.
 * It provides a rich ecosystem, allowing you to extend the functionality through add-ons.
 * It supports Continuous Integration and Continuous Delivery pipelines.
 * It is easy to start and allows us to quickly scale our applications, both horizontally and vertically. 
 * It provides the ability to automate backups


----

## Chapter 4. Containers


### **Introduction and Learning Objectives**

#### == **Introduction** ==

When multiple applications are deployed on one host, developers are faced with a challenge - how to isolate applications from one another to avoid conflicts between dependencies, libraries, and runtimes.

[Operating-System-level virtualization](https://en.wikipedia.org/wiki/OS-level_virtualization) allows us to run multiple isolated user-space instances in parallel. These user-space instances include the application source code, required libraries, and the required runtime to run the application without any external dependencies. These user-space instances are referred to as **containers**. They solve the dependencies, libraries, and runtimes conflict challenges developers have been facing in the past.

In this chapter, we will talk about containers, as well as provide details on basic Docker operations.

#### Learning Objectives


By the end of this chapter, you should be able to:

 * Discuss containers and their runtimes. 
 * Describe the basic Docker operations.
 * Understand how Project Moby helps create container platforms like Docker. 


### **Containers**

#### == **Introduction**  ==

Typically a developer's end goal to run an application is not too difficult to achieve. However, challenges are encountered with portability, when the application needs to work consistently on multiple hardware and platforms, such as the developer's laptop, VMs, data centers, public and private clouds, from the development environment all the way to production.

![Running an Application](LFS151-Running_an_Application.png)

Developers prefer not to trouble themselves with the portability and consistency aspects of applications and expect the application to just work, regardless of the underlying platform. By using a container technology like Docker, the application and all its dependencies can be bundled in a box.

![A Docker Container](LFS151-Docker_Container.jpeg)

The box can be shipped to different platforms and it will run identically on each one of them.

#### == **Images and Containers** ==

In the container world, this box containing our application source code and all its dependencies and libraries is referred to as an **image**. A running instance of this box is referred to as a **container**. We can run multiple containers from the same image.

An image contains the application, its dependencies, and the user-space libraries. User-space libraries like glibc enable switching from the user-space to the kernel-space. An image does not contain any kernel-space components.

When a container is created from an image, it runs as a process on the host's kernel. It is the host kernel's job to isolate the container process and to provide resources to each container.

#### == **The Container Technology: Building Blocks** ==

We will now take a look at some of the building blocks of the container technology, provided by the Linux kernel.

**Namespaces** 
A namespace wraps a particular global system resource like network, process IDs in an abstraction, that makes it appear to the processes within the namespace that they have their own isolated instance of the global resource. The following global resources are namespaced:

 * pid - provides each namespace to have the same PIDs. Each container has its own PID 1.
 * net - allows each namespace to have its network stack. Each container has its own IP address.
 * mnt - allows each namespace to have its own view of the filesystem hierarchy.
 * ipc - allows each namespace to have its own interprocess communication.
 * uts - allows each namespace to have its own hostname and domain name.
 * user - allows each namespace to have its own user and group ID number spaces. A root user inside a container is not the root user of the host on which the container is running.

**cgroups**
Control groups are used to organize processes hierarchically and distribute system resources along the hierarchy in a controlled and configurable manner. The following cgroups are available for Linux:

 * blkio
 * cpu
 * cpuacct
 * cpuset
 * devices
 * freezer
 * memory.

**Union filesystem**
The Union filesystem allows files and directories of separate filesystems, known as layers, to be transparently overlaid on top of each other, to create a new virtual filesystem. An image used in Docker is made of multiple layers and, while starting a new container, we merge all those layers to create a read-only filesystem. On top of a read-only filesystem, a container gets a read-write layer, which is an ephemeral layer and it is local to the container.

#### == **Container Runtimes** ==

Namespaces and cgroups have existed in the Linux kernel for quite some time, but consuming them to create containers was not easy. After Docker was launched in 2013, containers started to become mainstream. Docker hid all the complexities in the background and came up with an easy workflow to share and manage both images and containers.

Docker achieved this level of simplicity through a collection of tools that interact with a container runtime on behalf of the user. The container runtime ensures containers portability, offering a consistent environment for containers to run, regardless of the infrastructure. Some of the container runtimes are provided below:

**runC**
Over the past few years, we have seen rapid growth in the interest and adoption of container technologies. Most of the cloud providers and IT vendors offer support for containers. To make sure there is no vendor locking and no inclination towards a particular company or project, IT companies came together and formed an open governance structure, called The [Open Container Initiative](https://www.opencontainers.org/) (OCI), under the auspices of [The Linux Foundation](https://www.linuxfoundation.org/). The governance body came up with both [runtime](https://github.com/opencontainers/runtime-spec) and [image](https://github.com/opencontainers/image-spec) specifications to create standards on the Operating System process and application containers. runC is the CLI tool for spawning and running containers according to these specifications.

**containerd**
containerd is an OCI-compliant container runtime with an emphasis on simplicity, robustness, and portability. It runs as a daemon and manages the entire lifecycle of containers. It is available on Linux and Windows. Docker, which is a containerization platform, uses containerd as a container runtime to manage runC containers.

**rkt**
rkt (pronounced "rock-it") is an open source, Apache 2.0-licensed project from CoreOS. It implements the [App Container specification](https://github.com/rkt/rkt/blob/master/Documentation/app-container.md).

**CRI-O**
CRI-O is an OCI-compatible runtime, which is an implementation of the Kubernetes [Container Runtime Interface](https://kubernetes.io/blog/2016/12/container-runtime-interface-cri-in-kubernetes/) (CRI). It is a lightweight alternative to using Docker as the runtime for Kubernetes.

#### == **Containers vs VMs** ==

A virtual machine runs on top of a [hypervisor](https://en.wikipedia.org/wiki/Hypervisor), which virtualizes a host system's hardware by emulating CPU, memory and networking hardware resources so that a guest OS can be installed on top of them. Different kinds of guest OSes can run on top of one hypervisor. Between an application running inside a guest OS and in the outside world, there are multiple layers: the guest OS, the hypervisor, and at times the host OS.

![Docker Container vs VMs](/LFS151-Docker_Containers_vs._VMs.png)

In contrast to VMs, containers run directly as processes on the host OS. There is no indirection as we see in VMs, which helps containers to achieve near-native performance. Also, as the containers have a very light footprint, it becomes easier to pack a higher number of containers than VMs on the same physical machine. However, as containers run on the host OS, we need to make sure containers are compatible with the host OS.

#### == **Docker** ==


[Docker, Inc.](https://www.docker.com/) is a company that provides the **Docker Containerization Platform** to run applications using containers. It comes in two versions:

 * [Docker Enterprise](https://docs.docker.com/ee/)
    It is a paid, enterprise-ready container platform created to run and manage containers, currently part of [Mirantis](https://www.mirantis.com/software/docker/docker-enterprise/). 
 * [Docker Engine - Community](https://docs.docker.com/engine/)
    It is a free platform used to run and manage containers.

Docker has a client-server [architecture](https://docs.docker.com/get-started/overview/#docker-architecture), with a Docker Client connecting to a Docker Host server to execute commands.

#### == **Basic Docker Operations** ==

While not an exhaustive list, the basic Docker operations are enumerated below:

  * List images available in the local cache:
    <code>$ docker image ls</code>
    
  * Pulling an alpine image from the registry into the local cache:
    <code>$ docker image pull alpine</code>

  * Run a container from an image (if the image is not found in local cache it will be pulled from registry).
    The run command is the equivalent of `docker container create` followed by `docker container start` :
    <code>$ docker container run -it alpine sh </code>

  * Run a container in the background (-d option) from an nginx image:
    <code>$ docker container run -d nginx </code>

  * List only running containers:
    <code>$ docker container ls </code> 

  * List all containers: 
    <code>$ docker container ls -a </code>

  * Inject a process inside a running container. This will start a bash shell in interactive (-i option) terminal (-t option) mode inside the container:
    <code>$ docker container exec -it <container_id/name> bash </code>

  * Stop a running container:
    <code>$ docker container stop <container id/name></code>

  * Delete a stopped container:
    <code>$ docker container rm  <container id/name></code>


#### **Demo: Basic Operations with Docker**

The following video illustrates some of the basic operations with Docker.

Basic Operations with Docker

#### == **Benefits of Using Containers** ==

Key benefits of using containers are:

 * They have a very light footprint.
 * They can be deployed very fast (within milliseconds).
 * They are a flexible solution, as they can run on any computer, infrastructure, or cloud environment.
 * They can be scaled up or down with ease.
 * There is a very rich ecosystem built around them.
 * Problem containers can be easily and quickly isolated when troubleshooting and solving problems.
 * Containers use less memory and CPU than VMs running similar workloads.
 * Increased productivity with reduced overhead.
 * They offer portability and consistency.


### Project Moby

#### == **Project Moby** ==

As we know, a container platform like Docker runs on different platforms and architectures: bare metal (both x86 and ARM), Linux, Mac OS-X, and Windows. We can also find pre-configured VM images to run Docker on popular cloud infrastructures and virtualization providers.

From the user perspective, the experience is seamless, regardless of the underlying platform. However, behind the scenes, components such as the container runtime, networking, and storage are connected to ensure this high-quality experience. Open source projects like [containerd](https://containerd.io/) and [libnetwork](https://github.com/moby/libnetwork) are part of the container platform and have their own release cycles and governing models. The question is, however, how can we take those individual components and build a container platform like Docker?


[Project Moby](https://mobyproject.org/) is the answer. It is an open source project that provides a framework for assembling different container systems to build a container platform like Docker. Individual container systems provide features such as image, container, and secret management.

![Project Moby](LFS151-moby.png)

Moby is particularly useful if you want to build your container-based system or just want to experiment with the latest container technologies. It is not recommended for application developers and newbies who are looking for an easy way to run containers.

[LinuxKit](https://github.com/linuxkit), a tool to build minimal Linux distributions to run containers, uses Moby. You can find a few examples at its [GitHub repository](https://github.com/linuxkit/linuxkit).

----

## Chapter 5. Containers: Micro OSes for Containers

### **Introduction and Learning Objectives**

#### == **Introduction** ==

The current technological trend is to run applications in containers. In this context, it makes a lot of sense to eliminate all the packages and services of the host Operating System (OS), which are not essential for running containers. With that in mind, various vendors have come forward with specialized minimal OSes to run just containers.

Once we remove the packages that are not essential to boot the base OS and to run container-related services, we are left with specialized OSes, which are referred to as Micro OSes for containers. Some examples of Micro OSes are:

 * Alpine Linux
 * Atomic Host 
 * Fedora CoreOS (formerly known as Red Hat CoreOS)
 * RancherOS 
 * Ubuntu Core
 * VMware Photon.

![Micro OSes to Run Containers](LFS151-Micro_OSes_for_Containers_updated.png)

In this chapter, we will explore several of the most utilized Micro OSes.

#### == **Learning Objectives** ==


By the end of this chapter, you should be able to:

 * Discuss the characteristics and functionality of Micro OSes, which are specially designed to run containers.
 * Describe Micro OSes designed to run containers: Alpine Linux, Atomic Host, Fedora CoreOS, RancherOS, Ubuntu Core, and VMware Photon.
 * Deploy containers and containerized applications on Micro OSes.

### **Alpine Linux**

#### == **Introduction to Alpine Linux** ==

[Alpine Linux](https://www.alpinelinux.org/about/) is an independent, non-commercial, Linux distribution designed for security, simplicity and resource efficiency.

Although small at about 8 MB per container, it is more resource-efficient than typical distributions. Users can control what binary packages to install, thus ensuring a small yet efficient system.

Alpine Linux uses its own package manager called apk, the OpenRC init system, and set-up scripts. Users can add packages as needed such as PVR, iSCSI storage controllers, a mail server container, or an embedded switch.

Alpine Linux was designed with security in mind with embedded proactive security features that prevent the exploitation of entire classes of zero-day and other vulnerabilities.

#### == **Alpine Linux Features** ==

Upon [installation](https://wiki.alpinelinux.org/wiki/Installation) completion, Alpine Linux makes available tools for the initial configuration of the system. Once prepared for reboot, it can be configured to boot in one of the three available runtime modes:

 * **diskless mode**
   The default mode, where the entire system runs from RAM.
 * **data mode**
   Mostly runs from RAM but mounts `/var` as a writable data partition.
 * **sys mode**
   The typical hard-disk install that mounts `/boot`, `swap`, and `/` .

Alpine Linux is available in many flavors:

 * **Standard**
    It requires a network connection.
 * **Extended**
    Includes the most used packages, and runs from RAM.
 * **Netboot**
    Includes kernel, initramfs, and modloop.
 * **Mini root filesystem**
    Minimal for containers and chroots.
 * **Virtual**
    Lighter kernel than Standard.
 * **Xen**
    Supports the Xen hypervisor.
 * **Raspberry Pi**
    Includes the Raspberry Pi kernel.
 * **Generic ARM**
    Includes the default ARM kernel with the uboot bootloader.

#### **Demo: Basic Operations with Alpine Linux**

In the following video, we will take a look at some basic operations that we can do with Alpine Linux.

Basic Operations with Alpine Linux

#### == **Benefits of Using Alpine Linux** ==

Key benefits of using Alpine Linux are:

 * It is a minimal OS designed to run containerized applications as well.
 * It is designed for security, simplicity, and resource efficiency.
 * It requires 8 MB as a container.
 * It requires 130 MB as a standalone minimal OS installation.
 * It provides increased security by compiling user binaries as Position Independent Executables (PIE) with stack smashing protection.
 * It can be installed as a container, on bare metal, as well as VMs.
 * It offers flavors optimized to support Xen and Raspberry Pi. 


### **Atomic Host**

#### Introduction to Atomic Host


https://www.projectatomic.io/download/
[Atomic Host](http://www.projectatomic.io/) is a lightweight operating system, assembled out of a specific RPM content. It allows us to run just containerized applications in a quick and reliable manner in private or public clouds. Atomic Host is available in three editions: Fedora Atomic Host, CentOS Atomic Host, or Red Hat Enterprise Linux (RHEL) Atomic Host.


Atomic Host is the core sub-project of [Project Atomic](http://www.projectatomic.io/docs/introduction/), together with other sub-projects, such as [Cockpit](https://cockpit-project.org/), and [Atomic Developer Bundle](https://github.com/projectatomic/adb-atomic-developer-bundle). Project Atomic and its sub-projects aim to re-design the operating system around principles of immutable infrastructure, based on the LDK (Linux, Docker, Kubernetes) stack, with many components being upstream components of [OpenShift Origin](https://www.okd.io/). The project maintains and makes use of additional open source tools such as [Buildah](https://github.com/containers/buildah/blob/master/README.md), [Kompose](https://kompose.io/), [Bubblewrap](https://github.com/containers/bubblewrap), and [skopeo](https://github.com/containers/skopeo/blob/master/README.md).

Atomic Host comes out-of-the-box with Kubernetes installed together with several Kubernetes utilities, such as **etcd**, and **flannel**.

However, at the time of this writing, the Atomic Host platform is slowly being replaced with [*Fedora CoreOS*](https://getfedora.org/coreos/). 

*ANNOUNCE* << https://www.projectatomic.io/download/ : Atomic Host Is Being Sunset : The Atomic Host platform is being replaced by CoreOS. >>

While Fedora is no longer building new versions of Atomic Host, but still provides updates for the Fedora Atomic Host version 29, the CentOS project plans to rebuild the RHEL 7-based versions of Atomic Host, but there is no plan for a version 8 of RHEL or CentOS Atomic Host.

#### Components of Atomic Host

Atomic Host has a very minimal base OS, but it includes components like **systemd** and **journald** to help its users and administrators. It is built on top of the following:

 * **rpm-ostree** 
    One cannot manage individual packages on Atomic Host, as there is no rpm or other related commands. To get any required service, you would have to start a respective container. Atomic Host has two bootable, immutable, and versioned filesystems; one is used to boot the system and the other is used to fetch updates from upstream. <code>*rpm-ostree*</code> is the tool to manage these two versioned filesystems.
 * **systemd** 
    It is used to manage system services for Atomic Host.
 * **Docker** 
    Atomic Host currently supports Docker as a container runtime.
 * **Kubernetes** 
    With Kubernetes, we can create a cluster of Atomic Hosts to run applications at scale.

Atomic Host also comes with a command called atomic. This command provides a high-level, coherent entry point to the system, and fills in the gaps that are not filled by Linux container implementations, such as upgrading the system to the new rpm-ostree, running containers with pre-defined docker run options using labels, verifying an image, etc.

Atomic Host can be managed using tools such as Cockpit. Cockpit is a server manager that makes it easy to administer your GNU/Linux servers via a web browser.


#### **Demo: Basic Operations with Atomic Host**

In the following video, we will take a look at some basic operations that we can do with Atomic Host.

Basic Operations with Atomic Host

#### == **Benefits of Using Atomic Host** ==

Key benefits of using Atomic Host are:

 * It is an OS specifically designed to run containerized applications.
 * It provides close-to-VM like isolation but increased flexibility and efficiency. 
 * It enables us to perform quick updates and rollbacks.
 * It provides increased security through namespaces, cgroups, and SELinux.
 * It can be installed on bare metal, as well as VMs.
 * It is available in Fedora, CentOS, and Red Hat Enterprise Linux editions.
 * Containers can be deployed with Kubernetes and CRI-O.
 * It integrates with Cockpit, a cross-cluster container hosts management tool.

### **Fedora CoreOS**

#### == **Introduction to Fedora CoreOS** ==


[Fedora CoreOS](https://getfedora.org/en/coreos/) (FCOS) is an open source project partnered with the Fedora Project. It was formerly known as [Red Hat CoreOS](https://coreos.com/) and CoreOS Container Linux prior to that. It combines the best of both CoreOS Container Linux and [Fedora Atomic Host](https://www.projectatomic.io/) (FAH) while aiming to provide the best *container host to run containerized workloads* securely and at scale.

Fedora CoreOS is a minimal operating system for running containerized workloads, that updates automatically and is available on multiple platforms. Although a container-focused operating system, by design CoreOS, is operable in both clusters and standalone instances. In addition, it is optimized to work with Kubernetes but it also works very well without the containerized workload orchestrator.

#### Components of Fedora CoreOS

Fedora CoreOS (FCOS) combines features of both CoreOS Container Linux and Fedora Atomic Host (FAH). In order to provide a robust container host to run containerized workloads securely and at scale, it integrates the following:

 * **Ignition** from CoreOS Container Linux
    A provisioning utility designed specifically for Core OS Container Linux, which allows users to manipulate disks during early boot, such as partitioning disks, formatting partitions, writing files, and configuring users. Ignition runs early in the boot process (in the initramfs) and runs its configuration before the userspace boot, which provides advanced features to administrators.
 * **rpm-ostree** from FAH
    One cannot manage individual packages on Atomic Host, as there is no rpm or other related commands. To get any required service, you would have to start a respective container. Atomic Host has two bootable, immutable, and versioned filesystems; one is used to boot the system and the other is used to fetch updates from upstream. rpm-ostree is the tool to manage these two versioned filesystems.
 * **SELinux hardening** from FAH 
    Containers secured with SELinux provide close-to-VM isolation, security, increased flexibility, and efficiency.

FCOS offers multiple installation methods:

 * **Cloud launchable** 
    To launch directly on Amazon's AWS platform.
 * **Bare metal and virtualized** 
    For bare-metal installs from ISO, PXE (Preboot Execution Environment) or Raw, and virtual installs on OpenStack, QEMU, or VMware.
 * **For cloud operators** 
    Optimized for the following cloud services providers: Alibaba Cloud, AWS, Azure, DigitalOcean, Exoscale, GCP, and OpenStack.

#### == **Benefits of Using Fedora CoreOS** ==

Key benefits of using Fedora CoreOS are:

 * It is an OS designed to run containerized applications, in both clustered environment or as stand-alone.
 * It enables us to perform quick updates and rollbacks.
 * It provides increased security through SELinux.
 * It can be installed on bare metal, virtual environments, and the cloud, or launched directly on AWS.
 * It combines features of both Fedora Atomic Host and CoreOS Container Linux.
 * It works well with Kubernetes.
 * It uses Ignition as a provisioning tool for early boot disk partitioning, formatting, and other administrative configuration tasks.

### **RancherOS**

#### == **Introduction to RancherOS** ==

[RancherOS](http://rancher.com/rancher-os/) is a lightweight and secure Linux distribution that is made of containers to manage other containers. It has the smallest footprints among Micro OSes because it only includes the software needed to run Docker, while all other OS features are pulled dynamically through Docker. By leveraging both Docker and Kubernetes to manage containers, RancherOS makes it simple to run containers at scale in development, test, and production environments. The containerized system services of the OS provide a very reliable and easy method to manage a container-ready environment.

RancherOS is a product provided by [Rancher](http://rancher.com/), a Kubernetes-as-a-Service (KaaS) provider supporting enterprise containerized workloads in multi-cluster Kubernetes environments.

#### == **Components of RancherOS** ==

RancherOS runs two separate Docker daemon instances. Just after booting, it starts the first Docker daemon instance, the System Docker, with PID 1 to run system containers such as console, dhcp, ntpd, syslog, and udev.

In order to run user-level containers, the System Docker daemon creates a service to start the second Docker daemon instance, called Docker, that isolates the system containers from regular user commands.

![RancherOS Architecture](LFS151-Figure_6.4-_RancherOS_Architecture.png)

RancherOS also includes a CLI utility called <code>ros</code>, which can be used to control and configure the system.

#### **Demo: Starting Containers on RancherOS**

The following demo will illustrate how you can start containers on RancherOS.

Starting Containers on RancherOS

#### == **Benefits of Using RancherOS** ==

Key benefits of using RancherOS are:

 * It is a minimalist OS, by eliminating unnecessary libraries and services.
 * It decreases complexity and boot time.
 * It is highly secure due to a small code base and a decreased attack surface.
 * It runs directly on top of the Linux kernel.
 * It isolates user-level containers from system containers by running two separate Docker daemon instances.
 * It enables us to perform updates and rollbacks in a simple manner.
 * We can use the Rancher platform to set up Kubernetes.
 * It boots containers within seconds.
 * It automates OS configuration with cloud-init.
 * It can be customized to add custom system Docker containers using the cloud-init file or Docker Compose.


### **Ubuntu Core**

#### == **Introduction to Ubuntu Core** ==

[Ubuntu Core](https://ubuntu.com/core) is a lightweight version of [Ubuntu](https://ubuntu.com/), predominantly designed for IoT devices but also found in large container deployments. In comparison with other container OSes, its size of around 260MB places Ubuntu Core at the top of container OSes, besting CentOS Atomic and Fedora CoreOS. Similar to Ubuntu Server and Ubuntu Desktop, Ubuntu Core works with software packages called snaps.  

Security is a top concern for the designers of Ubuntu Core, implemented by features such as:

 * Hardened security with immutable packages and persistent digital signatures. 
 * Strict application isolation.
 * Reduced attack surface by keeping a small OS, stripped down to bare essentials.
 * Automatic vulnerability scanning of packages.

In addition, Ubuntu Core was designed with extreme reliability, implemented by:

 * Transactional updates that increase OS and data resiliency by allowing automated rollbacks when errors are encountered.  
 * Automated restore points to allow returns to the last working boot in the case of an unsuccessful kernel update.
 * Consistent application data snapshots.

Ubuntu Core was built for the enterprise by including secure app store management and license compliance. Developers using Core as their platform enjoy cross-platform portability of snaps from Desktop and Server to Core, together with CI/CD pipeline support and integration with Travis.

#### == **Components of Ubuntu Core** ==

Ubuntu Core is designed to run on bare-metal, on hypervisors such as KVM, or a wide range of hardware including Raspberry Pi and Qualcomm Snapdragon 410c. Its top features are:

 * Immutable image for simple and consistent installation and deployment.
 * Isolated applications run with explicit permissions, such as read-only access to the filesystem.
 * Transactional updates for signed, autonomous, atomic, and flexible updates.
 * Security implemented at snap level, from build and distribution to deployment.

Application security and isolation is implemented through:

 * AppArmor
 * Seccomp.

Snaps are secure, isolated, dependency-free, portable, software packages for Linux. They even include their own filesystems. Snaps benefits include:

 * Automatic updates.
 * Automated recovery in the case of failed updates.
 * Critical update provision for unscheduled updates.
 * Flexible hardware and network conditions support for unpredictable systems, including redundancy for roll-backs, and autonomous bootstrapping.

The snap packaging environment includes the following:

 * snap - the application package format and the command line interface (CLI)
 * snapd - the background service managing and maintaining snaps
 * snapcraft - the framework including the command to build custom snaps
 * Snap Store - the repository to store and share snaps.

There are several types of snap included in Ubuntu Core:

 * kernel - defines the Linux kernel
 * gadget - defines specific system properties
 * core - the execution environment for application snaps
 * app - including applications, daemons such as snapd, and various tools. 

#### == **Benefits of Using Ubuntu Core** ==

Key benefits of using Ubuntu Core are:

 * It has one of the smallest footprints of all Micro OSes available.
 * It supports automated updates and rollbacks.
 * It is highly resilient.
 * It boots the containers within seconds.
 * It is highly secure and extremely reliable. 
 * It provides application isolation through AppArmor and Seccomp.
 * It integrates with CI/CD pipelines.


### **VMware Photon**

#### == **Introduction to VMware Photon** ==

[Photon OS™](https://vmware.github.io/photon/) is a minimal Linux container host provided by [VMware](https://www.vmware.com/), optimized for cloud-native applications. It is designed with a small footprint in order to boot extremely quickly on VMware vSphere deployments and on cloud computing platforms. Photon can be deployed on Amazon EC2 and GCE instances, while supporting a variety of container formats, such as Docker, rkt, and Cloud Foundry Garden.

Photon OS™ is available in two versions, a minimal and a full version:

 * The minimal version is a lightweight container host runtime environment including a minimum of packaging and functionality to manage containers while still remaining a fast runtime environment.
 * The full version also includes packages of tools for the development, testing, and deployment of containerized applications. 


#### == **Features and Availability** ==

Photon OS™ is optimized for VMware products and cloud platforms. It supports Docker, rkt, and the Cloud Foundry Garden container specifications. It relies on an open source, yum-compatible package manager called Tiny DNF ([tdnf](https://github.com/vmware/tdnf)), and it manages services with systemd.

Photon OS™ is a security-hardened Linux. The kernel and other aspects of the Photon OS™ are built with an emphasis on security recommendations provided by the [Kernel Self-Protection Project](https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project) (KSPP).

It can be easily managed, patched, and updated. It also provides support for persistent volumes to store the data of cloud-native applications on VMware vSAN™.

If you want to try it out, Photon OS™ is available on Amazon EC2, GCE, Microsoft Azure, and Raspberry Pi 3.

#### **Demo: Upgrading the Existing OS on VMware Photon**

Next, we will see how we can upgrade the existing OS on VMware Photon, using the rpm ostree command.

Upgrading the Existing OS on VMware Photon

#### == **Benefits of Using VMware Photon** ==

Key benefits of using VMware Photon are:

 * It is an open source technology with a small footprint.
 * It supports Docker, rkt, and Cloud Foundry Garden container runtimes.
 * It includes Kubernetes in the full version, to allow for container cluster management, but it also supports Mesos.
 * It boots extremely quickly on VMware platforms.
 * It provides efficient lifecycle management with a yum-compatible package manager.
 * Its kernel is tuned for higher performance when it is running on VMware platforms.
 * It is a security-enhanced Linux as its kernel and other aspects of the operating system are configured according to the security parameter recommendations given by the Kernel Self-Protection Project.


---- 

## Chapter 6. Containers: Container Orchestration

### **Introduction and Learning Objectives**

#### == **Introduction** ==

Running containers on a single node may no longer satisfy the needs of an enterprise, where, especially in production, containerized workload needs to be managed at scale. That is how we benefit the most from containers. To run containers in a multi-host environment at scale, we need to find solutions to numerous issues, summarized below:

 * How to group multiple hosts together to form a cluster, and manage them as a single compute unit?
 * How to schedule containers to run on specific hosts?
 * How can containers running on one host communicate with containers running on other hosts?
 * How to provide the container with dependent storage, when it is scheduled on a specific host?
 * How to access containers over a service name, instead of accessing them directly through their IP addresses?

Container orchestration tools, together with different plugins (for networking and storage), aim to address the issues mentioned above. 

Container orchestration is an umbrella term that encompasses container scheduling and cluster management. Container scheduling allows us to decide on which host a container or a group of containers should be deployed. With cluster management orchestrators, we can manage the resources of cluster nodes, as well as add or delete nodes from the cluster. Some of the available solutions for container orchestration are:

 * Docker Swarm
 * Kubernetes
 * Mesos Marathon
 * Nomad
 * Amazon ECS.

In this chapter, we will provide more details on the options available.

#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Describe different container orchestration tools: Docker Swarm, Kubernetes, Mesos, Nomad, and Amazon ECS.
 * Deploy sample applications using various container orchestration tools: Docker Swarm, Kubernetes, Mesos, Nomad, and Amazon ECS.
 * Describe different Kubernetes hosted services like AWS Elastic Kubernetes Service (EKS), Azure Kubernetes Services (AKS), and Google Kubernetes Engine (GKE).

### **Docker Swarm**

#### **Introduction to Docker Swarm**


[Docker Swarm()(https://docs.docker.com/engine/swarm/)] is a native container orchestration solution from [Docker, Inc.](https://www.docker.com/) Docker, in swarm mode, logically groups multiple Docker Engines into a swarm, or cluster, that allows for applications to be deployed and managed at scale.

![Swarm Cluster Components](LFS151-Swarm_Cluster_Components.png)

The above illustration depicts two major components of a swarm:

 * **Swarm Manager Nodes**
    Accept commands on behalf of the cluster and make scheduling decisions. They also maintain the cluster state and store it using the Internal Distributed State Store, which uses the [Raft consensus algorithm](https://raft.github.io/). One or more nodes can be configured as managers for fault-tolerance. When multiple managers are present they are configured in active/passive modes.
 * **Swarm Worker Nodes**
    Run the Docker Engine and the sole purpose of the worker nodes is to run the container workload dispatched by the manager node(s).

#### == **Features** ==

Key characteristics of Docker Swarm:

 * It is compatible with Docker tools and API so that the existing workflow does not change much.
 * It provides native support to Docker networking and volumes.
 * It can scale up to large numbers of nodes.
 * It supports failover and High Availability for the cluster manager for fail-tolerance.
 * It uses a declarative approach to define the desired state of the various services of the application stack.
 * For each service, you can declare the number of tasks you want to run. When you scale up or down, the Swarm manager automatically adapts by adding or removing tasks to maintain the desired state.
 * The Docker Swarm manager node constantly monitors the cluster state and reconciles any differences between the actual state and your expressed desired state.
 * The communication between the nodes of Docker Swarm is enforced with Transport Layer Security (TLS), which makes it secure by default.
 * It supports rolling updates to control a delay between service deployment to different sets of nodes. If a task rollout is unsuccessful, you can roll back a task to a previous version of the service.

Since we are illustrating the characteristics for Docker Swarm, we will also briefly discuss Docker Machine and Docker Compose in the next few pages.

#### == **Docker Machine** ==

[Docker Machine](https://docs.docker.com/machine/overview/) helps us configure and manage one or more Docker Engines running locally or on cloud environments. With Docker Machine we can start, inspect, stop, and restart a managed host, upgrade the Docker client and daemon, and configure a Docker client to talk to our host.

![Provisioning Docker Hosts on Remote Systems](LFS151-provision-use-case.png)

Docker Machine has drivers for Amazon EC2, Google Cloud, Digital Ocean, Vagrant, etc., to set up Docker engines. You can also add already running instances of the Docker Engine to the Docker Machine:

 * Setting up the Docker Engine using the VirtualBox driver:
    
    `$ docker-machine create -d virtualbox dev1`

 * Setting up the Docker Engine using DigitalOcean:
    
    `$ docker-machine create --driver digitalocean --digitalocean-access-token=<TOKEN> dev2`

We can also use Docker Machine to configure a Swarm cluster.

In addition to Docker Machine, there are two newer products aimed to ease the Docker installation process on Windows and Mac. [Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/) and [Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/) are designed to install Docker Machine and Docker Compose on newer desktops and laptops. 

#### == **Docker Compose** ==


[Docker Compose](https://docs.docker.com/compose/overview/) allows us to define and run multi-container applications through a YAML configuration file. In this configuration file, we can define resources such as services, images, Dockerfiles, network, and storage. Below we provide a sample of a Compose file:

```yaml
version: '3.8'

services:
   db:
     image: mysql:5.7
     volumes:
       - db_data:/var/lib/mysql
     restart: always
     environment:
       MYSQL_ROOT_PASSWORD: somewordpress
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD: wordpress

   wordpress:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "8000:80"
     restart: always
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD: wordpress
volumes:
    db_data:
```


With the above Compose file, we would create two containers db and wordpress from the mysql:5.7 and wordpress:latest images, respectively. The wordpress container would connect to the db container to store all the configuration and user data. The db container would save all the persistent data inside the db_data volume, which resides outside the container.

Docker Swarm uses [docker stack](https://docs.docker.com/engine/swarm/stack-deploy/) to deploy complete distributed application stacks. We can use Docker Compose to generate the stack file, which can be used to deploy the application stack to the swarm.

#### **Demo: Deploying a Microservice with Docker Swarm**
In the following video, we will show how you can use Docker Swarm to deploy a microservice, and then scale up and scale down.

Deploying a Microservice with Docker Swarm

#### == **Benefits of Using Docker Swarm** ==

Key benefits of using Docker Swarm are:

 * It provides native clustering for Docker.
 * It is well integrated with the existing Docker tools and workflow.
 * Its setup is easy, straightforward and flexible.
 * It manages containerized workload in a cluster of Docker Engines that are treated as a single entity.
 * It provides scalability and supports High Availability.
 * Efficiency and productivity are increased by reducing deployment and management time, as well as duplication of efforts.


#### == **Introduction to Docker Enterprise Platform** ==

[Docker Enterprise Platform](https://docs.docker.com/ee/) is a Container-as-a-Service (CaaS) platform that manages the entire lifecycle of the applications on enterprise Linux or Windows operating systems and Cloud providers. At the time of this writing, the [Docker Enterprise Platform](https://www.mirantis.com/software/docker/docker-enterprise/) has been acquired by [Mirantis](https://www.mirantis.com/) and is part of their technology stack offered as-a-service. It supports Docker Swarm and Kubernetes as container orchestrators. It has the following three major components:

 * **Docker Engine - Enterprise**
    It is a commercially supported Docker Engine for creating images and running Docker containers.
 *** Docker Trusted Registry (DTR)**
    It is a production-grade image registry designed to store images, from Docker, Inc.
 * **Universal Control Plane (UCP)**
    It manages the Kubernetes and Swarm orchestrators, it deploys applications using the CLI and GUI, and supports High Availability. UCP also provides role-based access control (RBAC) to ensure that only authorized users can make changes and deploy applications to your cluster.

![Docker EE Architecture](LFS151-Docker_EE_Architecture.png)

#### == **Features and Benefits** ==

Key features and benefits of the Docker Enterprise Platform are the following:

 * It is a multi-Linux, multi-OS, multi-Cloud solution.
 * It supports Docker Swarm and Kubernetes as container orchestrators.
 * It provides centralized cluster management.
 * It has a built-in authentication mechanism with role-based access control (RBAC).


#### == **Docker Datacenter** ==

Docker offers a Container-as-a-Service (CaaS) solution, the [Docker Datacenter](https://hub.docker.com/bundles/docker-datacenter), which is built on top of the Universal Control Plane (UCP) and Docker Trusted Registry (DTR). Docker Datacenter is an enterprise container management and deployment service hosted on-premise behind a firewall or in the cloud.

With Docker Datacenter we can build an enterprise-class CaaS platform on-premises, as it is built on top of Docker Swarm and integrates well with Docker tools, and the Docker Registry. In addition, it provides LDAP and AD integration, monitoring, logging, and plugin support for network and storage.

![Docker Datacenter](LFS151-Fig_10.2_-_Docker_Datacenter.png)


### **Kubernetes**

#### == **Introduction to Kubernetes** ==

[Kubernetes](https://kubernetes.io/) is an Apache 2.0-licensed open source project for automating deployment, operations, and scaling of containerized applications. It was started by Google in 2014, but many other companies like Docker, Red Hat, and VMware contributed to its success.

In July of 2015, [Cloud Native Computing Foundation](https://cncf.io/) (CNCF), the nonprofit organization dedicated to advancing the development of cloud-native applications and services and driving alignment among container technologies, accepted Kubernetes as its first hosted project. The IP was transferred to CNCF from Google.

Kubernetes supports several container runtimes such as Docker, CRI-O, frakti, and rkt to run containers.

#### == **The Kubernetes Architecture** ==

The Kubernetes architecture and its key components are illustrated in the diagram below:

![Kubernetes Architecture](LFS151-components-of-kubernetes.png)

#### == **The Kubernetes Architecture - Key Components (Part I)** ==

The key components of the Kubernetes architecture are:

 * **Cluster** 
    The cluster is a group of systems (bare-metal or virtual) and other infrastructure resources used by Kubernetes to run containerized applications.
 * **Master Node**
    The master is a system that takes pod scheduling decisions and manages the worker nodes. Its main components are **kube-apiserver**, **etcd**, **kube-scheduler**, and **kube-controller-manager** and they coordinate tasks around container workload scheduling, deployment, scaling, self-healing, state persistence, and delegation of other tasks to worker node agents. Multiple master nodes may be found in clusters as a solution for High Availability. 
 * **Worker Node**
    A system on which containers are scheduled to run in pods. The node runs a daemon called **kubelet** to communicate with the master node by reporting node status information and receiving instructions from the master about container lifecycle management (this daemon is also found on master nodes). **kube-proxy**, a network proxy running on all nodes, allows applications running in the cluster to be accessed from the external world.
 * **Namespace**
    The namespace allows us to logically partition the cluster into virtual sub-clusters, for projects, applications, users, and teams' isolation.

#### The Kubernetes Architecture - Key Components (Part II)

The key API resources of the Kubernetes architecture:

 * **Pod** 
    The pod is a co-located group of containers with shared volumes, however, it often manages a single container. It is the smallest deployment unit in Kubernetes. A pod can be created independently, but it is recommended the use of controllers such as the ReplicaSet or Deployment, even if only a single pod is being deployed.
 * **ReplicaSet**
    The ReplicaSet is a mid-level controller that manages the lifecycle of pods. It ensures that the desired number of pods is running at all times. 
 * **Deployment**
    The Deployment is a top-level controller that allows us to provide declarative updates for pods and ReplicaSets. We can define Deployments to create new resources, or replace existing ones with new ones. Some typical use cases are presented below:
    * Create a Deployment to bring up a ReplicaSet and pods.
    * Check the status of a Deployment to see if it succeeds or not.
    * Later, update that Deployment to recreate the pods (to use a new image).
    * Rollback to an earlier Deployment revision if the current Deployment isn’t stable.
    * Pause and resume a Deployment. Below we provide a [sample deployment](http://kubernetes.io/docs/user-guide/deployments/):

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
    template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.17.9
        ports:
        - containerPort: 80
```

 * **Service** 
    The service groups sets of pods together and provides a way to refer to them from a single static IP address and the corresponding DNS name. It can reference a single pod, a group of individual pods, or pods managed by ReplicaSets or Deployments. Below, we provide a [sample service](https://kubernetes.io/docs/concepts/services-networking/service/):

```yaml
apiVersion: v1
kind: Service
metadata:
  name: frontend
  labels:
    app: dockchat
    tier: frontend
spec:
  type: LoadBalancer
  ports:
  - port: 9500
    targetPort: 5000
  selector:
    app: dockchat
    tier: frontend
```

 * **Label**
    The label is an arbitrary key-value pair that is attached to a resource like a pod or a ReplicaSet. In the example above, we defined labels as app and tier.
 * **Selector** 
    Selectors enable us to group resources based on labels. In the above example, the frontend service will select all pods which have the labels app==dockchat and tier==frontend.
 * **Volume** 
    The volume is an external filesystem or storage which is available to pods and is mounted on a container's filesystem. They are built on top of [Docker volumes](https://docs.docker.com/userguide/dockervolumes/). 


#### == **Features** ==

Key features of Kubernetes are:

 * It automatically distributes containers on cluster nodes based on containers' resource requirements and other custom constraints.
 * It supports horizontal scaling through the CLI or a UI. In addition, it can auto-scale based on resource utilization.
 * It supports rolling updates and rollbacks.
 * It supports several volume plugins from public cloud providers such as AWS, Azure, and GCP together with network storage plugins like NFS, iSCSI, CephFS, GlusterFS, Cinder, Flocker, and vsphereVolume to orchestrate storage volumes for containers running in pods.
 * It automatically self-heals by restarting failed containers, rescheduling containers from failed nodes, and supports custom health checks to ensure containers are continuously ready to serve.
 * It manages sensitive and configuration data for an application without rebuilding the image.
 * It supports batch execution.
 * It supports High Availability of the master node to add control plane resiliency.
 * It eliminates infrastructure lock-in by providing core capabilities for containers without imposing restrictions.
 * It supports application deployments and updates at scale. 
 * It allows services to be routed based on the topology of the cluster.


#### **Demo: Deploying a Containerized Application on Kubernetes**

In the following video, we will show how you can deploy a containerized application on Kubernetes.

Deploying a Containerized Application on Kubernetes

#### == **Benefits of Using Kubernetes** ==

Key benefits of using Kubernetes are:

 * It is an open source system that packages all the necessary features: orchestration, service discovery, and load balancing.
 * It manages multiple containers at scale.
 * It automates deployment, scaling, and operations of application containers.
 * It is portable, extensible, and self-healing.
 * It provides consistency across development, testing, and production environments, on-premise and across clouds.
 * It is highly efficient when utilizing resources.


### **Deploying Containers with Apache Mesos**

#### == **Introduction to Apache Mesos** ==

When we install and set up a physical machine, we generally use it for very specific purposes, such as running applications or frameworks as Hadoop, Spark, containers, or Jenkins. Some applications might not be using all the system resources (e.g. CPU, memory) while others might be starving for more. Therefore, it would be helpful to have the ability to combine all the physical resources across multiple machines and execute tasks on specific machines based on resource requirements.

[*Apache Mesos*](http://mesos.apache.org/) was created with this idea in mind so that we can optimally use the resources available, even if we are running disparate applications on a pool of nodes. Mesos is a distributed systems kernel that runs on every machine and introduces a level of abstraction that exposes APIs for resource management and workload scheduling. It helps us treat a cluster of nodes as a single compute unit, which manages CPU, memory, and other cluster resources provisioned across datacenter and cloud environments. 

Mesos provides functionality that crosses between Infrastructure as a Service (IaaS) and Platform as a Service (PaaS). It is an open source Apache project.

#### Mesos Architecture

The key components of the Mesos Architecture are illustrated in the diagram below: 

 * **Master**
    Master nodes are the brain of the cluster and provide a single source of truth for running tasks. There is one active master node at any point in time. The master node mediates between schedulers and slaves. Slaves advertise their resources to the master node, then the master node forwards them to the scheduler. Once the scheduler accepts the offer, it sends the task to run on the slave to the master, and the master forwards these tasks to the slave.
 * **Slave**
    Slaves manage resources at each machine level and execute the tasks submitted via the scheduler.
 * **Frameworks** 
    Frameworks are distributed applications that solve a specific use case. They consist of a scheduler and an executor. The scheduler receives a resource offer, which can be accepted or declined. The executor runs the job on the slave, which was scheduled by the scheduler. There are many existing frameworks and we can also create custom ones. Some of the existing frameworks are Hadoop, Spark, Marathon, Chronos, Jenkins, and Aurora.
 * **Executor** 
    Executors, that run jobs on slaves, can be shell scripts, Docker containers, or programs written in different languages such as Java. 

![Mesos Architecture ](LFS151-architecture3.png)

#### Mesos Features

The key features of Mesos are:

 * It can easily scale up to 10,000 nodes.
 * It uses ZooKeeper for fault-tolerant replicated master and slaves.
 * It provides support for Docker containers.
 * It enables native isolation between tasks with Linux containers.
 * It allows multi-resource scheduling (memory, CPU, disk, and ports).
 * It is cross-platform and cloud provider agnostic.
 * It uses Java, Python, and C++ APIs to develop new parallel applications.
 * It uses WebUI to view cluster statistics.
 * It allows for high resource utilization.
 * It helps to handle mixed workloads.
 * It provides an easy-to-use container orchestration right out of the box.

Mesos ships binaries for different components (e.g. master, slaves, frameworks, etc.), which we can bind together to create our Mesos cluster. The Apache Mesos website provides [detailed documentation](http://mesos.apache.org/documentation/latest/) explaining how to perform a cluster setup

#### Mesosphere DC/OS

[Mesosphere](https://d2iq.com/solutions/mesosphere/dcos) offers a commercial solution on top of Apache Mesos. Mesosphere is one of the primary contributors to the Mesos project and to frameworks like Marathon. Their commercial product, Mesosphere Distributed Cloud Operating System (DC/OS), offers a one-click installation together with enterprise features like security, monitoring, user interface, on top of Mesos. 

[Distributed Cloud Operating System](https://dcos.io/) (DC/OS) has recently been [open-sourced by Mesosphere](https://d2iq.com/blog/open-source-dcos).

By default, DC/OS comes with the Marathon framework, but others can be added as required.


The [Marathon framework](https://mesosphere.github.io/marathon/) has the following features:

 * It starts, stops, scales, and updates application containers.
 * It has a nice web interface, API.
 * It is highly available, with no single point of failure.
 * It uses native Docker support.
 * It supports rolling deploy/restart.
 * It supports application container health checks.
 * It provides artifact staging.

In addition to the Mesos features presented on the previous page, DC/OS provides the following ones:

 * It provides an easy-to-use container orchestration right out of the box.
 * It can configure multiple resource isolation zones.
 * It can support applications with multiple persistent and ephemeral storage options.
 * It allows you to install both public and private community packaged applications.
 * It allows you to manage your cluster and services using the web and command-line interfaces.
 * It allows you to easily scale up and scale down your services.
 * It provides automation for updating services and the systems with zero downtime.
 * Its Enterprise edition provides centralized management, control plane for service availability and performance monitoring.


#### == **Mesosphere DC/OS Architecture** ==

Mesosphere DC/OS is an infrastructure agnostic platform optimized for running distributed containerized applications, and services. Regardless of the infrastructure type, virtual or physical hardware, DC/OS is capable of managing resources such as compute, storage, and networking. The Mesosphere DC/OS Architecture includes the following three layers:

Software layer for package management coupled with a package repository to enable the installation and management of databases, message queues, stream processors, monitoring solutions, CI tools, SCM, and logging tools. However, we may install our own custom applications and services as well.

Platform layer includes several components grouped into the following categories:

 * Cluster Management
 * Container Orchestration
 * Container Runtimes
 * Logging and Metrics
 * Networking
 * Package Management
 * IAM and Security ENTERPRISE
 * Storage.

These components are found on multiple node types:

 * Master Nodes
 * Private Agent Nodes
 * Public Agent Nodes.

Infrastructure layer to allow the DC/OS installation on public or private clouds, or on-premises hardware. Installations are aided by automated provisioning tools and require multiple x86 machines with a shared IPv4 network.

![Mesosphere DC/OS Architecture Layers](LFS151-mesosphere-dcos-architecture-layers-redesigned.png)

#### **Demo: Deploying Containers on Mesos with Marathon**

In the following video, we will learn how we can deploy containers on Mesos, using its Marathon framework.

Deploying Containers on Mesos with Marathon

#### == **Benefits of Using Mesos** ==

Some of the benefits of using Mesos are:

 * It is an open source solution, that also has a commercial version available.
 * It provides support for Docker containers.
 * It allows multi-resource scheduling across several platforms and cloud providers.
 * It is highly available and scalable.
 * It provides service discovery and load balancing.

### **Nomad by HashiCorp**

#### == **Introduction to Nomad** ==


[HashiCorp Nomad](https://www.nomadproject.io/) is a cluster manager and resource scheduler from HashiCorp, which is distributed, highly available, and scales to thousands of nodes. It is designed to run microservices and batch jobs, and it supports different types of workloads, from containers (Docker) and VMs, to individual applications. In addition, it is capable of scheduling applications and services on different platforms like Linux, Windows, and Mac, both on-premise and clouds.

It is distributed as a single binary, which has all of its dependency and runs in a server and client mode. To submit a job, the user has to define it using a declarative language called [HashiCorp Configuration Language](https://github.com/hashicorp/hcl) (HCL) with its resource requirements. Once submitted, Nomad will find available resources in the cluster and run it to maximize resource utilization.

Below we provide a sample job file:

```go
# Define the hashicorp/web/frontend job
job "hashicorp/web/frontend" {

    # Job should run in the "us" region
    region = "us"

    # Run in two datacenters
    datacenters = ["us-west-1", "us-east-1"]

   # Only run workload on linux

    constraint {
        attribute = "$attr.kernel.name"
        value = "linux"
    }

    # Configure the job for rolling updates
    update {
        # Stagger updates every 30 seconds
        stagger = "30s"

        # Update a single task at a time
        max_parallel = 1
    }

    # Define the task group together with an individual task (unit of work)
    group "frontend" {
        # Ensure we have enough servers to handle traffic
        count = 10

        task "web" {
            # Use Docker to run our server
            driver = "docker"
            config {
                image = "hashicorp/web-frontend:latest"
            }

            # Specify resource limits
            resources {
                cpu = 500
                memory = 128
                network {
                    mbits = 10
                    dynamic_ports = ["http"]
                }
            }
        }
    }  
} 
```

which would use 10 containers from the `hashicorp/web-frontend:latest` Docker image.

#### == **Features** ==

The following are some of Nomad's key characteristics:

 * It handles both cluster management and resource scheduling.
 * It supports multiple workloads, like containers (Docker), VMs, unikernels, and individual applications.
 * It has multi-datacenter and multi-region support. We can have a Nomad client/server running in different clouds, while still part of the same logical Nomad cluster.
 * It bin-packs applications onto servers to achieve high resource utilization.
 * In Nomad, millions of containers can be deployed or upgraded by using the job file.
 * It provides a built-in dry run execution facility, which shows the scheduling actions that are going to take place.
 * It ensures that applications are running in failure scenarios.
 * It supports long-running services, as well as batch jobs and cron jobs.
 * It provides a built-in mechanism for rolling upgrades.
 * Blue-green and canary deployments are supported through a declarative job file syntax.
 * If nodes fail, Nomad automatically redistributes the applications from unhealthy nodes to healthy nodes.


#### **Demo: Deploying a Containerized Application with Nomad**

In the following demo, we will show how you can deploy a containerized application using Nomad.

Deploying a Containerized Application with Nomad


#### == **Benefits of Using Nomad** ==

Key benefits of using Nomad are:

 * It is an open source solution that is distributed as a single binary for servers and agents.
 * It is highly available and scalable.
 * It maximizes resource utilization.
 * It provides multi-datacenter and multi-region support.
 * It supports multi-cloud federation.
 * During maintenance, there is zero downtime to datacenter and services.
 * It simplifies operations, provides flexible workloads, and fast deployment.
 * It can integrate with the entire HashiCorp ecosystem of tools such as Terraform, Consul, and Vault for provisioning, service discovery, and secrets management.
 * It can support a cluster size of more than ten thousand nodes.


### **Kubernetes Hosted Solutions**

#### == **Kubernetes Hosted Solutions** ==

Kubernetes can be deployed anywhere, both on-premise or in the cloud. If we are deploying on-premise, then our Kubernetes administrators would have to perform all the Kubernetes management tasks to upgrade the cluster or backup resources. With an in cloud setup, we have different options. For instance, we can manage our own cluster in the cloud or opt for hosted Kubernetes services in which all the management tasks would be performed by the cloud services providers. There are many hosted solutions available for Kubernetes, including:

 * [Amazon Elastic Kubernetes Service](https://aws.amazon.com/eks/) (Amazon EKS) 
    Offers a managed Kubernetes service on AWS.
 * [Azure Kubernetes Service](https://azure.microsoft.com/en-us/services/kubernetes-service/) (AKS)
    Offers managed Kubernetes clusters on Microsoft Azure.
 * [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine/) (GKE)
    Offers managed Kubernetes clusters on Google Cloud Platform.
 * [IBM Cloud Kubernetes Service](https://www.ibm.com/cloud/container-service/)
    Fully managed Kubernetes service at scale, providing continuous availability and high availability, multi-zone, and multi-region clusters.
 * [NetApp Project Astra](https://cloud.netapp.com/project-astra) (fusion between NetApp and Stackpoint.io)
    Provides Kubernetes infrastructure automation and management for multiple public clouds optimized for stateful application data lifecycle management.
 * [Oracle Container Engine for Kubernetes](https://www.oracle.com/cloud/compute/container-engine-kubernetes.html) (OKE)
    Enterprise-grade Kubernetes service offering highly available clusters optimized to run on Oracle Cloud Infrastructure.
 * [Red Hat OpenShift](https://www.openshift.com/products)
    Offers managed Kubernetes clusters powered by Red Hat on various cloud infrastructures such as AWS, GCP, Microsoft Azure, and IBM Cloud.
 * [VMware Tanzu Kubernetes Grid](https://tanzu.vmware.com/kubernetes-grid) (TKG)
    An enterprise-grade multi-cloud Kubernetes service that runs both on-premise in vSphere and in the cloud. 

For a detailed list of hosted services, please refer to the Kubernetes documentation.

#### == **Amazon Elastic Kubernetes Service (EKS)** ==


[Amazon Elastic Kubernetes Service](https://aws.amazon.com/eks/) (EKS) is a hosted Kubernetes service offered by AWS.

With Amazon EKS, users don't need to worry about the infrastructure management, deployment, and maintenance of the Kubernetes control plane. EKS provides a scalable and highly-available control plane that runs across multiple AWS availability zones. It can automatically detect unhealthy Kubernetes control plane nodes and replace them as needed.

EKS also supports cluster autoscaling, to dynamically add worker nodes into the cluster, based on the workload and resource utilization. It also integrates with Kubernetes Role-Based Access Control (RBAC) to support AWS IAM authentication.

![Amazon Elastic Kubernetes Service (EKS)](LFS151-Amazon_Elastic_Container_Service_for_Kubernetes.png)

Although master nodes are managed by AWS, users will still need to pay the hosting cost. They will also have to pay for the worker nodes.

#### == **EKS Features and Benefits** ==

Key features and benefits of the Amazon Elastic Kubernetes Service (EKS) are listed below:

    Users do not manage the Kubernetes control plane.
    It provides secure communication between the worker nodes and the control plane.
    It supports auto-scaling in response to changes in load.
    It integrates well with various AWS services such as IAM and CloudTrail.
    It is a Certified hosted Kubernetes platform.


#### == **Azure Kubernetes Service (AKS)** ==


[Azure Kubernetes Service](https://azure.microsoft.com/en-us/services/kubernetes-service/) (AKS) is a hosted Kubernetes service offered by Microsoft Azure.

AKS offers a fully-managed Kubernetes container orchestration service, which reduces the complexity and operational overhead of managing Kubernetes. AKS handles all of the cluster management tasks, health monitoring, upgrades, and scaling.

AKS also supports cluster autoscaling to dynamically add worker nodes into the cluster, based on the workload and resource utilization. It supports Kubernetes Role-Based Access Control (RBAC) and can integrate with Azure Active Directory for identity and security management.

With AKS, users just need to pay for agent/worker nodes that get deployed. Master nodes are managed by AKS for free.


#### == **AKS Features and Benefits** ==

Key features and benefits of the Azure Kubernetes Service (AKS) are listed below:

 * Users do not manage the Kubernetes Control Plane.
 * It supports GUI and CLI-based deployment.
 * It integrates well with other Azure services.
 * It is a Certified hosted Kubernetes platform.
 * It is compliant with SOC and ISO/HIPAA/HITRUST.


#### == **Google Kubernetes Engine (GKE)** ==


[Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine/) (GKE) is a fully-managed solution for running Kubernetes on the Google Cloud Platform (GCP). As we have learned earlier, Kubernetes is used for automating deployment, operations, and scaling of containerized applications.

GKE Kubernetes can be integrated with all GCP services, like Stackdriver monitoring, diagnostics, and logging, identity and access management, etc.


#### == **GKE Features and Benefits** ==

Key features and benefits of the Google Kubernetes Engine (GKE) are listed below:

 * It has all of Kubernetes' features.
 * It runs on a container-optimized OS built and managed by Google.
 * It is a fully-managed service, so the users do not have to worry about managing and scaling the cluster.
 * We can store images privately, using a private container registry.
 * Logging can be enabled easily using Google [Cloud Logging](https://cloud.google.com/logging/docs/).
 * It supports Hybrid Networking to reserve an IP address range for the container cluster.
 * It enables fast setup of managed clusters.
 * It facilitates increased productivity for Dev and Ops teams.
 * It is Highly Available in multiple zones and SLA promises 99.5% of availability.
 * It has Google-grade managed infrastructure.
 * It can be seamlessly integrated with all GCP services.
 * It provides a feature called Auto Repair, which initiates a repair process for unhealthy nodes.

### **Cloud Container Orchestration Services**

#### == **Introduction to Amazon ECS** ==

[Amazon Elastic Container Service](https://aws.amazon.com/ecs/) (ECS) is part of the Amazon Web Services (AWS) offerings. It provides a fast, secure, and highly scalable container management service that makes it easy to run, stop and manage Docker containers on a cluster.

It can be configured in the following two launch modes:

 * Fargate Launch Type
    AWS Fargate allows us to run containers without managing servers and clusters. In this mode, we just have to package our applications in containers along with CPU, memory, networking, and IAM policies. We don't have to provision, configure, and scale clusters of virtual machines to run containers, as AWS will take care of it for us.

 * EC2 Launch Type
    With the EC2 launch type, we can provision, patch, and scale the ECS cluster. This gives more control to our servers and provides a range of customization options.

#### Amazon ECS Components

Key components of Amazon ECS are:

 * **Cluster** 
    It is a logical grouping of tasks or services. With the EC2 launch type, a cluster is also a grouping of container instances.
 * **Container Instance** 
    It is only applicable if we use the EC2 launch type. We define the Amazon EC2 instance to become part of the ECS cluster and to run the container workload.
 * **Container Agent**
    It is only applicable if we use the Fargate launch type. It allows container instances to connect to your cluster.
 * **Task Definition** 
    It specifies the blueprint of an application, which consists of one or more containers. Below, you can see an example of a sample task definition file (from docs.aws.amazon.com):

```
{
"containerDefinitions": [
  {
    "name": "wordpress",
    "links": [
      "mysql"
    ],
    "image": "wordpress",
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80
      }
    ],
    "memory": 500,
    "cpu": 10
  },
  {
    "environment": [
      {
        "name": "MYSQL_ROOT_PASSWORD",
        "value": "password"
      }
    ],
    "name": "mysql",
    "image": "mysql",
    "cpu": 10,
    "memory": 500,
    "essential": true
  }
],
"family": "hello_world"
}
```

 * **Scheduler** 
    It places tasks on the cluster.
 * **Service** 
    It allows one or more instances of tasks to run, depending on the task definition. Below you can see the template of a service definition (from docs.aws.amazon.com). If there is an unhealthy task, then the service restarts it. One elastic load balancer (ELB) is attached to each service.
```
{
    "cluster": "",
    "serviceName": "",
    "taskDefinition": "",
    "loadBalancers": [
        {
            "loadBalancerName": "",
            "containerName": "",
            "containerPort": 0
        }
    ],
    "desiredCount": 0,
    "clientToken": "",
    "role": "",
    "deploymentConfiguration": {
        "maximumPercent": 200,
        "minimumHealthyPercent": 100
    }
}

```
 * **Task** 
    It is a running container instance from the task definition.
 * **Container** 
    It is a Docker container created from the task definition.

####  **Amazon ECS Features**

Key features of Amazon ECS are:

 * It is compatible with Docker.
 * It provides a managed cluster so that users do not have to worry about managing and scaling the cluster.
 * The task definition allows the user to define the applications through a .json file. Shared data volumes, as well as resource constraints for memory and CPU, can also be defined in the same file.
 * It provides APIs to manage clusters, tasks, etc.
 * It allows easy updates of containers to new versions.
 * The monitoring feature is available through AWS CloudWatch.
 * The logging facility is available through AWS CloudTrail.
 * It supports third-party Docker Registry or Docker Hub.
 * AWS Fargate allows you to run and manage containers without having to provision or manage servers.
 * It allows you to build all types of containers. You can build a long-running service or a batch service in a container and run it on ECS.
 * You can apply your Amazon Virtual Private Cloud (VPC), security groups, and AWS Identity and Access Management (IAM) roles to the containers, which helps maintain a secure environment.
 * You can run containers across multiple availability zones within regions to maintain High Availability.
 * It can be integrated with AWS services like Elastic Load Balancing (ELB), Virtual Private Cloud (VPC), Identity and Access Management (IAM), Amazon ECR, AWS Batch, Amazon CloudWatch, AWS CloudFormation, AWS CodeStar, AWS CloudTrail, and more.

#### **Demo: Deploying an Application with Amazon ECS**

In the following demo, we will show how you can deploy a containerized application using Amazon Container Service (Amazon ECS).

Deploying an Application with Amazon ECS

#### == **Benefits of Using Amazon ECS** ==

Key benefits of Amazon ECS are:

 * It provides a managed cluster.
 * It is built on top of Amazon Elastic Compute Cloud (EC2).
 * It is highly available and scalable.
 * It leverages other AWS services, such as CloudWatch Metrics.
 * We can manage it using CLI, Dashboard, or APIs.

#### == **Introduction to Azure Container Instances (ACI)** ==

[Azure Container Instances]https://azure.microsoft.com/en-us/services/container-instances/ (ACI) are part of the Microsoft Azure family of cloud services and aim to simplify the container deployment process without the hassle of managing the infrastructure servers.

ACI is a solution designed for scenarios where container isolation is desired for simple applications, automated tasks, or jobs because it only provides some of the basic scheduling capabilities of dedicated orchestration platforms. Therefore, when advanced features such as service discovery and auto-scaling are desired, as provided by full container orchestration platforms, then the recommended solution is Azure Kubernetes Service (AKS). However, ACI can be used in conjunction with an orchestrator such as AKS in a layered approach, to schedule and manage single containers while the orchestrator manages the multi-container groups. 

#### Azure Container Instances (ACI) Features

Key features of Azure Container Instances (ACI) are:

 * They expose containers directly to the internet through IP addresses and fully qualified domain names (FQDN).
 * Allow user interaction with the environment of a running container by executing commands in the container through a shell. 
 * Offer VM-like application isolation in the container.
 * Allow for resource specification, such as CPU and memory.
 * They allow containers to mount directly Azure File shares to persist their state.
 * They support the running of both Linux and Windows containers.
 * They support the scheduling of single- and multi-container groups, thus allowing patterns like the sidecar pattern.

----

## Chapter 7. Unikernels

### **Introduction and Learning Objectives**

#### == **Introduction** ==

Earlier in the course, we mentioned that in today's world our end goal is, most of the time, to run an application. We also explained how container technology is helping us achieve this end goal. We learned that, to run containers, we also need to ship the entire user-space libraries of the respective distribution with the application. In most cases, the majority of the libraries would not be consumed by the application. Therefore, it makes sense to ship the application only with the set of user-space libraries which are needed by the application.  

With unikernels, we can also select the part of the kernel needed to run with the specific application. With unikernels, we can create a single address space executable, which has both application and kernel components. The image can be deployed on VMs or bare metal, based on the unikernel's type.  

According to the [unikernels website](http://unikernel.org/), 

    "Unikernels are specialised, single-address-space machine images constructed by using library operating systems". 

In this chapter, we will discuss unikernels, their characteristics, implementations, and benefits.

#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Explain the concept of unikernels.
 * Compare and contrast unikernels and containers.

### **Unikernels**

#### == **Creating Specialized VM Images** ==

The [Unikernel](http://unikernel.org/) goes one step further than other technologies, creating specialized virtual machine images with just:

 * The application code
 * The configuration files of the application
 * The user-space libraries needed by the application
 * The application runtime (like JVM)
 * The system libraries of the unikernel, which allow back and forth communication with the hypervisor.

According to the [protection ring](https://en.wikipedia.org/wiki/Protection_ring) of the x86 architecture, we run the kernel on ring0 and the application on ring3, which has the least privileges. Ring0 has the most privileges, like access to hardware, and a typical OS kernel runs on that. With unikernels, a combined binary of the application and the kernel runs on ring0.

Unikernel images would run directly on top of a hypervisor like Xen or on bare metal, based on the unikernel types. The following image shows how the Mirage Compiler creates a unikernel VM image. 

![Comparison of a Traditional OS Stack and a MirageOS Unikernel](LFS151-Fig8.1_-_Example_of_a_Unikernel_Architecture__as_Compared_to_a_Traditional_OS_Stack_.png)


#### == **Benefits of Unikernels** ==

The following are key benefits of unikernels:

 * A minimalistic VM image to run an application, which allows us to have more applications per host.
 * A faster boot time.
 * Efficient resource utilization.
 * A simplified development and management model.
 * A more secure application than the traditional VM, as the attack surface is reduced.
 * An easily-reproducible VM environment, which can be managed through a source control system like Git.


#### == **Unikernel Implementations** ==

There are many implementations of unikernels, and they are divided into two categories:

 * Specialized and purpose-built unikernels 
    They utilize all the modern features of software and hardware, without worrying about the backward compatibility. They are not POSIX-compliant. Some examples of specialized and purpose-built unikernels are LING, [HaLVM](https://galois.com/project/halvm/), [MirageOS](https://mirage.io/), and [Clive](http://lsub.org/ls/clive.html).
 * Generalized 'fat' unikernels 
    They run unmodified applications, which make them fat. Some examples of generalized 'fat' unikernels are [Rumprun](http://rumpkernel.org/), OSv, and [Drawbridge](https://www.microsoft.com/en-us/research/project/drawbridge/?from=http%3A%2F%2Fresearch.microsoft.com%2Fen-us%2Fprojects%2Fdrawbridge%2F).


#### == **Unikernels and Docker (MirageOS)** ==

In January of 2016, Docker acquired Unikernels to make them a first-class citizen of the Docker ecosystem. Both containers and unikernels can co-exist on the same host and can be managed by the same Docker binary.

Unikernels helped Docker to run the Docker Engine on top of [Alpine Linux](http://www.alpinelinux.org/) on Mac and Windows with their default hypervisors, which are xhyve Virtual Machine and Hyper-V VM respectively.

![Shared Kernel vs. Unikernel](LFS151-Fig8.2-SharedKernel-vs-Unikernel.png)

#### **Demo: Creating a Unikernel with Unik**

Next, we will see how we can use the Unik tool to create a very basic unikernel and then deploy it over VirtualBox.

Creating a Unikernel with Unik

----

## Chapter 8. Microservices

### **Introduction and Learning Objectives**

#### == **Introduction** ==

According to [Wikipedia](https://en.wikipedia.org/wiki/Microservices), 

"Microservices are small, independent processes that communicate with each other to form complex applications which utilize language-agnostic APIs. These [services](https://en.wikipedia.org/wiki/Service_(systems_architecture)) are small building blocks, highly [decoupled](https://en.wikipedia.org/wiki/Coupling_(computer_programming)) and focused on doing a small task, facilitating a [modular](https://en.wikipedia.org/wiki/Modularity) approach to [system](https://en.wikipedia.org/wiki/System-building. The microservices architectural style is becoming the standard for building continuously deployed systems".

In this chapter, we will focus on microservices and their role in today's cloud environment.

#### == **Learning Objectives** ==


By the end of this chapter, you should be able to:

 * Explain the concept of microservices.
 * Discuss the benefits and challenges of using microservices.


### **Microservices**

#### == **Technological Advancement Towards Microservices** ==

Over the last decade, building the right kind of tooling around virtualization and cloud management has accelerated the adoption and consumption of cloud technologies. Below we provide some relevant examples:

 * With the launch of [Amazon Web Services](https://aws.amazon.com/) (AWS) in 2006, we can get compute resources on demand from the web or the command-line interface. 
 * With the launch of [Heroku](http://heroku.com/) in 2007, we can deploy a locally-built application in the cloud with just a couple of commands.
 * With the launch of [Vagrant](https://www.vagrantup.com/) in 2010, we can easily create reproducible development environments.

With tools like the ones above in hand, software engineers and architects started to move away from large monolith applications, in which an entire application is managed via one code-base. Having one code-base makes the application difficult to manage and scale.

Over the years, with different experiments, we evolved towards a new approach, in which a single application is deployed and managed via a small set of services. Each service runs its own process and communicates with other services via lightweight mechanisms like REST APIs. Each of these services is independently deployed and managed. Technologies like containers and unikernels are becoming default choices for creating such services.

![Monolithic vs Microservices](LFS151-Monoliths-Microservices.png)


#### == **Refactoring a Monolith Into Microservices** ==

We have been designing applications for decades and have a very good understanding of modularizing the code. In simpler terms, we can create a microservices environment by extending those modules into individual services. Though there is no rule of thumb to follow every time we refactor a monolith into microservices, there are some approaches that we can look at:

 * If you have a complex monolith application, then it is not advisable to rewrite the entire application from scratch. Instead, you should start carving out services from the monolith, which implement the desired functionalities for the code we take out from the monolith. Over time, all or most functionalities will be implemented in the microservices architecture.
 * We can split the monoliths based on the business logic, front-end (presentation), and data access. In the microservices architecture, it is recommended to have a local database for individual services. And, if the services need to access the database from other services, then we can implement an event-driven communication between these services. 
 * As mentioned earlier, we can split the monolith based on the modules of the monolith application, and each time we do it, our monolith shrinks. 

Also, if we need a new functionality while we are refactoring the monolith to microservices, then we should create a microservice instead of adding more code to the monolith.

#### == **Benefits of Microservices** ==

Key benefits of microservices:

 * There is no language or technology lock-in. As each service works independently, we can choose any language or technology to develop it. We just need to make sure its API endpoints return the expected output.
 * Each service in a microservice can be deployed independently.
 * We do not have to bring an entire application offline just to update or scale a component. Each service can be updated or scaled independently. This gives us the ability to respond faster.
 * If one service fails, then its failure does not have a cascading effect. This helps in debugging as well.
 * Once the code of a service is written, it can be reused in other projects, where the same functionality is needed.
 * The microservice architecture enables continuous delivery.
 * Components can be deployed across multiple servers or even multiple data centers.
 * They work very well with container orchestration tools like Kubernetes, DC/OS, and Docker Swarm.


#### Challenges and Drawbacks of Microservices

Just like any other technology, there are also challenges and disadvantages to using microservices:

 * **Choosing the right service size** 
    While refactoring the monolith application or creating microservices from scratch, it is very important to choose the right functionality for a service. For example, if we create a microservice for each function of a monolith, then we would end up with lots of small services, which would bring unnecessary complexity. 

 * **Deployment**
    We can easily deploy a monolith application. However, to deploy a microservice, we need to use a distributed environment such as Kubernetes. 

 * **Testing** 
    With lots of services and their inter-dependency, sometimes it becomes challenging to perform end-to-end testing of a microservice.  

 * **Inter-service communication** 
    Inter-service communication can be very costly if it is not implemented correctly. There are options such as message passing, or RPC and we need to choose the one that fits our requirement and has the least overhead. 

 * **Managing databases** 
    When it comes to the microservices' architecture, we may decide to implement a database local to a microservice. But, to close a business loop, we might require changes in other related databases. This can create problems (e.g. partitioned databases). 

 * **Monitoring** 
    Monitoring individual services in a microservices environment can be challenging. This challenge is being addressed, and a new set of tools, like [Sysdig](https://sysdig.com/) or [Datadog](https://www.datadoghq.com/), is being developed to monitor and debug microservices.

Even with the above challenges and drawbacks, deploying microservices makes sense when applications are complex and continuously evolving.

----

## Chapter 9. Software-Defined Networking and Networking for Containers

### **Introduction and Learning Objectives**

#### == **Introduction** ==

Traditional networks cannot cope with the kind of demand mobile devices, cloud computing, and other similar technologies are generating. CPU and memory become more and more decentralized. Compute units such as VMs, services, microservices, and containers follow global deployment and availability patterns, which introduce new challenges in the management of such distributed architectures. With the advancement in compute and storage virtualization, we can quickly meet distributed compute and storage requirements. However, the most important piece of the puzzle, the one that aids in keeping all distributed units connected, is the network. Therefore, to connect devices, applications, VMs, and containers, we need a similar level of flexibility which can only be achieved by virtualizing the networking. This will allow us to meet the end-to-end business requirements.   

Software-Defined Networking (SDN) decouples the network control layer from the traffic forwarding layer. This allows SDN to program the control layer and to create custom rules in order to meet these new networking requirements. 

Part of this course has covered containers extensively. Next, let's take a look at container networking and see how it is one of the use cases for SDN. 

#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Define Software-Defined Networking.
 * Discuss the basics of Software-Defined Networking.
 * Discuss different networking options with containers using Docker, Kubernetes and Cloud Foundry.


### Software-Defined Networking (SDN)

#### SDN Architecture

In networking, there are three distinctive planes:

 * **Data Plane**
    The Data Plane, also called the Forwarding Plane, is responsible for handling data packets and apply actions to them based on rules which we program into lookup-tables. 
 * **Control Plane**
    The Control Plane is tasked with calculating and programming the actions for the Data Plane. This is where the forwarding decisions are made and where services such as Quality of Service (QoS) and VLANs are implemented. 
 * **Management Plane** 
    The Management Plane is the place where we can configure, monitor, and manage the network devices.


#### Activities Performed by a Network Device

Every network device has to perform three distinct activities:

 * *Ingress and egress packets* 
    These are performed the lowest layer, which decides what to do with ingress packets and which packets to forward, based on forwarding tables. These activities are mapped as Data Plane activities. All routers, switches, modem, etc. are part of this plane. 
 * *Collect, process, and manage the network information* 
    By collecting, processing, and managing the network information, the network device makes the forwarding decisions, which the Data Plane follows. These activities are mapped by the Control Plane activities. Some of the protocols which run on the Control Plane are routing and adjacent device discovery. 
 * *Monitor and manage the network* 
    Using the tools available in the Management Plane, we can interact with the network device to configure it and monitor it with tools like SNMP (Simple Network Management Protocol).  

In [Software-Defined Networking](https://en.wikipedia.org/wiki/Software-defined_networking), we decouple the Control Plane from the Data Plane. The Control Plane has a centralized view of the overall network, which allows it to create forwarding tables of interest. These tables are then submitted to the Data Plane to manage network traffic.

![The SDN Framework](LFS151-SDN-Framework.png)

The Control Plane has well-defined APIs that receive requests from applications to configure the network. After preparing the desired state of the network, the Control Plane communicates that to the Data Plane (also known as the Forwarding Plane), using a well-defined protocol like [OpenFlow](http://en.wikipedia.org/wiki/OpenFlow). 

We can use configuration tools like Ansible or Chef to configure SDN, adding lots of flexibility and agility on the operations side as well.

### **Networking for Containers**

#### == **Introduction to Networking for Containers** ==

Similar to VMs, we need to connect containers running on the same host and containers running on different hosts. The host uses the [network namespace](https://lwn.net/Articles/580893/) feature of the Linux kernel to isolate the network from one container to another on the system. Network namespaces can be shared between containers as well. 

On a single host, when using the virtual Ethernet (vEth) feature with Linux bridging, we can give a virtual network interface to each container and assign it an IP address. With technologies like [Macvlan](https://docs.docker.com/network/macvlan/) and [IPVlan](https://www.kernel.org/doc/Documentation/networking/ipvlan.txt) we can configure each container to have a unique world-wide routable IP address.

As of now, if we want to achieve multi-host networking with containers, the most common solution is to use some form of Overlay network driver, which encapsulates the Layer 2 traffic to a higher layer. Examples of this type of implementation are the Docker Overlay Driver, Flannel, and Weave. Other types of implementations are also available, such as Project Calico, which allows multi-host networking on Layer 3 using BGP (Border Gateway Protocol).

#### == **Container Networking Standards** ==

Two different standards have been proposed so far for container networking:

 * [Container Network Model](https://github.com/docker/libnetwork/blob/master/docs/design.md) (CNM)
    Docker, Inc. is the primary driver for this networking model. It is implemented using the [libnetwork](https://github.com/moby/libnetwork#libnetwork---networking-for-containers) project, which has the following utilizations:

    1. **Null**: NOOP implementation of the driver. It is used when no networking is required.
    2. **Bridge**: It provides a Linux-specific bridging implementation based in Linux Bridge.
    3. **Overlay**: It provides a [multi-host communication](https://github.com/docker/libnetwork/blob/master/docs/overlay.md) over VXLAN.
    4. **Remote**: It does not provide a driver. Instead, it provides a means of supporting drivers over a remote transport, by which we can write third-party drivers.

 * [Container Networking Interface](https://github.com/containernetworking/cni) (CNI) 
    It is a Cloud Native Computing Foundation (CNCF) project which consists of specifications and libraries for writing plugins to configure network interfaces in Linux containers, along with a number of supported plugins. It is limited to provide network connectivity of containers and removing allocated resources when the container is deleted. As such, it has a wide range of support. It is used by projects like Kubernetes, OpenShift, and Cloud Foundry. 

#### == **Service Discovery** ==

Now that we provided an overview of networking, let's take a moment to discuss **service discovery** as well. This becomes extremely important when we are looking to implement multi-host networking and use some form of container orchestration with Docker Swarm or Kubernetes. 

Service discovery is a mechanism by which processes and services can find each other automatically and talk to each other. With respect to containers, it is used to map a container name with its IP address, so that we can access the container directly by its name without worrying about its exact location (IP address) which may change during the life of the container. 

Service discovery is achieved in two steps:

 * **Registration**
    When a container starts, the container scheduler registers the container name to the container IP mapping in a key-value store such as **etcd** or **Consul**. And, if the container restarts or stops the scheduler updates the mapping accordingly.
 * **Lookup**
    Services and applications use Lookup to retrieve the IP address of a container so that they can connect to it. Generally, this is supported by DNS (Domain Name Server), which is local to the environment. The DNS used resolves the requests by looking at the entries in the key-value store, which is used for Registration. SkyDNS and Mesos-DNS are examples of such DNS services.

### **Docker Single-Host Networking**

#### == **Listing the Available Networks** ==

Let's explore the networking and its features available in Docker. More precisely, let's start with Docker's single-host networking to understand how is the networking setup on a single Docker host for its containers. If we list the available networks after installing the Docker daemon, we should see something like the following:

```bash
$ docker network ls
NETWORK ID          NAME          DRIVER
6f30debc5baf        bridge        bridge
a1798169d2c0        host          host
4eb0210d40bb        none          null
```

**bridge**, **null**, and **host** are different network drivers available on a single Docker host. Next, we will take a closer look at them.

#### == **Bridge Driver** ==

Similarly to the hardware bridge, we can emulate a software bridge on a Linux host. It can forward traffic between two networks based on MAC (hardware address) addresses. By default, Docker creates a docker0 Linux bridge. Each container running on a single host receives a unique IP address from this bridge unless we specify some other network with the --net= option. Docker uses Linux's virtual Ethernet (vEth) feature to create a pair of two virtual interfaces, with the interface on one end attached to the container and the interface on the other end of the pair attached to the docker0 bridge.

Looking at the network configuration after installing Docker on a single host, we can see the default bridge as illustrated below:

```bash
$ ifconfig
docker0   Link encap:Ethernet  HWaddr 02:42:A9:DB:AF:39
          inet addr:172.17.0.1  Bcast:0.0.0.0  Mask:255.255.0.0
          UP BROADCAST MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
```

We can create a new container using the following command, then list its IP address:

```bash
$ docker container run -it --name=c1 busybox /bin/sh
/ # ip a
1: lo: <LOOPBACK,UP,LOWE_UP> mtu 65536 qdisc noqueue qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
7: eth0@if8: <BROADCAST,MULTICAST,UP, LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:2/64 scope link
       valid_lft forever preferred_lft forever
```

As we can see, the new container received its IP address from the private IP address range 172.17.0.0/16, which is catered by the bridge network.

#### == **Inspecting a Bridge Network** ==

We can inspect a network to list detailed information about it. In the example below, container c1 appears to be on the bridge network:

```bash
$ docker network inspect bridge
[
     {
        "Name": "bridge",
        "Id": "6f30debc5baff467d437e3c7c3de673f21b51f821588aca2e30a7db68f10260c",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16"
                }
            ]
        },
        "Internal": false,
        "Containers": {
            "613f1c7812a9db597e7e0efbd1cc102426edea02d9b281061967e25a4841733f": {
                 "Name": "c1",
                 "EndpointID": "80070f69de6d147732eb119e02d161326f40b47a0cc0f7f14ac7d207ac09a695",
                 "MacAddress": "02:42:ac:11:00:02",
                 "IPv4Address": "172.17.0.2/16",
                 "IPv6Address": ""
             }
         },
         "Options": {
             "com.docker.network.bridge.default_bridge": "true",
             "com.docker.network.bridge.enable_icc": "true",
             "com.docker.network.bridge.enable_ip_masquerade": "true"
             "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
             "com.docker.network.bridge.name": "docker0",
             "com.docker.network.driver.mtu": "1500"
         },
         "Labels": {}
      }
 ]
```

#### == **Creating a Bridge Network** ==

We can also create our own custom bridge network by running the following command:

```bash
$ docker network create --driver bridge my_bridge
```

It creates a custom Linux bridge on the host system. To create a container and have it use the newly created network, we have to start the container with the --net=my_bridge option:

```bash
$ docker container run --net=my_bridge -itd --name=c2 busybox
```

Docker allows us to attach a container to as many networks as we like. A new container is attached to a network with the --net flag while an already running container is attached to an additional network with the docker network connect command. 

A bridge network does not support automatic service discovery, so we have to rely on the [legacy --link](https://docs.docker.com/network/links/) option.


![Creating a Bridge Network](LFS151-bridge2.png)


#### == **Null Driver** ==

As the name suggests, NULL means no networking. If we attach a container to a null driver, then it would just get the loopback interface. It would not be accessible from any other network.

```bash
$ docker container run -it --name=c3 --net=none busybox /bin/sh
/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 1disc noqueue qlen 1
         link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet ::1/128 scope host
       valid_lft forever preferred_lft forever
```

#### == **Host Driver** ==

Using the host driver, we can share the host machine's network namespace with a container. By doing so, the container would have full access to the host's network, which is not a recommended approach due to its security implications. We can see below that running an ifconfig command inside the container lists all the interfaces of the host system:

```bash
$ docker container run -it --name=c4 --net=host  busybox /bin/sh
/ # ifconfig
docker0   Link encap:Ethernet  HWaddr 02:42:A9:DB:AF:39

          inet addr:172.17.0.1  Bcast:0.0.0.0  Mask:255.255.0.0
          inet6 addr: fe80::42:a9ff:fedb:af39/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:8 errors:0 dropped:0 overruns:0 frame:0
          TX packets:8 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:536 (536.0 B)  TX bytes:648 (648.0 B)

eth0      Link encap:Ethernet  HWaddr 08:00:27:CA:BD:10
          inet addr:10.0.2.15  Bcast:10.0.2.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:feca:bd10/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:3399 errors:0 dropped:0 overruns:0 frame:0
          TX packets:2050 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:1021964 (998.0 KiB)  TX bytes:287879 (281.1 KiB)

eth1      Link encap:Ethernet  HWaddr 08:00:27:00:42:F9
          inet addr:192.168.99.100  Bcast:192.168.99.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:fe00:42f9/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:71 errors:0 dropped:0 overruns:0 frame:0
          TX packets:46 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:13475 (13.1 KiB)  TX bytes:7754 (7.5 KiB)

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:16 errors:0 dropped:0 overruns:0 frame:0
          TX packets:16 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1
          RX bytes:1021964376 (1.3 KiB)  TX bytes:1376 (1.3 KiB)

vethb3bb730 Link encap:Ethernet  HWaddr 4E:7C:8F:B2:2D:AD
          inet6 addr: fe80::4c7c:8fff:feb2:2dad/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:8 errors:0 dropped:0 overruns:0 frame:0
          TX packets:16 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:648 (648.0 B)  TX bytes:1296 (1.2 KiB)

```


#### == **Sharing Network Namespaces Among Containers** ==

Similar to host, we can share network namespaces among containers. As a result, two or more containers can share the same network stack and reach each other through localhost.

Let's run a new container and take a look at its IP address:

```bash
$ docker container run -it --name=c5 busybox /bin/sh
/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
        valid_lft forever preferred_lft forever

10: eth0@if11: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.3/16 scope global eth0
        valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:3/64 scope link
        valid_lft forever preferred_lft forever
```

Now, if we start a new container with the --net=container:CONTAINER option, we can see that the second container has the same IP address.

```bash
$ docker container run -it --name=c6 --net=container:c5 busybox /bin/sh
/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
        valid_lft forever preferred_lft forever

12: eth0@if13: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.3/16 scope global eth0
        valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:3/64 scope link
        valid_lft forever preferred_lft forever
```

Kubernetes uses the feature detailed above to share the same network namespaces among multiple containers in a Pod.

### **Docker Multi-Host Networking**

#### == **Introduction to Docker Multi-Host Networking** ==

In addition to the single-host networking, Docker also supports multi-host networking which allows containers from one Docker host to communicate with containers from another Docker host. By default, Docker supports two drivers for multi-host networking:


 * **Docker Overlay Driver**
    With the [Overlay](https://docs.docker.com/network/overlay/) driver, Docker encapsulates the container's IP packet inside a host's packet while sending it over the wire. While receiving, Docker on the other host decapsulates the whole packet and forwards the container's packet to the receiving container. This is accomplished with libnetwork, a built-in VXLAN-based overlay network driver.

![Docker Overlay Driver](LFS151-packetwalk.png)


 * Macvlan Driver
    With the Macvlan driver, Docker assigns a MAC (physical) address for each container and makes it appear as a physical device on the network. As the containers appear in the same physical network as the Docker host, we can assign them an IP from the network subnet as the host. As a result, we can do direct container-to-container communication between different hosts. Containers can also directly talk to hosts. However, we need hardware support to implement the Macvlan driver. For more information about Macvlan, please visit its [documentation available on the Docker website](https://docs.docker.com/network/macvlan/). 


#### **Demo: Multi-Host Networking with Docker**

In the following video, we will see how we can leverage Docker's multi-host networking and connect containers from different hosts.

Multi-Host Networking with Docker

### **Docker Network Driver Plugins**

The functionality of Docker Engine can be extended with plugins. With network plugins, third-party vendors can integrate their networking solutions with the Docker ecosystem. Some examples of Docker supported [network plugins](https://docs.docker.com/engine/extend/legacy_plugins/#network-plugins) are:

 * [**Contiv Networking Plugin**](https://github.com/contiv/netplugin) 
    Provides infrastructure and security policies for multi-tenant deployments.
 * [**Infoblox IPAM Plugin**](https://hub.docker.com/plugins/infoblox-ipam-plugin)
    Infoblox ipam-plugin is a Docker libnetwork plugin that interfaces with Infoblox to provide IP Address Management services.
 * [**Kuryr Network Plugin**](https://github.com/openstack/kuryr)
    It is a part of the OpenStack Kuryr project, which also implements libnetwork's remote driver API by utilizing Neutron, which is OpenStack's networking service.
 * [**Weave Net Network Plugin**](https://www.weave.works/docs/net/latest/install/plugin/plugin-how-it-works/)
    Weave Net provides multi-host container networking for Docker. It also provides service discovery and does not require any external cluster store to save the networking configuration. Weave Net has a Docker Networking Plugin which we can use with Docker deployment.

In addition, we can write our own driver with Docker remote driver APIs.

### **Kubernetes Networking**

As we know, the smallest deployment unit in Kubernetes is a Pod, which may include one or more containers. Kubernetes assigns a unique IP address to each Pod. Containers in a Pod share the same network namespace and can refer to each other by localhost. We have seen an example of network namespaces sharing between Docker containers earlier, a prior section. Containers in a Pod can expose unique ports, and become accessible through the same Pod IP address.

As each Pod gets a unique IP, Kubernetes assumes that Pods should be able to communicate with each other, irrespective of the nodes they get scheduled on. There are different ways to achieve this. Kubernetes introduced the [Container Network Interface](https://github.com/containernetworking/cni/blob/master/SPEC.md) (CNI) specification for container networking together with the following requirements that need to be implemented by Kubernetes networking driver developers:

 * All pods on a node can communicate with all pods on all nodes without NAT
 * All nodes can communicate with all pods (and vice versa) without NAT
 * The IP that a pod sees itself as is the same IP that other pods see it as.

Next, we provide some of the implementations of Kubernetes networking:

 * [**Cilium**](https://github.com/cilium/cilium)
    Provides secure network connectivity between application containers. It is L7/HTTP aware and can also enforce network policies on L3-L7.
 * [**Flannel**](https://github.com/coreos/flannel#flannel)
    Flannel uses the overlay network, as we have seen with Docker, to meet the Kubernetes networking requirements.
 * [**NSX-T**](https://docs.vmware.com/en/VMware-NSX-T-Data-Center/3.0/ncp-kubernetes/GUID-FB641321-319D-41DC-9D16-37D6BA0BC0DE.html)
    NSX-T from VMware provides network virtualization for a multi-cloud and multi-hypervisor environment. The NSX-T Container Plug-in (NCP) provides integration between NSX-T and container orchestrators such as Kubernetes.
 * [**Project Calico**](https://docs.projectcalico.org/v3.1/introduction/)
    Calico uses the BGP protocol to meet the Kubernetes networking requirements.
 * [**Romana**](https://romana.io/)
    A network and security automation solution that supports Kubernetes deployments without an overlay network. Romana supports Kubernetes Network Policy that provides network traffic isolation across Kubernetes namespaces.
 * [**Weave Net**](https://www.weave.works/oss/net/)
    Weave Net, a simple network for Kubernetes, may run as a CNI plug-in or stand-alone. It does not require additional configuration to run, and the network provides the one IP address per pod, as it is required and expected by Kubernetes.

For more details and other implementations, please refer to the Kubernetes Documentation.

### **Cloud Foundry: Container to Container Networking**

#### Cloud Foundry: Container-to-Container Networking

By default, [Gorouter](https://docs.cloudfoundry.org/concepts/architecture/router.html) routes the external and internal traffic to different Cloud Foundry (CF) components. However, in the event that the container-to-container networking feature of CF is disabled, the application-to-application communication is routed by Gorouter as well.

The [container-to-container networking](https://docs.cloudfoundry.org/concepts/understand-cf-networking.html) feature of CF enables application instances to communicate with each other directly. However, when the container-to-container networking feature is disabled, all application-to-application traffic must go through the Gorouter. 

Container-to-container networking is made possible by several components of the CF architecture:

 * **Policy Server**
    A management node hosting a database of app traffic policies.
 * **Garden External Networker**
    Sets up networking for each app through the CNI plugin, exposing apps to the outside, by allowing incoming traffic from Gorouter, TCP Router, and SSH Proxy.
 * **Silk CNI Plugin**
    For IP management through a share VXLAN overlay network that assigns each container a unique IP address. The overlay network is not externally routable and it prevents the container-to-container traffic from escaping the overlay. 
 * **VXLAN Policy Agent**
    Enforces network policies between apps. When creating routing rules for network policies, we should include the source app, destination app, protocol, and ports, without going through the Gorouter, a load balancer, or a firewall.


## Chapter 10. Software-Defined Storage and Storage Management for Containers

### **Introduction and Learning Objectives**

#### == **Introduction** ==

**Software-Defined Storage** (SDS) represents storage virtualization in which the underlying storage hardware is separated from the software that manages and provisions it. We can combine physical hardware from various sources and manage them with software, as a single storage pool. SDS replaces static and inefficient storage solutions backed directly by physical hardware with dynamic, agile, and automated solutions. In addition, SDS may provide resiliency features such as replication, erasure coding, and snapshots of the pooled resources. Once the pooled storage is configured in the form of a storage cluster, SDS allows multiple access methods such as File, Block, and Object. 

In this chapter, we will first look at some examples for Software-Defined Storage, and then dive into storage management for containers. 

Some examples of Software-Defined Storage are:

 * [**Ceph**](https://ceph.io/)
 * [**FreeNAS**](https://www.freenas.org/)
 * [**Gluster**](https://www.gluster.org/)
 * [**LINBIT**](https://www.linbit.com/)
 * [**MinIO**](https://min.io/)
 * [**Nexenta**](https://nexenta.com/)
 * [**OpenEBS**](https://openebs.io/)
 * [**OpenSDS**](https://github.com/sodafoundation/api/blob/master/OpenSDS%20Architecture.jpg)
 * [**Soda Dock**](https://sodafoundation.io/projects/soda-dock/)
 * [**VMware vSAN**](https://www.vmware.com/products/vsan.html).


#### **Learning Objectives**

By the end of this chapter, you should be able to:

 * Discuss the basics of Software-Defined Storage.
 * Discuss different storage options with containers using Docker, Kubernetes and Cloud Foundry.


### **Ceph**

#### == **Introduction to Ceph** ==

According to [ceph.io](https://ceph.io/): 

"Ceph is a unified, distributed storage system designed for excellent performance, reliability and scalability."

[**Ceph**](https://ceph.io/) supports applications with different storage interface needs, as it provides *object*, *block*, and *file system* storage in a single unified storage cluster, making Ceph flexible, highly reliable, and easy to manage.


#### == **Ceph Architecture** ==

Everything in Ceph is stored as objects. Ceph uses the CRUSH (Controlled Replication Under Scalable Hashing) algorithm to deterministically find, write, and read the location of objects.


![Ceph Architecture](LFS151-Fig_12.2_Ceph_Architecture.png)

Next, we will take a closer look at Ceph's architecture:

 * **Reliable Autonomic Distributed Object Store (RADOS)**
    It is the object store which stores the objects. This layer makes sure that data is always in a consistent and reliable state. It performs operations like replication, failure detection, recovery, data migration, and rebalancing across the cluster nodes. This layer has the following three major components:

     * Object Storage Device (OSD): This is where the actual user content is written and retrieved with read operations. One OSD daemon is typically tied to one physical disk in the cluster.
     * Ceph Monitors (MON): Monitors are responsible for monitoring the cluster state. All cluster nodes report to Monitors. Monitors map the cluster state through the OSD, Place Groups (PG), CRUSH, and Monitor maps.
     * Ceph Metadata Server (MDS): It is needed only by CephFS, to store the file hierarchy and metadata for files. 

    ![Components of Storage Clusters ](LFS-151-Fig_12.3_Components_of_Storage_Clusters.png)


 * **Librados** 
    It is a library that allows direct access to RADOS from languages like C, C++, Python, Java, PHP, etc. Ceph Block Device, RADOSGW, and CephFS are implemented on top of Librados.
 * **Ceph Block Device** (RBD) 
    This provides the block interface for Ceph. It works as a block device and has enterprise features like thin provisioning and snapshots.
 * **RADOS Gateway** (RADOSGW) 
    This provides a REST API interface for Ceph, which is compatible with Amazon S3 and OpenStack Swift.
 * **Ceph File System** (CephFS) 
    This provides a POSIX-compliant distributed filesystem on top of Ceph. It relies on Ceph MDS to track the file hierarchy.


#### **Demo: Creating a Block Object with Ceph**

Next, we will learn how to create a block object using Ceph.

Creating a Block Object with Ceph

#### **Benefits of Using Ceph**

Key benefits of using Ceph are:

 * It is an open source storage solution, which supports Object, Block, and File system storage.
 * It runs on any commodity hardware, without any vendor lock-in.
 * It provides data safety for mission-critical applications.
 * It provides an automatic balance of filesystems for maximum performance.
 * It is scalable and highly available, with no single point of failure.
 * It is a reliable, flexible, and cost-effective storage solution.
 * It achieves higher throughput by stripping files/data across multiple nodes.
 * It achieves adaptive load-balancing by replicating frequently accessed objects over multiple nodes.


### **GlusterFS**

#### == **Introduction to GlusterFS** ==

According to [gluster.org](http://www.gluster.org/):

"Gluster is a free and open source software scalable network file system."

Gluster can utilize common off-the-shelf hardware, to create large, distributed storage solutions for media streaming, data analysis, and other data- and bandwidth-intensive tasks.

#### == **GlusterFS Volumes** ==

To create shared storage, we need to start by grouping the machines in a trusted pool. Then, we group the directories (called bricks) from those machines in a GlusterFS volume, using [FUSE](https://docs.gluster.org/en/latest/Quick-Start-Guide/Architecture/#fuse) (Filesystem in Userspace). GlusterFS supports different [types of volumes](https://docs.gluster.org/en/latest/Quick-Start-Guide/Architecture/#types-of-volumes):

 * Distributed GlusterFS volume
 * Replicated GlusterFS volume
 * Distributed replicated GlusterFS volume
 * Dispersed GlusterFS volume
 * Distributed dispersed GlusterFS volume.

Below, we present an example of a distributed GlusterFS volume:

![Distributed GlusterFS Volume](LFS151-Fig_12.4-Distributed_GlusterFS_Volume.png)

GlusterFS does not have a centralized metadata server. It uses an elastic [hashing](https://en.wikipedia.org/wiki/Hash_function) algorithm to store files on bricks.

The GlusterFS volume can be accessed using one of the following methods:

 * Native FUSE mount
 * NFS (Network File System)
 * CIFS (Common Internet File System).

#### == **Demo: Mounting a GlusterFS Volume** ==

In the following video, we will show how you can mount a GlusterFS volume and look at a file inside it.

Mounting a GlusterFS Volume


#### == **Benefits of Using GlusterFS** ==

Key benefits of using GlusterFS are:

 * It scales to several petabytes.
 * It can be configured on commodity hardware.
 * It is an open source storage solution that supports Object, Block, and Filesystem storage.
 * It does not have a metadata server.
 * It is scalable, modular, and extensible.
 * It provides features such as replication, quotas, geo-replication, snapshots, and BitRot detection.
 * It is POSIX-compliant, providing High Availability via local and remote data replication.
 * It allows optimization for different workloads.


### **Storage Management for Containers**

#### **Introduction to Storage Management for Containers**

Containers are ephemeral in nature, meaning that all data stored inside the container's file system would be lost when the container is deleted. It is best practice to store data outside the container, which keeps the data accessible even after the container is deleted.

In a multi-host or clustered environment, containers can be scheduled to run on any host. We need to make sure the data volume required by the container is available on the host on which the container is scheduled to run. 

In this section, we will see how Docker uses the Docker Volume feature to store persistent data and allows vendors to support their storage to its ecosystem, using Docker Volume Plugins. We will start by looking into different storage backends, which Docker supports in order to store images, containers, and other metadata.


#### == **Docker Storage Backends** ==

Docker uses the [copy-on-write](https://en.wikipedia.org/wiki/Copy-on-write) mechanism when containers are started from container images. The container image is protected from direct edits by being saved on a read-only filesystem layer. All of the changes performed by the container to the image filesystem are saved on a writable filesystem layer of the container. Docker images and containers are stored on the host system and we can choose the storage backend for Docker storage, depending on our requirements. Docker supports the following storage backends on Linux: 

 * AUFS (Another Union File System)
 * BtrFS
 * Device Mapper
 * Overlay
 * VFS (Virtual File System)
 * ZFS.


##### == **Managing Data in Docker** ==

Docker supports several options for storing files on a host system:

 * **Volumes**
    On Linux, volumes are stored under the /var/lib/docker/volumes directory and they are directly managed by Docker. Volumes are the recommended method of storing persistent data in Docker.
 * **Bind Mounts**
    Allow Docker to mount any file or directory from the host system into a container.
 * **Tmpfs**
    Stored in the host's memory only but not persisted on its filesystem.
 * **Named pipes** (npipe)
    Commonly used for direct communication between a container and the Docker host.

In the cases of **volumes** and **bind mounts**, Docker bypasses the Union Filesystem, by using the copy-on-write mechanism. The writes happen directly to the host directory. 

Docker also supports third party volume plugins, which we will look at later.


#### == **Creating a Container with Volumes** ==

In Docker, a container with a mounted volume can be created using either the docker container run or the docker container create commands:

`$ docker container run -d --name web -v webvol:/webdata myapp:latest`

The above command would create a Docker volume inside the Docker working directory /var/lib/docker/volumes/webvol/_data on the host system, mounted on the container at the /webdata mount point. We may list the exact mount path via the docker container inspect command:

`$ docker container inspect web`

#### **Demo: Creating a Named Volume**

We can give a specific name to a Docker volume and then use it for different operations. To create a named volume, we can run the following command:

`$ docker volume create --name my-named-volume`

and then mount it.

Creating a Named Volume with Docker

#### == **Mounting a Host Directory Inside the Container** ==

In Docker, a container with a bind mount can be created using either the docker container run or the docker container create commands:

`$ docker container run -d --name web -v /mnt/webvol:/webdata myapp:latest`

It mounts the host's /mnt/webvol directory to /webdata mount point on the container as it is being started.


### **Volume Plugins for Docker**

#### == **Introduction to Volume Plugins for Docker** ==

We can extend the functionality of the Docker Engine with the use of plugins. With volume plugins, third-party vendors can integrate their storage solutions with the Docker ecosystem. Some examples of Docker supported [volume plugins](https://docs.docker.com/engine/extend/legacy_plugins/#volume-plugins) are:

 * Azure File Storage
 * Blockbridge
 * DigitalOcean Block Storage
 * Flocker
 * gce-docker
 * GlusterFS
 * Local Persists
 * NetApp (nDVP)
 * OpenStorage
 * REX-Ray
 * VMware vSphere Storage.

Volume plugins are especially helpful when we migrate a stateful container, like a database, on a multi-host environment. In such an environment, we have to make sure that the volume attached to a container is also migrated to the host where the container is migrated or started afresh. 

Flocker manages Docker containers and data volumes together, by allowing volumes to follow containers when they move between different hosts in the cluster.

#### == **GlusterFS Volume Plugin** ==

Earlier in this chapter, we saw how we can create a shared storage pool using GlusterFS.

Docker also provides support for a [GlusterFS volume plugin](https://github.com/calavera/docker-volume-glusterfs), which we can use to attach/detach on-demand storage to one or more containers deployed in a multi-host environment.

#### **Demo: Using GlusterFS Volume Plugin to Mount the GlusterFS Volume**

In the following demo, we will see how we can use the GlusterFS volume plugin for Docker to mount the GlusterFS volume created on a remote machine to the container.

Using GlusterFS Volume Plugin to Mount the GlusterFS Volume


### **Volume Management in Kubernetes**

####  ==**Volume Management in Kubernetes** ==

Kubernetes uses volumes to attach external storage to containers managed by Pods. A volume is essentially a directory, backed by a storage medium. The storage medium and its contents are determined by the volume type.

![Multi-container Pod with Shared Volume](LFS151-pod-volume-multi-container.svg)

A volume in Kubernetes is linked to a Pod and shared among containers of that Pod. The volume has the same lifetime as the Pod but it outlives the containers of that Pod, meaning that data remains preserved across container restarts. However, once the Pod is deleted, the volume and all its data are lost as well.

A volume may be shared by some of the containers running in the same Pod. The diagram above illustrates a Pod with two containers, a File Puller and a Web Server, sharing a storage Volume.


#### == **Volume Types** ==

A volume mounted inside a Pod is backed by an underlying volume type. A volume type decides the properties of the volume, such as size and content type. Some of the volume types supported by Kubernetes are:

 * **awsElasticBlockStore**
    To mount an [AWS EBS](https://aws.amazon.com/ebs/) volume on containers of a Pod.
 * **azureDisk**
    To mount an [Azure Data Disk](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/managed-disks-overview?toc=%2Fazure%2Fvirtual-machines%2Flinux%2Ftoc.json) on containers of a Pod.
 * **azureFile**
    To mount an Azure File Volume on containers of a Pod.
 * **cephfs**
    To mount a CephFS volume on containers of a Pod.
 * **configMap**
    To attach a decoupled object that includes configuration data, scripts, and possibly entire filesystems, to containers of a Pod. 
 * **emptyDir**
    An empty volume is created for the Pod as soon as it is scheduled on a worker node. The life of the volume is tightly coupled with the Pod. If the Pod dies, the content of emptyDir is deleted forever.
 * **gcePersistentDisk**
    To mount a [Google Compute Engine](https://cloud.google.com/compute/docs/disks/) (GCE) persistent disk into a Pod.
 * **glusterfs**
    To mount a [Glusterfs](https://www.gluster.org/) volume on containers of a Pod.
 * **hostPath**
    To share a directory from the host with the containers of a Pod. If the Pod dies, the content of the volume is still available on the host.
 * **nfs**
    To mount an NFS share on containers of a Pod.
 * **persistentVolumeClaim**
    To attach a persistent volume to a Pod. Persistent Volumes are covered in the next section.
 * rbd
    To mount a [Rados Block Device](https://docs.ceph.com/docs/master/rbd/) volume on containers of a Pod.
 * **secret**
    To attach sensitive information such as passwords, keys, certificates, or tokens to containers in a Pod.
 * **vsphereVolume**
    To mount a vSphere VMDK volume on containers of a Pod.

#### == **Persistent Volumes** == 

In a typical IT environment, storage is managed by the storage/system administrators. The end-user gets instructions to use the storage and he/she does not have to worry about the underlying storage management.

In the containerized world, we would like to follow similar rules, but it becomes very challenging given the many volume types we have seen earlier. In Kubernetes, this problem is solved with the persistent volume subsystem, which provides APIs to manage and consume the storage. To manage the volume it uses the [PersistentVolume (PV)](https://kubernetes.io/docs/concepts/storage/persistent-volumes/) resource type and to consume it, it uses the [PersistentVolumeClaim (PVC)](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims) resource type.

Persistent volumes can be provisioned statically or dynamically. In the example below, a Kubernetes Administrator has statically created a few PVs:

For dynamic provisioning of PVs, Kubernetes uses the StorageClass resource, which contains pre-defined provisioners and parameters for the PV creation. With PersistentVolumeClaim (PVC), a user sends the requests for dynamic PV creation, which gets wired to the StorageClass resource.

![Using PVC in a Pod](LFS151-pv.png)

Some of the [volume types ](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#types-of-persistent-volumes)that support managing storage using PersistentVolume are:

 * GCEPersistentDisk
 * AWSElasticBlockStore
 * AzureFile
 * AzureDisk
 * NFS
 * iSCSI
 * RBD
 * CephFS
 * GlusterFS
 * VsphereVolume
 * StorageOS.

#### == **Persistent Volumes Claim** ==

A [PersistentVolumeClaim](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#types-of-persistent-volumes) (PVC) is a request for storage by a user. Users request for PV resources based on size, access modes, and volume type. Once a suitable PV is found, it is bound to PVC:

![User Requesting PV Resources](LFS151-pvc2.png)

After a successful bind, the PVC can be used in a Pod, to allow the containers' access to the PV.

Once a user completed his/her tasks and the Pod is deleted, the PVC may be detached from the PV releasing it for possible future use. Keep in mind, however, that the PVC may be detached from the PV once all the Pods using the same PVC have completed their activities and have been deleted. Once released, the PV can be either deleted, retained, or recycled for future usage, all based on the [reclaim policy](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#reclaim-policy) the user has configured on the PV.

#### **Demo: Adding External Storage to Pods**

In this demo, we will learn how to attach external storage to the pods, which containers in the pod can mount.

Adding External Storage to Pods


### **Container Storage Interface (CSI)**

#### == **Container Storage Interface (CSI)** ==

For a while, container orchestrators like Kubernetes, Mesos, Docker, and Cloud Foundry have had their specific methods of managing external storage volumes. As a result, for storage vendors, it was difficult to manage different volume plugins for different orchestrators. Storage vendors and community members from different orchestrators have been working together to standardize the volume interface to avoid duplicate work. This is the aim of the [Container Storage Interface ](https://github.com/container-storage-interface/spec)(CSI), that the same volume plugin would work with different container orchestrators out of the box.

Such interoperability may only be achieved through an industry standard to be implemented by all storage providers which are developing universal plugins expected to work with all container orchestrators. The role of CSI is to maintaining the CSI [specification](https://github.com/container-storage-interface/spec/blob/master/spec.md) and the [protobuf](https://github.com/container-storage-interface/spec/blob/master/csi.proto).

The goal of the CSI specification is to define APIs for dynamic provisioning, attaching, mounting, consumption, and snapshot management of storage volumes. In addition, it defines the plugin configuration steps to be taken by the container orchestrator together with deployment configuration options.


### **Cloud Foundry Volume Service**

#### == **Cloud Foundry Volume Service** ==

On Cloud Foundry, applications connect to other services via a service marketplace. Each service has a service broker, which encapsulates the logic for creating, managing, and binding services to applications.

With volume services, the volume service broker allows Cloud Foundry applications to attach external storage.

![Cloud Foundry Volume Service](LFS151-cf-volume-service.png)

With the volume bind command, the service broker issues a volume mount instruction that instructs the Diego scheduler to schedule the app instances on cells that have the appropriate volume driver. In the backend, the volume driver gets attached to the device. Cells then mount the device into the container and start the app instance.

Following are some examples of CF Volume Services:

 * [nfs-volume-release](https://github.com/cloudfoundry/nfs-volume-release)
    This allows for easy mounting of external [Network File System](https://en.wikipedia.org/wiki/Network_File_System) (NFS) shares for Cloud Foundry applications.
 * [smb-volume-release](https://github.com/cloudfoundry/smb-volume-release)
    This allows for easy mounting of external [Server Message Block](https://docs.microsoft.com/en-us/windows/win32/fileio/microsoft-smb-protocol-and-cifs-protocol-overview) (SMB) shares for Cloud Foundry applications.


#### **Demo: Using the Volume Services of Cloud Foundry**

In this demo, we will learn how we can use the volume services of Cloud Foundry.

Using the Volume Services of Cloud Foundry

----

## Chapter 11. DevOps and CI/CD

### **Introduction and Learning Objectives**

#### == **Introduction** ==

Every industry thrives for better quality and faster innovation. The IT industry is no exception and has to address numerous challenges:

 * It must quickly go from business idea to market.
 * It must lower the failure rate for new releases.
 * It must have a shorter lead time between fixes.
 * It must have a faster mean time to recovery.

Over the last decade or so, we gradually shifted from the [Waterfall model](https://en.wikipedia.org/wiki/Waterfall_model) to [Agile software development](https://en.wikipedia.org/wiki/Agile_software_development), in which teams deliver working software in smaller and more frequent increments. In this process, the IT operations (Ops) teams were unintentionally left behind, which put a lot of pressure on them, due to high end-to-end deployment rates. By putting the Ops teams in the loop from the very beginning of the development cycle, we can reduce this burn-out and can take advantage of their expertise in the continuous integration process.

The collaborative work between Developers and Operations is referred to as **DevOps**. DevOps is more of a mindset, a way of thinking, versus a set of processes implemented in a specific way.

Besides **Continuous Integration** (CI), DevOps also enables **Continuous Deployment** (CD), which can be seen as the next step of CI. In CD, we deploy the entire application/software automatically, provided that all the tests' results and conditions have met the expectations.

Some of the software used in the CI/CD domain are Jenkins, Travis CI, Shippable, and Concourse, which we will explore in this chapter.

#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Explain the concept of DevOps.
 * Discuss Continuous Integration and Continuous Deployment.
 * Run automated tests using tools like Jenkins, Travis CI, Shippable, and Concourse.
 * Understand Cloud Native CI/CD.

### **CI/CD: Jenkins**

#### == **Introduction to Jenkins** ==

[Jenkins](https://www.jenkins.io/) is one of the most popular automation tools, part of the [CD Foundation](https://cd.foundation/). It is an open source automation system that can provide Continuous Integration (CI) and Continuous Deployment (CD), and it is written in Java.

There are several methods to use Jenkins, from different distributions to cloud-hosted solutions:

 * [CloudBees](https://www.cloudbees.com/), also one of the primary sponsors for the Jenkins open source project, offers a variety of Jenkins based CI/CD products.
 * [Servana](https://servanamanaged.com/services/managed-jenkins/), a managed Jenkins service provider, uses CloudBees Jenkins distribution hosted in Servana's own datacenters. 
 * DigitalOcean provides its own Jenkins-as-a-Service offering, [Onjection Jenkins](https://marketplace.digitalocean.com/apps/onjection-jenkins).
 * Jenkins can also be hosted on major cloud services providers, such as Amazon AWS, Google Cloud Platform, Kamatera, and Microsoft Azure.

#### == **Jenkins Functionality** ==

Jenkins can build Freestyle, Apache Ant, and Apache Maven-based projects. We can also extend the functionality of Jenkins, using [plugins](https://plugins.jenkins.io/ui/search?query=). Currently, Jenkins supports more than 1700 plugins in different categories, like Source Code Management, Slave Launchers, Build tools, and External tools/site integration.

Jenkins also has the functionality to build a pipeline, which allows us to define an entire application lifecycle. A **pipeline** is most useful for performing Continuous Deployment (CD).

According to the [Jenkins documentation](https://jenkins.io/doc/book/pipeline/), Pipeline's "features are:

 * **Code**: Pipelines are implemented in code and typically checked into source control, giving teams the ability to edit, review, and iterate upon their delivery pipeline.
 * **Durable**: Pipelines can survive both planned and unplanned restarts of your Jenkins master.
 * **Pausable**: Pipelines can optionally stop and wait for human input or approval before completing the jobs for which they were built.
 * **Versatile**: Pipelines support complex real-world CD requirements, including the ability to fork or join, loop, and work in parallel with each other.
 * **Extensible**: The Pipeline plugin supports custom extensions to its DSL (domain scripting language) and multiple options for integration with other plugins."

The flowchart below illustrates a sample deployment using the Pipeline Plugin:

 ![Jenkins Pipeline](LFS151-Fig_13.1-Jenkins_Pipeline.png)

#### **Demo: Running Unit Tests with Jenkins**

In the following demo, we will see how we can use Jenkins to run unit tests.

Running Unit Tests with Jenkins


#### == **Benefits of Using Jenkins** ==

Key benefits of using Jenkins are:

 * It is an open source automation system.
 * It supports Continuous Integration and Continuous Deployment.
 * It is extensible through plugins.
 * It can be easily installed, configured, and distributed.


### **CI/CD: Travis CI**

#### == **Introduction to Travis CI** ==

[Travis CI](https://travis-ci.com/) is a hosted, distributed CI solution for projects hosted on GitHub, Bitbucket, and more.

To run the test with CI, first we have to link our GitHub account with Travis and select the project (repository) for which we want to run the test. In the project's repository, we have to create a .travis.yml file, which defines how our build should be executed step-by-step.

#### == **Executing Build with Travis** ==

A typical build with Travis consists of two steps:

 * install: to install any dependency or pre-requisite
 * script: to run the build script.

We can also add other optional steps, including the deployment steps. The following are all the build options one can put in a .travis.yml file.

 * before_install
 * install
 * before_script
 * script
 * OPTIONAL before_cache
 * after_success or after_failure 
 * OPTIONAL before_deploy
 * OPTIONAL deploy
 * OPTIONAL after_deploy
 * after_script

#### == **Travis CI Characteristics** ==

Travis CI supports various databases, such as MYSQL, RIAK, and memcached. We can also use Docker during the build process.

Travis CI supports most languages. For a detailed list of supported languages, please take a look at the ["Language-specific Guides"](https://docs.travis-ci.com/user/language-specific/) page.

After running the test, we can deploy the application in many cloud providers, such as Heroku, AWS Codedeploy, Cloud Foundry, and OpenShift. A detailed list of providers is available on the "[Supported Providers](https://docs.travis-ci.com/user/deployment)" page.

#### ***Demo: Triggering Automated Test Cases for a GitHub Repository with Travis CI**

Next, we will show you how to use Travis CI to trigger test cases for a GitHub repository.

Triggering Automated for a GitHub Repository with Travis CI

#### == **Benefits of Using Travis CI** ==

Key benefits of using Travis CI are:

 * It is a hosted, distributed solution integrated with GitHub, and Bitbucket.
 * It can be easily set up and configured.
 * It is free for open source projects.
 * It supports testing for different versions of the same runtime. 


### **CI/CD: Shippable**

#### == **Introduction to Shippable** ==

As per [Shippable](http://docs.shippable.com/) website:

"Shippable is a DevOps Assembly Lines Platform that helps developers and DevOps teams achieve CI/CD and make software releases frequent, predictable, and error-free. We do this by connecting all your DevOps tools and activities into a event-driven, stateful workflow".

![The shippable.yml Structure](LFS151-codeToProdPipelines.png)

One can use Shippable as a SaaS service, install on-premise using Shippable Server or attach their own servers to Shippable Subscription.

Shippable runs all the builds inside a Docker container, which are called minions. Shippable has minions for all the different combinations of programming languages and versions they support. But, if you are already familiar with development with Docker, you can either use your own image or build a new one, while running the CI.

#### == **Testing with Shippable** ==

To run CI tests with Shippable, we have to create a configuration file inside the project's source code repository which we want to test, called shippable.yml:

```yaml
resources:
  - name: docs_repo
    type: gitRepo
    integration: ric03uec-github
    pointer:
      sourceName: shippable/docs
      branch: master

  - name: aws_rc_cli
    type: cliConfig
    integration: aws_rc_access
    pointer:
      region: us-east-1

  - name: aws_prod_cli
    type: cliConfig
    integration: aws_prod_access
    pointer:
      region: us-west-2

  jobs:
  - name: publish_prod_docs
    type: runSh
    steps:
      - IN: docs_repo
        switch: off
      - IN: aws_prod_cli
      - TASK:
        - script: |
            pushd $(shipctl get_resource_state "docs_repo")
              ./deployDocs.sh s3://docs.shippable.com us-west-2 production
            popd

  - name: publish_rc_docs
    type: runSh
    steps:
      - IN: docs_repo
      - IN: aws_rc_cli
      - TASK:
        - script: |
            pushd $(shipctl get_resource_state "docs_repo")
              ./deployDocs.sh s3://rcdocs.shippable.com us-east-1 rc
            popd
```

#### == **Programming Languages Supported by Shippable** ==

During [Continuous Integration](http://docs.shippable.com/ci/why-continuous-integration/) (CI) an application goes through multiple phases such as code changing, building, unit testing, and packaging. The application package can also be pushed to a PaaS/IaaS or artifact repository. 

Currently, Shippable supports the following programming languages for CI:

 * C/C++
 * Clojure
 * Go
 * Java
 * Node.js
 * PHP
 * Python
 * Ruby
 * Scala.

#### == **Integrations** ==

Shippable [integrates](http://docs.shippable.com/platform/integration/overview/#supported-integration-types) well with all popular CI/CD and DevOps tools like GitHub, Bitbucket, Docker Hub, GCR, Quay, JUnit, Kubernetes, Amazon ECS, Slack, Terraform, Ansible, Puppet, or Chef.

For more details about these and other tools please refer to [Shippable's website](https://www.shippable.com/integrations.html).

#### **Demo: Running Test Cases with Shippable**

The following demo will show how we can use Shippable to do automated testing for our projects.

Running Test Cases with Shippable

#### == **Benefits of Using Shippable** ==

Key benefits of using Shippable are:

 * Increased build speed.
 * It supports builds against multiple runtimes, environment variables, and platforms.
 * It supports on-premise systems for builds.
 * It achieves Continuous Delivery by automating CI and DevOps activities.
 * It optimizes DevOps operations.
 * It provides security with native secrets management and RBAC.



### **CI/CD: Concourse**

#### == **Concourse** ==

[Concourse](https://concourse-ci.org/) is an open source CI/CD system, which was started by Alex Suraci and Christopher Brown in 2014. Later, Pivotal sponsored the project. It is written in the Go language.

With Concourse, we run series of tasks to perform desired operations. Each task runs inside a container. Using series of tasks along with resources, we can build a job or pipeline. Following is a sample [task](https://concourse-ci.org/tasks.html) file:

```yaml
platform: linux

image_resource:
  type: docker-image
  source: {repository: busybox}

run:
  path: echo
  args: [hello world]
```


With the above configuration, we would be creating a Linux container using the busybox container image from Docker Hub, and inside that container, we would run the “echo hello world” program.

Concourse is primarily driven via a CLI, which is referred to as fly. We can use fly to login to our Concourse setup and to execute tasks.

#### **Demo: Using Concourse**

In the following demo, we will see how to use Concourse.

Using Concourse

#### == **Benefits of Using Concourse** ==

Benefits of using Concourse:

 * It is an open source tool.
 * It can be set up on-premise or in the cloud.
 * It is a very simple method of configuring and managing CI pipelines.
 * It has good visualization for our pipeline and tasks.
 * It can be scaled across many servers.
 * In Concourse, the necessary data to run the pipeline can be provided by resources. These resources never affect the performance of a worker.


### **Cloud Native CI/CD**

#### == **Cloud Native CI/CD** ==

So far, we have seen that containers are now playing a major role in an application's lifecycle, from packaging to deployment. Containers have brought Dev, QA, and Ops teams together, which led to a significant improvement in CI/CD. We can code our CI/CD pipeline and store it along with the source code as we've seen in the earlier sections.

We have also seen various methods to run and manage containers at scale using container orchestrators. There are many options for orchestrators, but Kubernetes seems to be the preferred orchestration tool.

In the Cloud Native approach, we design a package and run applications on top of our infrastructure (on-premise or cloud) using operations tools like containers, container orchestration, and services like continuous integration, logging, monitoring, etc. Kubernetes integrated with several tools meets our requirements to run Cloud Native applications.

#### == **Tools** ==

We have already looked at Kubernetes in earlier chapters. Let's now look at some of the tools that help us with CI/CD, which integrate well with Kubernetes for Cloud Native applications:

 * [**Helm**](https://www.helm.sh/)
    It is a popular package manager for Kubernetes. Helm packages Kubernetes applications into Charts, with all the artifacts, objects, and dependencies an entire application needs in order to successfully be deployed in a Kubernetes cluster. Using Helm Charts, which are stored in repositories, we can share, install, upgrade, or rollback an application that was built to run on Kubernetes. Helm is a graduated project of [CNCF](https://www.cncf.io/).
 * [**Draft**](https://draft.sh/)
    It is being promoted as a developer tool for cloud-native applications running on Kubernetes, and it allows for an application to be containerized and deployed on Kubernetes.
 * [**Skaffold**](https://skaffold.dev/)
    It is a tool from Google that helps us build, push, and deploy code to the Kubernetes cluster. It supports Helm, and it also provides building blocks and describes customizations for a CI/CD pipeline.
 * [**Argo**](https://argoproj.github.io/)
    It is a container-native workflow engine for Kubernetes. Its use cases include running Machine Learning, Data Processing, and CI/CD tasks on Kubernetes.
 * [**Jenkins X**](https://jenkins-x.io/)
    Jenkins is a very popular tool for CI/CD that can be used on Kubernetes as well. But the Jenkins team built a new cloud-native CI/CD tool, Jenkins X, from the ground up. The new tool leverages Docker, Draft, and Helm to deploy a Jenkins CI/CD pipeline directly on Kubernetes, by simplifying and automating full CI/CD pipelines. In addition, Jenkins X automates the preview of pull requests for fast feedback before changes are merged, and then it automates the environment management and the promotion of new application versions between different environments. Jenkins X is a graduated project of [CD Foundation](https://cd.foundation/).
 * [Spinnaker](https://www.spinnaker.io/)
    It is an open source multi-cloud continuous delivery platform from Netflix for releasing software changes with high velocity. It supports all the major cloud providers like Amazon Web Services, Microsoft Azure, Google Cloud Platform, and OpenStack. It supports Kubernetes natively. Spinnaker is a graduated project of [CD Foundation](https://cd.foundation/).


#### **GitOps, SecOps, NetOps, and NetSecOps**

While we are talking about CI/CD, let's talk about GitOps and SecOps practices as well, which have emerged recently. So far, we have been using Git as a single point of truth for code commits. With GitOps, it is extended to the entire system. Git becomes the single point of truth for code, deployment configuration, monitoring rules, etc. With just a Git pull request, we can do/update the complete application deployment. Some of the examples of GitOps are Weave Flux and GitKube.

With SecOps, security would not be an afterthought when it comes to application deployment. It would be included along with the application deployment, to avoid any security risks. In addition to security, networking and its security also become part of Operations, as NetOps and NetSecOps. Software-defined networking blends right into DevOps practices and allows network and security teams to automate many operational steps by utilizing a policy-driven framework to drive the configuration and operation of the infrastructure. 

----
## Chapter 12. Tools for Cloud Infrastructure I (Configuration Management)

### **Introduction and Learning Objectives**

#### == **Introduction** ==

When we have numerous systems (both bare-metal and virtual) to manage in different environments like Development, QA, and Production, we would prefer to automate it. At any point in time, we want to have a consistent and desired state of systems and software installed on them. This is also referred to as **Infrastructure as Code**.

**Configuration Management** tools allow us to define the desired state of the systems in an automated way. In this section, we will take a look at some of the Configuration Management tools available today, such as Ansible, Chef, Puppet, and Salt.

#### == **Learning Objectives**
 
By the end of this chapter, you should be able to:

 * List tools used to configure and manage the Cloud Infrastructure.
 * Discuss and use configuration management tools: Ansible, Puppet, Chef, and Salt Open.

### **Ansible**

#### == **Introduction to Ansible** ==

Red Hat's [Ansible](http://www.ansible.com/) is an easy-to-use, open source Configuration Management (CM) tool. It is an agentless tool that works through SSH. In addition, Ansible can automate infrastructure provisioning (on-premise or cloud), application deployment, and orchestration.

Due to its simplicity, Ansible quickly became one of the top in-demand CM tools in the DevOps world.

#### == **Nodes** ==

One of Ansible's advantages is its agentless architecture. This ensures that maintenance tasks are restricted to a single management node, and not to every managed instance in the cluster. As a result, all updates to managed nodes are pushed via SSH, to lists of nodes that are managed through inventory files. To list the nodes which we want to manage in an inventory file, we must do the following:

```yaml
[webservers]
www1.example.com
www2.example.com

[dbservers]
db0.example.com
db1.example.com
```

![Ansible Multi-Node Deployment Workflow](LFS151-Fig_14.1_Ansible_Multi-Node_Deployment_Workflow.png)

The nodes can be grouped together as shown in the picture above. Ansible also supports dynamic inventory files for cloud providers like AWS and OpenStack. The management node connects to these nodes with a password or it can do passwordless login, using SSH keys.

Ansible ships with a default [set of modules,](https://github.com/ansible/ansible/tree/devel/lib/ansible/modules) like packaging, network, etc., which can be executed directly or via [Playbooks](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html). We can also create custom modules.

#### == **Playbooks** ==

[Playbooks](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html) are Ansible’s configuration, deployment, and orchestration language.

Below we provide an example of a playbook which performs multiple tasks based on roles:

```yaml
---
# This playbook deploys the whole application stack in this site. 
- name: apply common configuration to all nodes
  hosts: all
  remote_user: root

  roles:
    - common

- name: configure and deploy the webservers and application code
  hosts: webservers
  remote_user: root

  roles:
    - web

- name: deploy MySQL and configure the databases
  hosts: dbservers
  remote_user: root

  roles:
    - db
```

The sample tasks mentioned in the playbook are:

```yaml
---
# These tasks install http and the php modules.

- name: Install http and php etc
  yum: name={{ item }} state=present
  with_items:
   - httpd
   - php
   - php-mysql
   - git
   - libsemanage-python
   - libselinux-python

- name: insert iptables rule for httpd
  lineinfile: dest=/etc/sysconfig/iptables create=yes state=present regexp="{{ httpd_port }}" insertafter="^:OUTPUT "
              line="-A INPUT -p tcp  --dport {{ httpd_port }} -j  ACCEPT"
  notify: restart iptables

- name: http service state
  service: name=httpd state=started enabled=yes
```

The Ansible management node connects to the nodes listed in the inventory file and runs the tasks included in the playbook. A management node can be installed on any Unix-based system like Linux or Mac OS X. It can manage any node which supports SSH and Python.

[Ansible Galaxy](https://galaxy.ansible.com/) is a free site for finding, downloading, and sharing community-developed Ansible roles.

Ansible also has an enterprise product called [Red Hat Ansible Tower](https://www.ansible.com/products/tower), which introduces a GUI interface, access control, and central management.

#### **Demo: Configuring a System with Ansible**

In the following video, we will see how we can use Ansible to install and configure Nginx on a CentOS system.

#### == **Benefits of Using Ansible** ==

Key benefits of using Ansible are:

 * It is an easy-to-use open source configuration management tool.
 * It is an agentless tool.
 * It automates cloud provisioning, application deployment, and orchestration.
 * It provides consistent, reliable, and secure management.
 * It is supported by a large and active community of developers.
 * It has a low learning curve.
 * It provides role-based access control.
 * It is available for all major operating systems.

### **Puppet**

#### == **Introduction to Puppet ** == 

[Puppet](https://puppet.com/) is an open source configuration management tool. It mostly uses the agent/master (client/server) model to configure the systems. The agent is referred to as the Puppet Agent and the master is referred to as the Puppet Master. The Puppet Agent can also work locally and is then referred to as Puppet Apply.

Besides Puppet, the company also has an enterprise product called [Puppet Enterprise](https://puppet.com/products/puppet-enterprise/) and provides services and training for this product as well.

#### == **Puppet Agent** ==

The Puppet Agent needs to be installed on each system we want to manage/configure with Puppet. Each agent is responsible for:

 * Connecting securely to the Puppet Master to get the series of instructions in a file referred to as the Catalog File.
 * Performing operations from the Catalog File to get to the desired state.
 * Sending back the status to the Puppet Master.

Puppet Agent can be installed on the following platforms:

 * Linux
 * Windows
 * Mac OSX.

#### == **Puppet Master** ==

Puppet Master can be installed only on Unix-based systems. It is responsible for:

 * Compiling the Catalog File for hosts based on system, configuration, manifest file, etc.
 * Sending the Catalog File to agents when they query the master.
 * Storing information about the entire environment, such as host information, metadata such as authentication keys.
 * Gathering reports from each agent and then preparing the overall report.

#### == **The Catalog File** ==

Puppet prepares a Catalog File based on the manifest file. A manifest file is created using the Puppet Code:

```ruby
user { 'student':
  ensure     => present,
  uid        => '1001',
  gid        => '1001',
  shell      => '/bin/bash',
  home       => '/home/student'
}
```
which defines and creates a user student with:

 * UID/GID as 1001
 * The login shell is set to /bin/bash
 * The home directory is set to /home/student.

A manifest file can have one or more sections of code as we exemplified above, and each of these sections of code can have a signature like the following:

```ruby
resource_type { 'resource_name'
  attribute => value
  ...
}
```
Puppet defines resources on a system as Type which can be file, user, package, service, etc. They are well-documented in their [documentation](https://puppet.com/docs/puppet/latest/resource_types.html).

After processing the manifest file, the Puppet Master prepares the Catalog File based on the target platform.

#### == **Puppet Tools** ==

Puppet also has nice tooling around it, like:

 * Centralized reporting through [PuppetDB](https://puppet.com/docs/puppetdb/latest/index.html), which helps us generate reports, search a system, etc.
 * Live system management.
 * [Puppet Forge](https://forge.puppet.com/), which has ready-to-use modules for manifest files from the community.


#### **Demo: Using the Puppet Master to Install Packages on Nodes**

Next, we will see how we can use Puppet to install packages on nodes.

Using the Puppet Master to Install Packages on Nodes


#### == **Benefits of Using Puppet** ==

Key benefits of using Puppet are:

 * It is an open source configuration management tool.
 * It provides scalability, automation, and centralized reporting.
 * It is available on all major operating systems.
 * It provides role-based access control.


### **Chef**

#### == **Introduction to Chef** ==

[Chef](https://www.chef.io/) uses the client/server model to perform configuration management. The client is installed on each host which we want to manage and is referred to as [Chef Client](https://docs.chef.io/chef_client_overview/). The server is referred to as [Chef Server](https://docs.chef.io/server_overview/). Additionally, there is another component called [Chef Workstation](https://docs.chef.io/workstation/), which is used to:

 * Develop [cookbooks](https://docs.chef.io/chef_overview/#cookbooks) and [recipes](https://docs.chef.io/recipes/).
 * Synchronize [chef-repo](https://docs.chef.io/chef_repo/) with the version control system.
 * Run command-line tools.
 * Configure [policy](https://docs.chef.io/policy/), [roles](https://docs.chef.io/roles/), etc.
 * Interact with nodes to perform a one-off configuration.


#### == **Chef Cookbook** ==

A [Chef Cookbook](https://docs.chef.io/cookbooks.html) is the basic unit of configuration which defines a scenario and contains everything that supports the scenario. Two of its most important components are:

 * [**Recipes**](https://docs.chef.io/recipes.html) 
    A recipe is the most fundamental unit for configuration, which mostly contains resources, resource names, attribute-value pairs, and actions:
    ```ruby
    package "apache2" do
    action :install
    end

    service "apache2" do
    action [:enable, :start]
    end
    ```
 * [**Attributes**](https://docs.chef.io/attributes.html) 
    An attribute helps us define the state of the node. After each chef-client run, the node's state is updated on the Chef Server.

[Knife](https://docs.chef.io/workstation/knife/) provides an interface between a local chef-repo and the Chef Server.



#### == **Supported Platforms** ==

Chef supports the following [platforms](https://downloads.chef.io/chef/current) for Chef Client:

 * AIX
 * Linux distributions and FreeBSD
 * Unix-based systems
 * Mac OSX
 * Windows.

Chef Server is supported on the following [platforms](https://downloads.chef.io/chef-server/current):

 * Red Hat Enterprise Linux
 * SUSE Linux Enterprise Server
 * Ubuntu Linux.

Chef also has a GUI built on top of Chef Server, which can help us with running the cookbook from a browser, prepare reports, etc.


#### **Demo: Configuring a System with Chef**

Next, we will see how we can configure a system to a given state using Chef.

Configuring a System with Chef

#### == **Benefits of Using Chef** ==


Key benefits of using Chef are:

 * Chef is an open source systems integration framework.
 * It provides automation, scalability, High Availability, and consistency in deployment.
 * It is available for all major operating systems.
 * It provides role-based access control.
 * It provides real-time visibility, audits, and compliance with [Chef Automate](https://automate.chef.io/).


### **Salt Open**

#### == **Introduction to Salt Open** ==

[Salt Open](https://www.saltstack.com/open-source-projects/), or simply Salt, is an open source configuration management system built on top of a remote execution framework. It can be used in a client/server model or agentless model as well. In a client/server model the server sends commands and configurations to all the clients in a parallel manner, which the clients run, returning back the status. In the agentless mode, the server connects with remote systems via SSH. 

[SaltStack, Inc.](https://www.saltstack.com/), which stands behind Salt Open, offers also an enterprise product called [SaltStack Enterprise](https://www.saltstack.com/products/saltstack-enterprise/).

#### == **Salt Minions** ==

Each client is referred to as a Salt minion, receiving configuration commands from the Master, and reporting back results:

![Salt Minions](LFS151-salt-minions.png)

Minions can be installed on:

 * Unix-based systems
 * Windows
 * Mac OS X.


#### == **Salt Masters** ==

A server is referred to as a Salt master. Multi-master configuration is also supported:

![Salt Master](LFS151-salt-master.png)

In a default setup, the Salt master and minions communicate over a high-speed data bus, ZeroMQ, which requires an agent to be installed on each minion. Salt also supports an agentless setup using SSH for secure and encrypted communication between the master and the managed systems.


#### == **Other Components: Modules, Returners, Grains, Pillar Data**

Remote execution is based on [Execution Modules](https://docs.saltstack.com/en/latest/ref/modules/all/index.html) and [Returner Modules](https://docs.saltstack.com/en/latest/ref/returners/all/index.html).

Execution Modules provide basic functionality, like installing packages, managing files, managing containers, etc. All support modules are listed in the [Salt documentation](https://docs.saltstack.com/en/latest/py-modindex.html). We can also write custom modules.

Returners allow for minions' responses to be saved on the master or other locations. We can use default Returners or write custom ones.

All the information collected from minions is saved on the master. The collected information is referred to as Grains. Private information like cryptographic keys and other specific information about each minion which the master has is referred to as Pillar Data. Pillar Data is shared between the master and the individual minion.

By combining [Grains](https://docs.saltstack.com/en/latest/topics/grains/) and [Pillar Data](https://docs.saltstack.com/en/latest/topics/pillar/), the master can target specific minions to run commands on them. For example, we can query the hostname of all the nodes where the installed OS is Fedora 23.

With the above tools and information in hand, the master can easily set up a minion with a specific state. This can also be referred to as Configuration Management. Salt has different [State Modules](https://docs.saltstack.com/en/latest/ref/states/all/index.html) to manage a state.

#### **Demo: Configuring a System to a Given State Using Salt**

Next, we will see how to configure a system to a given state, using Salt.

Configuring a System to a Given State Using Salt

Salt Workshop Video 
https://www.youtube.com/watch?v=Tj2QHLW5UwQ&feature=youtu.be


#### == **Benefits of Using Salt Open**

Key benefits of using Salt Open are:

 * It is an open source configuration management system.
 * It provides automation, High Availability, and an event-driven infrastructure.
 * It provides role-based access control.
 * It supports agent-based and agentless deployments.
 * It is available for all major operating systems.


---- 
##  Chapter 13. Tools for Cloud Infrastructure II (Build & Release)

### **Introduction and Learning Objectives**

#### == **Introduction** ==

With source code management tools like Git, we can easily version the code and retrieve the same bits we saved in the past. This saves a lot of time and helps developers automate most of the non-coding activities, like creating automated builds, running tests, etc. Extending the same analogy to infrastructure would allow us to create a reproducible deployment environment, which is referred to as Infrastructure as a Code. 

Infrastructure as a Code helps us create a near production-like environment for development, staging, etc. With some tooling around them, we can also create the same environments on different cloud providers.  

By combining Infrastructure as a Code with versioned software, we are guaranteed to have a reproducible build and release environment every time. In this chapter, we will take a look at such tools: **Terraform**, **CloudFormation**, and **BOSH**.

#### == **Learning Objectives** ==


By the end of this chapter, you should be able to:

 * Discuss and use build and release tools: Terraform, CloudFormation, and BOSH.

### Terraform

#### == **Introduction to Terraform** ==

[Terraform](https://www.terraform.io/) is a tool that allows us to define the infrastructure as code. This helps us deploy the same infrastructure on Virtual Machines, bare metal, or cloud. It helps us treat the infrastructure as software. The configuration files can be written in [HCL](https://github.com/hashicorp/hcl) (HashiCorp Configuration Language).

#### ==  Terraform Providers

Physical machines, VMs, network switches, or containers are treated as resources, which are exposed by providers.

A provider is responsible for understanding API interactions and exposing resources, which makes Terraform agnostic to the underlying platforms. A custom provider can be created through plugins.

Terraform has providers in different stacks:

 * **IaaS**: AWS, DigitalOcean, GCP, OpenStack, Azure, Alibaba Cloud, etc.
 * **PaaS**: Heroku, Cloud Foundry, etc.
 * **SaaS**: Atlas, DNSimple, etc.


#### == **Features** ==

According to the [Terraform website](https://www.terraform.io/intro/index.html), it has the following "key features:

 * **Infrastructure as Code**: Infrastructure is described using a high-level configuration syntax. This allows a blueprint of your datacenter to be versioned and treated as you would any other code. Additionally, infrastructure can be shared and re-used.
 * **Execution Plans**: Terraform has a "planning" step where it generates an execution plan. The execution plan shows what Terraform will do when you call apply. This lets you avoid any surprises when Terraform manipulates infrastructure.
 * **Resource Graph**: Terraform builds a graph of all your resources, and parallelizes the creation and modification of any non-dependent resources. Because of this, Terraform builds infrastructure as efficiently as possible, and operators get insight into dependencies in their infrastructure.
 * **Change Automation**: Complex changesets can be applied to your infrastructure with minimal human interaction. With the previously mentioned execution plan and resource graph, you know exactly what Terraform will change and in what order, avoiding many possible human errors".

#### **Demo: Using Terraform to Treat Our Infrastructure as a Code**

The following demo will show us how to use Terraform to treat our infrastructure as a code.


#### == Benefits of Using Terraform

Key benefits of using Terraform are:

 * It allows us to build, change, and version infrastructure in a safe and efficient manner.
 * It can manage existing, as well as customized service providers. It is agnostic to underlying providers.
 * It can manage a single application or an entire datacenter.
 * It is a flexible abstraction of resources.


### CloudFormation

#### == **Introduction to CloudFormation** ==

[CloudFormation](https://aws.amazon.com/cloudformation/) is a tool that allows us to define our infrastructure as code on Amazon AWS. This helps us treat our AWS infrastructure as software. The configuration files can be written in YAML or JSON format, while CloudFormation can also be used from a web console or from the command line.

#### == **Features** ==

AWS CloudFormation provides an easy method to create and manage resources on AWS, adding order and predictability to the provisioning and updating processes. CloudFormation uses templates to describe the AWS resources and dependencies needed to run applications. It also allows for resources to be modified and updated while applying version control to the AWS infrastructure, similarly to software version control.

CloudFormation presents the following features:

 * **Extensibility** 
    It supports the modeling, provisioning, and management of third-party app resources through [AWS CloudFormation Registry](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/registry.html), for monitoring, incident management, and version control.
 * **Authoring with JSON or YAML**
    To model the infrastructure and describe resources in a text file. The CloudFormation Designer helps with visual design when required.  
 * **Authoring with programming languages**
    Through AWS Cloud Development Kit (AWS CDK) it supports TypeScript, Python, Java, and .Net to model cloud applications, integrated with CloudFormation for infrastructure provisioning.
 * **Safety controls**
    It provides Rollback Triggers to safely roll back to a prior state.
 * **Preview environment changes**
    To model the possible impact of the proposed changes to any of the existing resources.
 * **Dependency management**
    Determines actions sequence during stack operations.
 * **Cross-account and cross-region management**
    Allowed from a single template, called StackSet.


#### **Demo: Using CloudFormation to Treat Our Infrastructure as a Code**

The following demo will show us how to use CloudFormation to treat our AWS infrastructure as a code.

Using CloudFormation to Treat Our Infrastructure as a Code

#### == **Benefits of Using CloudFormation** ==

Key benefits of using CloudFormation are:

 * It allows us to build, change, and version all AWS infrastructure in a safe and efficient manner.
 * It supports text file configuration files, multiple programming languages, and CLI.
 * It allows for safe and repeatable infrastructure management.
 * It treats infrastructure as code, allowing quick edits and version control.


### **BOSH**

#### == **Introduction to BOSH** ==

According to [bosh.io](https://bosh.io/),

    "BOSH is an open source tool for release engineering, deployment, lifecycle management, and monitoring of distributed systems". 

BOSH was primarily developed to deploy the Cloud Foundry PaaS, but it can deploy other software as well (e.g. Hadoop). BOSH supports multiple Infrastructure as a Service (IaaS) providers. BOSH creates VMs on top of IaaS, configures them to suit the requirements, and then deploys the applications on them. Supported IaaS providers for BOSH are:

 * Amazon Web Services EC2
 * OpenStack
 * VMware vSphere
 * vCloud Director.

With the Cloud Provider Interface (CPI), BOSH supports additional IaaS providers such as [Google Compute Engine](https://github.com/cloudfoundry/bosh-google-cpi-release) and [Apache CloudStack](https://github.com/orange-cloudfoundry/bosh-cloudstack-cpi-release).

#### == **Key Concepts** ==

Key concepts around BOSH are detailed next:

 * [**Stemcell**](https://bosh.io/docs/stemcell.html) 
    A Stemcell is a versioned, IaaS-specific, operating system image with some pre-installed utilities such as the BOSH Agent. Stemcells do not contain any application-specific code. The BOSH team is in charge of releasing stemcells, which are listed on the "Stemcells" web page. 
 * [**Release**](https://bosh.io/docs/release.html) 
    A Release is placed on top of a Stemcell, and consists of a versioned collection of configuration properties, templates, scripts, and source code to build and deploy software. 
 * [**Deployment**](https://bosh.io/docs/deployment.html) 
    A Deployment is a collection of VMs which are built from Stemcells, populated with specific Releases on top of them, and having disks to keep persistent data.
 * [**BOSH Director**](https://bosh.io/docs/bosh-components.html#director)
    The BOSH Director is the central orchestrator component of BOSH, which controls the VM creation and deployment. It also controls software and service lifecycle events. We need to upload Stemcells, Releases, and Deployment manifest files to the Director. The Director processes the manifest file and runs the deployment.



#### == **Sample Deployment** ==

Next, we will reproduce an example of a sample [deployment manifest](https://bosh.io/docs/deployment-basics.html) from the BOSH website. This sample deployment manifest must be then uploaded to the BOSH Director:

```
---
name: zookeeper

releases:
- name: zookeeper
  version: 0.0.5
  url: https://bosh.io/d/github.com/cppforlife/zookeeper-release?v=0.0.5
  sha1: 65a07b7526f108b0863d76aada7fc29e2c9e2095

stemcells:
- alias: default
  os: ubuntu-xenial
  version: latest

update:
  canaries: 2
  max_in_flight: 1
  canary_watch_time: 5000-60000
  update_watch_time: 5000-60000

instance_groups:
- name: zookeeper
  azs: [z1, z2, z3]
  instances: 5
  jobs:
  - name: zookeeper
    release: zookeeper
    properties: {}
  vm_type: default
  stemcell: default
  persistent_disk: 10240
  networks:
  - name: default

- name: smoke-tests
  azs: [z1]
  lifecycle: errand
  instances: 1
  jobs:
  - name: smoke-tests
    release: zookeeper
    properties: {}
  vm_type: default
  stemcell: default
  networks:
  - name: default

```


#### **Demo: Deploying the Infrastructure with BOSH**

In the following demo, we will see how we can use BOSH to deploy our infrastructure, and then deploy an application on top of it.

Deploying the Infrastructure with BOSH

#### == **Benefits of Using BOSH** ==

Some of the benefits of using BOSH are:

 * It is an open source tool "for release engineering, deployment, lifecycle management, and monitoring of distributed systems" (bosh.io).
 * It allows for easy identification of a release's components, such as the source, tools, and environment.
 * It guarantees stability and reproducibility.
 * It provides a stable framework for the development and consistent deployments of software components.
 * It integrates with CI/CD.
 * It supports IaaS providers like AWS, OpenStack, VMware vSphere, Google Compute Engine, etc.

-----

## Chapter 14. Tools for Cloud Infrastructure III (Key-Value Pair Store)

### **Introduction and Learning Objectives**

#### == **Introduction** ==

While building any distributed and dynamically-scalable environment, we need an endpoint which is a single point of truth. For example, we need such an endpoint if we want each node in a distributed environment to look up specific configuration values before performing an operation. For such cases, all the nodes can reach out to a central location and retrieve the value of a desired variable or key.

As the name suggests, the **Key-Value Pair Storage** provides the functionality to store or retrieve the value of a key. Most of the Key-Value stores provide REST APIs to support operations like GET, PUT, and DELETE, which help with operations over HTTP. Some examples of Key-Value stores are:

 * **etcd**
 * **Consul**
 * **ZooKeeper**.


#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Discuss key-value store tools like etcd, Consul, and ZooKeeper.


### **etcd**

#### == **Introduction to etcd** ==

[etcd](https://etcd.io/) is an open source distributed key-value pair storage, and it uses the [Raft consensus algorithm](https://raft.github.io/) for communication between instances of a distributed multi-instance scenarios. It was started by [CoreOS](https://coreos.com/) and it is written in Go. Currently, the etcd project is part of the [CNCF](https://www.cncf.io/) family of cloud-native technologies. 


Raft presentation : http://thesecretlivesofdata.com/raft

#### == **Features** ==

etcd can be configured to run standalone or in a distributed cluster. In cluster mode, for High Availability, it can gracefully handle the master election during network partitions and can tolerate machine failures, including the master.

It allows users or services to watch the value of a key, to then perform certain operations as a result of any change in that particular value.

It is currently being used in many projects such as Kubernetes, rook, locksmith, vulcand, Doorman, CoreDNS, and OpenStack. 

#### == **Use Cases** ==

Some of the [cases](https://github.com/etcd-io/etcd/blob/master/ADOPTERS.md) in which etcd is used are:

 * Store connections, configuration, cluster bootstrapping keys, and other settings.
 * Service Discovery in conjunction with tools like [skyDNS](https://github.com/skynetservices/skydns).
 * Metadata and configuration data for service discovery.
 * Container management.

#### == **Benefits of Using etcd** ==

Key benefits of using etcd are:

 * It is an open source distributed key-value pair storage.
 * It provides reliable data storage across a cluster of machines.
 * It is fast, benchmarked at 10,000 writes/s per instance. 
 * It is easy to deploy, set up, and use.
 * It provides seamless cluster management across a distributed system.
 * It is secured by optional SSL client certificates for authentication and offers very good documentation.

### **Consul**


#### == **Introduction to Consul** == 

[Consul](https://www.consul.io/), of [HashiCorp](https://www.hashicorp.com/), is a distributed, highly-available system which can be used for service discovery and configuration.

Other than providing a distributed key-value store, it also provides features like:

 * Service discovery in conjunction with DNS or HTTP
 * Health checks for services and nodes
 * Multi-datacenter support.

While Consul can be configured on a single node, a multi-node configuration is recommended. Consul is built on top of [Serf](https://www.serf.io/), which provides membership, failure detection, and event broadcast. Consul also uses the [Raft consensus algorithm](https://raft.github.io/) for leadership election and consistency.

#### == **Use Cases** ==

Some of the cases in which Consul is used are:

 * Store connections, configuration, and other settings.
 * Service discovery and health checks in conjunction with DNS or HTTP.
 * Network infrastructure automation with dynamic load balancing while reducing downtime and outages.
 * Multi-platform secure service-to-service communication.


#### == **Benefits of Using Consul** ==

Key benefits of using Consul are:

 * It is a distributed, scalable, highly-available system which can be used for service discovery and configuration.
 * It provides health checks for services and nodes, together with load balancing.
 * It provides out-of-the-box native multi-datacenter support.
 * It implements an embedded service discovery and telemetry. 
 * Traffic management by leveraging Blue/Green or Canary deployment patterns.

### **ZooKeeper**


#### == **Introduction to ZooKeeper** ==

[ZooKeeper](https://zookeeper.apache.org/) is a centralized service for maintaining configuration information, providing distributed synchronization together with group services for distributed applications. It is an open source project under The [Apache Software Foundation](https://www.apache.org/).

ZooKeeper aims to provide a simple interface to a centralized coordination service, that is also distributed and highly reliable. It implements Consensus, group management, and presence protocols on behalf of applications.

#### == **Use Cases** ==

Some of the cases in which ZooKeeper is used are:

 * Implement node coordination in a clustered environment.
 * To manage cloud node memberships while coordinating distributed jobs.
 * As a backing store for distributed and scalable data structures.
 * Election of a High Availability master.
 * As light-weight failover and load balancing manager.


#### == **Benefits of Using ZooKeeper** ==

Key benefits of using ZooKeeper are:

 * It offers a hierarchical key-value store for distributed systems.
 * It supports high availability through redundant services.
 * Its nodes store data in a hierarchical namespace, similar to a file system or a tree data structure. 
 * It manages updates in an ordered fashion by behaving like an atomic broadcast system.


----

## Chapter 15. Tools for Cloud Infrastructure IV (Image Building)

### **Introduction and Learning Objectives**

#### == **Introduction** ==

In an immutable infrastructure environment, we prefer to replace an existing service with a new one, to fix any problems/bugs or to perform an update.

Whether we start a new service or replace an older service with a new one, we need the image from which the service can be started. These images should be created in an automated fashion. In this section, we will take a look into the creation of **Docker** container images, and VM images for different cloud platforms using **Packer**.

#### == **Learning Objectives** ==


By the end of this chapter, you should be able to:

 * Create Docker images and VM images for different cloud platforms using Packer.


### **Building Docker Images**

#### == **Dockerfiles** ==

We can create a custom Docker image by starting a container from the base image and, after making the required changes (e.g. installing the software), we can use the `docker commit `command to save it to persistent storage. This is not a scalable and efficient solution.

Docker has a feature that allows it to read instructions from a text file and then generate the requested image. Internally, it creates a container after each instruction and then commits it to persistent storage. The file with instructions is referred to as a [Dockerfile](https://docs.docker.com/engine/reference/builder/). Below we present a sample Dockerfile:

```
FROM fedora
RUN dnf -y update && dnf clean all
RUN dnf -y install nginx && dnf clean all
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN echo "nginx on Fedora" > /usr/share/nginx/html/index.html

EXPOSE 80

CMD [ "/usr/sbin/nginx" ]
```

`FROM`, `RUN`, `EXPOSE`, and `CMD` are reserved instructions, and are followed by arguments. The instructions are well-documented in the [Docker Documentation](https://docs.docker.com/engine/reference/builder/).

Typically, Dockerfiles start from a parent image or a [base image](https://docs.docker.com/develop/develop-images/baseimages/), specified with the FROM instruction. A parent image is an image used as a reference, modified by subsequent Dockerfile instructions. Parent images can be built directly out of working machines, or with tools such as [Debootstrap](https://wiki.debian.org/Debootstrap). A base image has FROM scratch in the Dockerfile.


#### *Demo: Creating a Custom Image from Dockerfile*

You may have noticed that we always start with a base image when creating an image from Dockerfile. Someone has to build those images for the first time. There are tools like [Debootstrap](https://wiki.debian.org/Debootstrap) or [supermin](https://github.com/libguestfs/supermin) which can help you build base images.

The [Docker GitHub source code repository](https://github.com/docker/docker/tree/master/contrib) also has helper scripts to create base images.

In the following video, we will see how we can create a custom image from Dockerfile.

Creating a Custom Image from Dockerfile


#### == **Multi-Stage Dockerfile** ==

A [multi-stage build](https://docs.docker.com/develop/develop-images/multistage-build/) is a newer feature of Docker, very useful for optimizing the Dockerfiles and minimizing the size of a container image. Through a multi-stage build, we create a new Docker image in every stage. Every stage can copy files from images created either in earlier stages or from already available images.

For example, in the first stage, we can copy the source code, compile it to create a binary, and then, in the second stage, we can copy just the binary to a resulting image. Before multi-stage builds, we would need to create multiple Dockerfiles to achieve similar results.

Let's take a look at the following Dockerfile:

```
# stage - 1
FROM ubuntu AS buildstep
RUN apt update && apt install -y build-essential gcc
COPY hello.c /app/hello.c
WORKDIR /app
RUN gcc -o hello hello.c && chmod +x hello

# stage - 2
FROM ubuntu
RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app
COPY --from=buildstep /app/hello ./hello
COPY ./start.sh ./start.sh
ENV INITSYSTEM=on
CMD ["bash", "/usr/src/app/start.sh"]
```
The `COPY --from=buildstep` line copies only the built artifact from the previous stage into this new stage. The C SDK and any intermediate artifacts are not copied in the final image.


### **Packer**

#### == **Introduction to Packer** ==

[Packer](https://www.packer.io/) from [HashiCorp](https://www.hashicorp.com/) is an open source tool for creating virtual images from a configuration file for different platforms. Configuration files are written using HCL (HashiCorp Configuration Language).

Below is a sample configuration file for two different cloud platforms, Amazon EC2 and Google Compute Engine:
```
{
  "variables": {
    "aws_access_key": "abc",
    "aws_secret_key": "xyz",
    "atlas_token": "123"
  },
  "builders": [{
    "type": "amazon-ebs",
    "access_key": "{{user `aws_access_key`}}",
    "secret_key": "{{user `aws_secret_key`}}",
    "region": "us-west-2",
    "source_ami": "ami-9abea4fb",
    "instance_type": "t2.micro",
    "ssh_username": "ubuntu",
    "ami_name": "packer-example {{timestamp}}"
  }, {
  "type": "googlecompute",
  "account_file": "user.json",
  "project_id": "useapp",
  "source_image": "ubuntu-1404-trusty-v20160114e",
  "zone": "us-central1-a",
  "image_name": "myimage"
  }],
  "provisioners": [{
    "type": "shell",
    "inline": [
      "sleep 30",
      "#!/bin/bash",
      "sudo apt-get -y update",
      "sudo apt-get -y install apache2",
      .....
      .....
    ]
  }],
  "post-processors": [{
        "type": "atlas",
        "only": ["amazon-ebs"],
        "token": "{{user `atlas_token`}}",
        "artifact": "user/stack",
        "artifact_type": "amazon.image",
        "metadata": {
          "created_at": "{{timestamp}}"
        }
      }, {
        "type": "atlas",
        "only": ["googlecompute"],
        "token": "{{user `atlas_token`}}",
        "artifact": "user/stack",
        "artifact_type": "googlecompute.image",
        "metadata": {
          "created_at": "{{timestamp}}"
        }
  }]
}
```


#### == **Steps to Create Virtual Images** ==

In general, there are three steps to create virtual images:

 * **Building the base image** 
    This is defined under the builders section of the configuration file. Packer supports the following platforms: Amazon EC2 (AMI), DigitalOcean, Docker, Google Compute Engine, Microsoft Azure, OpenStack, Parallels (PVM), QEMU, VirtualBox (OVF), and VMware (VMX). Other platforms can be added via plugins. In the example we provided, we have created images for Amazon EC2 and Google Compute Engine. Packer offers an advanced feature where it supports multiple images to be created from the same template file. It is called parallel builds, and it ensures "near-identical" images built for different environments.  
 * **Provision the base image for configuration**
    Once we built a base image, we can then provision it, to further configure it and make changes like install software, etc. Packer supports different provisioners such as Shell, Ansible, Puppet, and Chef. In the example provided, we are using Shell as a provisioner.
 * **Perform optional post-build operations** 
    Post-processors allow us to copy/move the resulted image to a central repository or create a Vagrant box.


#### **Demo: Building Images with Packer**

Next, let's see how we can build images using Packer.

Building Images with Packer

#### == **Benefits of Using Packer** ==

Key benefits of using Packer are:

 * It is an open source tool for creating virtual images from a configuration file for different platforms.
 * It is easy to use.
 * It automates the creation of images from a template.
 * It provides an extremely fast infrastructure deployment, benefiting both development and production environments. 
 * It offers a multi-provider portability.



---- 
## Chapter 16. Tools for Cloud Infrastructure V (Debugging, Logging, and Monitoring for Containerized Applications)


### **Introduction and Learning Objectives**

#### == **Introduction** ==

When we experience problems in development or production, we resort to debugging, logging, and monitoring tools to find the root cause of those problems. Some of the tools we should be familiar with are:

 * strace
 * SAR (System Activity Reporter)
 * tcpdump
 * GDB (GNU Project Debugger)
 * syslog
 * Nagios
 * Zabbix.

We can use the same tools on bare metal and VMs, but containers bring additional challenges:

 * Containers are ephemeral, so, when they are deleted, all their metadata, including logs, gets deleted as well, unless we store it in some other, possibly persistent storage location.
 * Containers do not have kernel space components.
 * We want to keep a container's footprint as small as possible, but installing debugging and monitoring tools make that nearly impossible.
 * Collecting per container statistics, debugging information individually, and then analyzing data from multiple containers is a tedious process.

It would be beneficial to have external tools for logging and monitoring instead of collecting them directly from individual containers. This may be achieved because each container runs as a process on the host OS, which has complete control of that process. Once we have collected the data from all the containers running on a system or in a cluster, like Kubernetes or Swarm, we can run a logical mapping of all collected logs and get a system/cluster-wide view.

Below are some of the tools which we can use for containerized applications:

 * **Debugging**: Docker CLI, Sysdig
 * **Logging**: Docker CLI, Docker Logging Driver
 * **Monitoring**: Docker CLI, Sysdig, cAdvisor, Prometheus, Datadog, New Relic.


#### == **Native Docker Features for Debugging** ==


Docker has some built-in command-line options that help with debugging, logging, and monitoring:

 * Debugging:

   1. `docker inspect`
   2. `docker logs`

 * Logging: 

    1. `docker logs`
    2. [Docker Logging Drivers](https://docs.docker.com/config/containers/logging/configure/): With the logging driver we can choose a Docker daemon wide or per container logging policy. Depending on the policy, Docker forwards the logs to the corresponding drivers. Docker supports the following drivers: jsonfile, syslog, journald, gelf (Graylog Extended Log Format), fluentd, awslogs, splunk. Once the logs are saved in a central location, we can use the respective tools to get the insights.

 * Monitoring:

    1. `docker stats`
    2. `docker top`


#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Describe and use debugging, logging, and monitoring tools for containerized applications.
 * Describe tools such as Sysdig, cAdvisor, Datadog, Fluentd, and Prometheus.

Learning Objectives Icon: a page in a notepad with three bullet points


### **Sysdig**

#### == **Introduction to Sysdig** ==

Sysdig provides an on-cloud and on-premise platform for container security, monitoring and forensics. According to [sysdig.com](https://sysdig.com/), sysdig is

    "strace + tcpdump + htop + iftop + lsof + awesome sauce".

It has two open source tools along with their paid enterprise-class offerings.

 * **Sysdig**
    It saves low-level system information from a running Linux instance, that can be filtered and further analyzed.
 * **Sysdig Monitor**
    It is a paid offering that provides full-stack monitoring and alerting in addition to the open source version, together with a dashboard and Prometheus compatibility.
 * **Enterprise Falco**
    It is a container-native tool that can help us gain visibility of containers and applications down to the finest details. It can collect information at the system, network, and file level. With rule-sets, we can provide our container security information and then take action based on them. For example, if a container does not satisfy the security requirements, Falco can kill the container, or send out a notification.
 * **Sysdig Secure**
    It is also a paid offering that can identify vulnerabilities, check compliance, block threats, and improved response time.

A Sysdig agent needs to be installed on all the nodes if we want to collect the information at a central location. In addition, a kernel component has to be installed in order to capture system calls and OS events.

#### == **Features of Sysdig Tools** ==

Following are some of the key features of Sysdig tools:

 * Sysdig tools have native support to many applications, infrastructure, and container technologies, including Docker, Kubernetes, Mesos, AWS, and Google Cloud Platform.
 * Paid offerings provide alerting, a dashboard, and team management.
 * They offer a programmatic interface with every part of Sysdig Monitor.


#### == **Benefits of Sysdig Tools** ==

Key benefits of Sysdig tools are:

 * The tools capture low-level system information from the running Linux instance and containers.
 * They offer native support for all Linux container technologies such as Docker and LXC.
 * They are easy to install.
 * They are built to run in production, minimizing performance overhead and the risk of crashes.
 * They are Kubernetes-aware.


### **cAdvisor**

#### == **Introduction to cAdvisor** ==

[cAdvisor](https://github.com/google/cadvisor/) (Container Advisor) is an open source tool to collect resource usage and performance characteristics for the host system and running containers. It collects, aggregates, processes, and exports information about running containers. As of now, it has native support for Docker and should also support other container runtimes out of the box.

#### == **Using cAdvisor** ==

We can enable the cAdvisor tool to run as a Docker container and start collecting statistics with the following command:

```
sudo docker run \
  --volume=/:/rootfs:ro \
  --volume=/var/run:/var/run:rw \
  --volume=/sys:/sys:ro \
  --volume=/var/lib/docker/:/var/lib/docker:ro \
  --publish=8080:8080 \
  --detach=true \
  --name=cadvisor \
  google/cadvisor:latest
```

and point the browser to http://host_IP:8080 to get live statistics. cAdvisor exposes its raw and processed statistics via a versioned remote REST API. It also supports exporting statistics for [InfluxDB](https://www.influxdata.com/products/influxdb-overview/). cAdvisor exposes container statistics as [Prometheus metrics](https://prometheus.io/). Prometheus is an open source community-driven system and service monitoring toolkit.

#### == **Host System Resource Usage with cAdvisor** ==

In the screenshots below, we can see details about the processes on the host system, as well as the CPU usage.

![Host System Resource Usage with cAdvisor](LFS151-cAdvisor-Processes_on_the_Host_System_and_CPU_Usage.jpg)


#### == **Docker Host Specific Details with cAdvisor** ==

he images below show the number of running containers, as well as details about Docker Engine and images.

![Docker Host Specific Details with cAdvisor](LFS151-cAdvisor-Running_Containers__Docker_Engine__and_Images.jpg)


### **Elasticsearch**

#### == **Introduction to Elasticsearch** ==

[Elasticsearch](https://www.elastic.co/elasticsearch/) is the distributed search and analytics engine at the core of the Elastic Stack. It is responsible for data indexing, search, and analysis. [Kibana](https://www.elastic.co/kibana) is an interface that enables interactive data visualization while providing insights into the data. 

Elasticsearch provides real-time search and analytics for structured or unstructured text, numerical data, or geospatial data. It is optimized to efficiently store and index data in order to speed up the search process. 

#### == **Features and Benefits** ==

Elasticsearch paired with Kibana is the desired open source solution for diverse use cases such as machine learning, security, and reporting. 

Key features and benefits of Elasticsearch are:

 * It is a distributed solution, that is scalable, highly available, supporting cross-cluster and cross-datacenter replication.
 * It offers a variety of management tools and APIs to allow control over data, users, cluster operations, snapshots and restores.
 * It secures both the access methods to data through encryption and the data itself.
 * It provides alerts when certain changes have been identified in the data.
 * It supports a variety of clients in addition to standard RESTful APIs and JSON, such as Java, Python, SQL, and PHP.
 * Its functionality can be extended through plugins.
 * It can be deployed in the private, public, or hybrid cloud.
 * It supports tools that allow for data to be ingested and enriched through analyzers, tokenizers, and filtering.
 * It supports a variety of data types, such as text, numbers, vectors, histograms, date & time series, structured and unstructured data. 
 * It supports advanced searching mechanisms, analytics, aggregation, machine learning, and alerting. 


### **Fluentd**

#### == **Introduction to Fluentd** ==

According to [fluentd.org](http://www.fluentd.org/), Fluentd is

    "an open source data collector, which lets us unify the data collection and consumption for a better use and understanding of data".

![Fluentd Architecture](LFS151-Fluentd_Architecture.png)

Fluentd tries to structure the data in JSON as much as possible. It supports more than 300 [plugins](http://www.fluentd.org/plugins) to connect input sources to output sources, after performing filtering, buffering and routing.

#### == **Docker Support for Fluentd** ==

From version 1.8, Docker supports [logging drivers](https://docs.docker.com/config/containers/logging/configure/), and Fluentd is one of them.

![Fluentd and Docker Integrated](LFS151-Fluentd_and_Docker_Integrated.png)

We can either configure the logging driver for the Docker daemon or specify it while starting a container.

#### == **Benefits of Using Fluentd** ==

Key benefits of using Fluentd are:

 * It is an open source data collector.
 * It is simple, fast, and flexible.
 * It is performant and developer-friendly. 


### **Datadog**

#### == **Introduction to Datadog** ==

[Datadog](https://www.datadoghq.com/) provides monitoring and analytics as a service for Development and OPs teams. Some of the systems, applications and services it connects to are:

 * Amazon EC2
 * Apache
 * Java
 * MySQL
 * CentOS.

A detailed [list of integration](https://www.datadoghq.com/product/integrations/) can be found in the official documentation. We need to install an agent in the host system, which sends the data to the Datadog's server. Once the data is sent, we can:

 * Build an interactive dashboard.
 * Search and co-relate matrices and events.
 * Share the matrices and events.
 * Receive alerts.


#### == **Docker Containers: Kubernetes Monitoring with Datadog** ==

The image below illustrates valuable information about containers:

 * The number of nodes in the cluster
 * The running and stopped containers
 * The most resource-consuming pods
 * Docker logs, etc. 

![Docker Containers - Kubernetes Monitoring with Datadog](LFS151-docker-2fcb7c38.png)

#### == **Benefits of Using Datadog** ==

Key benefits of using Datadog are:

 * It comes pre-integrated with well-known third-party applications.
 * It provides a seamless workflow, regardless of platform, location, or language.
 * It configures information filtration to get only needed metrics.
 * It allows us to enable the system to send alerts or notifications when serious issues arise.
 * It offers tools for team collaboration.
 * It is scalable. 


### **Prometheus**

#### == **Introduction to Prometheus** ==

[Prometheus](https://prometheus.io/) is an open source tool used for system monitoring and alerting. It was originally developed by SoundCloud and is now one of the graduated projects at the [CNCF](https://cncf.io/) Foundation.

Prometheus is suitable for recording any purely numeric time-series data. It works well for both machine-centric monitoring like CPU, memory usage, and monitoring of highly dynamic service-oriented architectures. It is primarily written in Go.

#### == **Prometheus Features** ==

Key Prometheus features include:

 * It is very reliable.
 * It supports a multi-dimensional data model with time series data identified by metric name and key/value pairs.
 * It supports a query language to effectively query the collected time series data.
 * It support metrics collection through pull- and push-based mechanism.
 * It can discover target endpoints via service discovery or static configuration.
 * It can connect with external tools like Grafana and Pagerduty for dashboarding and alerting.
 * It supports client libraries for various programming languages such as Go, Java, and Python to add instrumentation to their code.


#### == **Prometheus Architecture** ==

The following diagram shows the Prometheus architecture:

 ![Prometheus Architecture](LFS151-Prometheus_Architecture1.png)

As shown in the above graphic, the Prometheus server is the primary component that scrapes and stores pull- and push-based metrics. Once the data is collected, we can query it using the Prometheus Web UI or export it using HAProxy, StatsD, or Graphite. The [Alertmanager](https://prometheus.io/docs/alerting/alertmanager/) handles alerts.

#### == **Demo: Exploring Prometheus** ==

In this video, we will explore Prometheus.

Exploring Prometheus


### **Splunk**

#### == **Introduction to Splunk**

[Splunk](https://splunk.com/) includes a family of products aiming to deliver highly scalable and fast real-time insight into enterprise data. It allows for data aggregation and analysis, with unique investigative methods. 

Splunk can be deployed on-premise or in the cloud, while also capable to scale across many data sources. It provides interactive dashboards for users to visualize data, and also to setup actions and alerts. Dashboards are highly customizable and support augmented reality together with virtual reality to enhance the level of experience even for the non-technical users.

#### == **Features and Benefits** ==

Key features and benefits of Splunk are:

 * It provides easy data aggregation and analysis.
 * It is reliable and can be deployed securely in the cloud as a highly scalable service.
 * It supports real-time data streaming while collecting, processing, and distributing the data.
 * It supports advanced features such as custom access control, performance acceleration, high availability, and clustering as part of the Enterprise and Cloud platforms. 


----

## Chapter 17. Service Mesh

### **Introduction and Learning Objectives**

#### == **Introduction** ==

Service mesh is a network communication infrastructure layer for a microservices-based application. When multiple microservices are communicating with each other, a service mesh allows us to decouple resilient communication patterns such as circuit breakers and timeouts from the application code.

#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Describe service mesh features.
 * Discuss service mesh technologies such as Envoy, Istio, and Linkerd.


### **Features and Implementation of Service Mesh**

#### == **Features and Implementation of Service Mesh** ==

Service mesh is generally implemented using a sidecar proxy. A sidecar is a container that runs alongside the primary application and complements it with additional features like logging, monitoring, and traffic routing. In the service mesh architecture, the sidecar pattern implements inter-service communication, monitoring, or other features that can be decoupled and abstracted away from individual services.

The following are some of the features of service mesh:

 * **Communication**
    It provides flexible, reliable, and fast communication between various service instances.
 * **Circuit Breakers**
    It restricts traffic to unhealthy service instances.
 * **Routing**
    It passes a REST request for /foo from the local service instance, to which the service is connected.
 * **Retries and Timeouts**
    It can automatically retry requests on certain failures and can timeout requests after a specified period.
 * **Service Discovery**
    It discovers healthy, available instances of services.
 * **Observability**
    It monitors latency, traces traffic flow, and generates access logs.
 * **Authentication and Authorization**
    It can authenticate and authorize incoming requests.
 * **Transport Layer Security (TLS) Encryption**
    It can secure service-to-service communication using TLS.


#### == **Data Plane and Control Plane** ==

Similar to the Software-Defined Networking explored earlier, a service mesh also features Data and Control Planes.

 * **Service Mesh Data Plane**
    It provides features that we mentioned on the previous page. It touches every packet/request in the system.
 * **Service Mesh Control Plane**
    It provides policy and configuration for the Data Plane. For example, by using the control plane, we can specify settings for load balancing and circuit breakers.


#### == **Service Mesh Project Landscape** ==

There are many service mesh projects, which can be divided into two categories:

 * Data Plane

    * Linkerd
    * NGINX
    * HAProxy
    * Envoy
    * Traefik/Maesh.

 * Control Plane

    * Istio
    * Nelson
    * SmartStack.



### **Consul**

#### == **Introduction to Consul** ==

[Consul](https://www.consul.io/) by HashiCorp is an open source project aiming to provide a secure multi-cloud service networking through automated network configuration and service discovery.

Consul's strength lies in its support to connect services running in multiple datacenters. It is highly available for fault tolerance and increased performance, while it supports thousands or tens of thousands of simultaneous client services. 

Client services are capable of automatic discovery of servers, while a distributed agent and node failure detection supports scalability more than the traditional heartbeating schemes. Very low coupling between datacenters, together with failure detection, connection caching, and multiplexing ensures fast and reliable cross-datacenter requests.

Consul forwards RPC requests between remote Consul servers when a request is made for a resource available in another datacenter. If the remote datacenter is not available, then the remote resources will not be available either.

Data caching is also supported by Consul, like connect certificates or optional results, which help with local decisions making about incoming connection requests even if the connection between servers is disrupted or if the servers are temporarily unavailable.

#### == **Features and Benefits of Consul** ==

The following are key features and benefits of Consul:

 * It is an open source project.
 * It can be deployed on any runtime or infrastructure, including bare-metal, VMs, any cloud, and Kubernetes clusters.
 * It deploys easily on Kubernetes through Helm, and it is injected as a sidecar container in Kubernetes objects.
 * It integrates with CI/CD tools.
 * It provides dynamic load balancing, service discovery, health checking, and reduced downtimes.
 * It provides good monitoring using statistics, logging, and distributed tracing.
 * It provides mTLS communication encryption between resources.


### **Envoy**

#### == **Introduction to Envoy** ==

[Envoy](https://www.envoyproxy.io/) is a Cloud Native Computing Foundation (CNCF) project, which was originally built by [Lyft](https://www.lyft.com/). It is an open source project that provides an L7 proxy and communication bus for large, modern, service-oriented architectures.

Envoy has an out-of-process architecture, which means it is not dependent on the application code. It runs alongside the application and communicates with the application on localhost. We referred to this earlier as a sidecar pattern.

With the Envoy sidecar implementation, applications need not be aware of the network topology. Envoy can work with any language and can be managed independently.

Envoy can be configured as service and edge proxy. In the service type of configuration, it is used as a communication bus for all traffic between microservices. With the edge type of configuration, it provides a single point of ingress to the external world.


#### == **Features and Benefits of Envoy** ==

The following are key features and benefits of the Envoy proxy:

 * It is an open source project.
 * It makes the network transparent to the applications.
 * Due to its out-of-process architecture, it can be run alongside any language or runtime.
 * It has support for HTTP/2 and gRPC for both incoming and outgoing connections.
 * It provides all the features of service mesh that we mentioned earlier, like load balancing, service discovery, circuit breakers, etc.
 * It provides good monitoring using statistics, logging, and distributed tracing.
 * It can provide SSL communication.


### **Istio**

#### == **Introduction to Istio** ==

[Istio](https://istio.io/) is one of the most popular service mesh solutions. It is an open source platform, backed by companies like Google, IBM and Lyft.

#### == **Istio Architecture** ==

Istio is divided into the following two planes:

 * **Data Plane**
    It is composed of a set of Envoy proxies deployed as sidecars to provide a medium for communication and to control all network communication between microservices.
 * **Control Plane**
    It manages and configures proxies to route traffic, enforces policies at runtime, and collects telemetry. The control plane includes the Citadel, Gallery, and Pilot.

 ![Istio Architecture](LFS151-istio-arch.svg)

#### == **Istio Components** ==

The main components of Istio are:

 * **Envoy Proxy**
    Istio uses an extended version of the Envoy proxy, using which it implements features like dynamic service discovery, load balancing, TLS termination, circuit breakers, health checks, etc. Envoy is deployed as sidecars.
 * **Istiod**
    Istiod provides service discovery, configuration, and certificate management.



#### == **Features and Benefits of Istio** ==

Key features and benefits of Istio are:

 * Traffic control to enforce fine-grained traffic control with rich routing rules and automatic load balancing for HTTP, gRPC, WebSocket, and TCP traffic.
 * Network resiliency to setup retries, failovers, circuit breakers, and fault injection.
 * Security and authentication to enforce security policies and enforce access control and rate limiting defined through the configuration API.
 * Pluggable extensions model based on WebAssembly that allows for custom policy enforcement and telemetry generation for mesh traffic.


#### **Demo: Using Istio Service Mesh for Traffic Management**

Next, we will use the Istio service mesh for traffic management.

Using Istio Service Mesh for Traffic Management


### **Kuma**

#### == **Introduction to Kuma** ==

[Kuma](https://kuma.io/) by [Kong](https://konghq.com/) is a platform-agnostic, modern and universal open source control plane for Service Mesh. It can be easily setup on VMs, bare-metal, and on Kubernetes.

Kuma is based on the Envoy proxy - a sidecar proxy designed for cloud-native applications, that supports monitoring, security, and reliability for microservice applications at scale. With Envoy used as a data plane, Kuma can handle any L4/L7 traffic to observe and route traffic between services.

While Kuma can be used by novices, it also provides data plane configuration policies for more experienced Service Mesh users.

#### == **Kuma Architecture** ==

Kuma includes the following two planes:

 * **Control-Plane**
    Kuma is a control-plane that creates and configures policies that manage services within the Service Mesh.
 * **Data-Plane**
    Implemented on top of Envoy, runs as an instance together with every service to process incoming and outgoing requests.

![Kuma Architecture](LFS151-kuma-arch-main-diagram_2x.png)


#### == **Kuma Modes** ==

Kuma is a universal control plane that can run on modern environments like Kubernetes and on more traditional VM instances. Such flexibility is achieved through two separate running modes:

 * **Universal Mode**
    Installed on a Linux compatible system such as MacOS, VMs, or bare metal, including containers built on Linux-based MicroOSes. In this mode, Kuma needs to store its state in a PostgreSQL database.
 * **Kubernetes Mode**
    When deployed on Kubernetes, Kuma stores its state and configuration directly on the Kubernetes API Server, which injects a proxy sidecar into desired Kubernetes Pods.


#### == **Features and Benefits of Kuma** ==

Key features and benefits of Kuma are: 

 * It is universal, Kubernetes-native, and platform-agnostic because it runs on bare-metal, VMs, and Kubernetes.
 * It is easy to use and automate.
 * Simple to deploy on any supported platform.
 * It is built on top of Envoy, the most popular Service Mesh proxy.


### **Linkerd**

#### == **Introduction to Linkerd** ==

[Linkerd](https://linkerd.io/) is an open source network proxy and one of the Cloud Native Computing Foundation (CNCF) projects.

It supports all features of the service meshes listed earlier. In addition, Linkerd can be installed per host or instance, as a replacement for the sidecar deployment.

#### == **Features and Benefits of Using Linkerd** ==

Key features and benefits of Linkerd include:

 * It is open source.
 * It can be run on different platforms like VM, Docker, Kubernetes, DC/OS, Amazon ECS.
 * It's fast, scalable, and performant.
 * It runs as a transparent proxy alongside existing applications and integrates with the existing infrastructure.
 * Integrates with most service discovery systems.

### **Maesh**

#### == **Introduction to Maesh** ==

[Maesh](https://containo.us/maesh/) is a simple open source service mesh, from [Containous](https://containo.us/), the developers of [Traefik](https://containo.us/traefik/) ingress proxy. It is a simple and easy to configure service mesh, that provides traffic visibility and management inside a Kubernetes cluster.

Maesh improves cluster security through monitoring, logging, visibility, and access controls. Communication monitoring and tracing also help with traffic optimization and increased application performance.  

Its simplicity and ease of implementation allow administrators to spend their valuable time focusing on business applications.

Maesh is able to reveal underutilized resources, or overloaded services, which helps with proper resources allocation.

#### == **Maesh Architecture** ==

Maesh being non-invasive by design does not require any sidecar containers to be injected into Kubernetes pods. Instead, it routes through proxy endpoints, called mesh controllers, that run on each node as dedicated pods. 

![Maesh Architecture](LFS151-maesh-graphic.png)

#### == **Features and Benefits of Using Maesh** ==

Key features and benefits of Linkerd include:

 * It is open source and prevents vendor lock-in.
 * It is easy to install, and non-invasive as it does not require sidecar container injections.
 * It supports OpenTracing, and metrics through Prometheus and Grafana.
 * It supports HTTP, HTTP/2, native gRPC, Websockets, and TCP connection routing.
 * It supports weighted round-robin load balancing and canary deployments.
 * It offers resiliency with automated retries and failover, together with circuit breaker mechanisms and rate limits.
 * It is secured by access control policies.

### **Tanzu Service Mesh**

#### == **Introduction to Tanzu Service Mesh** ==

[Tanzu Service Mesh](https://tanzu.vmware.com/service-mesh) (briefly introduced as [NSX Service Mesh](https://blogs.vmware.com/networkvirtualization/2019/11/nsx-service-mesh-on-vmware-tanzu.html/)) is an enterprise-class service mesh developed by VMware, built on top of [VMware NSX](https://www.vmware.com/products/nsx.html). It aims to simplify the connectivity, security, and monitoring of applications on any runtime and on any cloud. In addition, as a modern distributed solution, it brings together application owners, DevOps, SRE, and SecOps.

#### == **Tanzu Service Mesh Architecture** ==

Tanzu Service Mesh consistently connects and secures applications running on all Kubernetes multi-clusters and multi-cloud environments. It can be installed on VMware Tanzu Kubernetes Grid clusters or any Kubernetes clusters, including managed Kubernetes services.

![Tanzu Service Mesh](LFS151-Tanzu-Service-Mesh.png)

A unique characteristic of Tanzu Service Mesh is its ability to support cross-cluster and cross-cloud applications through Global Namespaces (GNS). A GNS further isolates an application from its Kubernetes cluster namespace and networking, which helps the application securely stretch across clusters and clouds. Global Namespaces introduce consistency in traffic routing, application resiliency, and security policies for applications across clouds.

![Tanzu Service Mesh Global Namespaces](LFS151-TSM-Global-Namespace.png)

#### == **Features and Benefits of Using Tanzu Service Mesh** ==

Key features and benefits of Tanzu Service Mesh include:

 * It simplifies the service mesh lifecycle management through cross-platform portability. 
 * It offers cross-cloud observability of communication between users, data, and services.
 * It offers a unified policy to manage multi-cluster mesh topologies through federation.
 * It normalizes the communication infrastructure across multi-clouds.
 * It utilizes global namespaces for portability and isolation. 
 * It allows users to manage and configure failover and service behavior policies.
 * it implements the security and auditing of services running on multi-clouds.


----

## Chapter 18. Internet of Things (IoT)

### **Introduction and Learning Objectives**

#### == **Introduction** ==

Internet of Things (IoT) is made of two terms Internet and Things. By Internet, we mean that the Things are connected. By Things, however, we don’t just mean the end devices, but also people and interconnected systems. Collecting, processing, analyzing data, and then taking action accordingly also comes under IoT.

According to [Wikipedia](https://en.wikipedia.org/wiki/Internet_of_things):

”The Internet of Things (IoT) is the network of physical devices, vehicles, home appliances, and other items embedded with electronics, software, sensors, actuators, and connectivity which enables these things to connect and exchange data, creating opportunities for more direct integration of the physical world into computer-based systems, resulting in efficiency improvements, economic benefits, and reduced human exertions".

We now have sensors in our watches, shoes, cars, and appliances, and these IoT-enabled devices are now becoming part of our day-to-day lives. For example, we now have IoT-enabled refrigerators, that track an inventory of goods available in the refrigerator, and order them in advance, depending on our consumption. IoT is making our air travel safer, reducing cost by automation, and helping us remotely monitor other devices, goods, and locations. There are many use cases like these, which we will explore in the next section.

Looking at the trends, in the next few years, all of the devices would be IoT-enabled by default.

#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Explain IoT and its use cases.
 * Understand computing, data management and analytics for IoT.
 * Describe IoT-specific solutions provided by different cloud providers. 



### **Internet of Things**

#### == **IoT Use Cases** ==

IoT has use cases in different verticals, as it provides value by saving time, money, etc. Let's look at some of them:

 * Consumer Applications
    Wearable devices like watches, connected vehicles, appliances, and smart homes.
 * Infrastructure
    Monitoring and controlling railway tracks and wind turbines. IoT also offers preventive maintenance.
 * Manufacturing
    Asset management, optimizing supply-chain.
 * Agriculture
    Collecting data on temperature, rainfall, humidity, and soil composition.
 * Energy Management
    Optimizing energy consumption.
 * Environmental Monitoring
    Monitoring air and water quality, soil and atmospheric conditions.
 * Medical and Healthcare
    Remote health monitoring and emergency notification.

#### == **Network for IoT** ==

As there are so many IoT devices, the IPv4 network range cannot accommodate them. We need to use the IPv6 network to address those devices. These devices can be installed at remote locations; they can be continuous in motion, etc. Other than the traditional network, community and companies have built different network standards to fulfill these unique needs. Let's list some of them, based on the network ranges:

 * Short-range Wireless

    * [Bluetooth mesh networking](https://en.wikipedia.org/wiki/Bluetooth_mesh_networking)
    * [Light fidelity (Li-Fi)](https://en.wikipedia.org/wiki/Li-Fi)
    * [Radio-frequency identification](https://en.wikipedia.org/wiki/Radio-frequency_identification) (RFID)
    * [Near-field communication](https://en.wikipedia.org/wiki/Near-field_communication) (NFC)
    * [Wi-Fi](https://en.wikipedia.org/wiki/Wi-Fi)
    * [Zigbee](https://en.wikipedia.org/wiki/Zigbee)

 * Medium-range Wireless

    * [Wi-Fi HaLow](https://en.wikipedia.org/wiki/IEEE_802.11ah)
    * [LTE Advanced](https://en.wikipedia.org/wiki/LTE_Advanced)

 * Long-range Wireless

    * [Low-Power Wide-Area Network](https://en.wikipedia.org/wiki/LPWAN) (LPWAN)
    * [Very small aperture terminal](https://en.wikipedia.org/wiki/Very-small-aperture_terminal) (VSAT)

 * Wired

    * [Ethernet](https://en.wikipedia.org/wiki/Ethernet)
    * [Multimedia over Coax Alliance](https://en.wikipedia.org/wiki/Multimedia_over_Coax_Alliance) (MoCA)
    * [Power-line communication](https://en.wikipedia.org/wiki/Power-line_communication) (PLC)


#### == **Computing for IoT** ==

Similar to networking, computing has evolved for IoT. We would need to handle situations like the following:

 * Latency between the actual device and the datacenter (cloud) where we store the actual data.
 * Not sending unnecessary data, like frequent keep-alive messages from the devices/sensors to the backend datacenter.

To handle such situations, Distributed Computing Architecture has evolved to include edge and fog computing. By using [edge](https://en.wikipedia.org/wiki/Edge_computing) and [fog](https://en.wikipedia.org/wiki/Fog_computing) computing we bring computing applications, data, and services away from some central nodes (datacenter) to the other logical extreme (the edge) of the Internet.

#### == **Data Management and Analytics for IoT** ==

IoT devices are just one part of the ecosystem. We need to regularly collect data from them, transmit it, store it and analyze it to make smart decisions. In some cases, we need to make decisions in real time, such as in the case of self-driving cars.

With IoT devices, we generate a variety of data in large volumes at very high speeds, which makes very good use for Big Data technologies like [Apache Hadoop](https://en.wikipedia.org/wiki/Apache_Hadoop). We can make smart decisions, then augment this via [machine learning](https://en.wikipedia.org/wiki/Machine_learning) and [artificial intelligence](https://en.wikipedia.org/wiki/Artificial_intelligence).


#### == **IoT Solutions Provided by Different Cloud Providers** ==

Below are a few of the cloud services providers that also have specialized IoT offerings:

 * **Amazon Web Services (AWS)**
    [AWS IoT ](https://aws.amazon.com/iot/)offers a variety of products and services like Amazon FreeRTOS, AWS IoT Core, AWS IoT Device Management, AWS IoT Analytics, and many others.
 * **Google Cloud Platform (GCP)**
    [Google Cloud IoT](https://cloud.google.com/solutions/iot/) is the IoT offering from Google Cloud Platform, including Cloud IoT Core, and Edge TPU services.
 * Microsoft Azure
    [Azure IoT](https://azure.microsoft.com/en-us/overview/iot/) offers services like Azure IoT Hub, Azure IoT Central, and Azure IoT Edge which provides artificial intelligence (AI), Azure services, and custom logic directly on cross-platform IoT devices.

#### == **IoT Challenges** ==

As with any other technologies, IoT also faces a few challenges:

 * **Scale**
    With millions of devices added every year, we need to have the infrastructure to support their network with specific storage requirements.
 * **Security**
    How can we make sure that there is always a secure communication between connection endpoints? Also, if there is a breach, how much information can one extract from the system?
 * **Privacy**
    There are great concerns about privacy. For example, should one share his/her entire health information to some third party companies/services?
 * **Interoperability**
    Newer devices have interfaces for IoT communication, but what about other legacy devices that are not IoT-enabled?

----

## Chapter 19. Serverless Computing

### **Introduction and Learning Objectives**

#### == **Introduction** ==

**Serverless computing** or just **serverless** is a method of running applications without concerns about the provisioning of computer servers or any of the compute resources. However, behind the scenes, compute servers are most definitely involved. Serverless is similar to the wireless Internet where the wires exist, but they are not visible to end-users.

Serverless computing requires compute resources to run applications, but the server management and capacity planning decisions are completely abstracted from developers and users.

#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Explain serverless computing. 
 * Present serverless/function services provided by different cloud providers.
 * Understand serverless frameworks, which are built on top of containers.

### **Serverless Computing**

#### == **Serverless Computing** ==

In serverless computing, we generally write applications/functions that focus and master one particular task. We then upload that application on the cloud provider, which gets invoked via different events, such as HTTP requests, webhooks, etc.

The most common use case of serverless computing is to run any stateless applications like data processing or real-time stream processing. However, it can also augment a stateful application. Internet of Things and ChatBots are common use cases of serverless computing.

All major cloud providers like AWS, Google Cloud Platform, or Microsoft Azure include serverless offerings. We will explore them in this chapter. We can also build our own serverless solutions on container orchestrators like Docker Swarm, and Kubernetes.

Most often, serverless computing is referred via services offered by cloud providers. We will also follow the same practice. In addition, we will explicitly talk about custom or self-managed serverless computing whenever needed.

#### == **Features and Benefits of Serverless Computing** ==

Key features and benefits of serverless computing include:

 * No Server Management
    When we use serverless offerings by cloud providers, no server management and capacity planning is required by us. It is all handled by the cloud services provider.
 * Cost-Effective
    We only need to pay for a CPU time when our applications/functions are executed. There is no charge when code is not running. Also, there is no need to rent or purchase fixed quantities of servers.
 * Flexible Scaling
    We don’t need to set up or tune autoscaling. Applications are automatically scaled up/down based on demand.
 * Automated High Availability and Fault Tolerance
    High availability and fault tolerance are automatically included by the underlying cloud infrastructure providers. Developers are not required to specifically program for such features.


#### == **Drawbacks of Serverless Computing** ==

There are some drawbacks of serverless computing that include:

 * **Vendor Lock-In**
    Serverless features and implementations vary from vendor to vendor. So, the same application/function may not behave the same way if you change the provider, and changing your provider can incur additional expenses.
 * **Multitenancy and Security**
    You cannot be sure what other applications/functions run alongside yours, which raises multitenancy and security concerns.
 * **Performance**
    If the application is not in use, the service provider can take it down, which will affect performance.
 * **Resource Limits**
    Cloud providers set resource limits for our serverless applications/functions. Therefore, it is safer not to run high-performance, or resource-intensive workloads using serverless solutions.
 * **Monitoring and Debugging**
    It is more challenging to monitor serverless applications compared to applications running on traditional servers.


### **AWS Lambda**

#### == **Introduction to AWS Lambda** ==

[AWS Lambda](https://aws.amazon.com/lambda/) is the serverless service offered through [Amazon Web Services Compute](https://aws.amazon.com/products/compute/) collection of products. AWS Lambda can be triggered in different ways, such as an HTTP request, a new document upload to S3 bucket, a scheduled job, an AWS Kinesis data stream, a notification from AWS Simple Notification Service, or a REST API call through the Amazon API Gateway.

![AWS Lambda](LFS151-AWS_Lambda.png)

Currently, AWS Lambda can natively support the following languages:

 * Node.js
 * Java
 * C#
 * Go
 * PowerShell
 * Python
 * Ruby
 * Runtime API for additional programming language support.


#### == **Features and Benefits of AWS Lambda** ==

AWS Lambda offers all serverless features and benefits listed earlier. In addition, it is also capable of:

 * Integrating with other AWS services
    It provides built-in logging and monitoring through Amazon CloudWatch.
 * Extending other AWS services
    It can provide custom logic to existing AWS resources such as S3 buckets, DynamoDB tables, or Kinesis streams. It can also use Lambda@Edge to run code in response to CloudFront events.
 * Building custom backend services
    It can create new backend services for applications that can be triggered using the Lambda API.
 * Bringing users own code
    It supports different programming languages such as Node.js, Java, C#, Go, and Python. This allows users to run and deploy their custom applications with AWS Lambda. In addition, the Runtime API allows for additional programming language support.
 * Connecting to relational databases
    It uses the Amazon RDS Proxy to manage concurrent connections to relational databases such as MySQL and Aurora.
 * Connecting to shared files systems
    It supports secure reads and writes at any scale with Elastic File System (EFS) for Lambda.
 * Integrating security
    It allows for secure access to AWS services with integrated AWS IAM support. It supports VPC, SG, and NACL configurations, and it is HIPAA, ISO, and PCI compliant.


#### **Demo: Creating an AWS Lambda Function**

Next, we will create an AWS Lambda Function.

Creating an AWS Lambda Function

### **Google Cloud Functions**

#### == **Introduction to Google Cloud Functions** ==

[Google Cloud Functions](https://cloud.google.com/functions/) is part of Google’s serverless services, which offers all the typical serverless benefits/features mentioned earlier. An application that runs with Cloud Functions can connect to other cloud services. Cloud Functions is essentially a Function-as-a-Service (FaaS) offering and it is complemented by two additional services, [App Engine](https://cloud.google.com/appengine) and [Cloud Run](https://cloud.google.com/run):

 * **App Engine** allows users to build highly scalable applications on a fully managed serverless platform. It supports Node.js, Java, Ruby, C#, Go, Python, PHP, or other programming languages through custom runtime support. Scalability and flexibility are achieved by encapsulating the applications in Docker containers.
 * **Cloud Run** is a managed compute platform for fast and secure deployment and scaling of containerized applications. It supports Node.js, Java, Ruby, Go, and Python, and it improves the developer experience by integrating with services such as Cloud Code, Cloud Build, Cloud Monitoring, and Cloud Logging. Cloud Run also enables application portability by implementing the Knative open standard and supporting the Docker runtime.

Google Cloud Functions can be written in Node.js, Python, Go, or Java and can be executed on Debian or Ubuntu systems on the Google Cloud Platform, simplifying and easing portability and local testing.

![Google Cloud Functions](LFS151-Google_Cloud_Functions.png)

#### == **Features and Benefits of Google Cloud Functions** ==

In addition to the typical features of serverless computing that we discussed earlier, Google Cloud Functions also supports:

 * **Integrating with other Google and AWS services**
    It connects well with other Google services such as GCP, Firebase, Google Assistant, Cloud Storage, Cloud Firestone, IoT Core, and Cloud Vision API. It also connects with Amazon Simple Notification Service (SNS).
 * **Supports Node.js, Python, Go, and Java**
    It supports JavaScript (Node.js), Python, Go and Java programming languages to write serverless functions.


#### **Demo: Deploying a Serverless Function with Google Cloud Functions** 

Next, we will deploy a serverless function with Google Cloud Functions.

Deploying a Serverless Function with Google Cloud Functions


### **Azure Functions**

#### == **Introduction to Azure Functions** ==

[Azure Functions](https://azure.microsoft.com/en-us/services/functions/) is part of Microsoft Azure’s serverless compute offering, together with Serverless Kubernetes and Serverless application environments.

**Azure Functions** offers an event-driven environment. It is available as a managed service in Azure and Azure Stack, but it also works on Kubernetes, Azure IoT Edge, on-premises, and other clouds.

**Serverless Kubernetes** allows users to create serverless, Kubernetes-based applications orchestrated with Azure Kubernetes Service (AKS) and AKS virtual nodes, based on the open-source Virtual Kubelet project.

**Serverless application environments** allow the running and scaling of web, mobile, and API applications on any platform of your choice through Azure App Service.

Besides the pay-as-you-go model, it has another cost-based plan, the Azure App Service. If you are already using the Azure App Service, then you can add your functions on the same plan at no additional cost. Azure Functions supports application and functions in the following programming languages:

 * C#
 * JavaScript
 * F#
 * Java
 * Python
 * TypeScript
 * PowerShell.


#### == **Features and Benefits of Azure Functions** ==

Additional features and benefits provided by Azure Functions include:

 * **Integration with other Azure services**
    It integrates well with other Azure services, such as Azure Event Hubs and Azure Storage.
 * **Bringing your own dependencies**
    It supports [NuGet](https://www.nuget.org/) and [NPM](https://www.npmjs.com/), so you can use custom libraries.
 * **Integrated security**
    It provides OAuth security for HTTP-triggered functions with OAuth providers such as Azure Active Directory, Facebook, Google, Twitter, and Microsoft Account.
 * **Open source**
    Azure Functions has an open source runtime.


#### **Demo: Creating a Serverless Function in Your Azure Portal**

Next, we will create a serverless function in our Azure Portal.

Creating a Serverless Function in Your Azure Portal


### **Serverless and Containers**

#### == **Serverless Computing and Containers** ==

Earlier in this chapter, we learned that with serverless computing we can run applications/functions which focus on performing one task, and they perform it well. Also, earlier in the course, we saw that with container images, we can package applications and run them as containers. We run containers using different container runtimes and orchestrate them using container orchestrators like Kubernetes, Docker Swarm, and Amazon ECS.

#### == **Projects That Use Containers to Execute Serverless Applications** ==

Container images became a prominent choice for packaging serverless applications, and container orchestrators became the preferred choice for executors. Let us list some of the projects that are using containers to execute serverless applications:

 * [Azure Container Instances](https://azure.microsoft.com/en-in/services/container-instances/)
    Azure Container Instances (ACI) is the service offered by Microsoft Azure, which allows users to run containers without managing servers. It provides hypervisor isolation for each container group to ensure containers run in isolation without sharing a kernel.
 * [AWS Fargate](https://aws.amazon.com/fargate/)
    AWS Fargate is the service offered by Amazon Web Services, providing serverless compute for containers. It runs containers on Amazon Elastic Container Service (ECS) and Amazon Elastic Kubernetes Service (EKS) services.
 * [Kubeless](https://kubeless.io/)
    Kubeless is an open source project from Bitnami, which provides a serverless framework on Kubernetes.
 * [Fission](https://fission.io/)
    Fission is an open source project from Platform9, which provides a serverless Function-as-a-Service (FaaS) framework on Kubernetes.
 * [Fn Project](https://fnproject.io/)
    Fn Project is an open source container-native serverless platform that runs on any platform local or cloud.
 * [Virtual Kubelet](https://virtual-kubelet.io/)
    Virtual Kubelet connects Kubernetes to other APIs and masquerades them as Kubernetes nodes. These other APIs include ACI, Fargate, and IoT Edge. It is a Cloud Native Computing Foundation (CNCF) project. 

Additional projects are covered in the next few sections. We can expect to see new solutions emerging for serverless and containers in the near future.

#### == **Knative** ==

[Knative](https://knative.dev/) is an open source platform based on Kubernetes that allows for the deployment and management of serverless applications. It is portable, running on any Kubernetes distribution without vendor lock-in. It is implemented in the form of controllers that get installed on the Kubernetes cluster and then registers custom API resources with the Kubernetes API Server. Knative is flexible and it supports plugins for logging, monitoring, networking, and even service mesh. In addition, it can be used with popular tools and frameworks such as Django, Ruby on Rails, and Spring.

Although an open source project, there are commercial Knative products available as well, such as Google Cloud Run, Managed Knative for IBM Cloud Kubernetes Service, OpenShift Serverless, or Pivotal Function Service (PFS).

Key features of Knative are:

 * **Serving**
    It runs serverless applications in containers on Kubernetes. Knative automates the networking, ingress, scaling, application deployment rollout and revisions, logging, and monitoring.
 * **Eventing**
    It allows applications to publish and subscribe to event streams such as Kafka or Google Cloud Pub/Sub.


#### == **OpenFaaS** ==

[OpenFaaS](https://www.openfaas.com/) is an open source project that aims to simplify functions and code deployment directly to Kubernetes, OpenShift, or Docker Swarm. It runs on any cloud, both public and private. It leverages Docker as container runtime and it supports many programming languages such as Node.js, Python, C#, Django, or .NET.

OpenFaaS allows both microservices and functions deployment in containers, aided by a functions store and a templating system which enable collaboration, sharing, and reusing of both functions and code. Once the code is packaged in a Docker container image, it becomes a highly scalable endpoint that can be monitored as well.

OpenFaaS deployments can be complex, requiring multi-node clusters, or light-weight on a single-host. For complex production deployments, Kubernetes is the recommended container orchestrator for OpenFaaS, while for the single-host installation it uses [faasd](https://github.com/openfaas/faasd/), a tool built with containerd and CNI. 

OpenFaaS provides the following features and benefits:

 * It is easy to use through web portal or one-click install.
 * It supports functions written in any language for both Linux and Windows.
 * It support functions packaged in Docker/OCI image format.
 * It is portable as it runs on bare-metal, VM, or cloud.
 * It is Kubernetes, OpenShift, and Docker Swarm native.
 * It includes a YAML friendly CLI tool to build functions.
 * It auto-scales on demand.


----

## Chapter 20. Distributed Tracing

### **Introduction and Learning Objectives**

#### == **Introduction** ==

Organizations are adopting microservices for agility, easy deployment, scaling, etc. However, with a large number of services working together, sometimes it becomes difficult to pinpoint the root cause of latency issues or unexpected behaviors. To overcome such situations, we would need to instrument the behavior of each participating service in our microservices-based application. After collecting and combining the instrumented data from each participating service, we should be able to gain visibility of the entire system. This is generally referred to as **distributed tracing**.

There are various distributed tracing tools like Zipkin, Dapper, HTrace, and X-Trace, but they instrument applications using their own specific APIs, which are not compatible with each other. Due to this tight coupling, developers do not feel very comfortable with them. Enter [OpenTracing](http://opentracing.io/), which offers consistent, expressive, vendor-neutral APIs. OpenTracing is an open source project under CNCF.

#### == **Learning Objectives** ==

By the end of this chapter, you should be able to:

 * Understand the concept of distributed tracing and how we can collect data using the OpenTracing API.
 * Describe Jaeger as a distributed tracing tool.


### **OpenTracing**

#### == **Tracing with OpenTracing** ==

In general, a trace represents the details about a transaction, like how much time it took to call a specific function. In OpenTracing, a trace is referred by the directed acyclic graph (DAG) of spans. Each span can be referred to as a timed operation between contiguous segments of work. In distributed tracing, each service would contribute to its own span or set of [spans](https://opentracing.io/specification/#the-opentracing-data-model). A parent can start other spans, either in serial or in parallel. A general tracing for microservices-based applications looks something like the following:

![A General Tracing for Microservices-based Applications](LFS151-A_general_tracing_for_microservices-based_application.png)

However, it does not give us timing information and is difficult to understand when parallelism is involved. On the other hand, the following visualization adds the context of time, sets up service hierarchy, and demonstrates serial and parallel execution.

![Visualization of a Trace Collected via OpenTracing](LFS151-TBD.png)

We collect information to create graphs like the one above using different tracers.

#### == **Language Support for OpenTracing**

OpenTracing APIs are available for the following programming languages:

 * Go
 * Python
 * JavaScript
 * Java
 * C#
 * Objective-C
 * C++
 * Ruby
 * PHP.


#### == **OpenTracing Supported Tracers** ==

Using OpenTracing, we collect tracing spans for our services and then forward them to different tracers. Tracers are then used to monitor and troubleshoot microservices-based applications. Following are some of the supported tracers for OpenTracing:

 * [Jaeger](https://jaegertracing.io/) 
    Supports Java, Go, Node.js, Python, C++, and C#.
 * [LightStep](https://lightstep.com/) 
    Supports Javascript, Go, Python, Java, PHP, Objective-C, C++, and Ruby.
 * [Instana](https://www.instana.com/) 
    Supports Crystal, Java, Go, Node.js, Ruby, and Python.
 * [Elastic APM](https://www.elastic.co/apm) 
    Supports Java, Node.js, Python, Ruby, RUM JavaScript, and Go. 
 * [Wavefront by VMware](https://wavefront.com/) 
    Supports Java, Python, and .NET.

### **Jaeger**

#### == **Introduction to Jaeger** ==

[Jaeger](https://jaegertracing.io/) is an open source tracer which is compatible with the OpenTracing data model to support spans. Jaeger was open sourced by [Uber Technologies](https://uber.github.io/) and is now part of CNCF. It can be used for the following:

 * Distributed content propagation
 * Distributed transaction monitoring
 * Root cause analysis
 * Service dependency analysis
 * Performance and latency optimization.

#### == **Jaeger Architecture** ==

Below you can see the architecture of Jaeger:

![Jaeger Architecture](LFS151-jaeger-architecture-v1.png)

Jaeger currently supports Java, Go, Python, Node.js, C++, and C# programming languages. Jaeger clients, which are language-specific, get attached to the OpenTracing APIs to instrument the applications.

Jaeger clients then send the collected tracing data to the Jaeger agent running on the host or container which runs the application. All of the agents send the collected instrumented data to a central storage called Jaeger collector. The collector supports [Cassandra](https://www.jaegertracing.io/docs/deployment/#cassandra) and [ElasticSearch](https://www.jaegertracing.io/docs/deployment/#elasticsearch) as storage backends. We can then query the collected data using Jaeger-UI.

#### == **Features and Benefits of Jaeger** ==

Below are the key features and benefits of Jaeger:

 * Open source tracer, which natively supports OpenTracing.
 * Deployment to Kubernetes is enabled by a Helm chart, an operator, and templates. 
 * Support for languages like Java, Go, Python, Node.js, C++, and C#. 
 * Highly scalable.
 * Supports multiple storage backends.
 * Modern web user interface (UI).
 * Observability via [Prometheus](https://prometheus.io/), and other metrics backends.


#### **Demo: Exploring Jaeger**

Next, we will look at how we can use Jaeger UI to explore the data collected from the OpenTracing API.

Exploring Jaeger

#### == 

----

##  Chapter 21. How to Be Successful in the Cloud

### **Introduction and Learning Objectives**

#### == **Introduction** ==

With cloud computing's pay-as-you-go and software-defined everything models, startups now have a very low barrier to take an enterprise assignment. And, with open source tools and ecosystems built around them, startups can innovate and adapt very fast.

Any company, be it small or big, has to innovate fast, listen to customer feedback, and then iterate over it. To do that, companies need to bring in DevOps practices and allow small teams to manage the entire lifecycle of their products, from Development to Support. Technologies like Container as a Service and Continuous Integration and Deployment allow small teams to have access of Development, QA and Deployment environments. This allows individual teams that work in an enterprise to work like startups, which is good for business.

#### == **Learning Objectives** ==



By the end of this chapter, you should be able to:

 * Discuss what aspects of the cloud you need to know in order to develop your skills set and stay competitive.
 * Explain what challenges can individuals and companies face when transitioning to cloud technologies.


### **Developing Skills**

#### == **Where Do We Go?** ==

Nowadays, IT is becoming part of the business process. Due to the emergence of cloud technologies, DevOps, microservices architecture, etc., each business is expected to respond to customer feedback sooner rather than later.

Container technologies like Docker and their ecosystems are helping us standardize and streamline the way we package and deploy code. If we want to adopt the technologies we discussed in this course, we realize that Developers, QA and OPs cannot work in silos. To be efficient and successful, they have to work together and need to have basic knowledge about each others' workflows. This is very different from the traditional IT approach and has an initial steep learning curve. The bigger the company, the bigger the challenges.

#### == **Developing The Necessary Skills Set** ==

Though not everyone has to master all the topics we have discussed in this course, you should at least have a basic understanding of the following:

 * Different cloud offerings (IaaS, PaaS, SaaS) and cloud models (public, private, and hybrid)
 * Container technologies like Docker, Kubernetes, and their ecosystem
 * DevOps
 * Continuous Integration and Continuous Deployment
 * Software Defined Networking and Storage
 * Debugging, Logging, and Monitoring cloud applications.


### **Challenges**

#### == **About Challenges** ==

Once you decide to move to the cloud, you will inevitably face some challenges. Most of them you will encounter for the first time. In this section, we will talk about some of the challenges that you may encounter in your journey to the cloud.

#### == **Choosing the Right Cloud Provider** ==

There are different cloud providers like Amazon AWS, Google Cloud Platform, Microsoft Azure, OpenStack, etc. Each of them provides different services.

Similarly, there are different cloud models like public, private, and hybrid. Each of them has their advantages and shortcomings. For example, the hybrid model is useful when you want to keep your data on-premise and serve the request from public clouds.

Companies have to spend a significant amount of time to evaluate different cloud providers and models in the beginning, as it affects the overall operations and costs.

#### == **Choosing the Right Technology Stack** ==

To avail fully of the benefits provided by the Cloud, we have to choose the right technology stack as well. For example, should we go for IaaS or PaaS solutions? Should we choose VMs or containers to deploy the applications? Most of these questions have multiple answers. As a company, you need to hire cloud architects who can make the right decision for you.

#### == **Security Concerns** ==

Security is one of the biggest concerns when moving towards the cloud. Companies worry about privacy, data access, accountability, account control, multi-tenancy, etc. For example, with 100% public cloud deployment, the entire data is managed on the cloud, which is not the preferred way for many companies and may not comply with regulations like FISMA (Federal Information Security Modernization Act) or HIPAA (Health Insurance Portability and Accountability Act). In such cases, companies adopt the Hybrid Cloud approach.

Organizations like the [Cloud Security Alliance](https://cloudsecurityalliance.org/) (CSA) “promote the use of best practices for providing security assurance within Cloud Computing, and to provide education on the uses of Cloud Computing to help secure all other forms of computing”.

Nowadays, there are many third-party tools available which can do security audits for applications deployed on the cloud.

#### == **Cloud Cost Management** ==

Studies say that, by moving to the cloud, an organization can save a good amount of money as compared to setting up on-premise solutions. One thing we have to be very careful with is to ensure that we know when this is true, and when it is not, for any given use case. Most cloud providers can now predict and manage the cost based on usage, which can help companies keep track of their spending.

#### == **Vendor Lock-In** ==

Companies also worry about vendor lock-in when it comes to cloud computing. Cloud providers allow migration from other providers, but that is not an ideal solution. With containers becoming mainstream and using innovative solutions like Kubernetes, Docker Enterprise Edition, etc. on top of them, we can deploy the applications across datacenters on top of different cloud providers. The use of containers definitely addresses part of the vendor lock-in problem.

#### == **Resistance from Existing Employees** ==

This is one of the biggest challenges companies are facing. With cloud technologies, the existing workflow is changing dramatically, and that is the need of the hour: to keep up with the business demands. Oftentimes, many IT employees resist learning new things and changing their ways, which is not good for the business. To avoid or minimize this problem, the top management of a company has to guide its employees through the change process of learning new technologies. On the other hand, as an employee, you should also be committed to continuous education and lifelong learning, as learning new things will help you stay relevant in a competitive and ever-changing industry.


----





