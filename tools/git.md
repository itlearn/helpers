
<!--
NB use gitlab TOC

Table of Contents
=================

- [Table of Contents](#table-of-contents)
- [Glossary](#glossary)
- [Config](#config)
  - [Levels of config](#levels-of-config)
  - [Set config](#set-config)
  - [Identity](#identity)
  - [Credentials](#credentials)
    - [Credential helper](#credential-helper)
  - [Editor](#editor)
  - [Pager](#pager)
    - [Turn off pager (used by log, diff, list)](#turn-off-pager-used-by-log-diff-list)
    - [Use most as pager](#use-most-as-pager)
  - [Merge tool](#merge-tool)
    - [Using graphical external tool](#using-graphical-external-tool)
    - [Remove the backup .orig](#remove-the-backup-orig)
  - [Dealing with end of line](#dealing-with-end-of-line)
  - [Auto-correct settings](#auto-correct-settings)
  - [submodule](#submodule)
- [Basics](#basics)
  - [Setup repository](#setup-repository)
  - [Working directory and Staging](#working-directory-and-staging)
  - [Show status](#show-status)
  - [Committing](#committing)
  - [Branching](#branching)
  - [Merging](#merging)
  - [Rebasing](#rebasing)
  - [Fast fowarding merge](#fast-fowarding-merge)
  - [Cherry picking](#cherry-picking)
  - [Squashing](#squashing)
  - [Merge conflicts](#merge-conflicts)
- [Infos](#infos)
  - [References](#references)
    - [HEAD pointer](#head-pointer)
    - [Retrieve HEAD commit id](#retrieve-head-commit-id)
    - [Moving HEAD (=\> move branch label)](#moving-head--move-branch-label)
    - [Retrieve branch commit id](#retrieve-branch-commit-id)
    - [Retrieve git context info](#retrieve-git-context-info)
  - [Logs](#logs)
  - [Show file content](#show-file-content)
  - [Diff](#diff)
    - [Diff using an external tool](#diff-using-an-external-tool)
- [Remotes](#remotes)
  - [Pulling data](#pulling-data)
  - [Pushing data](#pushing-data)
- [Tags](#tags)
  - [Getting remote Tags](#getting-remote-tags)
  - [Pulling tags](#pulling-tags)
  - [Pushing tags](#pushing-tags)
  - [Deleting tags](#deleting-tags)
- [Utils](#utils)
  - [Using git outside the git workspace](#using-git-outside-the-git-workspace)
  - [Check if a commit exist](#check-if-a-commit-exist)
  - [Setting file permissions under windows](#setting-file-permissions-under-windows)
  - [show ignored files](#show-ignored-files)
  - [Stashing](#stashing)
  - [Alias](#alias)
  - [Submodule](#submodule-1)
- [Plumbing commands](#plumbing-commands)
  - [reflog](#reflog)
  - [Internals](#internals)
    - [Tips to retrieve last file content added to index](#tips-to-retrieve-last-file-content-added-to-index)
    - [stored object types](#stored-object-types)
- [Git ref](#git-ref)

-->

[[_TOC_]]

# Glossary

 * Commit : stores the current contents of the index in a new commit along with a log message from the user describing the changes
 * Branch : a pointer to a commit
 * Master : the default name for the first branch
 * HEAD : a pointer to the most recent commit on the current branch
 * Merge : joining two or more commit histories
 * Workspace : the colloquial name for your local copy of a Git repository
 * Working tree : the current branch in your workspace; you see this in git status output all the time
 * Cache : a space intended to temporarily store uncommitted changes
 * Index : the cache where changes are stored before they are committed
 * Tracked and untracked files : files either in the index cache or not yet added to it
 * Stash : another cache, that acts as a stack, where changes can be stored without committing them
 * Origin : the default name for a remote repository
 * Local repository : another term for where you keep your copy of a Git repository on your workstation
 * Remote repository : a secondary copy of a Git repository where you push changes for collaboration or backup
 * Upstream repository : the colloquial term for a remote repository that you track
 * Pull request : a GitHub-specific term to let others know about changes you've pushed to a branch in a repository
 * Merge request : a GitLab-specific term to let others know about changes you've pushed to a branch in a repository
 * 'origin/master' : the default setting for a remote repository and its primary branch


# Config

## Levels of config

> [!info] 3 different levels of configuration

* _/etc/gitconfig_ : values for every user on the system
 ```git config --list --system```
* _~/.gitconfig_ : Specific to your user
  ```git config --list --global```
* _.git/config_ config file in the git directory : Specific to that single repository
 ```git config --list```

## Set config

> [!info] To set a new key/value in git config

```bash
git config [--global | --system ] key value
# alternative
git config [--global | --system] -e # edit file [keysection] keyname=value
```

> [!info] To unset values

```bash
git config [--global | --system] --unset key
# alternative
git config [--global | --system] -e # edit file [keysection] remove keyname
```

## Identity

> [!info] Setting identity (minimal required to add new commit)

```bash
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```


## Credentials

### Credential helper

> [!info] Turn on the credential helper so that Git will save your password in memory for some time.

```bash
# Set git to use the credential memory cache
# By default, Git will cache your password for 15 minutes.
git config --global credential.helper cache

# Set the cache to timeout after 1 hour
git config --global credential.helper 'cache --timeout=3600'

# or defined permanently
git config credential.helper store

```
## Editor

```bash
git config --global core.editor vim
```

## Pager

### Turn off pager (used by log, diff, list)

> [!info] Turn off permanently

```bash
git config --global core.pager ''

# or only for the current git repo
git config core.pager ''

```

> [!info] or temporary using `--no-pager` option for git (=> before subcommand)

```bash
git --no-pager log --oneline
```

> [!info] restore initial (system) setting

```bash
git config --global --unset core.pager
```

### Use most as pager

> [!info] by default pager is `less`

```bash
git config --global core.pager most
```

## Merge tool

### Using graphical external tool

> [!info] *temporary* : to launch graphical diff tool (e.g meld, kdiff3) instead of the default diff

```bash
# -y no prompt option
git difftool -y -t meld
```

> [!info] *permanently* : configure merge tool

```bash
git config --global merge.tool meld
```

> [!info] then use mergetool to resolve conflicts

```bash
git mergetool -y
```

### Remove the backup .orig

> [!tip] To automatically remove the backup .orig as files are successfully merged by git mergetool set `mergetool.keepBackup` to false

```bash
git config --global mergetool.keepBackup false
```

## Dealing with end of line

```bash
# for windows using cross compile env => checkout convert LF to CRLF, always commit LF
git config --system core.autocrlf true


# for linux or windows wanting LF end of line, checkout and commit LF
git config --system core.autocrlf input
```

## Auto-correct settings

```bash
# enables automatic typo detection
git config --system help.autocorrect 15
```

## submodule

```bash
# Specifies commands recurse into submodules by default
# (=> automatically use --recurse-submodules option except for clone)
git config --global submodule.recurse true
```
---

# Basics

## Setup repository

```bash
# create a new empty repository
git init

# or clone the repository specified by <repo>
git clone <repo_url>

# clone and checkout in a specific branch or tag
git clone <repo_url> --branch <branch>

# clone fetching only the last commit : clones the last clone-depth-number revisions of your repository.
git clone <repo_url> --depth 1

# create a new empty repository without checkout (server side)
git init --bare test.init

# clone remote repository as a bare repo
git clone --bare url repository.git

```

## Working directory and Staging

```bash
# to add all modified files in index
git add .  # <=> git stage

# to add only modified files already in source control
git add -u

# to unstage file <=> reset [--mixed]
git reset file

# to unstage all modified files added to index
git reset  # <=> git reset HEAD

# to remove file from index
git rm --cached file

# to remove file from index & workspace
git rm file

# move or rename file
git mv file newfile

# revert change in index & workspace
git checkout HEAD file # <=> git reset file && git checkout file
# <=> git reset file && git show HEAD:file > file
# <=> git extras : git reset-file file

# revert change in workspace using index
git checkout file

# revert all changes using index
git checkout .

# revert all changes with last revision
git checkout HEAD -- .  # <=> git reset && git checkout .

# revert all changes including new files added in index => also removed from workspace !!!
git reset --hard # <=> git reset --hard HEAD

# Remove the latest commit, keeping change in index
git reset --soft HEAD~ # git extras: git back

# Remove the latest commit, keeping change in workspace
git reset HEAD~ # <=> git extras : git undo

# Remove the latest commit
git reset --hard HEAD~ # <=> git extras : git undo --hard

# Revert some existing commits
git revert HEAD --no-edit # use default commit message 'revert "<HEAD commit msg"'


# Remove untracked files from the working tree
git clean -n # dry run
git clean -i # Interactive mode
git clean -f # force mode

```

## Show status

```bash
# checking workspace status
git status

git status -s # short desc
git status -bs # show current branch with short desc
git status -v # show status and patch index
```

## Committing

```bash
# commit changes staged in index
# -v  commit verbosely, i.e. includes the diff of the contents being committed in the commit message screen
# => open the configured editor to edit the commit message
git commit -v

# -m specify directly commit message
git commit -m 'commit msg'

# automatically stage every modified file that is already tracked before doing the commit
git commit -a -m 'commit msg'
# <=> git add -u && git commit -m 'commit msg'

# bypass pre-commit hook : -n or --no-verify
git commit --no-verify -m 'commit msg'

# amend the last commit : change the commit message, or modify some files => git add files
git commit --amend # -m 'new commit message'
 # <=> to
git reset --soft HEAD~ && git add some_files && git commit -m "new msg"
# <=> git extras
git back && git add some_files && git commit -m "new msg"

# amend the last commit , keeping the commit message
git commit --amend --no-edit

```

> [!note] How to Write a Git Commit Message  
> [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)

> [!note] How to Write Better Git Commit Messages – A Step-By-Step Guide  
> https://www.freecodecamp.org/news/how-to-write-better-git-commit-messages/


## Branching

> [!abstract] a branch in Git is : a simple pointer or reference to the head of a line of work designated by sha1

```bash
# display value of master branch
cat  .git/refs/heads/master

# list all local branches
git branch
# <=> ls .git/refs/heads

# retrieve current branch name
git rev-parse --abbrev-ref HEAD # | sed "s#heads/##"  # Special case when tag and branch have the same name
git symbolic-ref --short HEAD # | sed "s#heads/##"

# list all local branches in verbose mode (for each branch showing sha1 and commit message)
git branch -v

# list all local branches and associated upstream branch
git branch -vv

# list all remote branches
git branch -r

# list all local and remote branches
git branch -a

# list all local and remote branches with all details
git branch -av

# show branches merged in the current branches with their last commit log
git branch -v --merged

# show branches not merged in current branch
git branch --no-merged

# create a new branch named <branch>, referencing the same point in history
# as the current branch or HEAD pointer
git branch <branch> # <=> git branch <branch> HEAD
# <=> git update-ref refs/heads/<branch> HEAD
# <=> git update-ref refs/heads/<branch> $(git rev-parse HEAD)
# <=> git rev-parse HEAD > .git/refs/heads/<branch>

# create a new branch named <branch>, referencing <start-point>
# (e.g. commit-id, tag or branch name)
git branch <branch> <start-point>
# <=> git update-ref refs/heads/<branch> <start-point>
# <=> git rev-parse <start-point> > .git/refs/heads/<branch>

# rename branch
git branch -m branch new_name
# git extras : git rename-branch new_name [old_branch] # work only if remote branch exist

# move branch label
git update-ref refs/heads/<branch> new_rev
# <=>
git checkout branch
git reset --hard new_rev


# switch to branch
git checkout <branch>
# => udpate HEAD pointer i.e. .git/HEAD content and also update the working copy
# <=>
git symbolic-ref HEAD refs/heads/newbranch  && git reset --hard
# <=>
sed -i s/heads$/heads\/newbranch/ .git/HEAD && git reset --hard

# git 2.27 https://git-scm.com/docs/git-switch
git switch branch


# create a new branch <new> referencing <start-point>, and check it out.
git checkout -b <new> <start-point>
# <=>
git branch <new> <start-point>
git checkout <new>

# git extras - create-branch : create a new branch (local & remote) ;
#  checkout this one and setup a tracking remote branch
git create-branch demo -r
# <=> git checkout -b demo ; git push --set-upstream origin demo
# <=> git push origin HEAD:refs/heads/demo ; git checkout demo

# remove branch (locally)
git branch -d <branch>
# <=>
rm .git/refs/head/<branch>

# remove branch even if not merged in remaining one
git branch -D <branch>

# git extras : git delete-branch <branch> # NB delete local and remote branch
git delete-branch demo # ok if remote also exist

```

## Merging

> [!tip] to merge another branch in the current branch (=> checkout the branch in which the merge is wanted)

```bash
git merge <branch_name> [--no-commit] [--no-ff]

# --no-ff : flag causes the merge to always create a new commit object,
#  even if the merge could be performed with a fast-forward.
# --no-commit to not autocommit the  result

# NB after merge, HEAD points to current branch

```

![](img/git-local-merge.png)

## Rebasing

> [!info] To rebase modifications from branch_b over branch_a ( or top_branch over bottom_branch)


```bash
git rebase <bottom_branch> <top_branch>

git rebase branch_a branch_b
# <=>
git checkout branch_b
git rebase branch_a

# NB after rebase, HEAD points to branch_b
```

```bash
git rebase bottom_branch branch
# Before
# A---B---C---F---G (bottom_branch)
#          \
#           D---E---H---I (top_branch)
# After
# A---B---C---F---G (branch)
#                  \
#                   D'--E'---H'---I' (HEAD my-branch)
```


![](img/git-local-rebase.png)

> [!warning] **Do not rebase commits that you have pushed to a public repository.**
> If you treat rebasing as a way to clean up and work with commits **before** you push them, and if you only rebase commits that have never been available publicly, then you’ll be fine.

Git rebase --onto an overview  
https://womanonrails.com/git-rebase-onto

> [!tip] Change the current parent of a commit to new one

```bash
# git rebase --onto <newparent> <oldparent>
git rebase --onto F D

# Before
# A---B---C---F---G (branch)
#         \
#          D---E---H---I (HEAD my-branch)
# After
# A---B---C---F---G (branch)
#             \
#             D'-- E'---H'---I' (HEAD my-branch)

```

> [!tip] Removing commits from the current branch

```bash
git rebase --onto B D

# Before
# A---B---C---D---E---F (HEAD branch)
# After
# A---B---E'---F' (HEAD branch)

```

## Fast fowarding merge

```bash
# tip determine if commit is ff
if [ $(git merge-base $base $dest) == $(git rev-parse $base) ] ; then echo ff ; fi

# if commit is ff
git checkout $base ; git merge $dest
# <=>
git rebase $dest $base
```

## Cherry picking

```bash
git cherry-pick commit-id

# cherry-pick multiple commits into a single commit
# -n, --no-commit : applies the changes necessary to cherry-pick each named commit
# to your working tree and the index, without making any commit.
git cherry-pick -n commit-id
git cherry-pick -n commit-id2
git cherry-pick -n commit-id3
git commit -m "multiple cherry-pick"

# -x append a line that says "(cherry picked from commit ...)" to the original commit message
git cherry-pick -x commit-id

```

## Squashing

```bash
# Takes all the work on the specified branch and squashes it into one non-merge commit
# on top of the branch you’re on.
git merge <branch_name_from> --squash # => then git commit

# Using git extras squash command
git squash <branch_name_from> <commit-msg> # => checkout a branch from which rebasing on the squash commit
git squash <commit-id> <commit-msg> # squash on the same branch

# alternative use from HEAD position on current branch or a new branch : git reset --soft commit-id
git reset --soft start-commit-id # then git commit

# heavy way
git rebase -i <commit-id> # rewrite history
git rebase --continue

```
## Merge conflicts

> [!info] Git adds standard conflict-resolution markers to the files that have conflicts (issued by merge, rebase, squash or unstash ops).
> => run `git add` on each file to mark it as resolved.
> NB using `git mergetool`, saving file after modification, will add it automatically to the index
>
> Then depending on the current operation, to resolve the merge conflict
> * merging => git commit -m "Merge ..."
> * rebasing =>  git rebase --continue

![](img/git-conflict.png)


> [!tip] merge using an external tool

```bash
git mergetool -y [-t ext-tool]
```

# Infos

## References

> HEAD : pointer to the current branch reference
> * *Relative Commit Names* : using the caret (^) and tilde (~)

> _Ancestry References_
>  * ancestry of a commit : `commit-id~` (first ancestor <=> parent)
>  * to designate the nth ancestor use `commit-id~n`, e.g. grandparent of Head : `HEAD~2`
>  * parent of a commit : `commit-id^`  ( <=> `commit-id~`) e.g. parent of HEAD (current commit) use `HEAD^`
>  * In case of merge commit having severals parents to see the nth parent use `commit-id^n`


*Commit Ranges*

> * Range notation : `<rev1>..<rev2>`
>   <br>all commits reachable by ref2 that aren’t reachable by ref1

```bash
# shows any commits in the current branch that aren’t in the the remote master branch
git log origin/master..HEAD

# show any commits in the remote branch not in the current branch
git log $br..origin/$br --oneline
```

> * Triple Dot Symmetric Difference Notation  : `<rev1>...<rev2>`
>   <br>Include commits that are reachable from either rev1 or rev2 but exclude those that are reachable from both.
>   When either rev1 or rev2 is omitted, it defaults to HEAD.

### HEAD pointer

```bash
# show HEAD value
git symbolic-ref HEAD
# <=>
cat .git/HEAD | cut -f2 -d' '

# only branch name (remove refs/heads/ prefix)
git symbolic-ref --short HEAD

```

### Retrieve HEAD commit id

```bash
git rev-parse --verify HEAD
# <=>
cat .git/$(git symbolic-ref HEAD)
cat .git/$(cat .git/HEAD| cut -f2 -d' ')
```

### Moving HEAD (=> move branch label)

```bash
# Move head to the previous commit keeping HEAD modif in index & workspace
git reset --soft HEAD~

# reset working copy
git reset --hard  # implicitely HEAD

# moving HEAD on another commit, and withdraw all commits after this one
git reset --hard commit-id
```

### Retrieve branch commit id

```bash
git rev-parse master
# <=>
cat .git/refs/heads/master
```

```bash
# List references in a local repository
git show-ref
<=>
find .git/refs -type f | xargs -I% bash -c "cat % | tr -d '\n' ; echo ' %' | sed -e 's/.git\///'"

```

### Retrieve git context info

```bash
# Getting the top-level directory
git rev-parse --show-toplevel

# Getting  hidden .git folder.
git rev-parse --git-dir

# display path figuring how to get to the top-level directory from your current working directory.
git rev-parse --show-cdup

# true if insdie .git dir
git rev-parse --is-inside-git-dir

# true if inside working directory execpt .dir directory
git rev-parse --is-inside-work-tree

```

## Logs

> [!info] Show commit history

```bash
git log # --decorate=short by default
git log -1 # the last one
git log branch  #  to see commits related to a specific branch
git log --all # all branches
git log --summary # summary of extended header information such as creations, renames and mode changes.
git log --stat  # display changed files : diff stat
git log --oneline # compact description including --abbrev-commit
git log --oneline -p # --patch or -p to print pacth format
git log --no-decorate
git log --decorate=full
git log --abbrev-commit

# without pager use
git --no-pager log

# options
  -p, -u, --patch : Generate patch.
  -s, --no-patch : Suppress diff


```

> [!info] Show the last commit


```bash
git log -1 # the last one
<=>
git show --no-patch HEAD  # <=> -s
```

> [!info] Show details on the last commit

```bash
git log -1 --patch
# <=>
git show HEAD

```

> [!info] Show commit ranges

```bash
# the last 2 commit
git log HEAD~3.. # <=> HEAD~3..HEAD
# from the last third to the first one
git log HEAD~3
```

> [!info] Compare commits between two branches

```bash

# A---B---E---F  master
#      \
#       C---D  experiment

# show  all commits in experiment that aren’t in master
git log master..experiment

# show all commits on remote branch not in local branch
git log master..origin/master

# triple-dot syntax, which specifies all the commits that are reachable
# by either of two references but not by both of them.
# NB --left-right shows you which side of the range each commit is in
git log --left-right master...experiment

```

```bash
# display commits created on a specific day
git log --oneline --since="2022-04-22" --until="2022-04-24"

```

```bash
# show all commits that exist *only* on one specific branch, and not *any* others
git log --first-parent --no-merges HEAD
```

```bash
# git extras show-tree
git show-tree
```

## Show file content

```bash
### show content of file in HEAD version
git show HEAD:README

### show content of file staged in index
git show :README

# <=>
object-id=$(git ls-files -s README | awk '{print $2}')
git cat-file -p ${object-id}
git show ${object-id}


### show content of file in commit-id version
git show commit-id:README

# show commits where specified file change
git whatchanged -p README
git log --oneline -p README

# using graphical tool
gitk README
```

## Diff

> [!tip] `--name-only` can be used to show only changed files

```bash
git diff # workspace vs index
git diff --cached # index vs HEAD
git diff HEAD # workspace & index vs HEAD

# NB --name-only can be used to show only changed files
```

```bash
# show difference between two commits of a specific file use : git diff old-sha1 new-sha1 file
git diff HEAD~ HEAD changelog

git diff changelog  # workspace vs index
```

```bash
# A---B---E---F  master
#      \
#       C---D  feature


# differenes between current branch and an other branch
git diff feature  #  D vs F  <=> feature.. with working copy changes
git diff feature~ #  C vs F

# differences between two branches
# Git will compare the tip of both branches (also called the HEAD) i.e. F vs D
git diff master..feature

# differences between two tags
git diff br/start..br/end

# differences between a local branch and a remote branch : compare the head of local branch and remote branch
git diff master..origin/master
git diff origin/master  # <=> origin/master...

# Comparing two branches using triple dot syntax
# compares the top of the right branch (the HEAD) with the common ancestor of the two branches. 
git diff master...feature  # B vs D
git diff feature...master  # B vs F


```

> [!info] Compare specific file between two branches

```bash
git diff feature README  # diff D:README & F:README

git diff master..feature -- README  # diff F:README & D:README

git diff master...feature -- README  # diff B:README & D:README
git diff feature...master -- README  # diff B:README & F:README
```

### Diff using an external tool

> [!note] by default use external tool defined by merge.tool setting

```bash
git difftool -y [-t ext-diff-tool]
```

# Remotes

> [!note] If you clone a repository, the command automatically adds that remote repository under the name `origin`.

```bash
# To display the URL that Git has stored for the shortname
$ git remote -v
```

> [!note] NB `.git/packed-refs` contains refs of all remote branches and tags at the orgininal clone


```bash
# Add remote
$ git remote add [shortname] [url]

# Add upstream repo
git remote add upstream https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git

# <=> add following section in .git/config
#  [remote "upstream"]
#      url = https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git
#      fetch = +refs/heads/*:refs/remotes/upstream/*te

# remote set-url origin url
git remote set-url origin git://github.com/chief/global.git
# <=>
git remote remove origin # <=> git remote rm origin
git remote add origin git://github.com/chief/global.git

# rename remote
git remote rename origin public

# set push remote url
git remote set-url --push origin git@github.com:User/forked.git

# Inspecting a Remote*
$ git remote show [remote-name]

```

## Pulling data

```bash

# To get data from your remote projects
$ git fetch [remote-name] [<src>\<dst>]
git fetch
git fecth origin :refs/remotes/origin/<specific-branc>
```

```bash
# Merging remote
git pull
# <=>
git fetch; git merge origin/master
```

![](img/git-remote-merge.png)

```bash
# Rebasing onto remote
git pull --rebase
# <=>
git fetch; git rebase origin/master master

```

![](img/git-remote-rebase.png)


```bash
# fetch data and update a specific local branch
# Updating from a remote branch into a currently not checked-out branch dev:
# Merge remote branch origin/dev into local branch dev
git fetch origin dev:dev

```


```bash
# Working with remote branch origin/dev
$ git checkout --track origin/dev
# <=>
git checkout dev

# track a remote branch
git branch --set-upstream-to=origin/master master # <=> git branch -u origin/master master

# remove ref of remote branches removed from remote
git remote prune origin
git fetch --prune origin # fetch and prune
```

## Pushing data

```bash
 git push [remote-name] [src_localbranch]:[dst_remotebranch]

# push all local branches
git push origin --all

# Set upstream branch using git push
# NB  pushing to HEAD is equivalent to pushing to a remote branch
# having the same name as your current branch.
git push -u origin HEAD

git push --set-upstream origin dev # <=> git push -u origin dev
# <=> add following section in .git/config
# [branch "dev"]
# 	remote = origin
# 	merge = refs/heads/dev
```

```bash
# delete a remote branch
$ git push [remote-name] :[remote-branch]
# <=> git extras: git delete-branch branch_name # delete local and remote branch
```


# Tags

> [!info] tags are simple pointer

```bash
# list tags
git tags
# <=>
ls .git/refs/tags

# Create annoted tag
git tag -a <name> -m <message> [commit-id]

# Create lightweight tag
git tag <name> [commit-id]
```

```bash
# Retrieve latest tag from HEAD
git describe HEAD --tags --abbrev=0

# test if tag is annoted
git cat-file -t v0.2

# Retrieve the referenced commit id for annoted tag : tagname^0
git rev-parse v0.2^0  # return tag if annoted tag, commit otherwise

# Move tag
git tag -d v0.1   # i.e. remove tag of the amended commit no more referenced
git tag v0.1

# git extras:  rename-tag : Rename a tag (local and remote)
git rename-tag <old-tag-name> <new-tag-name>

```

## Getting remote Tags

```bash
git ls-remote --tags git@gitlab.thalesdigital.io:mds-edge/product/mds-product.git
```

## Pulling tags

> Explicitely pull tags (for instance tags not in merged branches, or moved in remote)

```bash
git pull --tags

git fetch --tags
```

Prune tags : Remove local git tags that are no longer on the remote repository

```bash
git fetch --prune --prune-tags origin
git fetch --force --prune --prune-tags origin # to force local tag deletion
```


## Pushing tags

> By default, the git push command doesn’t transfer tags to remote servers.

```bash
# push a specific tag
$ git push remote-name tagname

# transfer all of your tags to the remote server that are not already there.
$ git push remote-name --tags
```

## Deleting tags

```bash
# remove local tag
git tag -d tag-name
# <+>
rm .git/refs/tags/<tag>

# remove remote tag
git push remote-name :refs/tags/tag-name
```

# Utils

## Using git outside the git workspace

> * `--git-dir=path` : required for command using only the git database e.g. log, tag, show
> * `--work-tree=path` : required for command using also the workspace e.g. status, add, commit etc...
> * `-C path` : this option is equivalent to use `--git-dir=path/.git --work-tree=path`

```bash
git --git-dir=./test/.git tag
git --git-dir=./test/.git log --oneline

git -C ./test status -bs
```

Another way with environment variables
```bash
export GIT_WORK_TREE="/home/d5ve/checkout2"
export GIT_DIR="${GIT_WORK_TREE}/.git"
```


## Check if a commit exist

```bash
git cat-file -e <commit-id>
```

## Setting file permissions under windows

```bash
git ls-files -s # list files in index with file permissions
# Set the execute permissions on the updated files.
git update-index --chmod=+x 'name-of-shell-script'

```

## show ignored files

```bash
git ls-files --other --ignored --exclude-standard
```

## Stashing

```bash
# save your local modifications to a new stash
git stash
# list all current stashes
git stash list
# show the list of changes in the first stash
git stash show stash@{0}
# display patch of a specified stash
git stash show stash@{0} -p
# <=>
git difftool stash@{0}
# ~ <=>
git show stash@{0} --oneline

# show diff using difftool for all files in stash
git difftool stash@{0}
# show diff using difftool for the specified file in stash
git difftool stash@{0} -- <filename>

# restore the changes from the most recent stash, and remove it from the stack of stashed changes
git stash pop

# drop the stash
git stash drop [<stash-ref>]

# Creating a Branch from a Stash
git stash branch <branch-name>

```

## Alias

To define an alias use ```git config --system alias.k '!gitk --all&'```

Using git-extras : ```git alias k '!gitk --all&'```

```bash
# list defined aliases
git config --list | grep alias

git alias # if git-extras installed
```

> Alias sample

```
br = branch -av
ci = commit
co = checkout
ca = commit -a --amend
cf = commit -a --amend --no-edit
dc = diff --cached
df = diff
gl = config --list
head = !git --no-pager show -s --pretty='tformat:%h (%s, %ad)' --date=short
k = !gitk --all&
kc = difftool -y --cached
kf = difftool -y
last = !git --no-pager log -1 HEAD --stat
l = log --oneline --graph
la = log --oneline --graph --all
lob = log --oneline --first-parent
lobo = log --oneline --first-parent --no-merges
lol = log --graph --pretty=oneline --abbrev-commit
lola = log --graph --pretty=oneline --abbrev-commit --all
pf = push --force
pof = config core.pager ''
pon = config --unset core.pager
r = remote -v
rv = checkout HEAD --
s = status -bs
st = status
us = reset HEAD
```

## Submodule

`repository` is the URL of the new submodule’s origin repository. This may be either an absolute URL, or (if it begins with ./ or ../), the location relative to the superproject’s default remote repository


```bash
git submodule add repository dir
```

> => add entry in .gitmodules
> ```bash
> [submodule "dir"]
>	  path = dir
>    url = https://github.com/puppetlabs/puppetlabs-vcsrepo
> ```

```bash
cat .gitmodules
git ls-files -s dir # show commit-id used by submodule

# initialize submodule in a cloned repo
git submodule init   # update .git/config
git submodule update # clone the submodule repo NB associated .git is stored in .git/modules/name
# <=>
git submodule update --init

git clone --recursive <url> # clone repo and init submodules

# every time you pull down a submodule change
git submodule update # --recursive to Traverse submodules recursively

# or use
git pull --recurse-submodules

# Show the status of the submodules.
git submodule status # <=> git submodule

# update submodule reference
# manually checkout a new version of submodule repository => submodule refers a new commit
cd path/submodulename ; git pull
git add path/submodulename
git commit -m "update submodule"

# <=>
git submodule update --remote path/submodulename

# applying command to each submodule
git submodule foreach git pull origin master

# remove submodule
# delete the relevant section from the .gitmodules file
git config --file=.gitmodules --remove-section submodule.name
git add .gitmodules
git rm --cached path/to/submodule
rm -rf .git/modules/path/to/submodule
rm -rf path/to/submodule

# Unregister the given submodules, i.e. remove the whole  submodule.$name section
# from .git/config together with their work tree.
git submodule deinit name

# <=> git extras: git delete-submodule
git delete-submodule name
```

# Plumbing commands

```bash
# display content of file e.g. README in git index
git show :README
# <=>
# extract sha1 of README in index
object-id=$(git ls-files -s README | awk '{print $2}')
git cat-file -p ${object-id}
git show ${object-id}


# retrieve the directory path containing .git dir
git rev-parse --show-toplevel
# <=> git extras: git root
git root

```


## reflog

```bash
git reflog
git log -g
```

## Internals

```bash
# objects store contains commit, tag, tree and blob objects
ls .git/objects

# finding an object in store (=> if not already packed)
find .git/objects -name ${object_id:2}

```

### Tips to retrieve last file content added to index

```bash
objectid=$((find .git/objects -type f -print0 | xargs -0 ls -t | head -n 1) 2> /dev/null | awk -F/ '{ print $3 $4 }')

# to list last created files in object store
# find .git/objects -type f -cmin -60 | awk -F/ '{ print $3 $4 }'

# alternative to extract object id
# a=$((find .git/objects -type f -print0 | xargs -0 ls -t | head -n 1) 2> /dev/null)
# id=$(echo ${a#.git/objects/} | sed s#/##)

git-cat file -p $objectid

```

### stored object types

> Any object have a normalized format concatenating a header (`object_type size\0` with object_type =`blob|tree|commit|tag`) and original object data. This new content is used to calculate the object sha1 checksum, then compressed using zlib and stored in a file which name is based on checksum.


```bash
git show --format=raw commit-id

git cat-file -t object-id

```

# Git ref

Visualizing Git Concepts with D3 : http://onlywei.github.io/explain-git-with-d3/
